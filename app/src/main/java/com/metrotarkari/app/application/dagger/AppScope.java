package com.metrotarkari.app.application.dagger;

import javax.inject.Scope;

@Scope
public @interface AppScope {
}
