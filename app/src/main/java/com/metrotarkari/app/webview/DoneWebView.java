package com.metrotarkari.app.webview;

import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.metrotarkari.app.R;
import com.metrotarkari.app.ui.base.BaseView;
import com.metrotarkari.app.utils.Constant;

import butterknife.BindView;

/**
 * Created by Avinash on 6/13/2017.
 */

public class DoneWebView extends BaseView{

    @BindView(R.id.appBarLayout)
    public AppBarLayout appBarLayout;

    @BindView(R.id.toolbar)
    public Toolbar toolbar;

    @BindView(R.id.progressBar)
    public ProgressBar progressBar;

    @BindView(R.id.progressBarRl)
    public RelativeLayout progressBarRl;

    @BindView(R.id.doneWebView)
    public WebView doneWebView;

    private String title,url;

    private AppCompatActivity appCompatActivity;


    public DoneWebView(@NonNull AppCompatActivity activity) {
        super(activity);
        this.appCompatActivity = activity;


        title = appCompatActivity.getIntent().getStringExtra(Constant.TITLE);
        url = appCompatActivity.getIntent().getStringExtra(Constant.URL);

        if (title!=null && url!=null) {
            appCompatActivity.setSupportActionBar(toolbar);
            appCompatActivity.getSupportActionBar().setTitle(title);
            doneWebView.setWebViewClient(new MyWebViewClient());
            WebSettings webSettings = doneWebView.getSettings();
            webSettings.setJavaScriptEnabled(true);
            webSettings.setLoadsImagesAutomatically(true);
            doneWebView.loadUrl(url);
        }
    }

    private class MyWebViewClient extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                view.loadUrl(request.getUrl().toString());
            }
            return true;
        }

        @SuppressWarnings("deprecation")
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }


        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view,url);
            progressBar.setVisibility(View.GONE);
            progressBarRl.setVisibility(GONE);
        }
    }

    @Override
    public int getContentLayout() {
        return R.layout.activity_web_view;
    }

}
