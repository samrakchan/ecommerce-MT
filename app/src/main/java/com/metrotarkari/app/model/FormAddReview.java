
package com.metrotarkari.app.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FormAddReview {

    @SerializedName("rates")
    @Expose
    private List<Rate> rates = null;
    @SerializedName("form_review")
    @Expose
    private FormReview formReview;

    public List<Rate> getRates() {
        return rates;
    }

    public void setRates(List<Rate> rates) {
        this.rates = rates;
    }

    public FormReview getFormReview() {
        return formReview;
    }

    public void setFormReview(FormReview formReview) {
        this.formReview = formReview;
    }

}
