package com.metrotarkari.app.ui.dashboard.dagger;

import android.support.v7.app.AppCompatActivity;

import com.metrotarkari.app.application.network.DoneNetwork;
import com.metrotarkari.app.db.DoneDbAdapter;
import com.metrotarkari.app.ui.dashboard.mvp.MainModel;
import com.metrotarkari.app.ui.dashboard.mvp.MainPresenter;
import com.metrotarkari.app.ui.dashboard.mvp.MainView;
import com.metrotarkari.app.utils.DoneUserPreferenceManager;

import dagger.Module;
import dagger.Provides;

@Module
public class MainModule {

  private final AppCompatActivity activity;

  public MainModule(AppCompatActivity activity) {
    this.activity = activity;
  }

  @Provides
  @MainScope
  public MainView view() {
    return new MainView(activity);
  }

  @Provides
  @MainScope
  public MainModel model(DoneNetwork doneNetwork){
    return new MainModel(activity, doneNetwork);
  }

  @Provides
  @MainScope
  public MainPresenter presenter(MainView homeView, MainModel model, DoneDbAdapter doneDbAdapter, DoneUserPreferenceManager preferenceManager) {
    return new MainPresenter(homeView, model, doneDbAdapter, preferenceManager);
  }

}
