
package com.metrotarkari.app.ui.reviewandratings.reviewpost.model;

import java.util.HashMap;

public class PostReview {
    private Integer product_id;

    private HashMap<String, Integer> ratings;

    private HashMap<String, String> ratingsTexts;

    public Integer getProductId() {
        return product_id;
    }

    public void setProductId(Integer productId) {
        this.product_id = productId;
    }

    public HashMap<String, Integer> getRatings() {
        return ratings;
    }

    public void setRatings(HashMap<String, Integer> ratings) {
        this.ratings = ratings;
    }

    public HashMap<String, String> getRatingsTexts() {
        return ratingsTexts;
    }

    public void setRatingsTexts(HashMap<String, String> ratingsTexts) {
        this.ratingsTexts = ratingsTexts;
    }
}
