package com.metrotarkari.app.application.network;

import com.metrotarkari.app.model.AddressResponse;
import com.metrotarkari.app.model.AuthToken;
import com.metrotarkari.app.model.CartQuote;
import com.metrotarkari.app.model.CartQuoteContent;
import com.metrotarkari.app.model.ConfigResponse;
import com.metrotarkari.app.model.CustomerResponse;
import com.metrotarkari.app.model.Customer_;
import com.metrotarkari.app.model.Detail;
import com.metrotarkari.app.model.ForgetPasswordResponse;
import com.metrotarkari.app.model.Home;
import com.metrotarkari.app.model.LogoutResponse;
import com.metrotarkari.app.model.Order;
import com.metrotarkari.app.model.OrderResponse;
import com.metrotarkari.app.model.PostOrder;
import com.metrotarkari.app.model.ProductList;
import com.metrotarkari.app.model.ReviewResponse;
import com.metrotarkari.app.model.StoreViewResult;
import com.metrotarkari.app.model.WishList;
import com.metrotarkari.app.model.Wishlistitem;
import com.metrotarkari.app.ui.addressbook.addresslist.model.PostAddress;
import com.metrotarkari.app.ui.addressbook.addresslist.model.PostAddressResponse;
import com.metrotarkari.app.ui.checkout.model.CheckOutOrder;
import com.metrotarkari.app.ui.order.orderdetail.model.OrderDetailResponse;
import com.metrotarkari.app.ui.order.orderlist.model.OrderListResponse;
import com.metrotarkari.app.ui.reviewandratings.reviewpost.model.PostReview;
import com.metrotarkari.app.ui.reviewandratings.reviewpost.model.PostReviewResponse;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface DoneNetwork {

    @GET("v2/storeviews/1")
    Observable<StoreViewResult> getStoreViews();

    @POST("v2/customers/login")
    Observable<CustomerResponse> doLogin(@Body HashMap<String, String> loginParams);

    @POST("v2/customers/forgetpassword")
    Observable<ForgetPasswordResponse> sendMailForGetPassword(@Body HashMap<String, String> loginParams);

    @POST("v2/customers")
    Observable<CustomerResponse> doSignup(@Body HashMap<String,String> signupParams);

    @GET("v2/customers/logout")
    Observable<LogoutResponse> getLogOut();

    @POST("v2/reviews")
    Observable<PostReviewResponse> postReview(@Body HashMap<String, Object> postReview);

    @DELETE("v2/addresses/{addressID}")
    Observable<AddressResponse> deleteAddress(@Path("addressID")Integer addressID);


    @GET("v2/auth")  //token == 13c93fa4e122c8e6ec2da81d4273f16d
    Observable<AuthToken> doAuthorization(@Header("token") String token);

    @GET("v2/token") //eg. refreshToken = Bearer 9d2e1db749570fd3349493fd9a5a5ab2106dd65
    Observable<String> getRefreshToken(@Header("Authorization") String refreshToken);

    @GET("v2/config")
    Observable<ConfigResponse> getCategories();

    @GET("v2/addresses")
    Observable<AddressResponse> getAddressBook();

    @GET("v2/orders")
    Observable<OrderResponse> getOrders();

    @GET("v2/orders")
    Observable<OrderListResponse> getCustomerOrders();


    @GET("v2/orders/{orderId}")
    Observable<OrderDetailResponse> getOrderDetail(@Path("orderId") String orderID);

    //filter[cat_id]

    @GET("v2/products")
    Observable<ProductList> getProductList(@QueryMap Map<String, String> filterOptions, @Query("page") int page, @Query("limit") int itemPerPage, @Query("order") String orderBy, @Query("dir") String ascDesc);

    @GET("v2/reviews")
    Observable<ReviewResponse> getReview(@QueryMap Map<String,String> filterOption);
 /*   @GET("v2/products")
    Observable<ProductList> searchProduct(@Query("filter[q]") String searchText, @Query("page") int page, @Query("limit") int itemPerPage, @Query("order") String orderBy, @Query("dir") String ascDesc);
*/

    @GET("v2/products/{productId}")
    Observable<Detail> getProductDetail(@Path("productId") String productId);

    //AddToCart
    @POST("v2/quoteitems")
    Observable<CartQuoteContent> postQuoteItem(@Body CartQuote cartQuote);

    @GET("v2/quoteitems")
    Observable<CartQuoteContent> getQuoteItem(@Query("page") int page, @Query("limit") int itemPerPage, @Query("order") String orderBy, @Query("dir") String ascDesc);

    @GET("v2/wishlistitems")
    Observable<WishList> getWishList(@Query("page") int page, @Query("limit") int itemPerPage, @Query("order") String orderBy, @Query("dir") String ascDesc);

    @GET("v2/wishlistitems")
    Observable<Wishlistitem> postWishList(@QueryMap HashMap<String, String> product);

    @PUT("v2/quoteitems")
    Observable<CartQuoteContent> putQuoteItem(@Body HashMap<String, Integer> itemQty);

    @PUT("v2/addresses")
    Observable<PostAddressResponse> postAddressForEdit(@Body PostAddress postAddress);

    @GET("v2/homes")
    Observable<Home> getHomeItems(@Query("limit") int limit);

    @POST("v2/orders")
    Observable<Order> postOrders(@Body CheckOutOrder postOrder);

    //{"address_id":"1272"}
    @PUT("v2/orders/onepage")
    Observable<OrderResponse> getCheckoutInfos(@Body HashMap<String, Object> items);

    @POST("v2/addresses")
    Observable<PostAddressResponse> postAddress(@Body PostAddress postAddress);

    @GET("v2/customers/check_rp_tokne")
    Observable<Customer_> getCustomerToken(@QueryMap HashMap<String, String> idToken);

    /**
     * @param newPassword {"new_password":"","com_password":""}
     * @return
     */
    @POST("v2/customers/change_password")
    Observable<Customer_> changePassword(@QueryMap HashMap<String, String> idToken, @Body HashMap<String, String> newPassword);

}
