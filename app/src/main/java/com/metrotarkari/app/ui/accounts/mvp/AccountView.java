package com.metrotarkari.app.ui.accounts.mvp;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.metrotarkari.app.R;
import com.metrotarkari.app.model.IconTextId;
import com.metrotarkari.app.ui.accounts.AccountsAdapter;
import com.metrotarkari.app.ui.accounts.AccountsFragment;
import com.metrotarkari.app.ui.base.BaseViewFragment;
import com.metrotarkari.app.ui.notifications.NotificationActivity;
import com.metrotarkari.app.ui.order.orderlist.OrderListActivity;
import com.metrotarkari.app.utils.Constant;
import com.metrotarkari.app.utils.DoneItemClickListener;
import com.metrotarkari.app.utils.DoneUserPreferenceManager;
import com.metrotarkari.app.webview.DoneWebViewActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import cc.cloudist.acplibrary.ACProgressFlower;
import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;

/**
 * Created by samrakchan on 4/27/17.
 */

public class AccountView extends BaseViewFragment implements DoneItemClickListener {

    private AccountsFragment fragment;

    @Nullable
    @BindView(R.id.contentRv)
    public RecyclerView contentRv;


    private AccountsAdapter mAdapter;
    private DoneUserPreferenceManager doneUserPreferenceManager;

    private final BehaviorSubject<Object> logoutBehaviorSubject = BehaviorSubject.create();
    private final BehaviorSubject<Object> addressBookBehaviourSubject = BehaviorSubject.create();

    private final ACProgressFlower progressDialog = new ACProgressFlower.Builder(getContext())
            .themeColor(Color.WHITE)
            .fadeColor(Color.DKGRAY).build();

    public AccountView(AccountsFragment fragment){
        super(fragment);
        this.fragment = fragment;

        mAdapter = new AccountsAdapter(getContext(),this);
        contentRv.setLayoutManager(new LinearLayoutManager(getContext()));

        doneUserPreferenceManager = new DoneUserPreferenceManager(getContext());

        List<IconTextId> iconTextIds = new ArrayList<>();
        if(doneUserPreferenceManager.isLoggedInUser()){
            iconTextIds.add(new IconTextId(1,R.mipmap.ic_parcel,getContext().getString(R.string.orders)));
        }
        //iconTextIds.add(new IconTextId(2,R.mipmap.ic_adapter_help,getContext().getString(R.string.notifications)));
        iconTextIds.add(new IconTextId(3, R.mipmap.ic_policy,getContext().getString(R.string.policy)));
        //   iconTextIds.add(new IconTextId(4, R.mipmap.ic_adapter_rating, getContext().getString(R.string.rate_app)));
        if (doneUserPreferenceManager.isLoggedInUser()) {
            iconTextIds.add(new IconTextId(5,R.mipmap.ic_user_icon,getContext().getString(R.string.address_book)));
            iconTextIds.add(new IconTextId(6, R.mipmap.ic_pause, getContext().getString(R.string.logout)));
        }else {
            iconTextIds.add(new IconTextId(6, R.mipmap.ic_login, getContext().getString(R.string.login)));
        }

        mAdapter.addItems(iconTextIds);

        contentRv.setAdapter(mAdapter);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        Drawable verticalDivider = ContextCompat.getDrawable(getContext(), R.drawable.divider_half_line);
        dividerItemDecoration.setDrawable(verticalDivider);

        contentRv.addItemDecoration(dividerItemDecoration);

    }
    @Override
    public int getContentLayout() {
        return R.layout.content_recycler_view;
    }


    @Override
    public void onItemClickListener(int id, int viewId, Object object) {

        if (id == 1){
            //navigation to orderList
            Intent intent  = new Intent(getContext(), OrderListActivity.class);
            fragment.startActivity(intent);
        }else if (id == 2){
            //navigation to notification
            Intent intent = new Intent(getContext(), NotificationActivity.class);
            fragment.startActivity(intent);
        } else if (id == 3){
            //navigation to WebActivity
            Intent intent = new Intent(getContext(), DoneWebViewActivity.class);
            intent.putExtra(Constant.TITLE,getContext().getString(R.string.policy));
            intent.putExtra(Constant.URL,Constant.BASE+"/privacy-policy");
            fragment.startActivity(intent);

        } else if (id == 3){
            //help center
            Intent intent = new Intent(getContext(), DoneWebViewActivity.class);
            intent.putExtra(Constant.TITLE,getContext().getString(R.string.help_center));
            intent.putExtra(Constant.URL,"http://google.com");
            fragment.startActivity(intent);
        }else if (id == 4){
            //logout login
            logoutBehaviorSubject.onNext("");
        }else if (id == 5){
            //address book
            addressBookBehaviourSubject.onNext("");
        }else if (id == 6){
            //logout
            logoutBehaviorSubject.onNext("");
        }
    }

    public void finishActivity(){
        if(fragment!=null && fragment.getActivity() !=null) {
            fragment.getActivity().finish();
        }
    }


    public Observable<Object> logoutClickObservable(){
        return logoutBehaviorSubject;
    }

    public Observable<Object> addressBookClickObservable(){
        return addressBookBehaviourSubject;
    }

    public void showLoading(boolean loading){
        if(loading){
            progressDialog.setCancelable(false);
            progressDialog.show();
        }else{
            progressDialog.dismiss();
        }
    }
}
