package com.metrotarkari.app.ui.accounts.dragger;

import com.metrotarkari.app.application.network.DoneNetwork;
import com.metrotarkari.app.ui.accounts.AccountsFragment;
import com.metrotarkari.app.ui.accounts.mvp.AccountModel;
import com.metrotarkari.app.ui.accounts.mvp.AccountPresenter;
import com.metrotarkari.app.ui.accounts.mvp.AccountView;
import com.metrotarkari.app.utils.DoneTokenPreferenceManager;
import com.metrotarkari.app.utils.DoneUserPreferenceManager;

import dagger.Module;
import dagger.Provides;

@Module
public class AccountModule {

  private final AccountsFragment fragment;

  public AccountModule(AccountsFragment fragment) {
    this.fragment = fragment;
  }

  @Provides
  @AccountScope
  public AccountView view() {
    return new AccountView(fragment);
  }

  @Provides
  @AccountScope
  public AccountModel model(DoneNetwork doneNetwork){
    return new AccountModel(fragment, doneNetwork);
  }

  @Provides
  @AccountScope
  public AccountPresenter presenter(AccountView homeView, AccountModel model, DoneUserPreferenceManager preferenceManager) {
    return new AccountPresenter(homeView, model, preferenceManager);
  }

}
