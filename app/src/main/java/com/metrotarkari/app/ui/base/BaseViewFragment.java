package com.metrotarkari.app.ui.base;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;

import com.metrotarkari.app.R;
import com.metrotarkari.app.model.ItemParams;
import com.metrotarkari.app.utils.NetworkBroadcastReceiver;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import cc.cloudist.acplibrary.ACProgressFlower;
import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;

import static com.metrotarkari.app.utils.NetworkBroadcastReceiver.IS_NETWORK_AVAILABLE;

/**
 * Created by samrakchan on 4/23/17.
 */

public abstract class BaseViewFragment extends FrameLayout {
    private static final String TAG = BaseViewFragment.class.getSimpleName();

    @Nullable
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Nullable
    @BindView(R.id.noInternetConnectionView)
    public View noInternetConnectionView;

    public final BehaviorSubject<HashMap<String, String>> addItemToB2BWishListBehavior = BehaviorSubject.create();

    public final ACProgressFlower progressDialog = new ACProgressFlower.Builder(getContext())
            .themeColor(Color.WHITE)
            .fadeColor(Color.DKGRAY).build();


    public BaseViewFragment(@NonNull Fragment fragment) {
        super(fragment.getContext());
        inflate(getContext(), getContentLayout(), this);

        ButterKnife.bind(this);


        IntentFilter intentFilter = new IntentFilter(NetworkBroadcastReceiver.NETWORK_AVAILABLE_ACTION);
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                boolean isNetworkAvailable = intent.getBooleanExtra(IS_NETWORK_AVAILABLE, false);
                if(isNetworkAvailable){
                    Log.i(TAG, isNetworkAvailable+" Network Availability");
                    noInternetConnectionView.setVisibility(isNetworkAvailable?View.GONE:View.VISIBLE);
                }
            }
        }, intentFilter);
    }

    public abstract int getContentLayout();

    public void showNoInternetConnection(boolean showView){
        if(showView) {
            noInternetConnectionView.setVisibility(View.VISIBLE);
        }else{
            noInternetConnectionView.setVisibility(View.GONE);
        }
    }

    public void showProgressDialog(){
        progressDialog.show();
    }

    public void hideProgressDialog(){
        progressDialog.dismiss();
    }

    public Observable<HashMap<String, String>> addItemToB2BWishList() {
        return addItemToB2BWishListBehavior;
    }
}
