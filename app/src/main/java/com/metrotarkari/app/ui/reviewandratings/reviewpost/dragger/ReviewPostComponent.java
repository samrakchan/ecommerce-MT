package com.metrotarkari.app.ui.reviewandratings.reviewpost.dragger;

import com.metrotarkari.app.application.dagger.AppComponent;
import com.metrotarkari.app.ui.reviewandratings.reviewpost.ReviewPostActivity;

import dagger.Component;

@ReviewPostScope
@Component(modules = { ReviewPostModule.class }, dependencies = AppComponent.class)
public interface ReviewPostComponent {

  void inject(ReviewPostActivity reviewPostActivity);

}
