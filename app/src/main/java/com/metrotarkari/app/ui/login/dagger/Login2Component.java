package com.metrotarkari.app.ui.login.dagger;

import com.metrotarkari.app.application.dagger.AppComponent;
import com.metrotarkari.app.ui.login.LoginActivity2;

import dagger.Component;

@LoginScope
@Component(modules = { LoginModule.class }, dependencies = AppComponent.class)
public interface Login2Component {

  void inject(LoginActivity2 activity);

}
