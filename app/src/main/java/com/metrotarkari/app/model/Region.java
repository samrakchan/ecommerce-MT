
package com.metrotarkari.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class Region implements AutoData{

    @SerializedName("code")
    @Expose

    private String code;
    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("city")
    @Expose
    private List<City> city = null;


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public List<City> getCity() {
        return city;
    }

    public void setCity(List<City> city) {
        this.city = city;
    }



}
