package com.metrotarkari.app.ui.list.dagger;

import android.support.v4.app.Fragment;

import com.metrotarkari.app.application.network.DoneNetwork;
import com.metrotarkari.app.ui.list.mvp.ItemListModel;
import com.metrotarkari.app.ui.list.mvp.ItemListPresenter;
import com.metrotarkari.app.ui.list.mvp.ItemListView;
import com.metrotarkari.app.utils.DoneUserPreferenceManager;

import java.util.HashMap;

import dagger.Module;
import dagger.Provides;

@Module
public class ItemListModule {

  private final Fragment fragment;
    private HashMap<String, String> hashMap;

  public ItemListModule(Fragment fragment, HashMap<String, String> hashMap) {
    this.fragment = fragment;
      this.hashMap = hashMap;
  }

  @Provides
  @ItemListScope
  public ItemListView view() {
    return new ItemListView(fragment);
  }

  @Provides
  @ItemListScope
  public ItemListModel model(DoneNetwork doneNetwork){
    return new ItemListModel(fragment, doneNetwork);
  }

  @Provides
  @ItemListScope
  public ItemListPresenter presenter(ItemListView homeView, DoneUserPreferenceManager preferenceManager, ItemListModel model) {
    return new ItemListPresenter(homeView, model, preferenceManager,  hashMap);
  }

}
