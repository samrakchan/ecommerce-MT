package com.metrotarkari.app.ui.home.mvp;

import android.content.Context;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import java.util.List;

/**
 * Created by samrakchan on 4/18/17.
 */

public class TabBarBehaviour extends AppBarLayout.ScrollingViewBehavior {
    private static final String TAG = TabBarBehaviour.class.getSimpleName();
    private boolean isTabVisible = true;

    public TabBarBehaviour(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean layoutDependsOn(CoordinatorLayout parent, View child, View dependency) {
        return super.layoutDependsOn(parent, child, dependency) || dependency instanceof TabLayout;
    }

    @Override
    public boolean onStartNestedScroll(CoordinatorLayout coordinatorLayout, View child, View directTargetChild, View target, int nestedScrollAxes) {
        return nestedScrollAxes == ViewCompat.SCROLL_AXIS_VERTICAL || super.onStartNestedScroll(coordinatorLayout, child, directTargetChild, target, nestedScrollAxes);
    }

    @Override
    public void onNestedScroll(CoordinatorLayout coordinatorLayout, View child, View target, int dxConsumed, int dyConsumed, int dxUnconsumed, int dyUnconsumed) {
        super.onNestedScroll(coordinatorLayout, child, target, dxConsumed, dyConsumed, dxUnconsumed, dyUnconsumed);
        Log.i(TAG, TAG+" "+dxConsumed+" "+dyConsumed+" "+dxUnconsumed+" "+dyUnconsumed);
        if (dyConsumed > 0) {
            // User scrolled up -> hide the FAB
            List<View> dependencies = coordinatorLayout.getDependencies(child);
            for (View view : dependencies) {
                if (view instanceof TabLayout && isTabVisible) {
                    view.setVisibility(View.GONE);
                    //view.animate().translationYBy(view.getHeight()).start();

                    isTabVisible = false;
                }
            }
        } else if (dyConsumed < 0) {
            // User scrolled down -> show the FAB
            List<View> dependencies = coordinatorLayout.getDependencies(child);
            for (View view : dependencies) {
                if (view instanceof TabLayout && !isTabVisible) {
                    //view.animate().translationYBy(-view.getHeight()).start();
                    view.setVisibility(View.VISIBLE);
                    isTabVisible = true;
                }
            }
        }
    }
}
