package com.metrotarkari.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by samrakchan on 5/29/17.
 */

public class Image {

    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("position")
    @Expose
    private String position;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }
}
