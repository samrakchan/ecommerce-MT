package com.metrotarkari.app.application.dagger.module;

import android.content.Context;

import com.metrotarkari.app.application.dagger.AppScope;
import com.metrotarkari.app.utils.DoneUserPreferenceManager;

import dagger.Module;
import dagger.Provides;

/**
 * Created by samrakchan on 7/4/17.
 */
@Module
public class PreferenceModule {

    @AppScope
    @Provides
    public DoneUserPreferenceManager getPreference(Context context) {
        return new DoneUserPreferenceManager(context);
    }
}
