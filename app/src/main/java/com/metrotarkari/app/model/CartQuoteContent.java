package com.metrotarkari.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by samrakchan on 6/2/17.
 */

public class CartQuoteContent extends ErrorResponse {

    @SerializedName("all_ids")
    @Expose
    private List<String> allIds = null;
    @SerializedName("quoteitems")
    @Expose
    private List<Quoteitem> quoteitems = null;
    @SerializedName("total")
    @Expose
    private Total total;
    @SerializedName("page_size")
    @Expose
    private Integer pageSize;
    @SerializedName("from")
    @Expose
    private Integer from;
    @SerializedName("total_page")
    @Expose
    private Integer totalPage;
    @SerializedName("current_page")
    @Expose
    private Integer currentPage;
    @SerializedName("message")
    @Expose
    private List<String> message = null;
    @SerializedName("cart_total")
    @Expose
    private Integer cartTotal;
    @SerializedName("quote_id")
    @Expose
    private String quoteId;

    @SerializedName("best_sell")
    @Expose
    private BestSell bestSell;

    public List<String> getAllIds() {
        return allIds;
    }

    public void setAllIds(List<String> allIds) {
        this.allIds = allIds;
    }

    public List<Quoteitem> getQuoteitems() {
        return quoteitems;
    }

    public void setQuoteitems(List<Quoteitem> quoteitems) {
        this.quoteitems = quoteitems;
    }

    public Total getTotal() {
        return total;
    }

    public void setTotal(Total total) {
        this.total = total;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getFrom() {
        return from;
    }

    public void setFrom(Integer from) {
        this.from = from;
    }

    public Integer getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(Integer totalPage) {
        this.totalPage = totalPage;
    }

    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

    public List<String> getMessage() {
        return message;
    }

    public void setMessage(List<String> message) {
        this.message = message;
    }

    public Integer getCartTotal() {
        return cartTotal;
    }

    public void setCartTotal(Integer cartTotal) {
        this.cartTotal = cartTotal;
    }

    public String getQuoteId() {
        return quoteId;
    }

    public void setQuoteId(String quoteId) {
        this.quoteId = quoteId;
    }

    public BestSell getBestSell() {
        return bestSell;
    }

    public void setBestSell(BestSell bestSell) {
        this.bestSell = bestSell;
    }
}
