package com.metrotarkari.app.ui.list;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.metrotarkari.app.R;
import com.metrotarkari.app.model.AppReviews;
import com.metrotarkari.app.model.Product;
import com.metrotarkari.app.ui.productdetail.DetailActivity;
import com.metrotarkari.app.utils.Constant;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by samrakchan on 3/18/17.
 */

public class ItemListAdapter extends RecyclerView.Adapter<ItemListAdapter.CategoryItemList>{
    private List<Product> mData;
    private Context mContext;
    private ViewType viewType = ViewType.GRID;

    public ItemListAdapter(Context context){
        mData = new ArrayList<>();
        this.mContext = context;
    }

    public void setViewType(ViewType viewType){
        this.viewType = viewType;
        notifyDataSetChanged();

    }

    public ViewType getCurrentViewType(){
        return viewType;
    }

    public void addData(List<Product> datas){
        this.mData.addAll(datas);
        notifyDataSetChanged();
    }

    public void removeAllData(){
        mData.clear();
        notifyDataSetChanged();
    }


    @Override
    public CategoryItemList onCreateViewHolder(ViewGroup parent, int viewType) {
        if (this.viewType == ViewType.LIST){
            View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_list_item_list,parent,false);
            return new CategoryItemList(view);
        }else if(this.viewType == ViewType.GRID){
            View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_list_item_grid,parent,false);
            return new CategoryItemList(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(CategoryItemList holder, int position) {


        Product product = mData.get(position);
        holder.listItemNameTv.setText(product.getName());

        String weightValue = product.getMetroWeightTypeValue()!=null?"/"+ product.getMetroWeightTypeValue():""; //metro_weight_type_value

      //  holder.listItemPriceTv.setText(mContext.getString(R.string.price, Utility.convertToTwoDecimalPlace(product.getFinalPrice()))+weightValue);
          holder.listItemPriceTv.setText(product.getFormattedFinalPrice());
        if(product.getPrice().equalsIgnoreCase(product.getFinalPrice())){
            holder.listItemOldPriceTv.setVisibility(View.GONE);
        }else{
            holder.listItemOldPriceTv.setPaintFlags(holder.listItemOldPriceTv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
      //      holder.listItemOldPriceTv.setText(mContext.getString(R.string.price, Utility.convertToTwoDecimalPlace(product.getPrice())));
            holder.listItemOldPriceTv.setText(product.getFormattedPrice());
            holder.listItemOldPriceTv.setVisibility(View.VISIBLE);
        }
        if(product.getDiscountLabel() !=null){
            holder.listItemOfferLabelTv.setText(product.getDiscountLabel());
            holder.listItemOfferLabelTv.setVisibility(View.VISIBLE);
        }else{
            holder.listItemOfferLabelTv.setVisibility(View.GONE);
        }

        holder.listItemRatingLl.setVisibility(View.GONE);
        AppReviews appReviews = product.getAppReviews();
        if(appReviews !=null){
            holder.listItemRatingTv.setText(appReviews.getNumber()+"");
        }

        if(product.getImages()!=null && !product.getImages().isEmpty())
        Glide.with(mContext).load(product.getImages().get(0).getUrl()).placeholder(R.mipmap.ic_placeholder_mt).dontAnimate().centerCrop().into(holder.listItemImageIv);

        Log.i("IMG", product.getImages().get(0).getUrl()+"");

    }

    @Override
    public int getItemViewType(int position) {
       return viewType.getViewType();
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    protected class CategoryItemList extends RecyclerView.ViewHolder{

        private AppCompatImageView listItemImageIv;
        private AppCompatTextView listItemPriceTv;
        private AppCompatTextView listItemOldPriceTv;
        private AppCompatTextView listItemNameTv;
        private AppCompatTextView listItemRatingTv;
        private AppCompatTextView listItemOfferLabelTv;
        private View listItemRatingLl;
        private AppCompatRatingBar listItemRatingBar;

        public CategoryItemList(View itemView) {
            super(itemView);

            listItemImageIv = (AppCompatImageView)itemView.findViewById(R.id.listItemImageIv);
            listItemPriceTv = (AppCompatTextView)itemView.findViewById(R.id.listItemPriceTv);
            listItemOldPriceTv = (AppCompatTextView)itemView.findViewById(R.id.listItemOldPriceTv);
            listItemNameTv = (AppCompatTextView)itemView.findViewById(R.id.listItemNameTv);
            listItemRatingTv = (AppCompatTextView) itemView.findViewById(R.id.listItemRatingTv);
            listItemOfferLabelTv = (AppCompatTextView) itemView.findViewById(R.id.listItemOfferLabelTv);
            listItemRatingLl = itemView.findViewById(R.id.listItemRatingLl);
            listItemRatingBar = (AppCompatRatingBar) itemView.findViewById(R.id.listItemRatingBar);

            itemView.setOnClickListener(v -> {
                Product product = mData.get(getAdapterPosition());
                Intent intent = new Intent(mContext, DetailActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                intent.putExtra(Constant.PRODUCT_ID, product.getEntityId());
                mContext.startActivity(intent);
            });
        }
    }
}
