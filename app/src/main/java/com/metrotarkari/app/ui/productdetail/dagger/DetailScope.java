package com.metrotarkari.app.ui.productdetail.dagger;

import javax.inject.Scope;

@Scope
public @interface DetailScope {
}
