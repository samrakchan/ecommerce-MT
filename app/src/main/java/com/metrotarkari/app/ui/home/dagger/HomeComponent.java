package com.metrotarkari.app.ui.home.dagger;

import com.metrotarkari.app.application.dagger.AppComponent;
import com.metrotarkari.app.ui.b2b.add.B2bAddModule;
import com.metrotarkari.app.ui.home.HomeFragment;

import dagger.Component;

@HomeScope
@Component(modules = { HomeModule.class, B2bAddModule.class}, dependencies = AppComponent.class)
public interface HomeComponent {

  void inject(HomeFragment homeFragment);

}
