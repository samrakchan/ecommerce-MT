package com.metrotarkari.app.utils;

import com.metrotarkari.app.model.Sort;

/**
 * Created by samrakchan on 6/11/17.
 */

public interface OrderListener {
    void onOrderListener(Sort order);

}
