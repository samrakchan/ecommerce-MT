package com.metrotarkari.app.ui.login.mvp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import com.metrotarkari.app.model.CustomerResponse;
import com.metrotarkari.app.ui.forgetpassword.ForgetPasswordActivity;
import com.metrotarkari.app.model.AuthToken;
import com.metrotarkari.app.ui.dashboard.MainActivity;
import com.metrotarkari.app.application.network.DoneNetwork;
import com.metrotarkari.app.ui.signup.SignupActivity;
import com.metrotarkari.app.utils.Constant;

import java.util.HashMap;

import io.reactivex.Observable;


/**
 * Created by samrakchan on 4/30/17.
 */

public class LoginModel {
    private final AppCompatActivity activity;
    private DoneNetwork network;

    public LoginModel(AppCompatActivity activity, DoneNetwork network){
        this.activity = activity;
        this.network = network;
    }

    public void navigateToMainActivity(){
        Intent intent = new Intent(activity, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        activity.startActivity(intent);
    }

    public void navigateToSignupActivity(){
        Intent inIntent = activity.getIntent();
        Intent intent = new Intent(activity, SignupActivity.class);
        if (inIntent.getIntExtra(Constant.INTENT_FOR,-1) == Constant.CHECKOUT){
            intent.putExtra(Constant.INTENT_FOR,Constant.CHECKOUT);
            activity.startActivityForResult(intent,Constant.CHECKOUT_ACTIVITY_FROM_SIGNUP_REQUEST_CODE);
        }else {
            activity.startActivity(intent);
        }

    }

    public void navigateToForgetPasswordActivity(){
        Intent intent = new Intent(activity, ForgetPasswordActivity.class);
        activity.startActivity(intent);
    }

    public void finishActivity(){
        activity.finish();
    }


    public Observable<CustomerResponse> doLogin(String username, String password){
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("email", username);
        hashMap.put("password", password);
        return network.doLogin(hashMap);
    }

    public Observable<AuthToken> getAuth(String token){
        return network.doAuthorization(token);
    }

}
