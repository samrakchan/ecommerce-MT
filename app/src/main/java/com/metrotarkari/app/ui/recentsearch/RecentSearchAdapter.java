package com.metrotarkari.app.ui.recentsearch;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metrotarkari.app.R;
import com.metrotarkari.app.ui.recentsearch.model.RecentSearch;
import com.metrotarkari.app.utils.DoneItemClickListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Avinash on 7/6/2017.
 */

public class RecentSearchAdapter extends RecyclerView.Adapter<RecentSearchAdapter.RecentSearchItemViewHolder> {

    private Context mContext;
    private List<RecentSearch> mData;
    private DoneItemClickListener listener;


    public RecentSearchAdapter(Context mContext, DoneItemClickListener listener) {
        this.mContext = mContext;
        mData = new ArrayList<>();
        this.listener = listener;
    }

    public void addRecentSearchData(List<RecentSearch> recentSearches){
        mData.clear();
        mData.addAll(recentSearches);
        notifyDataSetChanged();
    }

    @Override
    public RecentSearchItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_recent_search_child,parent,false);
        return new RecentSearchAdapter.RecentSearchItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecentSearchItemViewHolder holder, int position) {
        RecentSearch recentSearchItem = mData.get(position);

        holder.adapterSearchTv.setText(recentSearchItem.getRecentSearchItemName());

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    protected class RecentSearchItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        AppCompatTextView adapterSearchTv;
        AppCompatImageView adapterSearchDeleteIv;


        public RecentSearchItemViewHolder(View itemView) {
            super(itemView);
            adapterSearchTv = (AppCompatTextView) itemView.findViewById(R.id.adapterSearchTv);
            adapterSearchDeleteIv = (AppCompatImageView) itemView.findViewById(R.id.adapterSearchDeleteIv);
            itemView.setOnClickListener(this);
            adapterSearchDeleteIv.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            RecentSearch recentSearch = mData.get(getAdapterPosition());
            if (v.getId() == itemView.getId()){
                listener.onItemClickListener(getAdapterPosition(),itemView.getId(),recentSearch);
            }else if (v.getId() == R.id.adapterSearchDeleteIv){
                listener.onItemClickListener(getAdapterPosition(),R.id.adapterSearchDeleteIv,recentSearch);
            }

        }
    }
}
