package com.metrotarkari.app.ui.addressbook.addressentry.mvp;

import android.support.v4.app.Fragment;

import com.metrotarkari.app.application.network.DoneNetwork;
import com.metrotarkari.app.model.ItemParams;
import com.metrotarkari.app.model.ProductList;
import com.metrotarkari.app.ui.addressbook.addresslist.model.PostAddress;
import com.metrotarkari.app.ui.addressbook.addresslist.model.PostAddressResponse;

import java.util.Map;

import io.reactivex.Observable;

/**
 * Created by samrakchan on 4/27/17.
 */

public class AddressEntryModel {
    private final Fragment fragment;
    private final DoneNetwork doneNetwork;

    public AddressEntryModel(Fragment fragment, DoneNetwork doneNetwork){
        this.fragment = fragment;
        this.doneNetwork = doneNetwork;
    }

    public Observable<ProductList> getCategories(Map<String, String> catId, ItemParams itemParams){
        return doneNetwork.getProductList(catId, itemParams.getPage(), itemParams.getLimit(), itemParams.getOrderBy(), itemParams.getDir());
    }

  /*  public Observable<ProductList> getSearchResult(String searchText, ItemParams itemParams){
        return doneNetwork.searchProduct(searchText, itemParams.getPage(), itemParams.getLimit(), itemParams.getOrderBy(), itemParams.getDir());
    }*/

  public Observable<PostAddressResponse> postAddress(PostAddress postAddress){
      return doneNetwork.postAddress(postAddress);
  }

  public Observable<PostAddressResponse> postAddressForEdit(PostAddress postAddress){
      return doneNetwork.postAddressForEdit(postAddress);
  }
}
