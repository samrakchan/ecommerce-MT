package com.metrotarkari.app.ui.addressbook.addresslist.dragger;

import javax.inject.Scope;

@Scope
public @interface AddressBookScope {
}
