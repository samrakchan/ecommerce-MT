package com.metrotarkari.app.ui.categories;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metrotarkari.app.application.DoneApplication;
import com.metrotarkari.app.ui.categories.dagger.CategoriesModule;
import com.metrotarkari.app.ui.categories.dagger.DaggerCategoriesComponent;
import com.metrotarkari.app.ui.categories.mvp.CategoriesPresenter;
import com.metrotarkari.app.ui.categories.mvp.CategoriesView;

import javax.inject.Inject;

/**
 * Created by samrakchan on 4/18/17.
 */

public class CategoriesFragment extends Fragment {

    @Inject
    CategoriesView categoriesView;

    @Inject
    CategoriesPresenter categoriesPresenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setRetainInstance(true);
        DaggerCategoriesComponent.builder().appComponent(DoneApplication.get(this).component())
                .categoriesModule(new CategoriesModule(this)).build().inject(this);

        categoriesPresenter.getCategoriesData();

        return categoriesView;
    }



    /*@Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.content_recycler_view, container, false);

        contentRv = (RecyclerView) view.findViewById(R.id.contentRv);
        mAdapter = new CategoriesAdapter(getContext());
        contentRv.setLayoutManager(new GridLayoutManager(getContext(), ViewUtils.calculateNoOfColumns(getContext())));

        iconTextIds = new ArrayList<>();
        iconTextIds.add(new IconTextId(1, R.mipmap.ic_adapter_policy, getString(R.string.policy)));
        iconTextIds.add(new IconTextId(2, R.mipmap.ic_adapter_rating, getString(R.string.rate_app)));
        iconTextIds.add(new IconTextId(3, R.mipmap.ic_adapter_help, getString(R.string.help_center)));
        iconTextIds.add(new IconTextId(3, R.mipmap.ic_adapter_help, getString(R.string.help_center)));


        mAdapter.addItems(iconTextIds,this);

        contentRv.setAdapter(mAdapter);


        DividerItemDecoration verticalDividerDecoration = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        DividerItemDecoration horizontalDividerDecoration = new DividerItemDecoration(getContext(), DividerItemDecoration.HORIZONTAL);

        Drawable horizontalDivider = ContextCompat.getDrawable(getActivity(), R.drawable.divider_center_line_horizontal);
        Drawable verticalDivider = ContextCompat.getDrawable(getActivity(), R.drawable.divider_center_line_vertical);

        verticalDividerDecoration.setDrawable(verticalDivider);
        horizontalDividerDecoration.setDrawable(horizontalDivider);

        contentRv.addItemDecoration(verticalDividerDecoration);
        contentRv.addItemDecoration(horizontalDividerDecoration);

        return view;
    }

    @Override
    public void onItemClickListener(int id, int viewId, Object object) {
        if (id == 1){
            Intent intent = new Intent(getContext(), DoneWebViewActivity.class);
            intent.putExtra(Constant.TITLE,getString(R.string.policy));
            intent.putExtra(Constant.URL,"http://google.com");
            startActivity(intent);
        } else if (id == 2){
            Toast.makeText(getActivity(),"RATE",Toast.LENGTH_SHORT).show();
        } else if (id == 3){
            Intent intent = new Intent(getContext(), DoneWebViewActivity.class);
            intent.putExtra(Constant.TITLE,getString(R.string.help_center));
            intent.putExtra(Constant.URL,"http://google.com");
            startActivity(intent);
        }
    }
*/

}
