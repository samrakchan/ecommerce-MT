package com.metrotarkari.app.dialogue.filter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.metrotarkari.app.R;
import com.metrotarkari.app.dialogue.filter.model.FilterItem;
import com.metrotarkari.app.model.LayerFilter;
import com.metrotarkari.app.utils.QueryListener;

import java.util.List;

/**
 * Created by samrakchan on 4/3/17.
 */

public class FilterDialog implements View.OnClickListener{
    private RecyclerView dialogFilterRv;
    private FilterAdapter filterAdapter;
    private AppCompatImageView dialogFilterCloseIv;
    private AlertDialog.Builder builder;
    private AlertDialog alertDialog;


    public FilterDialog(final Activity activity, List<LayerFilter> filterItemList, QueryListener queryListener){
        builder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_filter, null);

        filterAdapter = new FilterAdapter(activity, filterItemList);

        dialogFilterCloseIv = (AppCompatImageView) view.findViewById(R.id.dialogFilterCloseIv);
        dialogFilterCloseIv.setOnClickListener(this);

        dialogFilterRv = (RecyclerView) view.findViewById(R.id.dialogFilterRv);
        dialogFilterRv.setLayoutManager(new LinearLayoutManager(activity.getApplicationContext()));
        dialogFilterRv.setAdapter(filterAdapter);

        builder.setPositiveButton(R.string.ok, (dialog, which)->{
            queryListener.onQueryListener(filterAdapter.getFilterQuery());
        });

        builder.setView(view);
        builder.create();
        alertDialog = builder.show();

    }

    @Override
    public void onClick(View v) {
        if(alertDialog!=null){
            alertDialog.dismiss();
        }
    }
}
