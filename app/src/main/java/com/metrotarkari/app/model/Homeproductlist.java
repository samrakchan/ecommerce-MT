
package com.metrotarkari.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Homeproductlist {

    @SerializedName("productlist_id")
    @Expose
    private String productlistId;
    @SerializedName("list_title")
    @Expose
    private String listTitle;
    @SerializedName("list_image")
    @Expose
    private String listImage;
    @SerializedName("list_image_tablet")
    @Expose
    private String listImageTablet;
    @SerializedName("list_type")
    @Expose
    private String listType;
    @SerializedName("list_products")
    @Expose
    private Object listProducts;
    @SerializedName("list_status")
    @Expose
    private String listStatus;
    @SerializedName("sort_order")
    @Expose
    private String sortOrder;
    @SerializedName("matrix_width_percent")
    @Expose
    private String matrixWidthPercent;
    @SerializedName("matrix_height_percent")
    @Expose
    private String matrixHeightPercent;
    @SerializedName("matrix_width_percent_tablet")
    @Expose
    private String matrixWidthPercentTablet;
    @SerializedName("matrix_height_percent_tablet")
    @Expose
    private String matrixHeightPercentTablet;
    @SerializedName("matrix_row")
    @Expose
    private String matrixRow;
    @SerializedName("entity_id")
    @Expose
    private String entityId;
    @SerializedName("content_type")
    @Expose
    private String contentType;
    @SerializedName("item_id")
    @Expose
    private String itemId;
    @SerializedName("store_view_id")
    @Expose
    private String storeViewId;
    @SerializedName("width")
    @Expose
    private Integer width;
    @SerializedName("height")
    @Expose
    private Integer height;
    @SerializedName("width_tablet")
    @Expose
    private Integer widthTablet;
    @SerializedName("height_tablet")
    @Expose
    private Integer heightTablet;
    @SerializedName("type_name")
    @Expose
    private String typeName;
    @SerializedName("type_code")
    @Expose
    private String typeCode;
    @SerializedName("product_array")
    @Expose
    private ProductArray_ productArray;

    public String getProductlistId() {
        return productlistId;
    }

    public void setProductlistId(String productlistId) {
        this.productlistId = productlistId;
    }

    public String getListTitle() {
        return listTitle;
    }

    public void setListTitle(String listTitle) {
        this.listTitle = listTitle;
    }

    public String getListImage() {
        return listImage;
    }

    public void setListImage(String listImage) {
        this.listImage = listImage;
    }

    public String getListImageTablet() {
        return listImageTablet;
    }

    public void setListImageTablet(String listImageTablet) {
        this.listImageTablet = listImageTablet;
    }

    public String getListType() {
        return listType;
    }

    public void setListType(String listType) {
        this.listType = listType;
    }

    public Object getListProducts() {
        return listProducts;
    }

    public void setListProducts(Object listProducts) {
        this.listProducts = listProducts;
    }

    public String getListStatus() {
        return listStatus;
    }

    public void setListStatus(String listStatus) {
        this.listStatus = listStatus;
    }

    public String getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }

    public String getMatrixWidthPercent() {
        return matrixWidthPercent;
    }

    public void setMatrixWidthPercent(String matrixWidthPercent) {
        this.matrixWidthPercent = matrixWidthPercent;
    }

    public String getMatrixHeightPercent() {
        return matrixHeightPercent;
    }

    public void setMatrixHeightPercent(String matrixHeightPercent) {
        this.matrixHeightPercent = matrixHeightPercent;
    }

    public String getMatrixWidthPercentTablet() {
        return matrixWidthPercentTablet;
    }

    public void setMatrixWidthPercentTablet(String matrixWidthPercentTablet) {
        this.matrixWidthPercentTablet = matrixWidthPercentTablet;
    }

    public String getMatrixHeightPercentTablet() {
        return matrixHeightPercentTablet;
    }

    public void setMatrixHeightPercentTablet(String matrixHeightPercentTablet) {
        this.matrixHeightPercentTablet = matrixHeightPercentTablet;
    }

    public String getMatrixRow() {
        return matrixRow;
    }

    public void setMatrixRow(String matrixRow) {
        this.matrixRow = matrixRow;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getStoreViewId() {
        return storeViewId;
    }

    public void setStoreViewId(String storeViewId) {
        this.storeViewId = storeViewId;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getWidthTablet() {
        return widthTablet;
    }

    public void setWidthTablet(Integer widthTablet) {
        this.widthTablet = widthTablet;
    }

    public Integer getHeightTablet() {
        return heightTablet;
    }

    public void setHeightTablet(Integer heightTablet) {
        this.heightTablet = heightTablet;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public ProductArray_ getProductArray() {
        return productArray;
    }

    public void setProductArray(ProductArray_ productArray) {
        this.productArray = productArray;
    }

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }
}
