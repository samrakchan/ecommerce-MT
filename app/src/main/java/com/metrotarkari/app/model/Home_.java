
package com.metrotarkari.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Home_ {

    @SerializedName("homebanners")
    @Expose
    private Homebanners homebanners;

    @SerializedName("homecategories")
    @Expose
    private HomeCategories homecategories;

    @SerializedName("homeproductlists")
    @Expose
    private Homeproductlists homeproductlists;

    public Homebanners getHomebanners() {
        return homebanners;
    }

    public void setHomebanners(Homebanners homebanners) {
        this.homebanners = homebanners;
    }

    public HomeCategories getHomecategories() {
        return homecategories;
    }

    public void setHomecategories(HomeCategories homecategories) {
        this.homecategories = homecategories;
    }

    public Homeproductlists getHomeproductlists() {
        return homeproductlists;
    }

    public void setHomeproductlists(Homeproductlists homeproductlists) {
        this.homeproductlists = homeproductlists;
    }
}
