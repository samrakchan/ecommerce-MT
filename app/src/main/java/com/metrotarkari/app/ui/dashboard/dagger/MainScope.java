package com.metrotarkari.app.ui.dashboard.dagger;

import javax.inject.Scope;

@Scope
public @interface MainScope {
}
