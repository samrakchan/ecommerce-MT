
package com.metrotarkari.app.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LogoutResponse extends ErrorResponse{

    @SerializedName("customer")
    @Expose
    private List<Object> customer = null;
    @SerializedName("message")
    @Expose
    private List<String> message = null;

    public List<Object> getCustomer() {
        return customer;
    }

    public void setCustomer(List<Object> customer) {
        this.customer = customer;
    }

    public List<String> getMessage() {
        return message;
    }

    public void setMessage(List<String> message) {
        this.message = message;
    }

}
