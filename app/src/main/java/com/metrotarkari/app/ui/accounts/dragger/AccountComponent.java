package com.metrotarkari.app.ui.accounts.dragger;

import com.metrotarkari.app.application.dagger.AppComponent;
import com.metrotarkari.app.ui.accounts.AccountsFragment;

import dagger.Component;

@AccountScope
@Component(modules = { AccountModule.class }, dependencies = AppComponent.class)
public interface AccountComponent {

  void inject(AccountsFragment accountsFragment);

}
