package com.metrotarkari.app.ui.home;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metrotarkari.app.application.DoneApplication;
import com.metrotarkari.app.ui.b2b.add.mvp.B2bAddPresenter;
import com.metrotarkari.app.ui.home.dagger.DaggerHomeComponent;
import com.metrotarkari.app.ui.home.dagger.HomeModule;
import com.metrotarkari.app.ui.home.mvp.HomePresenter;
import com.metrotarkari.app.ui.home.mvp.HomeView;

import javax.inject.Inject;

/**
 * Created by samrakchan on 6/6/17.
 */

//@RuntimePermissions
public class HomeFragment extends Fragment {

    @Inject
    HomeView homeView;

    @Inject
    HomePresenter homePresenter;

    @Inject
    B2bAddPresenter b2bAddPresenter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setRetainInstance(true);
        DaggerHomeComponent.builder().appComponent(DoneApplication.get(this).component())
                .homeModule(new HomeModule(this)).build().inject(this);

        homePresenter.getHomeItems();

        return homeView;
    }

    @Override
    public void onResume() {
        super.onResume();

    }
}
