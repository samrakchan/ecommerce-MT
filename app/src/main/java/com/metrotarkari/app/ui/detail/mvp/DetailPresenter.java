package com.metrotarkari.app.ui.detail.mvp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.metrotarkari.app.R;
import com.metrotarkari.app.application.DoneApplication;
import com.metrotarkari.app.event.CartEvent;
import com.metrotarkari.app.model.AppOptions;
import com.metrotarkari.app.model.CartQuote;
import com.metrotarkari.app.model.CustomOption;
import com.metrotarkari.app.model.Error;
import com.metrotarkari.app.model.Product_;
import com.metrotarkari.app.utils.Constant;
import com.metrotarkari.app.utils.MyToast;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by samrakchan on 4/27/17.
 */

public class DetailPresenter {
    private final DetailView view;
    private final DetailModel model;
    private final AppCompatActivity activity;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private EventBus bus = EventBus.getDefault();

    public DetailPresenter(DetailView view, DetailModel model, AppCompatActivity activity){
        this.view = view;
        this.model = model;
        this.activity = activity;

        view.addToCartBtn()
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(aVoid ->{

                    CartQuote cartQuote = new CartQuote();
                    cartQuote.setProduct(view.getProductId());
                    cartQuote.setQty(view.getProductQty());

                    compositeDisposable.add(postQuote(cartQuote));
                });

        view.plusBtn().subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(aVoid->{
                    int qty = view.getQuantity();
                    view.setQuantity(qty+1);
                });

        view.minusBtn().subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(aVoid->{
                   int qty = view.getQuantity();
                    if(qty>1) {
                        view.setQuantity(qty -1);
                    }
                });

        view.shareBtn().subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(aVoid->{
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.setType("text/plain");
                    intent.putExtra(Intent.EXTRA_SUBJECT, view.getContext().getString(R.string.sharing_product));
                    intent.putExtra(Intent.EXTRA_TEXT, Constant.DEEP_LINK_PREFIX+Constant.SHARE_URL+view.getProductId()+Constant.DEEP_LINK_SUFFIX);
                    activity.startActivity(Intent.createChooser(intent, view.getContext().getString(R.string.share_product)));
                });
    }

    public void onDestroy(){
        if (compositeDisposable!=null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    public void getDetail(String productId){
        compositeDisposable.add(getProductDetail(productId));
    }

    private Disposable getProductDetail(String productId){
        view.showNoItem(false,false);
        view.showProgressBar(true);
        return model.getProductDetail(productId).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(detail -> {
                    view.showProgressBar(false);
                    view.showNoItem(false,false);
                    List<Error> errorResponse = detail.getErrors();

                   if(errorResponse!=null && !errorResponse.isEmpty()){
                        Error error = errorResponse.get(0);
                       MyToast.displayToast(view.getContext(), error.getMessage()+"", Toast.LENGTH_LONG);
                   }else if(detail!=null) {
                        view.setDetail(detail.getProduct());

                       Product_ product_ = detail.getProduct();

                        if(product_!=null) {
                            view.addRelatedProducts(product_.getProductRelated());
                            view.addYouMayBeInterestedProducts(product_.getProductUpsell());
                            view.addAppReview(product_.getAppReviews());

                            AppOptions appOptions = product_.getAppOptions();
                            List<CustomOption> customOptions = appOptions.getCustomOptions();
                            if(customOptions != null){
                               view.initCustomOptions(customOptions);
                            }else {
                                Log.i("CUSTOM_OPTION", null);
                            }
                            List<String> tierPrice = product_.getFormattedTierPrice();
                            if(tierPrice!=null){
                                view.initTierPrice(tierPrice);
                            }
                        }
                   }else {
                        view.showNoItem(true,false);
                   }

                }, throwable -> {
                    throwable.printStackTrace();
                    MyToast.displayToast(view.getContext(), R.string.sww, Toast.LENGTH_LONG);
                    view.showProgressBar(false);
                    view.showNoItem(true,true);
                });
    }

    private Disposable postQuote(CartQuote cartQuote){
        view.showProgressDialog();
        return model.postCartQuote(cartQuote)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(cartQuoteContent -> {
                    view.hideProgressDialog();
                    List<Error> errorResponse = cartQuoteContent.getErrors();
                    if(errorResponse!=null && !errorResponse.isEmpty()){
                        Error error = errorResponse.get(0);
                        Toast.makeText(view.getContext(),error.getMessage()+"",Toast.LENGTH_LONG).show();
                    }else if(cartQuoteContent!=null && !cartQuoteContent.getAllIds().isEmpty()){
                        cartQuoteContent.getCartTotal();

                        if(DoneApplication.getInstance()!=null && cartQuoteContent.getCartTotal() !=null) {

                            CartEvent cartEvent = new CartEvent(cartQuoteContent.getCartTotal());
                            bus.post(cartEvent);

                            DoneApplication.getInstance().setCartCount(cartQuoteContent.getCartTotal());
                        }

                        MyToast.displayToast(view.getContext(), R.string.successfully_added_to_cart, Toast.LENGTH_LONG);
                    }

                }, throwable -> {
                    view.hideProgressDialog();
                    throwable.printStackTrace();
                    MyToast.displayToast(view.getContext(), R.string.sww, Toast.LENGTH_LONG);
                });
    }
}
