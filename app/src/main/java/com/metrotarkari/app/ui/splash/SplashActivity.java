package com.metrotarkari.app.ui.splash;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.crashlytics.android.Crashlytics;
import com.metrotarkari.app.application.DoneApplication;
import com.metrotarkari.app.ui.splash.dagger.DaggerSplashComponent;
import com.metrotarkari.app.ui.splash.dagger.SplashModule;
import com.metrotarkari.app.ui.splash.mvp.SplashPresenter;
import com.metrotarkari.app.ui.splash.mvp.SplashView;

import javax.inject.Inject;

import io.fabric.sdk.android.Fabric;

/**
 * Created by samrakchan on 3/5/17.
 */

public class SplashActivity extends AppCompatActivity {

    @Inject
    SplashView view;

    @Inject
    SplashPresenter splashPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());

        DaggerSplashComponent.builder().appComponent(DoneApplication.get(this).component())
                .splashModule(new SplashModule(this)).build().inject(this);

        setContentView(view);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        splashPresenter.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        splashPresenter.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        splashPresenter.onPause();
    }


    private void removeOlderPreferences(){
        try {
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            int version = pInfo.versionCode;
            if(version>22){

            }

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }
}
