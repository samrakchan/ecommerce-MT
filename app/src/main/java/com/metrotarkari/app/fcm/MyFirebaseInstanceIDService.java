package com.metrotarkari.app.fcm;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.metrotarkari.app.utils.DoneUserPreferenceManager;

/**
 * Created by samrakchan on 12/28/16.
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";
    //private LocalBroadcastManager mLocalBroadCastManager;
    private DoneUserPreferenceManager mDoneUserPreferenceManager;

    @Override
    public void onCreate() {
        super.onCreate();
        //mLocalBroadCastManager = LocalBroadcastManager.getInstance(getApplicationContext());
        mDoneUserPreferenceManager = new DoneUserPreferenceManager(getApplicationContext());
    }

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Fcm Refreshed token: " + refreshedToken);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(refreshedToken);
    }
    // [END refresh_token]

    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(final String token) {

        mDoneUserPreferenceManager.setFcmToken(token);
        Log.i("FCM TOKEN", "TOKEN "+token);

        //Intent intent = new Intent(Constant.FCM_BROADCAST);
        //intent.putExtra(Constant.FCM_TOKEN, token);
        //Put your all data using put extra

        //mLocalBroadCastManager.sendBroadcast(intent);

    }
}
