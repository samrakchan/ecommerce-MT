package com.metrotarkari.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by samrakchan on 6/6/17.
 */

public class Homebanners {

    @SerializedName("all_ids")
    @Expose
    private List<String> allIds = null;
    @SerializedName("homebanners")
    @Expose
    private List<? extends Homebanner> homebanners = null;
    @SerializedName("total")
    @Expose
    private Integer total;
    @SerializedName("page_size")
    @Expose
    private Integer pageSize;
    @SerializedName("from")
    @Expose
    private Integer from;
    @SerializedName("total_page")
    @Expose
    private Integer totalPage;
    @SerializedName("current_page")
    @Expose
    private Integer currentPage;
    @SerializedName("car_total")
    @Expose
    private Integer carTotal;

    public List<String> getAllIds() {
        return allIds;
    }

    public void setAllIds(List<String> allIds) {
        this.allIds = allIds;
    }

    public List<? extends Homebanner> getHomebanners() {
        return homebanners;
    }

    public void setHomebanners(List<? extends Homebanner> homebanners) {
        this.homebanners = homebanners;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getFrom() {
        return from;
    }

    public void setFrom(Integer from) {
        this.from = from;
    }

    public Integer getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(Integer totalPage) {
        this.totalPage = totalPage;
    }

    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

    public Integer getCarTotal() {
        return carTotal;
    }

    public void setCarTotal(Integer carTotal) {
        this.carTotal = carTotal;
    }
}
