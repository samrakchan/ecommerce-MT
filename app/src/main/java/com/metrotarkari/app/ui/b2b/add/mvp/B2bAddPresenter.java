package com.metrotarkari.app.ui.b2b.add.mvp;

import android.widget.Toast;

import com.metrotarkari.app.ui.base.BaseViewFragment;
import com.metrotarkari.app.ui.home.mvp.HomeView;

import java.util.HashMap;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by samrakchan on 9/21/17.
 */

public class B2bAddPresenter {
    private static final String TAG = B2bAddPresenter.class.getSimpleName();

    private final BaseViewFragment view;
    private final B2bAddModel model;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public B2bAddPresenter(HomeView view, B2bAddModel model) {
        this.view = view;
        this.model = model;

        compositeDisposable.add(view.addItemToB2BWishList().subscribe(
                itemParams -> {
                    compositeDisposable.add(addToB2BWishList(itemParams));
                }
        ));
    }

    private Disposable addToB2BWishList(HashMap<String, String> map){
        view.showProgressDialog();
        return model.postWishList(map).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(wishlistitem -> {
                    view.hideProgressDialog();
                    if(wishlistitem.getErrors() !=null && wishlistitem.getErrors().size()>0){
                        Toast.makeText(view.getContext(), wishlistitem.getErrors().get(0).getMessage()+"", Toast.LENGTH_LONG).show();
                    }else if(wishlistitem.getQty()>0){
                        Toast.makeText(view.getContext(), wishlistitem.getName()+" Moved to wishlist", Toast.LENGTH_LONG).show();
                    }
                }, throwable -> {
                    view.hideProgressDialog();
                    throwable.printStackTrace();
                });
    }
}
