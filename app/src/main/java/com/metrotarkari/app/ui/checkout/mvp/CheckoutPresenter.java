package com.metrotarkari.app.ui.checkout.mvp;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.widget.Toast;

import com.google.gson.Gson;
import com.metrotarkari.app.R;
import com.metrotarkari.app.db.DoneDbAdapter;
import com.metrotarkari.app.model.Address;
import com.metrotarkari.app.model.Base;
import com.metrotarkari.app.model.Error;
import com.metrotarkari.app.model.Order2;
import com.metrotarkari.app.model.OrderResponse;
import com.metrotarkari.app.model.Order_;
import com.metrotarkari.app.model.Shipping;
import com.metrotarkari.app.model.StoreViewResult;
import com.metrotarkari.app.model.Total;
import com.metrotarkari.app.ui.checkout.model.CouponCode;
import com.metrotarkari.app.ui.checkout.model.BAddress;
import com.metrotarkari.app.ui.checkout.model.CheckOutOrder;
import com.metrotarkari.app.ui.checkout.model.PMethod;
import com.metrotarkari.app.ui.checkout.model.SAddress;
import com.metrotarkari.app.ui.checkout.model.SMethod;
import com.metrotarkari.app.utils.AlertUtils;
import com.metrotarkari.app.utils.Constant;
import com.metrotarkari.app.utils.MyLogCat;
import com.metrotarkari.app.utils.Utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by samrakchan on 4/27/17.
 */

public class CheckoutPresenter {
    private static final String TAG = CheckoutPresenter.class.getSimpleName();

    private final CheckoutView view;
    private final CheckoutModel model;
    private int totalItems = 0;
    private Double totalPrice = 0.0;
    private DoneDbAdapter doneDbAdapter;
    private String selectedAddress="";

    private AppCompatActivity activity;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public CheckoutPresenter(CheckoutView view, CheckoutModel model, AppCompatActivity activity, DoneDbAdapter doneDbAdapter){
        this.view = view;
        this.model = model;
        this.activity = activity;
        this.doneDbAdapter = doneDbAdapter;

        view.observerCheckoutButton().throttleFirst(5, TimeUnit.SECONDS)
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(aVoid -> {
                    makeOrder();
                });

        compositeDisposable.add(view.getCouponCodeBehaviorSubject().subscribe(
                map -> {
                    compositeDisposable.add(getCheckoutInfo(selectedAddress, map.get(Constant.COUPON_CODE)));
                }
        ));
        String addressId = activity.getIntent().getStringExtra(Constant.ADDRESS_ID);
        checkOutInfo(addressId);

    }

    public void checkOutInfo(String addressId){
        compositeDisposable.add(getCheckoutInfo(addressId, null));
    }

    private Disposable postOrder(CheckOutOrder postOrder){
        view.showProgressDialog();
        return model.postOrder(postOrder)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(order -> {
                    Order_ order_  = order.getOrder();
                    List<Error> errorResponse = order.getErrors();
                    view.hideProgressDialog();

                    if(errorResponse!=null && !errorResponse.isEmpty()){
                        Error error = errorResponse.get(0);
                        Toast.makeText(view.getContext(),error.getMessage()+"",Toast.LENGTH_LONG).show();
                    }else if(order_!=null){
                       String invoiceNumber = order_.getInvoiceNumber();
                       view.showInvoice(invoiceNumber);
                    }
                }, throwable -> {
                    view.hideProgressDialog();
                    Toast.makeText(view.getContext(),R.string.sww,Toast.LENGTH_LONG).show();
                    throwable.printStackTrace();
                });
    }

    private Disposable getCheckoutInfo(String addressId, String couponCode){
        HashMap<String, Object> jsonParams = new HashMap<>();
        HashMap<String, String> parms = new HashMap<>();
        selectedAddress = addressId;
        parms.put("entity_id", addressId);
        jsonParams.put("b_address", parms);
        jsonParams.put("s_address", parms);

        if(couponCode!=null) {
            jsonParams.put("coupon_code", couponCode);
        }
        view.showProgressBar(true);
        view.removeItemFromAdapter();
        return model.getCheckoutInfos(jsonParams)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(orderResponse -> {

                    view.showProgressBar(false);

                    Gson gson = new Gson();
                    String oR = gson.toJson(orderResponse, OrderResponse.class);

                    MyLogCat.logCat(TAG, oR);

                    String value = gson.toJson(orderResponse.getOrder().getTotal(), Total.class);
                    MyLogCat.logCat(TAG, value);

                    /*    SOption sOption = new SOption();
                    for (String str:orderResponse.getOrder().getShipping().get(0).getSOptions()){
                        sOption.setStringOptionValue(str);
                       // Log.i("OPTIONS",sr);
                    }*/

                    List<Object> objects = new ArrayList<>();
                    String string = new String(); //For Date and Time Adapter
                    Integer integer = new Integer(2); //For Payment Method Adapter

                    List<Error> errors = orderResponse.getErrors();
                    if(errors!=null && !errors.isEmpty()){

                        Error error = errors.get(0);
                        Toast.makeText(view.getContext(),error.getMessage()+"",Toast.LENGTH_LONG).show();

                    }else if(orderResponse!=null && orderResponse.getOrder()!=null) {
                        Order2 order2 = orderResponse.getOrder();
                        Total total = order2.getTotal();
                        if (total != null){
                            totalPrice = total.getGrandTotalExclTax();
                            objects.add(0, total);
                        }

                        if(order2.getQuoteItem()!=null) {
                            objects.addAll(order2.getQuoteItem().getQuoteitems());
                            totalItems = order2.getQuoteItem().getQuoteitems().size();
                        }

                        if(order2.getShippingAddress()!=null){
                            Address address = order2.getShippingAddress();
                            if(address!=null) {
                                objects.add(1,address);
                            }

                            //CheckOutShipping remember position = 2
                            objects.add(2, order2.getShipping());
                        }

                        if (order2.getPayment()!=null){
                            objects.add(3,string);
                        }

                        if (order2.getShipping()!=null){
                            objects.add(4,integer);
                        }

                        if (total!=null){
                            CouponCode cc = new CouponCode();
                            cc.setIsCoupenApplied(false);
                            if(total.getCouponCode()!=null) {
                                cc.setCoupenCode(total.getCouponCode());
                                cc.setIsCoupenApplied(total.getAppliedCoupon());
                            }
                            objects.add(5, cc);
                        }
                    }
                    view.addItemToCheckoutAdapter(objects);
                    view.setTotalItems(totalItems);

                }, throwable -> {
                    throwable.printStackTrace();
                    Toast.makeText(view.getContext(), R.string.sww,Toast.LENGTH_LONG).show();
                    view.showProgressBar(false);
                });

    }


    private void makeOrder(){
        doneDbAdapter.openReadableDb();
        String jsonString = doneDbAdapter.getStoreView();

        if(jsonString!=null) {
            Gson gson = new Gson();
            StoreViewResult configResponse = gson.fromJson(jsonString, StoreViewResult.class);
            if(configResponse!=null && configResponse.getStoreview()!=null) {
                List<Error> errorResponse = configResponse.getErrors();
                Base base = configResponse.getStoreview().getBase();
                if(errorResponse!=null && !errorResponse.isEmpty()){
                    Error error = errorResponse.get(0);
                    Toast.makeText(view.getContext(),error.getMessage()+"",Toast.LENGTH_LONG).show();
                }else if (base!=null){
                    
                    if(!checkIfDeliveryDateAndTimeSelected()){
                        AlertUtils.createAlert(activity, R.string.ok, view.getContext().getString(R.string.select_date_and_time)).show();
                    }else if(!checkIfSatisfyMinimumOrderRule(base, totalPrice)){
                        AlertUtils.createAlert(activity, R.string.ok, base.getMinOrderAmountMessage()).show();
                    }else{
                        doCheckOut();
                    }
                }else {
                    Toast.makeText(view.getContext(), R.string.sww,Toast.LENGTH_LONG).show();
                }
            }
        }

        doneDbAdapter.closeDb();
    }

    public void destroy(){
        compositeDisposable.dispose();
    }

    private boolean checkIfSatisfyMinimumOrderRule(Base base, double orderedAmount){
        // if min order amount == 1 is assumed minimum order rule is enabled.
        if(base.getMinOrderAmountEnable().equals("1")){
            String minimumOrderAmount = base.getMinOrderAmount();
            if(Double.parseDouble(minimumOrderAmount)>orderedAmount){
                return false;
            }
            return true;

        }else{
            return true;
        }
    }

    private boolean checkIfDeliveryDateAndTimeSelected(){
        if(view.getSelectedTime()!=null) {
            if (!TextUtils.isEmpty(view.getSelectedTime().getSMethodTitle()) && !TextUtils.isEmpty(view.getSelectedDate())) {
                return true;
            }
            return false;
        }
        return false;
    }

    private void doCheckOut(){
        String date = view.getSelectedDate();
        String stringDate = Utility.convertDateAndTime(date);
        Shipping selectedTime = view.getSelectedTime();


        //conformation dialog for proceed checkout
        AlertDialog.Builder builder = new AlertDialog.Builder(activity)
                .setMessage(view.getContext().getString(R.string.purchase_conformation_message, view.getTotalItems(), stringDate, selectedTime.getSMethodTitle(), totalPrice+""))
                .setCancelable(true)
                .setPositiveButton(R.string.ok, (dialogInterface, i) -> {
                    //checkout conformation done
                    CheckOutOrder checkOutOrder = new CheckOutOrder();
                    SAddress sAddress = new SAddress();
                    sAddress.setEntityId(view.getAddressId() + "");
                    BAddress bAddress = new BAddress();
                    bAddress.setEntityId(view.getAddressId() + "");
                    PMethod pMethod = new PMethod();
                    pMethod.setMethod("cashondelivery");
                    SMethod sMethod = new SMethod();
                    sMethod.setMethod(selectedTime.getSMethodCode());
                    checkOutOrder.setBAddress(bAddress);
                    checkOutOrder.setSAddress(sAddress);
                    checkOutOrder.setSMethod(sMethod);
                    checkOutOrder.setPMethod(pMethod);
                    checkOutOrder.setDeliveryDate(view.getSelectedDate());
                    checkOutOrder.setDeliveryTime(selectedTime.getSMethodTitle());

                    postOrder(checkOutOrder);
                })
                .setNegativeButton(R.string.cancel, ((dialogInterface, i) -> {
                    dialogInterface.dismiss();
                }));
        builder.create();
        builder.show();
    }
}
