package com.metrotarkari.app.menu;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;

import com.metrotarkari.app.R;
import com.metrotarkari.app.model.Category;
import com.metrotarkari.app.model.ChildCategory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by samrakchan on 4/6/17.
 */

public class ExpendedMenuAdapter extends BaseExpandableListAdapter {
    private Context mContext;
    private List<Category> mCategories;

    public ExpendedMenuAdapter(Context context){
        this.mContext = context;
        this.mCategories = new ArrayList<>();
    }

    public void addItems(List<Category> categories){
        mCategories.clear();
        mCategories.addAll(categories);
        notifyDataSetChanged();
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        /*return this.mCategoryChilds.get(this.mCategories.get(groupPosition))
                .get(childPosition);*/
        Category categories = mCategories.get(groupPosition);
        if(categories!=null){
            if(categories.getChildCategory()!=null && !categories.getChildCategory().isEmpty()){
                return categories.getChildCategory().get(childPosition);
            }
        }
        return null;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final ChildCategory childText = (ChildCategory) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.adapter_menu_category_child, null);
        }

        AppCompatTextView txtListChild = (AppCompatTextView) convertView.findViewById(R.id.menuCategoryTitleTv);

        txtListChild.setText(childText.getName());
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
            /*return this.mCategoryChilds.get(this.mCategories.get(groupPosition))
                    .size();*/
            List<ChildCategory> child= this.mCategories.get(groupPosition).getChildCategory();
            if(child!=null){
                return child.size();
            }
            return 0;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.mCategories.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.mCategories.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        Category category = (Category) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.adapter_menu_category, null);
        }

        AppCompatTextView lblListHeader = (AppCompatTextView) convertView.findViewById(R.id.menuCategoryTitleTv);
        lblListHeader.setText(category.getName());

        AppCompatImageView lblListChevronIv = (AppCompatImageView) convertView.findViewById(R.id.dialogFilterHeaderChevronIv);
        lblListChevronIv.setImageResource(isExpanded==true?R.drawable.ic_minus:R.drawable.ic_plus);
        if(category.getChildCategory()!=null && category.getChildCategory().size()>0){
            lblListChevronIv.setVisibility(View.VISIBLE);
        }else{
            lblListChevronIv.setVisibility(View.INVISIBLE);
        }

        AppCompatImageView imageView = (AppCompatImageView)convertView.findViewById(R.id.menuCategoryTitleIv);
        imageView.setVisibility(View.GONE);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

}
