package com.metrotarkari.app.ui.b2b.view.dagger;

import com.metrotarkari.app.application.dagger.AppComponent;
import com.metrotarkari.app.ui.b2b.view.B2bActivity;

import dagger.Component;

@B2bScope
@Component(modules = { B2bModule.class }, dependencies = AppComponent.class)
public interface B2bComponent {

  void inject(B2bActivity activity);

}
