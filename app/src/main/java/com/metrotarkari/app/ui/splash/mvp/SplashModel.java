package com.metrotarkari.app.ui.splash.mvp;

import android.app.Activity;
import android.content.Intent;

import com.metrotarkari.app.ui.dashboard.MainActivity;
import com.metrotarkari.app.ui.login.LoginActivity2;
import com.metrotarkari.app.walkthrough.WalkThroughActivity;

/**
 * Created by samrakchan on 4/27/17.
 */

public class SplashModel {
    private final Activity activity;

    public SplashModel(Activity activity){
        this.activity = activity;
    }

    public void startLoginActivity(){
        Intent intent = new Intent(activity, LoginActivity2.class);
        activity.startActivity(intent);
    }

    public void startWalkThroughActivity(){
        Intent intent = new Intent(activity, WalkThroughActivity.class);
        activity.startActivity(intent);
    }

    public void startMainActivity(){
        Intent intent = new Intent(activity, MainActivity.class);
        activity.startActivity(intent);
    }

    public void finishActivity(){
        activity.finish();
    }
}
