package com.metrotarkari.app.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by samrakchan on 7/9/17.
 */

public class Address_ {
    @SerializedName("address")
    private Address address;

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}
