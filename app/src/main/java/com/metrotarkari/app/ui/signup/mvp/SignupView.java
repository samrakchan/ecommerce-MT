package com.metrotarkari.app.ui.signup.mvp;

import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.widget.EditText;
import android.widget.FrameLayout;

import com.jakewharton.rxbinding2.view.RxView;
import com.kyleduo.switchbutton.SwitchButton;
import com.metrotarkari.app.R;
import com.metrotarkari.app.utils.Constant;

import butterknife.BindView;
import butterknife.ButterKnife;
import cc.cloudist.acplibrary.ACProgressFlower;
import io.reactivex.Observable;

/**
 * Created by samrakchan on 4/27/17.
 */

public class SignupView extends FrameLayout {

    @Nullable
    @BindView(R.id.input_firstname)
    AppCompatEditText input_firstname;

    @Nullable
    @BindView(R.id.input_lastname)
    AppCompatEditText input_lastname;

    @Nullable
    @BindView(R.id.input_email)
    AppCompatEditText input_email;

    @Nullable
    @BindView(R.id.input_email_mobile_no)
    AppCompatEditText input_email_mobile_no;

    @Nullable
    @BindView(R.id.input_password)
    AppCompatEditText input_password;

    @Nullable
    @BindView(R.id.input_confirmpassword)
    AppCompatEditText input_confirmpassword;

    @Nullable
    @BindView(R.id.signupSignupBtn)
    AppCompatButton signupSignupBtn;

    @Nullable
    @BindView(R.id.signUpLatestProductInfoSwitch)
    SwitchButton signUpLatestProductInfoSwitch;

    @Nullable
    @BindView(R.id.signUpTermsAndConditionSwitch)
    SwitchButton signUpTermsAndConditionSwitch;

    @BindView(R.id.singUpCrossIv)
    AppCompatImageView singUpCrossIv;


    private final ACProgressFlower progressDialog = new ACProgressFlower.Builder(getContext())
            .themeColor(Color.WHITE)
            .fadeColor(Color.DKGRAY).build();

    private final AppCompatActivity activity;

    public SignupView(AppCompatActivity activity) {
        super(activity);
        this.activity = activity;
        inflate(activity, R.layout.activity_sign_up, this);

        ButterKnife.bind(this);

        signUpLatestProductInfoSwitch.setChecked(true);

    }

    public Observable<Object> observeSignupButton() {
        return RxView.clicks(signupSignupBtn);
    }

    public Observable<Object> observeCrossButton() {
        return RxView.clicks(singUpCrossIv);
    }

   public int getNewsLetterByToggle(){
      if (signUpLatestProductInfoSwitch.isChecked()){
          return 1;
      }else {
          return 0;
      }
   }

    public EditText getFirstNameEt() {
        return input_firstname;
    }

    public EditText getLastNameEt() {
        return input_lastname;
    }

    public EditText getEmailEt() {
        return input_email;
    }

    public EditText getPasswordEt() {
        return input_password;
    }

    public EditText getPhoneNumber(){
        return input_email_mobile_no;
    }

    public EditText getConfirmPasswordEt() {
        return input_confirmpassword;
    }

    public String getFirstNameText() {
        return input_firstname.getText().toString().trim();
    }

    public String getLastNameText() {
        return input_lastname.getText().toString().trim();
    }

    public String getEmailText() {
        return input_email.getText().toString().trim();
    }

    public String getPhoneText() {
        return input_email_mobile_no.getText().toString().trim();
    }

    public String getPasswordText() {
        return input_password.getText().toString();
    }

    public String getConfirmPasswordText() {
        return input_confirmpassword.getText().toString();
    }

    public void showLoading(boolean loading) {
        if (loading) {
            progressDialog.setCancelable(false);
            progressDialog.show();
        } else {
            progressDialog.dismiss();
        }
    }

    public void finish() {
        activity.finish();
    }

    public void navigateBackToLogin() {
        Intent intent = activity.getIntent();
        if (intent != null && (intent.getIntExtra(Constant.INTENT_FOR, -1) == Constant.CHECKOUT)) {
            activity.setResult(activity.RESULT_OK);
            activity.finish();
        }
    }
}
