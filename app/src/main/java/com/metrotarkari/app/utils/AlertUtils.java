package com.metrotarkari.app.utils;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AlertDialog;

/**
 * Created by samrakchan on 6/7/17.
 */

public class AlertUtils {

    public static AlertDialog createAlert(Activity activity, int positiveBtn, int negativeBtn, String msg, Intent intent){
        AlertDialog.Builder builder = new AlertDialog.Builder(activity)
                .setMessage(msg)
                .setCancelable(true)
                .setPositiveButton(positiveBtn, (dialogInterface, i)->{
                    activity.startActivity(intent);
                    dialogInterface.dismiss();
                })
                .setNegativeButton(negativeBtn, ((dialogInterface, i) -> {
                    dialogInterface.dismiss();
                }))
                ;
        return builder.create();
    }

    public static AlertDialog createAlert(Activity activity, int positiveBtn, int negativeBtn, int msg, Intent intent){
        return createAlert(activity, positiveBtn, negativeBtn, activity.getString(msg), intent);
    }

    public static AlertDialog createAlert(Activity activity, int positiveBtn, String msg){
        AlertDialog.Builder builder = new AlertDialog.Builder(activity)
                .setMessage(msg)
                .setCancelable(true)
                .setPositiveButton(positiveBtn, (dialogInterface, i)->{
                    dialogInterface.dismiss();
                });
        return builder.create();
    }

    public static AlertDialog createAlert(Activity activity, int positiveBtn, String msg, Intent intent){
        AlertDialog.Builder builder = new AlertDialog.Builder(activity)
                .setMessage(msg)
                .setCancelable(true)
                .setPositiveButton(positiveBtn, (dialogInterface, i)->{
                    dialogInterface.dismiss();
                    activity.finishAffinity();
                    activity.startActivity(intent);
                });
        return builder.create();
    }

    public static AlertDialog createAlertForPostReview(Activity activity, int positiveBtn, String msg){
        AlertDialog.Builder builder = new AlertDialog.Builder(activity)
                .setMessage(msg)
                .setCancelable(true)
                .setPositiveButton(positiveBtn, (dialogInterface, i)->{
                    dialogInterface.dismiss();
                    activity.finish();
                });
        return builder.create();
    }

}
