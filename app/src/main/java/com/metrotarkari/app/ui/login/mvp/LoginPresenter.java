package com.metrotarkari.app.ui.login.mvp;

import android.util.Log;
import android.util.Patterns;
import android.widget.Toast;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.google.gson.Gson;
import com.metrotarkari.app.R;
import com.metrotarkari.app.db.DoneDbAdapter;
import com.metrotarkari.app.model.Customer;
import com.metrotarkari.app.model.Error;
import com.metrotarkari.app.utils.Constant;
import com.metrotarkari.app.utils.DoneTokenPreferenceManager;
import com.metrotarkari.app.utils.DoneUserPreferenceManager;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.basgeekball.awesomevalidation.ValidationStyle.UNDERLABEL;

/**
 * Created by samrakchan on 4/30/17.
 */

public class LoginPresenter {

    private final LoginView view;
    private final LoginModel model;
    private String username, password;


    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    private AwesomeValidation awesomeValidation = new AwesomeValidation(UNDERLABEL);
    private DoneUserPreferenceManager doneUserPreferenceManager;
    private DoneTokenPreferenceManager doneTokenPreferenceManager;
    private DoneDbAdapter doneDbAdapter;

    public LoginPresenter(LoginView view, LoginModel model, DoneDbAdapter doneDbAdapter, DoneUserPreferenceManager preferenceManager){
        this.view = view;
        this.model = model;
        this.doneDbAdapter = doneDbAdapter;
        doneUserPreferenceManager = preferenceManager;
        doneTokenPreferenceManager = new DoneTokenPreferenceManager(view.getContext());
    }

    public void onCreate() {
        //Validation for email and password pattern
        this.awesomeValidation.setContext(view.getContext());
        this.awesomeValidation.addValidation(view.getEmailEt(), Patterns.EMAIL_ADDRESS, view.getContext().getString(R.string.error_email_address));
        this.awesomeValidation.addValidation(view.getPasswordEt(), Constant.PASSWORD_VALIDATION_LOGIN_REG_EXP, view.getContext().getString(R.string.error_password_login));


        view.observeLoginButton().throttleFirst(5, TimeUnit.SECONDS)
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(aVoid -> {
                    if(awesomeValidation.validate()) {

                        username = view.getEmailText();
                        password = view.getPasswordText();

                        Disposable disposable = doLogin(username, password);
                        compositeDisposable.add(disposable);
                    }
                });


        view.observeForgetPasswordTv()
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(aVoid ->{
                    model.navigateToForgetPasswordActivity();
                });

        view.observeLoginCrossIv()
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(aVoid ->{
                    model.finishActivity();
                });


        view.observeSkipTv()
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(aVoid ->{
                    //compositeDisposable.add(doAuth());
                    doneUserPreferenceManager.setUserSkipLogin(true);
                    doneUserPreferenceManager.setLoggedInUser(false);
                    model.navigateToMainActivity();
                });

        view.observeSignUpTv()
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(aVoid ->{
                    model.navigateToSignupActivity();
                });
    }

    public void onDestroy(){
        if (compositeDisposable!=null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    private Disposable doAuth(){
        view.showLoading(true);
        return model.getAuth(Constant.APP_TOKEN)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(authToken -> {
                    view.showLoading(false);
                    doneTokenPreferenceManager.setAccessToken(authToken.accessToken());
                    doneTokenPreferenceManager.setRefreshToken(authToken.refreshToken());
                    doneTokenPreferenceManager.setTokenExpireTimestamp(authToken.timestamp());

                    doneUserPreferenceManager.setUserSkipLogin(true);
                    model.navigateToMainActivity();

                }, throwable -> {
                    throwable.printStackTrace();
                    Toast.makeText(view.getContext(),R.string.sww,Toast.LENGTH_LONG).show();
                    view.showLoading(false);
                });
    }

    private Disposable doLogin(String username, String password){
        view.showLoading(true);
        return model.doLogin(username, password)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(customerResponse -> {
                    view.showLoading(false);

                    Customer customer  = customerResponse.getCustomer();
                    List<Error> errorResponse = customerResponse.getErrors();

                    Log.d("CUSTOMER",customerResponse.getCustomer()+"");

                    if(errorResponse!=null && !errorResponse.isEmpty()){
                        Error error = errorResponse.get(0);
                        Toast.makeText(view.getContext(),error.getMessage()+"",Toast.LENGTH_LONG).show();
                    }else if (customer!=null){

                        doneDbAdapter.openWritableDb();
                        Gson gson = new Gson();
                        doneDbAdapter.insertInToCustomer(gson.toJson(customerResponse));
                        doneDbAdapter.closeDb();

                        List<String> message = customerResponse.getMessage();
                        String m = message.get(0);
                        if (m != null && !message.isEmpty()){

                            doneUserPreferenceManager.setPassword(password);
                            doneUserPreferenceManager.setUsername(username);

                            doneUserPreferenceManager.setLoggedInUser(true);

                            view.navigateToTheDestination();

                        }else {
                            Toast.makeText(view.getContext(), R.string.sww, Toast.LENGTH_LONG).show();
                        }
                    }
                }, throwable -> {
                    view.showLoading(false);
                    Toast.makeText(view.getContext(),R.string.sww,Toast.LENGTH_LONG).show();
                    throwable.printStackTrace();
                });

    }

}
