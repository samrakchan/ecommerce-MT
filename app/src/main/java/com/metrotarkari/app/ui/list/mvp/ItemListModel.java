package com.metrotarkari.app.ui.list.mvp;

import android.support.v4.app.Fragment;

import com.metrotarkari.app.application.network.DoneNetwork;
import com.metrotarkari.app.model.ItemParams;
import com.metrotarkari.app.model.ProductList;

import java.util.Map;

import io.reactivex.Observable;

/**
 * Created by samrakchan on 4/27/17.
 */

public class ItemListModel {
    private final Fragment fragment;
    private final DoneNetwork doneNetwork;

    public ItemListModel(Fragment fragment, DoneNetwork doneNetwork){
        this.fragment = fragment;
        this.doneNetwork = doneNetwork;
    }

    public Observable<ProductList> getCategories(Map<String, String> catId, ItemParams itemParams){
        return doneNetwork.getProductList(catId, itemParams.getPage(), itemParams.getLimit(), itemParams.getOrderBy(), itemParams.getDir());
    }

  /*  public Observable<ProductList> getSearchResult(String searchText, ItemParams itemParams){
        return doneNetwork.searchProduct(searchText, itemParams.getPage(), itemParams.getLimit(), itemParams.getOrderBy(), itemParams.getDir());
    }*/
}
