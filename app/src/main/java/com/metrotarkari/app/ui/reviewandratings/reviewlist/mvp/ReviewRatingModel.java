package com.metrotarkari.app.ui.reviewandratings.reviewlist.mvp;

import android.support.v7.app.AppCompatActivity;

import com.metrotarkari.app.application.network.DoneNetwork;
import com.metrotarkari.app.model.ReviewResponse;

import java.util.HashMap;

import io.reactivex.Observable;

/**
 * Created by samrakchan on 4/27/17.
 */

public class ReviewRatingModel {
    private final AppCompatActivity activity;
    private final DoneNetwork doneNetwork;

    public ReviewRatingModel(AppCompatActivity activity, DoneNetwork doneNetwork){
        this.activity = activity;
        this.doneNetwork = doneNetwork;
    }


    public Observable<ReviewResponse> getReview(String productId){
        HashMap<String,String> map = new HashMap<>();
        map.put("filter[product_id]",productId);
        return doneNetwork.getReview(map);
    }
}
