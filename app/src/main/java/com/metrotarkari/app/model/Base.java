
package com.metrotarkari.app.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Base {

    @SerializedName("country_code")
    @Expose
    private String countryCode;
    @SerializedName("country_name")
    @Expose
    private String countryName;
    @SerializedName("magento_version")
    @Expose
    private String magentoVersion;
    @SerializedName("locale_identifier")
    @Expose
    private String localeIdentifier;
    @SerializedName("store_id")
    @Expose
    private String storeId;
    @SerializedName("store_name")
    @Expose
    private String storeName;
    @SerializedName("store_code")
    @Expose
    private String storeCode;
    @SerializedName("group_id")
    @Expose
    private String groupId;
    @SerializedName("base_url")
    @Expose
    private Object baseUrl;
    @SerializedName("use_store")
    @Expose
    private String useStore;
    @SerializedName("is_rtl")
    @Expose
    private String isRtl;
    @SerializedName("is_show_sample_data")
    @Expose
    private String isShowSampleData;
    @SerializedName("android_sender")
    @Expose
    private Object androidSender;
    @SerializedName("currency_symbol")
    @Expose
    private String currencySymbol;
    @SerializedName("currency_code")
    @Expose
    private String currencyCode;
    @SerializedName("currency_position")
    @Expose
    private String currencyPosition;
    @SerializedName("thousand_separator")
    @Expose
    private String thousandSeparator;
    @SerializedName("decimal_separator")
    @Expose
    private String decimalSeparator;
    @SerializedName("min_number_of_decimals")
    @Expose
    private String minNumberOfDecimals;
    @SerializedName("max_number_of_decimals")
    @Expose
    private String maxNumberOfDecimals;
    @SerializedName("min_order_amount_enable")
    @Expose
    private String minOrderAmountEnable;
    @SerializedName("min_order_amount")
    @Expose
    private String minOrderAmount;
    @SerializedName("min_order_amount_message")
    @Expose
    private String minOrderAmountMessage;
    @SerializedName("currencies")
    @Expose
    private List<Currency> currencies = null;
    @SerializedName("is_show_home_title")
    @Expose
    private String isShowHomeTitle;
    @SerializedName("cust_group")
    @Expose
    private String custGroup;
    @SerializedName("customer_identity")
    @Expose
    private String customerIdentity;
    @SerializedName("customer_ip")
    @Expose
    private String customerIp;

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getMagentoVersion() {
        return magentoVersion;
    }

    public void setMagentoVersion(String magentoVersion) {
        this.magentoVersion = magentoVersion;
    }

    public String getLocaleIdentifier() {
        return localeIdentifier;
    }

    public void setLocaleIdentifier(String localeIdentifier) {
        this.localeIdentifier = localeIdentifier;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getStoreCode() {
        return storeCode;
    }

    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public Object getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(Object baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getUseStore() {
        return useStore;
    }

    public void setUseStore(String useStore) {
        this.useStore = useStore;
    }

    public String getIsRtl() {
        return isRtl;
    }

    public void setIsRtl(String isRtl) {
        this.isRtl = isRtl;
    }

    public String getIsShowSampleData() {
        return isShowSampleData;
    }

    public void setIsShowSampleData(String isShowSampleData) {
        this.isShowSampleData = isShowSampleData;
    }

    public Object getAndroidSender() {
        return androidSender;
    }

    public void setAndroidSender(Object androidSender) {
        this.androidSender = androidSender;
    }

    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getCurrencyPosition() {
        return currencyPosition;
    }

    public void setCurrencyPosition(String currencyPosition) {
        this.currencyPosition = currencyPosition;
    }

    public String getThousandSeparator() {
        return thousandSeparator;
    }

    public void setThousandSeparator(String thousandSeparator) {
        this.thousandSeparator = thousandSeparator;
    }

    public String getDecimalSeparator() {
        return decimalSeparator;
    }

    public void setDecimalSeparator(String decimalSeparator) {
        this.decimalSeparator = decimalSeparator;
    }

    public String getMinNumberOfDecimals() {
        return minNumberOfDecimals;
    }

    public void setMinNumberOfDecimals(String minNumberOfDecimals) {
        this.minNumberOfDecimals = minNumberOfDecimals;
    }

    public String getMaxNumberOfDecimals() {
        return maxNumberOfDecimals;
    }

    public void setMaxNumberOfDecimals(String maxNumberOfDecimals) {
        this.maxNumberOfDecimals = maxNumberOfDecimals;
    }


    public String getMinOrderAmountEnable() {
        return minOrderAmountEnable;
    }

    public void setMinOrderAmountEnable(String minOrderAmountEnable) {
        this.minOrderAmountEnable = minOrderAmountEnable;
    }

    public String getMinOrderAmount() {
        return minOrderAmount;
    }

    public void setMinOrderAmount(String minOrderAmount) {
        this.minOrderAmount = minOrderAmount;
    }

    public String getMinOrderAmountMessage() {
        return minOrderAmountMessage;
    }

    public void setMinOrderAmountMessage(String minOrderAmountMessage) {
        this.minOrderAmountMessage = minOrderAmountMessage;
    }

    public List<Currency> getCurrencies() {
        return currencies;
    }

    public void setCurrencies(List<Currency> currencies) {
        this.currencies = currencies;
    }

    public String getIsShowHomeTitle() {
        return isShowHomeTitle;
    }

    public void setIsShowHomeTitle(String isShowHomeTitle) {
        this.isShowHomeTitle = isShowHomeTitle;
    }

    public String getCustGroup() {
        return custGroup;
    }

    public void setCustGroup(String custGroup) {
        this.custGroup = custGroup;
    }

    public String getCustomerIdentity() {
        return customerIdentity;
    }

    public void setCustomerIdentity(String customerIdentity) {
        this.customerIdentity = customerIdentity;
    }

    public String getCustomerIp() {
        return customerIp;
    }

    public void setCustomerIp(String customerIp) {
        this.customerIp = customerIp;
    }

}
