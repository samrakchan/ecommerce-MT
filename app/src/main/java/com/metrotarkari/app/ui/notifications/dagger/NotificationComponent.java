package com.metrotarkari.app.ui.notifications.dagger;

import com.metrotarkari.app.application.dagger.AppComponent;
import com.metrotarkari.app.ui.notifications.NotificationActivity;

import dagger.Component;

@NotificationScope
@Component(modules = { NotificationModule.class }, dependencies = AppComponent.class)
public interface NotificationComponent {

  void inject(NotificationActivity activity);

}
