package com.metrotarkari.app.ui.order.orderlist.dagger;

import com.metrotarkari.app.application.dagger.AppComponent;
import com.metrotarkari.app.ui.order.orderlist.OrderListActivity;

import dagger.Component;

@OrderListScope
@Component(modules = { OrderListModule.class }, dependencies = AppComponent.class)
public interface OrderListComponent {

  void inject(OrderListActivity activity);

}
