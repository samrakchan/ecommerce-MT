package com.metrotarkari.app.model;

import com.google.auto.value.AutoValue;
import com.google.gson.TypeAdapter;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;
import com.google.gson.Gson;

@Generated("com.robohorse.robopojogenerator")
@AutoValue
public abstract class UsersItem{

	@SerializedName("address")
	public abstract String address();

	@SerializedName("optedin")
	public abstract boolean optedin();

	@SerializedName("city")
	public abstract String city();

	@SerializedName("work")
	public abstract String work();

	@SerializedName("dob")
	public abstract String dob();

	@SerializedName("name")
	public abstract String name();

	@SerializedName("id")
	public abstract int id();

	@SerializedName("email")
	public abstract String email();

	public static TypeAdapter<UsersItem> typeAdapter(Gson gson) {
		return new AutoValue_UsersItem.GsonTypeAdapter(gson);
	}
}