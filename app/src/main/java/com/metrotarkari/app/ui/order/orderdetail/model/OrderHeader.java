package com.metrotarkari.app.ui.order.orderdetail.model;

/**
 * Created by Avinash on 6/9/2017.
 */

public class OrderHeader {
    private int id;
    private String title;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
