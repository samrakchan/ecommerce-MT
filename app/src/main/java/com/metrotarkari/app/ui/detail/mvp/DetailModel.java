package com.metrotarkari.app.ui.detail.mvp;

import com.metrotarkari.app.application.network.DoneNetwork;
import com.metrotarkari.app.model.CartQuote;
import com.metrotarkari.app.model.CartQuoteContent;
import com.metrotarkari.app.model.Detail;
import com.metrotarkari.app.ui.detail.DetailActivity;

import io.reactivex.Observable;

/**
 * Created by samrakchan on 4/27/17.
 */

public class DetailModel {
    private final DetailActivity activity;
    private final DoneNetwork doneNetwork;

    public DetailModel(DetailActivity activity, DoneNetwork doneNetwork){
        this.activity = activity;
        this.doneNetwork = doneNetwork;
    }

    public void finishActivity(){
        activity.finish();
    }

    public Observable<Detail> getProductDetail(String productId){
        return doneNetwork.getProductDetail(productId);
    }

    public Observable<CartQuoteContent> postCartQuote(CartQuote quote){
        return doneNetwork.postQuoteItem(quote);
    }
}
