package com.metrotarkari.app.application.dagger.module;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.metrotarkari.app.application.dagger.AppScope;
import com.metrotarkari.app.application.network.ConnectivityInterceptor;
import com.metrotarkari.app.application.network.DoneNetwork;
import com.metrotarkari.app.application.network.OAuthInterceptor;
import com.metrotarkari.app.ext.Constants;
import com.metrotarkari.app.utils.Constant;
import com.metrotarkari.app.utils.DoneTokenPreferenceManager;
import com.metrotarkari.app.utils.DoneUserPreferenceManager;

import java.io.File;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetworkModule {
    private static final String TAG = NetworkModule.class.getSimpleName();

    @AppScope
    @Provides
    public Cache cache(Context context) {
    return new Cache(new File(context.getCacheDir(), Constants.HTTP_CACHE_DIR),
        Constants.HTTP_CACHE_SIZE);
    }

    @AppScope
    @Provides
    public OkHttpClient okHttpClient(HttpLoggingInterceptor loggingInterceptor, Cache cache, Context context, DoneTokenPreferenceManager tokenPreferenceManager, DoneUserPreferenceManager userPreferenceManager) {
    return new OkHttpClient.Builder()
            .addInterceptor(new ConnectivityInterceptor(context))
            .addInterceptor(new OAuthInterceptor(userPreferenceManager, tokenPreferenceManager))
            .addInterceptor(loggingInterceptor)
            //.addInterceptor(new CacheIntereceptor(context))
            .addInterceptor(chain -> {
                Request original = chain.request();
                return chain.proceed(original);
            })

           // .cache(cache)
            .build();
    }



    @AppScope
    @Provides
    public HttpLoggingInterceptor httpLoggingInterceptor() {
    HttpLoggingInterceptor logging = new HttpLoggingInterceptor(
        message -> Log.i("LOG", message));
      logging.setLevel(HttpLoggingInterceptor.Level.BASIC);
      logging.setLevel(HttpLoggingInterceptor.Level.HEADERS);
      logging.setLevel(HttpLoggingInterceptor.Level.BODY);
    return logging;
    }

    @AppScope
    @Provides
    public Retrofit retrofit(OkHttpClient okHttpClient, Gson gson) {
    return new Retrofit.Builder()
        .client(okHttpClient)
        .baseUrl(Constant.BASE_URL)
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build();
    }

    @AppScope
    @Provides
    public DoneNetwork magentoNetwork(Retrofit retrofit) {
    return retrofit.create(DoneNetwork.class);
    }
/*
    @AppScope
    @Provides
    public Picasso picasso(Context context, OkHttpClient okHttpClient) {
    return new Picasso.Builder(context)
        .downloader(new OkHttp3Downloader(okHttpClient))
        .build();
    }*/

    @AppScope
    @Provides
    public DoneTokenPreferenceManager preferenceManager(Context context){
        return new DoneTokenPreferenceManager(context);
    }

}
