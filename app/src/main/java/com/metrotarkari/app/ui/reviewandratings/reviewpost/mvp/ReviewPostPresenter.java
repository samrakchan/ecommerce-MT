package com.metrotarkari.app.ui.reviewandratings.reviewpost.mvp;

import android.widget.Toast;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.metrotarkari.app.R;
import com.metrotarkari.app.model.Error;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.basgeekball.awesomevalidation.ValidationStyle.UNDERLABEL;

/**
 * Created by Avinash on 8/8/17.
 */

public class ReviewPostPresenter {
    private final ReviewPostView view;
    private final ReviewPostModel model;
    private String productId = "";

    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private AwesomeValidation awesomeValidation = new AwesomeValidation(UNDERLABEL);

    public ReviewPostPresenter(ReviewPostView view, ReviewPostModel model){
        this.view = view;
        this.model = model;

    }

    public void onCreate() {

        view.observePostReviewButton().throttleFirst(5, TimeUnit.SECONDS)
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(aVoid -> {

                    HashMap<String, Object> postReview = view.getValues();
                    //if (awesomeValidation.validate()) {
                        compositeDisposable.add(postReview(postReview));
                    //}
                });
    }

    private Disposable postReview(HashMap<String, Object> postReview) {
            view.showLoading(true);
        return model.postReview(postReview)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(postReviewResponse -> {
                    view.showLoading(false);
                    List<Error> errorResponse = postReviewResponse.getErrors();
                    String message = postReviewResponse.getMessage();

                    if(errorResponse!=null && !errorResponse.isEmpty()){
                        Error error = errorResponse.get(0);
                        Toast.makeText(view.getContext(),error.getMessage()+"",Toast.LENGTH_LONG).show();
                    }else if (postReviewResponse!=null){
                        if (message!=null){
                            view.showPostReviewConformation(message);
                        }else {
                            Toast.makeText(view.getContext(),R.string.sww,Toast.LENGTH_LONG).show();
                        }
                    }
                }, throwable -> {
                    view.showLoading(false);
                    Toast.makeText(view.getContext(), R.string.sww,Toast.LENGTH_LONG).show();
                    throwable.printStackTrace();
                });
    }


    public void setProductId(String productId){
        this.productId = productId;
    }

    public String getProductId(){
        return productId;
    }


    public void onDestroy(){
        compositeDisposable.dispose();
    }
}
