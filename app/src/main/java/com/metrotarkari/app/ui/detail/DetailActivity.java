package com.metrotarkari.app.ui.detail;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.metrotarkari.app.R;
import com.metrotarkari.app.application.DoneApplication;
import com.metrotarkari.app.event.CartEvent;
import com.metrotarkari.app.search.SearchActivity;
import com.metrotarkari.app.ui.base.BaseActivity;
import com.metrotarkari.app.ui.cart.CartActivity;
import com.metrotarkari.app.ui.detail.dagger.DaggerDetailComponent;
import com.metrotarkari.app.ui.detail.dagger.DetailModule;
import com.metrotarkari.app.ui.detail.mvp.DetailPresenter;
import com.metrotarkari.app.ui.detail.mvp.DetailView;
import com.metrotarkari.app.utils.Constant;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;

import berlin.volders.badger.BadgeShape;
import berlin.volders.badger.Badger;
import berlin.volders.badger.CountBadge;

/**
 * Created by samrakchan on 3/18/17.
 */

public class DetailActivity extends BaseActivity{

    @Inject
    DetailView itemListView;

    @Inject
    DetailPresenter itemListPresenter;

    private CountBadge.Factory circleFactory;

    private Menu menu;

    private EventBus eventBus = EventBus.getDefault();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        circleFactory = new CountBadge.Factory(this, BadgeShape.circle(.5f, Gravity.END | Gravity.TOP));

        DaggerDetailComponent.builder().appComponent(DoneApplication.get(this).component())
                .detailModule(new DetailModule(this)).build().inject(this);

        setContentView(itemListView);

        Intent intent = getIntent();
        String detailId = intent.getStringExtra(Constant.PRODUCT_ID);

        itemListPresenter.getDetail(detailId);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_detail, menu);
        this.menu = menu;

        for(int i=0; i<menu.size(); i++) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                menu.getItem(i).getIcon().mutate().setTint(ContextCompat.getColor(this, R.color.colorGrey));
            }else{
                boolean isCartIcon = (this.menu.getItem(i).getItemId() == R.id.action_cart);
                if(isCartIcon) {
                    //activity.getMenu().getItem(i).getIcon().mutate().setColorFilter(ContextCompat.getColor(getContext(), color), PorterDuff.Mode.SRC_IN);
                    Drawable drawable = ContextCompat.getDrawable(this, R.mipmap.ic_toolbar_cart);
                    drawable.setColorFilter(ContextCompat.getColor(this, R.color.colorGrey), PorterDuff.Mode.SRC_ATOP);
                    this.getMenu().getItem(i).setIcon(drawable);
                    //this.updateCartInMenu(DoneApplication.getInstance().getCartCount());
                }else{
                    this.getMenu().getItem(i).getIcon().mutate().setColorFilter(ContextCompat.getColor(this, R.color.colorGrey), PorterDuff.Mode.SRC_IN);
                }
                //menu.getItem(i).getIcon().mutate().setColorFilter(ContextCompat.getColor(this, R.color.colorGrey), PorterDuff.Mode.SRC_IN);
            }
        }

        if(DoneApplication.getInstance()!=null) {
            updateCartInMenu(DoneApplication.getInstance().getCartCount());
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        if(item.getItemId() == R.id.action_cart){
            Intent intent = new Intent(getApplicationContext(), CartActivity.class);
            startActivity(intent);
            return true;
        }else if (item.getItemId() == R.id.action_search){
            Intent intent = new Intent(getApplicationContext(), SearchActivity.class);
            startActivity(intent);
            return true;

        }

        return false;
    }

    @Override
    protected void onDestroy() {
        itemListPresenter.onDestroy();
        super.onDestroy();
    }


    public void updateCartInMenu(int count){
        if(menu!=null) {
            Badger.sett(menu.findItem(R.id.action_cart), circleFactory).setCount(count);
            //new ActionItemBadgeAdder().act(this).menu(menu).itemDetails(0, R.id.action_cart, 1).showAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS).add(ActionItemBadge.BadgeStyles.GREY_LARGE, count);
        }
    }

    public Menu getMenu(){
        return menu;
    }

    @Override
    public void onStart() {
        super.onStart();
        eventBus.register(this);

    }

    @Override
    public void onStop() {
        super.onStop();
        eventBus.unregister(this);
    }


    @Override
    protected void onResume() {
        super.onResume();
        //updateMenuColorToWhite();//for older mobile version
        if(DoneApplication.getInstance()!=null) {
            updateCartInMenu(DoneApplication.getInstance().getCartCount());
        }
    }

    //change color of icons to white in older devices
    private void updateMenuColorToWhite(){
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP && menu!=null) {
            for (int i = 0; i < menu.size(); i++) {
                boolean isCartIcon = (this.menu.getItem(i).getItemId() == R.id.action_cart);
                if(isCartIcon) {
                    Drawable drawable = ContextCompat.getDrawable(this, R.mipmap.ic_toolbar_cart);
                    drawable.setColorFilter(ContextCompat.getColor(this, R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);
                    menu.getItem(i).setIcon(drawable);
                }
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(CartEvent event){
        updateCartInMenu(event.getCartCount());
    }
}
