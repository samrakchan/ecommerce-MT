package com.metrotarkari.app.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.metrotarkari.app.application.network.DoneAuthResponse;

import java.util.Calendar;

/**
 * Created by samrakchan on 5/18/17.
 */

public class DoneTokenPreferenceManager {
    private static final String SHARED_NAME = "DoneTokenPreferenceManager";

    private static final String EXPIRES_IN = "DoneTokenExpiresIn";
    private static final String ACCESS_TOKEN = "AccessToken";
    private static final String REFRESH_TOKEN = "RefreshToken";
    private static final String TOKEN_TYPE = "token_type";
    private static final String PRIVATE_CONTENT_VERSION = "PrivateContentVersion";
    private static final String PHP_SESSION_ID = "PhpSessionId";


    private SharedPreferences mSharedPreferences;

    public DoneTokenPreferenceManager(Context context){
        mSharedPreferences = context.getSharedPreferences(SHARED_NAME, Context.MODE_PRIVATE);
    }

    /**
     *
     * @param privateContentVersion A unique key, that gets generated when splash open for the first time.
     */
    public void setPrivateContentVersion(String privateContentVersion){
        mSharedPreferences.edit().putString(PRIVATE_CONTENT_VERSION, privateContentVersion).apply();
    }

    public String getPrivateContentVersion(){
        return mSharedPreferences.getString(PRIVATE_CONTENT_VERSION,"");
    }

    /**
     *
     * @param phpSessionId A unique key, that gets generated when splash open for the first time.
     */
    public void setPhpSessionId(String phpSessionId){
        mSharedPreferences.edit().putString(PHP_SESSION_ID, phpSessionId).apply();
    }

    public String getPhpSessionId(){
        return mSharedPreferences.getString(PHP_SESSION_ID, "");
    }

    /**
     *
     * @param duration in seconds and converted to milliseconds.
     */
    public void setTokenExpireTimestamp(int duration){
        Calendar timeout = Calendar.getInstance();
        //before 5 seconds
        timeout.setTimeInMillis(System.currentTimeMillis() + 1000*(duration-5));
        mSharedPreferences.edit().putLong(EXPIRES_IN,timeout.getTimeInMillis()).apply();
    }

    /**
     *
     * @return expiresIn milliseconds
     */

    public long getTokenExpireTimestamp(){
        return mSharedPreferences.getLong(EXPIRES_IN, 0);
    }

    public void setAccessToken(String accessToken){
        mSharedPreferences.edit().putString(ACCESS_TOKEN, accessToken).apply();
    }

    public String getAccessToken(){
        return mSharedPreferences.getString(ACCESS_TOKEN, "");
    }


    public void setTokenType(String tokenType){
        mSharedPreferences.edit().putString(TOKEN_TYPE, tokenType).apply();
    }

    public String getTokenType(){
        return mSharedPreferences.getString(TOKEN_TYPE, "");
    }

    public void setRefreshToken(String refreshToken){
        mSharedPreferences.edit().putString(REFRESH_TOKEN, refreshToken).apply();
    }

    public String getRefreshToken(){
        return mSharedPreferences.getString(REFRESH_TOKEN, "");
    }


    public void setDoneAuthResponse(DoneAuthResponse doneAuthResponse){
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(REFRESH_TOKEN, doneAuthResponse.getRefreshToken());
        editor.putString(ACCESS_TOKEN, doneAuthResponse.getAccessToken());
        editor.putLong(EXPIRES_IN, doneAuthResponse.getExpiresIn());
        editor.putString(TOKEN_TYPE, "bearer");

        Log.i("Auth", "Auth refresh token "+doneAuthResponse.getRefreshToken());
        Log.i("Auth", "Auth access token "+doneAuthResponse.getAccessToken());

        editor.apply();
    }

    public void clearDoneAuthTokens(){
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.remove(REFRESH_TOKEN);
        editor.remove(ACCESS_TOKEN);
        editor.remove(EXPIRES_IN);
        editor.remove(TOKEN_TYPE);
        editor.apply();
    }

    public void clearAllData(){
        mSharedPreferences.edit().clear().apply();
    }
}
