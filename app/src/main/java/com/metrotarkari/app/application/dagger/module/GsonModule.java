package com.metrotarkari.app.application.dagger.module;

import com.fatboyindustrial.gsonjodatime.Converters;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.metrotarkari.app.application.dagger.AppScope;
import com.metrotarkari.app.ext.MagentoAdapterFactory;

import dagger.Module;
import dagger.Provides;

@Module
public class GsonModule {

  @AppScope
  @Provides
  public Gson gson() {
    return Converters.registerAll(new GsonBuilder())
        .registerTypeAdapterFactory(MagentoAdapterFactory.create())
        .create();
  }
}
