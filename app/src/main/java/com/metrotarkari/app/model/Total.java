package com.metrotarkari.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by samrakchan on 6/2/17.
 */

public class Total {
    @SerializedName("tax")
    @Expose
    private Double tax;
    @SerializedName("discount")
    @Expose
    private Double discount;
    @SerializedName("subtotal_excl_tax")
    @Expose
    private Double subtotalExclTax;
    @SerializedName("subtotal_incl_tax")
    @Expose
    private Double subtotalInclTax;
    @SerializedName("grand_total_incl_tax")
    @Expose
    private Double grandTotalInclTax;
    @SerializedName("grand_total_excl_tax")
    @Expose
    private Double grandTotalExclTax;

    @SerializedName("formatted_tax")
    @Expose
    private String formattedTax;
    @SerializedName("formatted_subtotal_excl_tax")
    @Expose
    private String formattedSubtotalExclTax;
    @SerializedName("formatted_subtotal_incl_tax")
    @Expose
    private String formattedSubtotalInclTax;
    @SerializedName("formatted_grand_total_incl_tax")
    @Expose
    private String formattedGrandTotalInclTax;
    @SerializedName("formatted_grand_total_excl_tax")
    @Expose
    private String formattedGrandTotalExclTax;

    @SerializedName("formatted_discount")
    @Expose
    private String formattedDiscount;

    @SerializedName("coupon_code")
    @Expose
    private String couponCode;

    @SerializedName("applied_coupon")
    @Expose
    private boolean appliedCoupon;


    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public Double getTax() {
        return tax;
    }

    public void setTax(Double tax) {
        this.tax = tax;
    }

    public Double getSubtotalExclTax() {
        return subtotalExclTax;
    }

    public void setSubtotalExclTax(Double subtotalExclTax) {
        this.subtotalExclTax = subtotalExclTax;
    }

    public Double getSubtotalInclTax() {
        return subtotalInclTax;
    }

    public void setSubtotalInclTax(Double subtotalInclTax) {
        this.subtotalInclTax = subtotalInclTax;
    }

    public Double getGrandTotalInclTax() {
        return grandTotalInclTax;
    }

    public void setGrandTotalInclTax(Double grandTotalInclTax) {
        this.grandTotalInclTax = grandTotalInclTax;
    }

    public Double getGrandTotalExclTax() {
        return grandTotalExclTax;
    }

    public void setGrandTotalExclTax(Double grandTotalExclTax) {
        this.grandTotalExclTax = grandTotalExclTax;
    }

    public String getFormattedTax() {
        return formattedTax;
    }

    public void setFormattedTax(String formattedTax) {
        this.formattedTax = formattedTax;
    }

    public String getFormattedSubtotalExclTax() {
        return formattedSubtotalExclTax;
    }

    public void setFormattedSubtotalExclTax(String formattedSubtotalExclTax) {
        this.formattedSubtotalExclTax = formattedSubtotalExclTax;
    }

    public String getFormattedSubtotalInclTax() {
        return formattedSubtotalInclTax;
    }

    public void setFormattedSubtotalInclTax(String formattedSubtotalInclTax) {
        this.formattedSubtotalInclTax = formattedSubtotalInclTax;
    }

    public String getFormattedGrandTotalInclTax() {
        return formattedGrandTotalInclTax;
    }

    public void setFormattedGrandTotalInclTax(String formattedGrandTotalInclTax) {
        this.formattedGrandTotalInclTax = formattedGrandTotalInclTax;
    }

    public String getFormattedGrandTotalExclTax() {
        return formattedGrandTotalExclTax;
    }

    public void setFormattedGrandTotalExclTax(String formattedGrandTotalExclTax) {
        this.formattedGrandTotalExclTax = formattedGrandTotalExclTax;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public boolean getAppliedCoupon() {
        return appliedCoupon;
    }

    public void setAppliedCoupon(boolean appliedCoupon) {
        this.appliedCoupon = appliedCoupon;
    }

    public String getFormattedDiscount() {
        return formattedDiscount;
    }

    public void setFormattedDiscount(String formattedDiscount) {
        this.formattedDiscount = formattedDiscount;
    }

    public boolean isAppliedCoupon() {
        return appliedCoupon;
    }
}
