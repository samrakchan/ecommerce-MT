package com.metrotarkari.app.ui.list.filter;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metrotarkari.app.R;
import com.metrotarkari.app.generic.OnItemClickListener;
import com.metrotarkari.app.model.LayerFilter;
import com.metrotarkari.app.utils.QueryListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by samrakchan on 7/17/17.
 */

public class DrawerFilterFragment extends Fragment {

    private RecyclerView filterRv;
    private FilterAdapter filterAdapter;
    private List<LayerFilter> layerFilters = new ArrayList<>();
    private QueryListener listener;
    private OnItemClickListener itemClickListener;
    private AppCompatButton applyBtn;
    private View backIv;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getArguments()!=null) {
            layerFilters.addAll(getArguments().getParcelableArrayList("LIST"));
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.drawer_filter_fragment, container, false);
        filterRv = (RecyclerView) view.findViewById(R.id.filterRv);
        applyBtn = (AppCompatButton)view.findViewById(R.id.applyBtn);
        backIv = view.findViewById(R.id.backIv);
        filterRv.setLayoutManager(new LinearLayoutManager(getContext()));
        DividerItemDecoration dividerItemDecorationH = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        dividerItemDecorationH.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.bg_divider));
        filterRv.addItemDecoration(dividerItemDecorationH);
        filterAdapter = new FilterAdapter(getContext(), layerFilters);
        filterRv.setAdapter(filterAdapter);

        applyBtn.setOnClickListener(view1 -> {
            if(listener!=null && filterAdapter!=null){
                listener.onQueryListener(filterAdapter.getFilterQuery());
            }
        });

        backIv.setOnClickListener(view1 ->{
                if(itemClickListener!=null){
                    itemClickListener.setOnItemClickListener(null);
                }
        });



        return view;
    }

    public void setQueryListener(QueryListener queryListener){
        listener = queryListener;
    }

    public void setOnItemClickListener(OnItemClickListener listener){
        itemClickListener = listener;
    }


}
