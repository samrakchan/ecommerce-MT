package com.metrotarkari.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by samrakchan on 6/6/17.
 */

public class HomeCategory {

    @SerializedName("donecategory_id")
    @Expose
    private String donecategoryId;
    @SerializedName("donecategory_name")
    @Expose
    private String donecategoryName;
    @SerializedName("donecategory_filename")
    @Expose
    private String donecategoryFilename;
    @SerializedName("donecategory_filename_tablet")
    @Expose
    private String donecategoryFilenameTablet;
    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("website_id")
    @Expose
    private Object websiteId;
    @SerializedName("storeview_id")
    @Expose
    private Object storeviewId;
    @SerializedName("sort_order")
    @Expose
    private String sortOrder;
    @SerializedName("matrix_width_percent")
    @Expose
    private String matrixWidthPercent;
    @SerializedName("matrix_height_percent")
    @Expose
    private String matrixHeightPercent;
    @SerializedName("matrix_width_percent_tablet")
    @Expose
    private String matrixWidthPercentTablet;
    @SerializedName("matrix_height_percent_tablet")
    @Expose
    private String matrixHeightPercentTablet;
    @SerializedName("matrix_row")
    @Expose
    private String matrixRow;
    @SerializedName("entity_id")
    @Expose
    private String entityId;
    @SerializedName("content_type")
    @Expose
    private String contentType;
    @SerializedName("item_id")
    @Expose
    private String itemId;
    @SerializedName("store_view_id")
    @Expose
    private String storeViewId;
    @SerializedName("width")
    @Expose
    private Integer width;
    @SerializedName("height")
    @Expose
    private Integer height;
    @SerializedName("width_tablet")
    @Expose
    private Integer widthTablet;
    @SerializedName("height_tablet")
    @Expose
    private Integer heightTablet;
    @SerializedName("cat_name")
    @Expose
    private String catName;
    @SerializedName("has_children")
    @Expose
    private Boolean hasChildren;
    @SerializedName("product_array")
    @Expose
    private ProductArray productArray;

    public String getDonecategoryId() {
        return donecategoryId;
    }

    public void setDonecategoryId(String donecategoryId) {
        this.donecategoryId = donecategoryId;
    }

    public String getDonecategoryName() {
        return donecategoryName;
    }

    public void setDonecategoryName(String donecategoryName) {
        this.donecategoryName = donecategoryName;
    }

    public String getDonecategoryFilename() {
        return donecategoryFilename;
    }

    public void setDonecategoryFilename(String donecategoryFilename) {
        this.donecategoryFilename = donecategoryFilename;
    }

    public String getDonecategoryFilenameTablet() {
        return donecategoryFilenameTablet;
    }

    public void setDonecategoryFilenameTablet(String donecategoryFilenameTablet) {
        this.donecategoryFilenameTablet = donecategoryFilenameTablet;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Object getWebsiteId() {
        return websiteId;
    }

    public void setWebsiteId(Object websiteId) {
        this.websiteId = websiteId;
    }

    public Object getStoreviewId() {
        return storeviewId;
    }

    public void setStoreviewId(Object storeviewId) {
        this.storeviewId = storeviewId;
    }

    public String getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }

    public String getMatrixWidthPercent() {
        return matrixWidthPercent;
    }

    public void setMatrixWidthPercent(String matrixWidthPercent) {
        this.matrixWidthPercent = matrixWidthPercent;
    }

    public String getMatrixHeightPercent() {
        return matrixHeightPercent;
    }

    public void setMatrixHeightPercent(String matrixHeightPercent) {
        this.matrixHeightPercent = matrixHeightPercent;
    }

    public String getMatrixWidthPercentTablet() {
        return matrixWidthPercentTablet;
    }

    public void setMatrixWidthPercentTablet(String matrixWidthPercentTablet) {
        this.matrixWidthPercentTablet = matrixWidthPercentTablet;
    }

    public String getMatrixHeightPercentTablet() {
        return matrixHeightPercentTablet;
    }

    public void setMatrixHeightPercentTablet(String matrixHeightPercentTablet) {
        this.matrixHeightPercentTablet = matrixHeightPercentTablet;
    }

    public String getMatrixRow() {
        return matrixRow;
    }

    public void setMatrixRow(String matrixRow) {
        this.matrixRow = matrixRow;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getStoreViewId() {
        return storeViewId;
    }

    public void setStoreViewId(String storeViewId) {
        this.storeViewId = storeViewId;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getWidthTablet() {
        return widthTablet;
    }

    public void setWidthTablet(Integer widthTablet) {
        this.widthTablet = widthTablet;
    }

    public Integer getHeightTablet() {
        return heightTablet;
    }

    public void setHeightTablet(Integer heightTablet) {
        this.heightTablet = heightTablet;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public Boolean getHasChildren() {
        return hasChildren;
    }

    public void setHasChildren(Boolean hasChildren) {
        this.hasChildren = hasChildren;
    }

    public ProductArray getProductArray() {
        return productArray;
    }

    public void setProductArray(ProductArray productArray) {
        this.productArray = productArray;
    }
}
