package com.metrotarkari.app.ui.detail.mvp;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metrotarkari.app.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by samrakchan on 8/18/17.
 */

public class TierPriceAdapter extends RecyclerView.Adapter<TierPriceAdapter.NormalTirePriceViewHolder> {

    private List<String> mData;
    private Context mContext;

    public TierPriceAdapter(Context mContext) {
        this.mContext = mContext;
        mData = new ArrayList<>();
    }

    public void addAllData(List<String> objects){
        mData.addAll(objects);
        notifyDataSetChanged();
    }

    @Override
    public TierPriceAdapter.NormalTirePriceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_detail_tier_price, parent, false);
        return new NormalTirePriceViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TierPriceAdapter.NormalTirePriceViewHolder holder, int position) {
        holder.adapterTierPriceTv.setText(mData.get(position));
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    protected class NormalTirePriceViewHolder extends RecyclerView.ViewHolder{
        private AppCompatTextView adapterTierPriceTv;

        public NormalTirePriceViewHolder(View itemView) {
            super(itemView);
            adapterTierPriceTv = (AppCompatTextView) itemView.findViewById(R.id.adapterTierPriceTv);
        }
    }
}
