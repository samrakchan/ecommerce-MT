package com.metrotarkari.app.model;

import com.google.auto.value.AutoValue;
import com.google.gson.TypeAdapter;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;
import com.google.gson.Gson;

@Generated("com.robohorse.robopojogenerator")
@AutoValue
public abstract class AuthToken extends ErrorResponse{

	@SerializedName("access_token")
	public abstract String accessToken();

	@SerializedName("refresh_token")
	public abstract String refreshToken();

	@SerializedName("expire_time")
	public abstract int expireTime();

	@SerializedName("timestamp")
	public abstract int timestamp();



	public static TypeAdapter<AuthToken> typeAdapter(Gson gson) {
		return new AutoValue_AuthToken.GsonTypeAdapter(gson);
	}
}