package com.metrotarkari.app.ui.reviewandratings.reviewpost.mvp;

import android.support.v7.app.AppCompatActivity;

import com.metrotarkari.app.application.network.DoneNetwork;
import com.metrotarkari.app.ui.reviewandratings.reviewpost.model.PostReview;
import com.metrotarkari.app.ui.reviewandratings.reviewpost.model.PostReviewResponse;

import java.util.HashMap;

import io.reactivex.Observable;

/**
 * Created by Avinash on 8/8/17.
 */

public class ReviewPostModel {
    private final AppCompatActivity activity;
    private final DoneNetwork doneNetwork;

    public ReviewPostModel(AppCompatActivity activity, DoneNetwork doneNetwork){
        this.activity = activity;
        this.doneNetwork = doneNetwork;
    }

    public Observable<PostReviewResponse> postReview(HashMap<String, Object> postReview){
        return doneNetwork.postReview(postReview);
    }

}
