package com.metrotarkari.app.ext;

public final class Constants {

  private Constants() {
  }

  public static final String HTTP_CACHE_DIR = "magento_cache";
  public static final String LOGTAG = "MagentoApp";
  public static final int HTTP_CACHE_SIZE = 10 * 1024 * 1024; //10MB

    //Access Token
    public static final String ACCESS_TOKEN ="at";
    public static final String GCM_TOKEN = "gt";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String REFRESH_TOKEN = "rt";
}
