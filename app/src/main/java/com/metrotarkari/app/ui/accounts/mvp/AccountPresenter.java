package com.metrotarkari.app.ui.accounts.mvp;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import com.metrotarkari.app.R;
import com.metrotarkari.app.db.DoneDbAdapter;
import com.metrotarkari.app.model.Error;
import com.metrotarkari.app.ui.addressbook.addresslist.mvp.AddressListActivity;
import com.metrotarkari.app.ui.login.LoginActivity2;
import com.metrotarkari.app.ui.splash.SplashActivity;
import com.metrotarkari.app.utils.Constant;
import com.metrotarkari.app.utils.DoneTokenPreferenceManager;
import com.metrotarkari.app.utils.DoneUserPreferenceManager;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by samrakchan on 4/27/17.
 */

public class AccountPresenter {
    private final AccountView view;
    private final AccountModel model;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private DoneUserPreferenceManager doneUserPreferenceManager;
    private DoneTokenPreferenceManager tokenPreferenceManager;

    public AccountPresenter(AccountView view, AccountModel model, DoneUserPreferenceManager preferenceManager){
        this.view = view;
        this.model = model;
        this.doneUserPreferenceManager = preferenceManager;
        this.tokenPreferenceManager = new DoneTokenPreferenceManager(view.getContext());

        compositeDisposable.add(view.logoutClickObservable().subscribe(object->{
            if (doneUserPreferenceManager.isLoggedInUser()) {
                AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext())
                        .setMessage(R.string.logout_confirmation_message)
                        .setCancelable(true)
                        .setPositiveButton(R.string.ok, (dialogInterface, i)->{
                            doLogout();
                        })
                        .setNegativeButton(R.string.cancel, ((dialogInterface, i) -> {
                            dialogInterface.dismiss();
                        }));
                builder.create();
                builder.show();
            }else {
                Intent intent = new Intent(view.getContext(), LoginActivity2.class);
                view.getContext().startActivity(intent);
            }
        }));

        compositeDisposable.add(view.addressBookClickObservable().subscribe(object->{
            Intent intent = new Intent(view.getContext(), AddressListActivity.class);
            intent.putExtra(Constant.INTENT_FOR, Constant.ADDRESS_LISTING_ONLY);
            view.getContext().startActivity(intent);
        }));
    }

    private Disposable doLogout(){
        view.showLoading(true);
        return model.getLogout()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(logoutResponse -> {

                    view.showLoading(false);
                    List<Object> customer  = logoutResponse.getCustomer();
                    List<Error> errorResponse = logoutResponse.getErrors();

                    if(errorResponse!=null && !errorResponse.isEmpty()){
                        Error error = errorResponse.get(0);
                        Toast.makeText(view.getContext(),error.getMessage()+"",Toast.LENGTH_LONG).show();
                    }else if (customer.isEmpty() && errorResponse==null){
                            Intent intent = new Intent(view.getContext(), SplashActivity.class);
                            view.getContext().startActivity(intent);
                            doneUserPreferenceManager.clearAllData();
                            tokenPreferenceManager.clearAllData();
                            view.getContext().deleteDatabase(DoneDbAdapter.DB_NAME);
                            view.finishActivity();
                    }
                }, throwable -> {
                    view.showLoading(false);
                    Toast.makeText(view.getContext(),R.string.sww,Toast.LENGTH_LONG).show();
                    throwable.printStackTrace();
                });
    }


    public void onDestroy(){
        if (compositeDisposable!=null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }
}
