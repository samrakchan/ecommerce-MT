package com.metrotarkari.app.search;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;

import com.claudiodegio.msv.FilterMaterialSearchView;
import com.metrotarkari.app.R;
import com.metrotarkari.app.db.DoneDbAdapter;
import com.metrotarkari.app.ui.list.ItemListFragment;
import com.metrotarkari.app.ui.recentsearch.RecentSearchFragment;
import com.metrotarkari.app.ui.recentsearch.model.RecentSearch;
import com.metrotarkari.app.utils.DoneItemClickListener;

/**
 * Created by samrakchan on 4/13/17.
 */

public class SearchActivity extends AppCompatActivity implements View.OnClickListener,DoneItemClickListener {
    private FilterMaterialSearchView cast;

    private AppCompatImageView backBtn;

    private AppCompatEditText searchEt;

    private String recentSearchName;

    private DoneDbAdapter doneDbAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        backBtn = (AppCompatImageView)findViewById(R.id.backIconIv);
        //backBtn.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.abc_ic_ab_back_material));

        backBtn.setOnClickListener(this);


        doneDbAdapter = new DoneDbAdapter(getApplicationContext());

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        RecentSearchFragment recentSearchFragment = new RecentSearchFragment();

        fragmentTransaction.replace(R.id.frameLayout,recentSearchFragment).commit();


        searchEt = (AppCompatEditText)findViewById(R.id.searchEt);
        searchEt.setOnEditorActionListener((textView, actionId, event) -> {
            if (actionId != 0 || event.getAction() == KeyEvent.ACTION_DOWN) {
                FragmentTransaction fragmentTransaction1 = fragmentManager.beginTransaction();
                recentSearchName = searchEt.getText().toString().trim();
                if(!recentSearchName.equals("")) {
                    addSearchNameToDatabase(recentSearchName);
                }
                ItemListFragment listFragment = new ItemListFragment();
                Bundle bundle = new Bundle();
                bundle.putString("SEARCH",textView.getText().toString().trim());
                listFragment.setArguments(bundle);
                fragmentTransaction1.replace(R.id.frameLayout, listFragment).commit();

                return true;
            } else {
                return false;
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                supportFinishAfterTransition();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        finish();
    }


    public void addSearchNameToDatabase(String recentSearchName){
        doneDbAdapter.openReadableDb();
        doneDbAdapter.insertInToResentSearch(recentSearchName);
        doneDbAdapter.closeDb();
    }

    @Override
    public void onItemClickListener(int position, int viewId, Object object) {
        RecentSearch recentSearch = (RecentSearch) object;
        searchEt.setText(recentSearch.getRecentSearchItemName());
        searchEt.setSelection(searchEt.getText().length());
    }
}
