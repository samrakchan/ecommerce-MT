package com.metrotarkari.app.ui.accounts;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metrotarkari.app.application.DoneApplication;
import com.metrotarkari.app.ui.accounts.dragger.AccountModule;
import com.metrotarkari.app.ui.accounts.dragger.DaggerAccountComponent;
import com.metrotarkari.app.ui.accounts.mvp.AccountPresenter;
import com.metrotarkari.app.ui.accounts.mvp.AccountView;

import javax.inject.Inject;

/**
 * Created by samrakchan on 4/18/17.
 */

public class AccountsFragment extends Fragment {
    @Inject
    AccountView view;

    @Inject
    AccountPresenter presenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setRetainInstance(true);
        DaggerAccountComponent.builder().appComponent(DoneApplication.get(this).component())
                .accountModule(new AccountModule(this))

                .build().inject(this);

        return view;
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }
}

