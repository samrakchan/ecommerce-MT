package com.metrotarkari.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by samrakchan on 6/6/17.
 */

public class Layers {

    @SerializedName("layer_filter")
    @Expose
    private List<LayerFilter> layerFilter = null;

    public List<LayerFilter> getLayerFilter() {
        return layerFilter;
    }

    public void setLayerFilter(List<LayerFilter> layerFilter) {
        this.layerFilter = layerFilter;
    }
}
