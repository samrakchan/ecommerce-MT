package com.metrotarkari.app.ui.order.orderdetail;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;

import com.metrotarkari.app.R;
import com.metrotarkari.app.ui.order.orderdetail.model.OrderHeader;
import com.metrotarkari.app.ui.order.orderdetail.model.OrderI;
import com.metrotarkari.app.ui.order.orderdetail.model.OrderItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Avinash on 6/7/2017.
 */

public class OrderDetailAdapter extends BaseExpandableListAdapter {
    private Context context;
    private ArrayList<OrderHeader> orders;
    private HashMap<OrderHeader,Object> orderItemListDetail;

    private static final int VIEW_TYPE_NORMAL_CHILD = 1;
    private static final int VIEW_TYPE_RECYCLER_VIEW_CHILD_FOR_PRODUCT_DETAIL = 2;

    public OrderDetailAdapter(Context context) {
        this.context = context;
        this.orders = new ArrayList<>();
        this.orderItemListDetail = new HashMap<>();
    }

    public void addItems(List<OrderHeader> order, HashMap<OrderHeader,Object> orderItems ){
        orders.clear();
        this.orders.addAll(order);
        orderItemListDetail.clear();
        orderItemListDetail.putAll(orderItems);
        notifyDataSetChanged();
    }

    @Override
    public int getGroupCount() {
        return this.orders.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
       return  ((ArrayList<Object>) this.orderItemListDetail.get(orders.get(groupPosition))).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.orders.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        OrderHeader orderHeader = orders.get(groupPosition);
        return ((List<Object>) this.orderItemListDetail.get(orderHeader)).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        OrderHeader order = (OrderHeader) getGroup(groupPosition);

        LayoutInflater infalInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.adapter_order_header, null, false);
        AppCompatTextView adapterOrderItemDetailHeaderTv = (AppCompatTextView) convertView.findViewById(R.id.adapterOrderItemDetailHeaderTv);
        adapterOrderItemDetailHeaderTv.setText(order.getTitle());
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

       final Object object = getChild(groupPosition,childPosition);
        if(object == null){
            Log.i("TAG", object+" null");
        }

      if (object instanceof String){
          final String orderItemDetailString = (String) object;

            LayoutInflater infalInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.adapter_order_item, null, false);

            AppCompatTextView adapterOrderItemDetailInfoTv = (AppCompatTextView) convertView.findViewById(R.id.adapterOrderItemDetailInfoTv);
            adapterOrderItemDetailInfoTv.setText(orderItemDetailString);

        return convertView;

      }else{
          OrderI productLines = (OrderI) object;

          LayoutInflater infalInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
          convertView = infalInflater.inflate(R.layout.order_detail_child_recycler_view_type, null, false);

          RecyclerView orderItemProductDetailRv = (RecyclerView) convertView.findViewById(R.id.orderItemProductDetailRv);
          orderItemProductDetailRv.setNestedScrollingEnabled(false);
          OrderDetailChildRecyclerViewAdapter orderDetailChildRecyclerViewAdapter = new OrderDetailChildRecyclerViewAdapter(context);
          orderItemProductDetailRv.setLayoutManager(new LinearLayoutManager(context));
          orderDetailChildRecyclerViewAdapter.addProductLineData(productLines.getOrderItems());
          orderItemProductDetailRv.setAdapter(orderDetailChildRecyclerViewAdapter);

          return convertView;
      }
    }


    @Override
    public int getChildType(int groupPosition, int childPosition) {
        Object object = orderItemListDetail.get(childPosition);
        if (object instanceof String){
            return VIEW_TYPE_NORMAL_CHILD;

        }else if (object instanceof OrderItem){
            return VIEW_TYPE_RECYCLER_VIEW_CHILD_FOR_PRODUCT_DETAIL;

        }
        return -1;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
}
