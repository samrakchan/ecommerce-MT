package com.metrotarkari.app.ui.recentsearch;

import android.content.Context;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metrotarkari.app.R;
import com.metrotarkari.app.db.DoneDbAdapter;
import com.metrotarkari.app.ui.list.ItemListFragment;
import com.metrotarkari.app.ui.recentsearch.model.RecentSearch;
import com.metrotarkari.app.utils.DoneItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Avinash on 7/6/2017.
 */

public class RecentSearchFragment extends Fragment implements DoneItemClickListener {

    @BindView(R.id.recentSearchRv)
    public RecyclerView recentSearchRv;

    @BindView(R.id.imageMsgRl)
    public View noItemView;

    @BindView(R.id.imageMsgNoItemIv)
    protected AppCompatImageView imageMsgNoItemIv;

    @BindView(R.id.imageMsgNoItemTv)
    protected AppCompatTextView imageMsgNoItemTv;

    private RecentSearchAdapter adapter ;

    private DoneDbAdapter doneDbAdapter;

    @BindView(R.id.searchHeadingRl)
    View searchHeadingRl;


    DoneItemClickListener searchAdapterListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recent_search,container,false);

        ButterKnife.bind(this,view);

        doneDbAdapter = new DoneDbAdapter(getContext());

        imageMsgNoItemIv.setImageResource(R.mipmap.ic_search_melon);
        imageMsgNoItemTv.setText("");

        ColorMatrix matrix = new ColorMatrix();
        matrix.setSaturation(0);  //0 means grayscale
        ColorMatrixColorFilter cf = new ColorMatrixColorFilter(matrix);
        imageMsgNoItemIv.setColorFilter(cf);
        imageMsgNoItemIv.setImageAlpha(128);

        DividerItemDecoration diV = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        diV.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.bg_divider));

        DividerItemDecoration diH = new DividerItemDecoration(getContext(), DividerItemDecoration.HORIZONTAL);
        diH.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.bg_divider));

        recentSearchRv.addItemDecoration(diV);
        recentSearchRv.addItemDecoration(diH);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);

        recentSearchRv.setLayoutManager(linearLayoutManager);

        adapter = new RecentSearchAdapter(getContext(),this);
        addSearchData();
        recentSearchRv.setAdapter(adapter);


        return view;
    }

    public void addSearchData(){
        //fetching recent search data from database
        doneDbAdapter.openWritableDb();
        List<RecentSearch> recentSearchList = new ArrayList<>();
        recentSearchList.addAll(doneDbAdapter.getRecentSearchData());
        if (recentSearchList.size()<10) {
            if (recentSearchList != null && !recentSearchList.isEmpty()) {
                adapter.addRecentSearchData(recentSearchList);
            } else {
                searchHeadingRl.setVisibility(View.INVISIBLE);
                showNoItem(true);
            }
        }else {
            doneDbAdapter.deleteTheExtraData(1);
            addSearchData();
        }
        doneDbAdapter.closeDb();

    }

    @Override
    public void onItemClickListener(int position, int viewId, Object object) {
        RecentSearch recentSearch = (RecentSearch) object;

        if (viewId == R.id.adapterSearchDeleteIv){
            //deleting the selected search iten data
            doneDbAdapter.openReadableDb();
            doneDbAdapter.deleteRecentSearchData(recentSearch.getRecentSearchItemName());
            doneDbAdapter.closeDb();
            addSearchData();
        }else {
            //passing the selected search name from list to search bar to search the selected item
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            ItemListFragment listFragment = new ItemListFragment();
            Bundle bundle = new Bundle();
            bundle.putString("SEARCH", recentSearch.getRecentSearchItemName());
            listFragment.setArguments(bundle);
            fragmentTransaction.replace(R.id.frameLayout, listFragment).commit();
            searchAdapterListener.onItemClickListener(position,viewId,recentSearch);
        }
    }

    public void showNoItem(boolean showItem){
        if(showItem) {
            noItemView.setVisibility(View.VISIBLE);
            imageMsgNoItemTv.setText(R.string.no_search_item_found);
        }
        else {
            noItemView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            searchAdapterListener = (DoneItemClickListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement onSomeEventListener");
        }
    }
}
