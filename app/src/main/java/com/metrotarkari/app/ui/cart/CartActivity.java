package com.metrotarkari.app.ui.cart;

import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.widget.Toast;

import com.metrotarkari.app.R;
import com.metrotarkari.app.application.DoneApplication;
import com.metrotarkari.app.event.CartEvent;
import com.metrotarkari.app.ui.base.BaseActivity;
import com.metrotarkari.app.ui.cart.dagger.CartModule;
import com.metrotarkari.app.ui.cart.dagger.DaggerCartComponent;
import com.metrotarkari.app.ui.cart.mvp.CartPresenter;
import com.metrotarkari.app.ui.cart.mvp.CartView;
import com.metrotarkari.app.utils.Constant;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;

import berlin.volders.badger.BadgeShape;
import berlin.volders.badger.Badger;
import berlin.volders.badger.CountBadge;

/**
 * Created by samrakchan on 3/18/17.
 */

public class CartActivity extends BaseActivity{

    @Inject
    CartView itemListView;

    @Inject
    CartPresenter itemListPresenter;

    private Menu menu;

    private EventBus eventBus = EventBus.getDefault();

    private CountBadge.Factory circleFactory;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        circleFactory = new CountBadge.Factory(this, BadgeShape.circle(.5f, Gravity.END | Gravity.TOP));

        DaggerCartComponent.builder().appComponent(DoneApplication.get(this).component())
                .cartModule(new CartModule(this)).build().inject(this);

        setContentView(itemListView);
    }


    @Override
    protected void onDestroy() {
        if(itemListPresenter!=null){
            itemListPresenter.destroy();
        }
        super.onDestroy();
    }

    @Override
    public void onStart() {
        super.onStart();
        eventBus.register(this);

    }

    @Override
    public void onStop() {
        super.onStop();
        eventBus.unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(DoneApplication.getInstance()!=null) {
            updateCartInMenu(DoneApplication.getInstance().getCartCount());
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(CartEvent event){
        updateCartInMenu(event.getCartCount());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_home, menu);
        this.menu = menu;

        if(DoneApplication.getInstance()!=null) {
            updateMenuColorToWhite();
            updateCartInMenu(DoneApplication.getInstance().getCartCount());
        }
        return true;
    }

    public void updateCartInMenu(int count){
        if(menu!=null) {
            Badger.sett(menu.findItem(R.id.action_cart), circleFactory).setCount(count);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constant.CHECKOUT){
            if (resultCode == Activity.RESULT_OK){
                Toast.makeText(getApplicationContext(), R.string.can_proceed_to_checkout,Toast.LENGTH_LONG).show();
            }else{

            }
        }
    }

    //change color of icons to white
    private void updateMenuColorToWhite(){
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP && menu!=null) {
            for (int i = 0; i < menu.size(); i++) {
                boolean isCartIcon = (this.menu.getItem(i).getItemId() == R.id.action_cart);
                if(isCartIcon) {
                    Drawable drawable = ContextCompat.getDrawable(this, R.mipmap.ic_toolbar_cart);
                    drawable.setColorFilter(ContextCompat.getColor(this, R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);
                    menu.getItem(i).setIcon(drawable);
                }
            }
        }
    }
}
