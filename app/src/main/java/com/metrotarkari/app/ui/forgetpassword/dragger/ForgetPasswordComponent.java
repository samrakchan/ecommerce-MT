package com.metrotarkari.app.ui.forgetpassword.dragger;

import com.metrotarkari.app.application.dagger.AppComponent;
import com.metrotarkari.app.ui.forgetpassword.ForgetPasswordActivity;

import dagger.Component;

@ForgetPasswordScope
@Component(modules = { ForgetPasswordModule.class }, dependencies = AppComponent.class)
public interface ForgetPasswordComponent {

  void inject(ForgetPasswordActivity activity);

}
