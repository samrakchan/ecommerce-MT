package com.metrotarkari.app.ui.base.listeners;

/**
 * Created by samrakchan on 4/23/17.
 */

public interface ErrorView {

    void showError(String errorTitle, String errorMessage);

    void hideError();

}
