package com.metrotarkari.app.application.dagger.module;

import android.content.Context;

import com.metrotarkari.app.application.dagger.AppScope;
import com.metrotarkari.app.db.DoneDbAdapter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by samrakchan on 6/13/17.
 */

@Module
public class DbModule {

    @AppScope
    @Provides
    public DoneDbAdapter getDb(Context context) {
        return new DoneDbAdapter(context);
    }

}
