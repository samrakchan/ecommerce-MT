package com.metrotarkari.app.ui.cart.dagger;

import com.metrotarkari.app.application.dagger.AppComponent;
import com.metrotarkari.app.ui.cart.CartActivity;

import dagger.Component;

@CartScope
@Component(modules = { CartModule.class }, dependencies = AppComponent.class)
public interface CartComponent {

  void inject(CartActivity activity);

}
