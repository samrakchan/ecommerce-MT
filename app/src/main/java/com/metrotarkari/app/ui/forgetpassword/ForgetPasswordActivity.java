package com.metrotarkari.app.ui.forgetpassword;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.crashlytics.android.Crashlytics;
import com.metrotarkari.app.application.DoneApplication;
import com.metrotarkari.app.ui.forgetpassword.dragger.DaggerForgetPasswordComponent;
import com.metrotarkari.app.ui.forgetpassword.dragger.ForgetPasswordModule;
import com.metrotarkari.app.ui.forgetpassword.mvp.ForgetPasswordPresenter;
import com.metrotarkari.app.ui.forgetpassword.mvp.ForgetPasswordView;
import com.metrotarkari.app.utils.Constant;

import java.util.HashMap;

import javax.inject.Inject;

import io.fabric.sdk.android.Fabric;

/**
 * Created by Avinash on 6/16/2017.
 */

public class ForgetPasswordActivity extends AppCompatActivity {

    @Inject
    ForgetPasswordView view;

    @Inject
    ForgetPasswordPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());

        DaggerForgetPasswordComponent.builder()
                .appComponent(DoneApplication.get(this).component())
                .forgetPasswordModule(new ForgetPasswordModule(this))
                .build().inject(this);

        setContentView(view);

        Intent intent = getIntent();
        HashMap<String, String> hashMap = (HashMap<String, String>)intent.getSerializableExtra(Constant.DATA);

        if(hashMap!=null){
            presenter.verifyIdToken(hashMap);
        }else {
            presenter.initForgetPassword();
        }
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

}

