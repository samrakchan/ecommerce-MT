package com.metrotarkari.app.ui.notifications.mvp;

import android.support.v7.app.AppCompatActivity;

import com.metrotarkari.app.application.network.DoneNetwork;


/**
 * Created by samrakchan on 4/30/17.
 */

public class NotificationModel {
    private final AppCompatActivity activity;
    private DoneNetwork network;

    public NotificationModel(AppCompatActivity activity, DoneNetwork network){
        this.activity = activity;
        this.network = network;
    }

}
