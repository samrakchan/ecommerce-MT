package com.metrotarkari.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by samrakchan on 6/2/17.
 */

public class Quoteitem {

    @SerializedName("item_id")
    @Expose
    private String itemId;
    @SerializedName("quote_id")
    @Expose
    private String quoteId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("store_id")
    @Expose
    private Integer storeId;
    @SerializedName("parent_item_id")
    @Expose
    private Object parentItemId;
    @SerializedName("is_virtual")
    @Expose
    private Boolean isVirtual;
    @SerializedName("sku")
    @Expose
    private String sku;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private Object description;
    @SerializedName("applied_rule_ids")
    @Expose
    private String appliedRuleIds;
    @SerializedName("additional_data")
    @Expose
    private Object additionalData;
    @SerializedName("is_qty_decimal")
    @Expose
    private Boolean isQtyDecimal;
    @SerializedName("no_discount")
    @Expose
    private String noDiscount;
    @SerializedName("weight")
    @Expose
    private Object weight;
    @SerializedName("qty")
    @Expose
    private Integer qty;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("base_price")
    @Expose
    private Double basePrice;
    @SerializedName("custom_price")
    @Expose
    private Double customPrice;
    @SerializedName("discount_percent")
    @Expose
    private Double discountPercent;
    @SerializedName("discount_amount")
    @Expose
    private Double discountAmount;
    @SerializedName("base_discount_amount")
    @Expose
    private Double baseDiscountAmount;
    @SerializedName("tax_percent")
    @Expose
    private Double taxPercent;
    @SerializedName("tax_amount")
    @Expose
    private Double taxAmount;
    @SerializedName("base_tax_amount")
    @Expose
    private Double baseTaxAmount;
    @SerializedName("row_total")
    @Expose
    private Double rowTotal;
    @SerializedName("base_row_total")
    @Expose
    private Double baseRowTotal;
    @SerializedName("formatted_base_row_total")
    @Expose
    private String formattedBaseRowTotal;
    @SerializedName("row_total_with_discount")
    @Expose
    private String rowTotalWithDiscount;
    @SerializedName("row_weight")
    @Expose
    private Integer rowWeight;
    @SerializedName("product_type")
    @Expose
    private String productType;
    @SerializedName("base_tax_before_discount")
    @Expose
    private Object baseTaxBeforeDiscount;
    @SerializedName("tax_before_discount")
    @Expose
    private Object taxBeforeDiscount;
    @SerializedName("original_custom_price")
    @Expose
    private Object originalCustomPrice;
    @SerializedName("redirect_url")
    @Expose
    private Object redirectUrl;
    @SerializedName("base_cost")
    @Expose
    private Object baseCost;
    @SerializedName("price_incl_tax")
    @Expose
    private Double priceInclTax;
    @SerializedName("base_price_incl_tax")
    @Expose
    private Double basePriceInclTax;
    @SerializedName("row_total_incl_tax")
    @Expose
    private Double rowTotalInclTax;
    @SerializedName("base_row_total_incl_tax")
    @Expose
    private Double baseRowTotalInclTax;
    @SerializedName("discount_tax_compensation_amount")
    @Expose
    private Double discountTaxCompensationAmount;
    @SerializedName("base_discount_tax_compensation_amount")
    @Expose
    private Double baseDiscountTaxCompensationAmount;
    @SerializedName("free_shipping")
    @Expose
    private Boolean freeShipping;
    @SerializedName("gift_message_id")
    @Expose
    private Object giftMessageId;
    @SerializedName("weee_tax_applied")
    @Expose
    private Object weeeTaxApplied;
    @SerializedName("weee_tax_applied_amount")
    @Expose
    private Object weeeTaxAppliedAmount;
    @SerializedName("weee_tax_applied_row_amount")
    @Expose
    private Object weeeTaxAppliedRowAmount;
    @SerializedName("weee_tax_disposition")
    @Expose
    private Object weeeTaxDisposition;
    @SerializedName("weee_tax_row_disposition")
    @Expose
    private Object weeeTaxRowDisposition;
    @SerializedName("base_weee_tax_applied_amount")
    @Expose
    private Object baseWeeeTaxAppliedAmount;
    @SerializedName("base_weee_tax_applied_row_amnt")
    @Expose
    private Object baseWeeeTaxAppliedRowAmnt;
    @SerializedName("base_weee_tax_disposition")
    @Expose
    private Object baseWeeeTaxDisposition;
    @SerializedName("base_weee_tax_row_disposition")
    @Expose
    private Object baseWeeeTaxRowDisposition;
    @SerializedName("qty_options")
    @Expose
    private List<Object> qtyOptions = null;
    @SerializedName("product")
    @Expose
    private ProductDetail product;
    @SerializedName("tax_class_id")
    @Expose
    private String taxClassId;
    @SerializedName("has_error")
    @Expose
    private Boolean hasError;
    @SerializedName("qty_to_add")
    @Expose
    private Integer qtyToAdd;
    @SerializedName("calculation_price")
    @Expose
    private Object calculationPrice;
    @SerializedName("converted_price")
    @Expose
    private Double convertedPrice;
    @SerializedName("base_original_price")
    @Expose
    private String baseOriginalPrice;
    @SerializedName("base_calculation_price")
    @Expose
    private Double baseCalculationPrice;
    @SerializedName("tax_calculation_item_id")
    @Expose
    private String taxCalculationItemId;
    @SerializedName("tax_calculation_price")
    @Expose
    private Double taxCalculationPrice;
    @SerializedName("base_tax_calculation_price")
    @Expose
    private Double baseTaxCalculationPrice;
    @SerializedName("discount_calculation_price")
    @Expose
    private Double discountCalculationPrice;
    @SerializedName("base_discount_calculation_price")
    @Expose
    private Double baseDiscountCalculationPrice;
    @SerializedName("applied_taxes")
    @Expose
    private List<Object> appliedTaxes = null;
    @SerializedName("option")
    @Expose
    private List<Object> option = null;
    @SerializedName("image")
    @Expose
    private String image;

    private int itemQtyChangedTo = 0;

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getQuoteId() {
        return quoteId;
    }

    public void setQuoteId(String quoteId) {
        this.quoteId = quoteId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Integer getStoreId() {
        return storeId;
    }

    public void setStoreId(Integer storeId) {
        this.storeId = storeId;
    }

    public Object getParentItemId() {
        return parentItemId;
    }

    public void setParentItemId(Object parentItemId) {
        this.parentItemId = parentItemId;
    }

    public Boolean getIsVirtual() {
        return isVirtual;
    }

    public void setIsVirtual(Boolean isVirtual) {
        this.isVirtual = isVirtual;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getDescription() {
        return description;
    }

    public void setDescription(Object description) {
        this.description = description;
    }

    public String getAppliedRuleIds() {
        return appliedRuleIds;
    }

    public void setAppliedRuleIds(String appliedRuleIds) {
        this.appliedRuleIds = appliedRuleIds;
    }

    public Object getAdditionalData() {
        return additionalData;
    }

    public void setAdditionalData(Object additionalData) {
        this.additionalData = additionalData;
    }

    public Boolean getIsQtyDecimal() {
        return isQtyDecimal;
    }

    public void setIsQtyDecimal(Boolean isQtyDecimal) {
        this.isQtyDecimal = isQtyDecimal;
    }

    public String getNoDiscount() {
        return noDiscount;
    }

    public void setNoDiscount(String noDiscount) {
        this.noDiscount = noDiscount;
    }

    public Object getWeight() {
        return weight;
    }

    public void setWeight(Object weight) {
        this.weight = weight;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Double getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(Double basePrice) {
        this.basePrice = basePrice;
    }

    public Object getCustomPrice() {
        return customPrice;
    }

    public void setCustomPrice(Double customPrice) {
        this.customPrice = customPrice;
    }

    public Double getDiscountPercent() {
        return discountPercent;
    }

    public void setDiscountPercent(Double discountPercent) {
        this.discountPercent = discountPercent;
    }

    public Double getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(Double discountAmount) {
        this.discountAmount = discountAmount;
    }

    public Double getBaseDiscountAmount() {
        return baseDiscountAmount;
    }

    public void setBaseDiscountAmount(Double baseDiscountAmount) {
        this.baseDiscountAmount = baseDiscountAmount;
    }

    public Double getTaxPercent() {
        return taxPercent;
    }

    public void setTaxPercent(Double taxPercent) {
        this.taxPercent = taxPercent;
    }

    public Double getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(Double taxAmount) {
        this.taxAmount = taxAmount;
    }

    public Double getBaseTaxAmount() {
        return baseTaxAmount;
    }

    public void setBaseTaxAmount(Double baseTaxAmount) {
        this.baseTaxAmount = baseTaxAmount;
    }

    public Double getRowTotal() {
        return rowTotal;
    }

    public void setRowTotal(Double rowTotal) {
        this.rowTotal = rowTotal;
    }

    public Double getBaseRowTotal() {
        return baseRowTotal;
    }

    public void setBaseRowTotal(Double baseRowTotal) {
        this.baseRowTotal = baseRowTotal;
    }

    public String getRowTotalWithDiscount() {
        return rowTotalWithDiscount;
    }

    public void setRowTotalWithDiscount(String rowTotalWithDiscount) {
        this.rowTotalWithDiscount = rowTotalWithDiscount;
    }

    public Integer getRowWeight() {
        return rowWeight;
    }

    public void setRowWeight(Integer rowWeight) {
        this.rowWeight = rowWeight;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public Object getBaseTaxBeforeDiscount() {
        return baseTaxBeforeDiscount;
    }

    public void setBaseTaxBeforeDiscount(Object baseTaxBeforeDiscount) {
        this.baseTaxBeforeDiscount = baseTaxBeforeDiscount;
    }

    public Object getTaxBeforeDiscount() {
        return taxBeforeDiscount;
    }

    public void setTaxBeforeDiscount(Object taxBeforeDiscount) {
        this.taxBeforeDiscount = taxBeforeDiscount;
    }

    public Object getOriginalCustomPrice() {
        return originalCustomPrice;
    }

    public void setOriginalCustomPrice(Object originalCustomPrice) {
        this.originalCustomPrice = originalCustomPrice;
    }

    public Object getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(Object redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    public Object getBaseCost() {
        return baseCost;
    }

    public void setBaseCost(Object baseCost) {
        this.baseCost = baseCost;
    }

    public Double getPriceInclTax() {
        return priceInclTax;
    }

    public void setPriceInclTax(Double priceInclTax) {
        this.priceInclTax = priceInclTax;
    }

    public Double getBasePriceInclTax() {
        return basePriceInclTax;
    }

    public void setBasePriceInclTax(Double basePriceInclTax) {
        this.basePriceInclTax = basePriceInclTax;
    }

    public Double getRowTotalInclTax() {
        return rowTotalInclTax;
    }

    public void setRowTotalInclTax(Double rowTotalInclTax) {
        this.rowTotalInclTax = rowTotalInclTax;
    }

    public Double getBaseRowTotalInclTax() {
        return baseRowTotalInclTax;
    }

    public void setBaseRowTotalInclTax(Double baseRowTotalInclTax) {
        this.baseRowTotalInclTax = baseRowTotalInclTax;
    }

    public Double getDiscountTaxCompensationAmount() {
        return discountTaxCompensationAmount;
    }

    public void setDiscountTaxCompensationAmount(Double discountTaxCompensationAmount) {
        this.discountTaxCompensationAmount = discountTaxCompensationAmount;
    }

    public Double getBaseDiscountTaxCompensationAmount() {
        return baseDiscountTaxCompensationAmount;
    }

    public void setBaseDiscountTaxCompensationAmount(Double baseDiscountTaxCompensationAmount) {
        this.baseDiscountTaxCompensationAmount = baseDiscountTaxCompensationAmount;
    }

    public Boolean getFreeShipping() {
        return freeShipping;
    }

    public void setFreeShipping(Boolean freeShipping) {
        this.freeShipping = freeShipping;
    }

    public Object getGiftMessageId() {
        return giftMessageId;
    }

    public void setGiftMessageId(Object giftMessageId) {
        this.giftMessageId = giftMessageId;
    }

    public Object getWeeeTaxApplied() {
        return weeeTaxApplied;
    }

    public void setWeeeTaxApplied(Object weeeTaxApplied) {
        this.weeeTaxApplied = weeeTaxApplied;
    }

    public Object getWeeeTaxAppliedAmount() {
        return weeeTaxAppliedAmount;
    }

    public void setWeeeTaxAppliedAmount(Object weeeTaxAppliedAmount) {
        this.weeeTaxAppliedAmount = weeeTaxAppliedAmount;
    }

    public Object getWeeeTaxAppliedRowAmount() {
        return weeeTaxAppliedRowAmount;
    }

    public void setWeeeTaxAppliedRowAmount(Object weeeTaxAppliedRowAmount) {
        this.weeeTaxAppliedRowAmount = weeeTaxAppliedRowAmount;
    }

    public Object getWeeeTaxDisposition() {
        return weeeTaxDisposition;
    }

    public void setWeeeTaxDisposition(Object weeeTaxDisposition) {
        this.weeeTaxDisposition = weeeTaxDisposition;
    }

    public Object getWeeeTaxRowDisposition() {
        return weeeTaxRowDisposition;
    }

    public void setWeeeTaxRowDisposition(Object weeeTaxRowDisposition) {
        this.weeeTaxRowDisposition = weeeTaxRowDisposition;
    }

    public Object getBaseWeeeTaxAppliedAmount() {
        return baseWeeeTaxAppliedAmount;
    }

    public void setBaseWeeeTaxAppliedAmount(Object baseWeeeTaxAppliedAmount) {
        this.baseWeeeTaxAppliedAmount = baseWeeeTaxAppliedAmount;
    }

    public Object getBaseWeeeTaxAppliedRowAmnt() {
        return baseWeeeTaxAppliedRowAmnt;
    }

    public void setBaseWeeeTaxAppliedRowAmnt(Object baseWeeeTaxAppliedRowAmnt) {
        this.baseWeeeTaxAppliedRowAmnt = baseWeeeTaxAppliedRowAmnt;
    }

    public Object getBaseWeeeTaxDisposition() {
        return baseWeeeTaxDisposition;
    }

    public void setBaseWeeeTaxDisposition(Object baseWeeeTaxDisposition) {
        this.baseWeeeTaxDisposition = baseWeeeTaxDisposition;
    }

    public Object getBaseWeeeTaxRowDisposition() {
        return baseWeeeTaxRowDisposition;
    }

    public void setBaseWeeeTaxRowDisposition(Object baseWeeeTaxRowDisposition) {
        this.baseWeeeTaxRowDisposition = baseWeeeTaxRowDisposition;
    }

    public List<Object> getQtyOptions() {
        return qtyOptions;
    }

    public void setQtyOptions(List<Object> qtyOptions) {
        this.qtyOptions = qtyOptions;
    }

    public ProductDetail getProduct() {
        return product;
    }

    public void setProduct(ProductDetail product) {
        this.product = product;
    }

    public String getTaxClassId() {
        return taxClassId;
    }

    public void setTaxClassId(String taxClassId) {
        this.taxClassId = taxClassId;
    }

    public Boolean getHasError() {
        return hasError;
    }

    public void setHasError(Boolean hasError) {
        this.hasError = hasError;
    }

    public Integer getQtyToAdd() {
        return qtyToAdd;
    }

    public void setQtyToAdd(Integer qtyToAdd) {
        this.qtyToAdd = qtyToAdd;
    }

    public Object getCalculationPrice() {
        return calculationPrice;
    }

    public void setCalculationPrice(Object calculationPrice) {
        this.calculationPrice = calculationPrice;
    }

    public Double getConvertedPrice() {
        return convertedPrice;
    }

    public void setConvertedPrice(Double convertedPrice) {
        this.convertedPrice = convertedPrice;
    }

    public String getBaseOriginalPrice() {
        return baseOriginalPrice;
    }

    public void setBaseOriginalPrice(String baseOriginalPrice) {
        this.baseOriginalPrice = baseOriginalPrice;
    }

    public Double getBaseCalculationPrice() {
        return baseCalculationPrice;
    }

    public void setBaseCalculationPrice(Double baseCalculationPrice) {
        this.baseCalculationPrice = baseCalculationPrice;
    }

    public String getTaxCalculationItemId() {
        return taxCalculationItemId;
    }

    public void setTaxCalculationItemId(String taxCalculationItemId) {
        this.taxCalculationItemId = taxCalculationItemId;
    }

    public Double getTaxCalculationPrice() {
        return taxCalculationPrice;
    }

    public void setTaxCalculationPrice(Double taxCalculationPrice) {
        this.taxCalculationPrice = taxCalculationPrice;
    }

    public Double getBaseTaxCalculationPrice() {
        return baseTaxCalculationPrice;
    }

    public void setBaseTaxCalculationPrice(Double baseTaxCalculationPrice) {
        this.baseTaxCalculationPrice = baseTaxCalculationPrice;
    }

    public Double getDiscountCalculationPrice() {
        return discountCalculationPrice;
    }

    public void setDiscountCalculationPrice(Double discountCalculationPrice) {
        this.discountCalculationPrice = discountCalculationPrice;
    }

    public Double getBaseDiscountCalculationPrice() {
        return baseDiscountCalculationPrice;
    }

    public void setBaseDiscountCalculationPrice(Double baseDiscountCalculationPrice) {
        this.baseDiscountCalculationPrice = baseDiscountCalculationPrice;
    }

    public List<Object> getAppliedTaxes() {
        return appliedTaxes;
    }

    public void setAppliedTaxes(List<Object> appliedTaxes) {
        this.appliedTaxes = appliedTaxes;
    }

    public List<Object> getOption() {
        return option;
    }

    public void setOption(List<Object> option) {
        this.option = option;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getItemQtyChangedTo() {
        return itemQtyChangedTo;
    }

    public void setItemQtyChangedTo(int itemQtyChangedTo) {
        this.itemQtyChangedTo = itemQtyChangedTo;
    }

    public String getFormattedBaseRowTotal() {
        return formattedBaseRowTotal;
    }

    public void setFormattedBaseRowTotal(String formatedBaseRowTotal) {
        this.formattedBaseRowTotal = formatedBaseRowTotal;
    }
}