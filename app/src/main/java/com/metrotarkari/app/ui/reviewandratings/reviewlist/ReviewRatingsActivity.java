package com.metrotarkari.app.ui.reviewandratings.reviewlist;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.google.gson.Gson;
import com.metrotarkari.app.R;
import com.metrotarkari.app.application.DoneApplication;
import com.metrotarkari.app.model.Product_;
import com.metrotarkari.app.ui.addressbook.addressentry.AddressEntryActivity;
import com.metrotarkari.app.ui.base.BaseActivity;
import com.metrotarkari.app.ui.reviewandratings.reviewlist.dragger.DaggerReviewRatingComponent;
import com.metrotarkari.app.ui.reviewandratings.reviewlist.dragger.ReviewRatingModule;
import com.metrotarkari.app.ui.reviewandratings.reviewlist.mvp.ReviewRatingPresenter;
import com.metrotarkari.app.ui.reviewandratings.reviewlist.mvp.ReviewRatingView;
import com.metrotarkari.app.ui.reviewandratings.reviewpost.ReviewPostActivity;
import com.metrotarkari.app.utils.Constant;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Avinash on 7/26/2017.
 */

public class ReviewRatingsActivity extends BaseActivity{

    @Inject
    ReviewRatingView view;

    private String productId;

    private Product_ product_;

    private Gson gson;

    private String gsonString;

    @Inject
    ReviewRatingPresenter presenter;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        gsonString = intent.getStringExtra(Constant.PRODUCT_DETAIL);

        gson = new Gson();
        product_ = gson.fromJson(gsonString,Product_.class);

        productId =  product_.getEntityId();

        DaggerReviewRatingComponent.builder().appComponent(DoneApplication.get(this).component())
                .reviewRatingModule(new ReviewRatingModule(this)).build().inject(this);

        setContentView(view);

       compositeDisposable.add( presenter.getProductReview(productId));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_address_book, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        if(item.getItemId() == R.id.action_add_address){
            String product =  gson.toJson(product_, Product_.class);
            Intent intent = new Intent(getApplicationContext(), ReviewPostActivity.class);
            intent.putExtra(Constant.PRODUCT_DETAIL, product);
            startActivity(intent);
            return true;
        }
        return false;
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

}
