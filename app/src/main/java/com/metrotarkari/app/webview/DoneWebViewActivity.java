package com.metrotarkari.app.webview;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.crashlytics.android.Crashlytics;
import com.metrotarkari.app.ui.base.BaseActivity;

import io.fabric.sdk.android.Fabric;

/**
 * Created by Avinash on 6/13/2017.
 */

public class DoneWebViewActivity extends BaseActivity {


    DoneWebView doneWebView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());

        doneWebView = new DoneWebView(this);
        setContentView(doneWebView);
    }
}
