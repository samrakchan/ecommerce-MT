package com.metrotarkari.app.ui.b2b.add.mvp;

import com.metrotarkari.app.application.network.DoneNetwork;
import com.metrotarkari.app.model.Wishlistitem;

import java.util.HashMap;

import io.reactivex.Observable;

/**
 * Created by samrakchan on 4/27/17.
 */

public class B2bAddModel {
    private final DoneNetwork doneNetwork;

    public B2bAddModel(DoneNetwork doneNetwork){
        this.doneNetwork = doneNetwork;
    }

    /**
     *
     * @param productIdMap  {"product": "74"}
     * @return
     */
    public Observable<Wishlistitem> postWishList(HashMap<String, String> productIdMap){
        return doneNetwork.postWishList(productIdMap);
    }
//
//
//    public Observable<CartQuoteContent> putCartQuote(HashMap<String, Integer> itemQty){
//        return doneNetwork.putQuoteItem(itemQty);
//    }
//
//    public Observable<AddressResponse> getAddressBook(){
//        return doneNetwork.getAddressBook();
//    }

//    public Observable<WishList> getWishList(ItemParams itemParams){
//        return doneNetwork.getWishList(itemParams.getPage(), itemParams.getLimit(), itemParams.getOrderBy(), itemParams.getDir());
//    }
}
