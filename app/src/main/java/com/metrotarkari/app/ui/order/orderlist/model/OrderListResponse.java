
package com.metrotarkari.app.ui.order.orderlist.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.metrotarkari.app.model.ErrorResponse;

import java.util.List;

public class OrderListResponse extends ErrorResponse{

    @SerializedName("all_ids")
    @Expose
    private List<String> allIds = null;
    @SerializedName("orders")
    @Expose
    private List<Order> orders = null;
    @SerializedName("total")
    @Expose
    private Integer total;
    @SerializedName("page_size")
    @Expose
    private Integer pageSize;
    @SerializedName("from")
    @Expose
    private Integer from;
    @SerializedName("total_page")
    @Expose
    private Integer totalPage;
    @SerializedName("current_page")
    @Expose
    private Integer currentPage;

    public List<String> getAllIds() {
        return allIds;
    }

    public void setAllIds(List<String> allIds) {
        this.allIds = allIds;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getFrom() {
        return from;
    }

    public void setFrom(Integer from) {
        this.from = from;
    }

    public Integer getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(Integer totalPage) {
        this.totalPage = totalPage;
    }

    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

}
