package com.metrotarkari.app.ui.settings.mvp;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.metrotarkari.app.utils.DoneUserPreferenceManager;

import io.reactivex.disposables.CompositeDisposable;

import static com.basgeekball.awesomevalidation.ValidationStyle.UNDERLABEL;

/**
 * Created by samrakchan on 4/30/17.
 */

public class SettingsPresenter {

    private final SettingsView view;
    private final SettingsModel model;


    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    private AwesomeValidation awesomeValidation = new AwesomeValidation(UNDERLABEL);
    private DoneUserPreferenceManager doneUserPreferenceManager;

    public SettingsPresenter(SettingsView view, SettingsModel model){
        this.view = view;
        this.model = model;

   //     compositeDisposable.add(getOrders());

    }

  /*  private Disposable getOrders(){
        view.showNoItem(false);
        view.showProgressBar(true);
        return model.getOrder().observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(orderResponse -> {
                    if(orderResponse!=null) {
                        List<Order> orderList = orderResponse.getOrders();
                        if (orderList != null) {
                            if (orderList.isEmpty()) {
                                view.showNoItem(true);
                            }else {
                                view.setOrderListData(orderList);
                                view.showProgressBar(false);
                                view.showNoItem(false);

                            }
                        }else{
                            view.showNoItem(true);
                        }
                    }
                }, throwable -> {
                    throwable.printStackTrace();
                    view.showProgressBar(false);
                    view.showNoItem(true);
                });
    }
*/

}
