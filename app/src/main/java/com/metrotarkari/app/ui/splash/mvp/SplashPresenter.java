package com.metrotarkari.app.ui.splash.mvp;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import com.metrotarkari.app.utils.DoneTokenPreferenceManager;
import com.metrotarkari.app.utils.DoneUserPreferenceManager;
import com.metrotarkari.app.utils.Utility;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Observer;
import rx.Subscription;

/**
 * Created by samrakchan on 4/27/17.
 */

public class SplashPresenter {
    private final SplashView view;
    private final SplashModel model;

    private Subscription subscription;
    private DoneUserPreferenceManager doneUserPreferenceManager;
    private DoneTokenPreferenceManager tokenPreferenceManager;

    public SplashPresenter(SplashView view, SplashModel model, DoneUserPreferenceManager tokenPreferenceManager){
        this.view = view;
        this.model = model;
        this.doneUserPreferenceManager = tokenPreferenceManager;
        this.tokenPreferenceManager = new DoneTokenPreferenceManager(view.getContext());

        generateContentId();
        generateSessionId();
        updateAppVersionCode();
    }

    public void onDestroy(){
        subscription.unsubscribe();
    }

    public void onPause(){
        subscription.unsubscribe();
    }

    private void generateSessionId(){
        if(tokenPreferenceManager.getPhpSessionId().equals("")) {
            tokenPreferenceManager.setPhpSessionId(Utility.getSessionId());
        }
    }
    private void generateContentId(){
        if(tokenPreferenceManager.getPrivateContentVersion().equals("")) {
            tokenPreferenceManager.setPrivateContentVersion(Utility.getContentValue()+"");
        }
    }

    private void updateAppVersionCode(){
        if(doneUserPreferenceManager.getAppVersionCode() != getVersion()){
            doneUserPreferenceManager.setAppVersionCode(getVersion());
        }
    }

    private int getVersion(){
        try {
            PackageInfo pInfo = view.getContext().getPackageManager().getPackageInfo(view.getContext().getPackageName(), 0);
            return pInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return -1;
        }
    }

    public void onResume(){
        subscription = Observable.timer(3, TimeUnit.SECONDS).subscribe(new Observer<Long>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onNext(Long aLong) {
                if(doneUserPreferenceManager.getHideWalkThroughPreference()){
                    model.startWalkThroughActivity();
                }else if(doneUserPreferenceManager.getUserSkipLogin() || doneUserPreferenceManager.isLoggedInUser()){
                    model.startMainActivity();
                }else{
                    model.startLoginActivity();
                }
                model.finishActivity();
            }
        });
    }


}
