
package com.metrotarkari.app.ui.order.orderdetail.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.metrotarkari.app.model.ErrorResponse;

public class OrderDetailResponse extends ErrorResponse {

    @SerializedName("order")
    @Expose
    private Order order;

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

}
