package com.metrotarkari.app.ui.reviewandratings.reviewlist.mvp;

import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.metrotarkari.app.R;
import com.metrotarkari.app.ui.base.BaseView;
import com.metrotarkari.app.ui.reviewandratings.reviewlist.ReviewRatingAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by samrakchan on 4/27/17.
 */
public class ReviewRatingView extends BaseView{

    @BindView(R.id.reviewRatingRv)
    public RecyclerView reviewRatingRv;

    @BindView(R.id.imageMsgRl)
    public View noItemView;

    @BindView(R.id.progressBarRl)
    public View progressBar;

    @BindView(R.id.imageMsgNoItemTv)
    public AppCompatTextView imageMsgNoItemTv;

    private ReviewRatingAdapter reviewRatingAdapter;

    private AppCompatActivity activity;

    public ReviewRatingView(AppCompatActivity activity){
        super(activity);
        this.activity = activity;

        ButterKnife.bind(this);

        reviewRatingRv.setLayoutManager(new LinearLayoutManager(getContext()));
        DividerItemDecoration diH = new DividerItemDecoration(getContext(), DividerItemDecoration.HORIZONTAL);
        diH.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.bg_divider));

        DividerItemDecoration diV = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        diH.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.bg_divider));

        reviewRatingRv.addItemDecoration(diH);
        reviewRatingRv.addItemDecoration(diV);

        reviewRatingAdapter = new ReviewRatingAdapter(getContext());
        reviewRatingRv.setAdapter(reviewRatingAdapter);

        activity.getSupportActionBar().setTitle(R.string.reviews_and_rating);
    }

    @Override
    public int getContentLayout() {
        return R.layout.activity_review_rating;
    }

    public void setReviewRatingData(List<Object> objects){
        reviewRatingAdapter.addAllRatingReviewData(objects);
    }

    public void showNoItem(boolean showItem){
        if(showItem) {
            noItemView.setVisibility(View.VISIBLE);
            imageMsgNoItemTv.setText(R.string.no_review_found);
        } else {
            noItemView.setVisibility(View.GONE);
        }
    }

    public void showProgressBar(boolean showing){
        if(showing)
            progressBar.setVisibility(View.VISIBLE);
        else
            progressBar.setVisibility(View.GONE);
    }
}
