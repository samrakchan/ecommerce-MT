package com.metrotarkari.app.ui.dashboard;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.tasks.Task;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;
import com.google.firebase.iid.FirebaseInstanceId;
import com.metrotarkari.app.R;
import com.metrotarkari.app.application.DoneApplication;
import com.metrotarkari.app.event.CartEvent;
import com.metrotarkari.app.ui.cart.CartActivity;
import com.metrotarkari.app.ui.dashboard.dagger.DaggerMainComponent;
import com.metrotarkari.app.ui.dashboard.dagger.MainModule;
import com.metrotarkari.app.ui.dashboard.mvp.MainPresenter;
import com.metrotarkari.app.ui.dashboard.mvp.MainView;
import com.metrotarkari.app.ui.productdetail.DetailActivity;
import com.metrotarkari.app.ui.forgetpassword.ForgetPasswordActivity;
import com.metrotarkari.app.utils.Constant;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;

import javax.inject.Inject;

import berlin.volders.badger.BadgeShape;
import berlin.volders.badger.Badger;
import berlin.volders.badger.CountBadge;
import io.fabric.sdk.android.Fabric;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class MainActivity extends AppCompatActivity{

    @Inject
    MainView mainView;

    @Inject
    MainPresenter mainPresenter;

    private CountBadge.Factory circleFactory;

    private Menu menu;

    private EventBus eventBus = EventBus.getDefault();

    private String fcmToken;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());

        circleFactory = new CountBadge.Factory(this, BadgeShape.circle(.5f, Gravity.END | Gravity.TOP));

        DaggerMainComponent.builder().appComponent(DoneApplication.get(this).component())
                .mainModule(new MainModule(this))
                .build().inject(this);

        setContentView(mainView);

        fcmToken = FirebaseInstanceId.getInstance().getToken();
        Log.i("FCM Token", "FCM "+fcmToken);

        initDeepLink();
    }

    @Override
    public void onBackPressed() {
        if (mainView.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            mainView.drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_home, menu);
        this.menu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.action_cart){
            Intent intent = new Intent(getApplicationContext(), CartActivity.class);
            startActivity(intent);
            return true;
        }
        return false;
    }

    public void updateCartInMenu(int count){
        if(menu!=null) {
            Badger.sett(menu.findItem(R.id.action_cart), circleFactory).setCount(count);
        }
    }

    @Override
    protected void onDestroy() {
        mainPresenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onStart() {
        super.onStart();
        eventBus.register(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(DoneApplication.getInstance()!=null) {
            updateMenuColorToWhite(); //for older version only
            updateCartInMenu(DoneApplication.getInstance().getCartCount());
        }
    }

    //change color of icons to white in older devices
    private void updateMenuColorToWhite(){
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP && menu!=null) {
            for (int i = 0; i < menu.size(); i++) {
                boolean isCartIcon = (this.menu.getItem(i).getItemId() == R.id.action_cart);
                if(isCartIcon) {
                    Drawable drawable = ContextCompat.getDrawable(this, R.mipmap.ic_toolbar_cart);
                    drawable.setColorFilter(ContextCompat.getColor(this, R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);
                    menu.getItem(i).setIcon(drawable);
                }
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        eventBus.unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(CartEvent event){
        updateCartInMenu(event.getCartCount());
    }

    private void initDeepLink(){
        final Task<PendingDynamicLinkData> pendingDynamicLinkDataTask = FirebaseDynamicLinks.getInstance()
                .getDynamicLink(getIntent())
                .addOnSuccessListener(this, pendingDynamicLinkData -> {
                    // Get deep link from result (may be null if no link is found)
                    Uri deepLink = null;
                    if (pendingDynamicLinkData != null) {
                        deepLink = pendingDynamicLinkData.getLink();
                        Log.i("MetroDynamicLink", "URL " + deepLink + " toString " + pendingDynamicLinkData.toString());
                        //Toast.makeText(getApplicationContext(), "URL " + deepLink, Toast.LENGTH_LONG).show();

                        String deepLinkString = deepLink.toString();
                        deepLinkNavigation(deepLinkString);
                    }

                })
                .addOnFailureListener(this, e -> {
                    Toast.makeText(getApplicationContext(), R.string.link_not_opened, Toast.LENGTH_LONG).show();
                    Log.w("MetroDynamicLink", "getDynamicLink:onFailure", e);
                });
    }

    private void deepLinkNavigation(String deepLinkFullUrl){

        String[] splittedString = deepLinkFullUrl.split("/");

        if(deepLinkFullUrl!=null) {
            if (deepLinkFullUrl.contains(Constant.SHARE_URL)) {
                Intent intent = new Intent(getApplicationContext(), DetailActivity.class);
                intent.putExtra(Constant.PRODUCT_ID, getProductCodeFromUrl(splittedString));
                startActivity(intent);
            }else if(deepLinkFullUrl.contains(Constant.FORGOT_PASSWORD)){

                HashMap<String, String> keyValue = new HashMap<>();
                for(int i=0; i<splittedString.length; i++){
                    if(splittedString[i].equalsIgnoreCase("id")){
                        keyValue.put("id", getTextFromArray(splittedString, i+1));
                    }else if(splittedString[i].equalsIgnoreCase("token")){
                        keyValue.put("token", getTextFromArray(splittedString, i+1));
                    }
                }

                Intent intent = new Intent(getApplicationContext(), ForgetPasswordActivity.class);
                intent.putExtra(Constant.DATA, keyValue);
                startActivity(intent);
            }
        }
    }

    private String getTextFromArray(String[] array, int position){
        try {
            return array[position];
        }catch (ArrayIndexOutOfBoundsException|NegativeArraySizeException e){
            e.printStackTrace();
            return "";
        }
    }

    private String getProductCodeFromUrl(String[] splittedString){
        try {
            return splittedString[splittedString.length - 1];
        }catch (NegativeArraySizeException | ArrayIndexOutOfBoundsException e){
            e.printStackTrace();
            return "";
        }
    }
}


