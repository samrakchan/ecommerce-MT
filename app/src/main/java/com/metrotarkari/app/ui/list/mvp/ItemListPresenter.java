package com.metrotarkari.app.ui.list.mvp;

import android.widget.Toast;

import com.metrotarkari.app.R;
import com.metrotarkari.app.model.Error;
import com.metrotarkari.app.model.FilterQueryWithPagination;
import com.metrotarkari.app.model.Product;
import com.metrotarkari.app.model.ProductList;
import com.metrotarkari.app.utils.DoneUserPreferenceManager;

import java.util.HashMap;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by samrakchan on 4/27/17.
 */

public class ItemListPresenter {
    private final ItemListView view;
    private final ItemListModel model;
    HashMap<String, String> hashMap;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private DoneUserPreferenceManager doneUserPreferenceManager;

    public ItemListPresenter(ItemListView view, ItemListModel model, DoneUserPreferenceManager userPreferenceManager, HashMap<String, String> hashMap){
        this.view = view;
        this.model = model;
        this.hashMap = hashMap;
        doneUserPreferenceManager = userPreferenceManager;

        getCategories();
    }

    public void getCategories(){
        compositeDisposable.add(view.getFilterObservable().subscribe(
                filterQueryWithPagination -> {
                    filterQueryWithPagination.getQueryValue().putAll(hashMap);
                    compositeDisposable.add(getCategories(filterQueryWithPagination));
                }));
    }

   /* public void getCategories(String catId){

        compositeDisposable.add(view.getFilterObservable().subscribe(
                filterQueryWithPagination -> {
                    //HashMap<String, String> map = new HashMap<>();
                    //map.put("filter[cat_id]", catId);
                    //compositeDisposable.add(getCategories(map, filterQueryWithPagination));
                    compositeDisposable.add(getCategories(filterQueryWithPagination));

                }));

        *//*compositeDisposable.add(view.getOnScrollObservable().subscribe(
                itemParams -> compositeDisposable.add(getCategories(false, catId, itemParams)))
        );*//*
    }*/

/*    public void searchProduct(String searchText){
        compositeDisposable.add(view.getOnScrollObservable().subscribe(
                itemParams -> {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("filter[q]", searchText);
                    //compositeDisposable.add(getCategories(map, itemParams));
                    //compositeDisposable.add(getCategories(map, itemParams));
                })
        );
    }*/


    private Disposable getCategories(FilterQueryWithPagination filterQueryWithPagination){
        view.showListActionView(false);
        if(view.getCurrentItemSize()==0) {
            view.showProgressBar(true);
        }else{
            view.showProgressBar(false);
        }
        filterQueryWithPagination.getItemParams().setPage(filterQueryWithPagination.getItemParams().getPage()+1);

        return model.getCategories(filterQueryWithPagination.getQueryValue(), filterQueryWithPagination.getItemParams()).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(productList -> {
                    showProduct(productList);
                }, throwable -> {
                    view.showListActionView(false);
                    view.showNoItem(false);
                    view.showProgressBar(false);
                    Toast.makeText(view.getContext(), R.string.sww,Toast.LENGTH_LONG).show();
                    throwable.printStackTrace();
                });

    }

    private void showProduct(ProductList productList){
        view.showProgressBar(false);

        view.showNoInternetConnection(false);
        if(productList!=null) {

            List<Product> product = productList.getProducts();
            List<Error> errorResponse = productList.getErrors();

            if(errorResponse!=null && !errorResponse.isEmpty()){
                Error error = errorResponse.get(0);
                Toast.makeText(view.getContext(),error.getMessage()+"",Toast.LENGTH_LONG).show();
            }else if (product != null) {
                if (product.isEmpty()) {
                    view.showListActionView(false);
                    view.setTotalPage(0);
                    view.showNoItem(true);
                }else {
                    view.showListActionView(true);
                    view.setPageIncrement();
                    view.setTotalPage(productList.getTotalPage());
                    view.addItemToProductAdapter(productList.getProducts());

                    if(productList.getLayers()!=null && productList.getLayers().getLayerFilter()!=null) {
                        view.setFilterAttributes(productList.getLayers().getLayerFilter());
                    }

                    if(productList.getOrders()!=null){
                        view.setOrder(productList.getOrders());
                    }
                }
            }else{
                view.showListActionView(false);
                view.setTotalPage(0);
                view.showNoItem(true);
                view.showNoInternetConnection(true);
            }
        }
    }
}
