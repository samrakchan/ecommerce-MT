package com.metrotarkari.app.model;

/**
 * Created by samrakchan on 8/23/17.
 */

public class Tier {
    private String text;

    public Tier(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
