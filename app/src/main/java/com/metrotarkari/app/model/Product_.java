
package com.metrotarkari.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Product_ extends Product {

    @SerializedName("quantity_and_stock_status")
    @Expose
    private QuantityAndStockStatus quantityAndStockStatus;

    @SerializedName("wishlist_item_id")
    @Expose
    private Object wishlistItemId;

    @SerializedName("product_video")
    @Expose
    private Object productVideo;

    @SerializedName("formatted_tier_price")
    private List<String> formattedTierPrice;

    @SerializedName("product_related")
    @Expose(serialize = false, deserialize = false)
    private List<Product> productRelated;

    @SerializedName("product_upsell")
    @Expose(serialize = false, deserialize = false)
    private List<Product> productUpsell;

    @SerializedName("app_options")
    private AppOptions appOptions;


    public QuantityAndStockStatus getQuantityAndStockStatus() {
        return quantityAndStockStatus;
    }

    public void setQuantityAndStockStatus(QuantityAndStockStatus quantityAndStockStatus) {
        this.quantityAndStockStatus = quantityAndStockStatus;
    }

    public Object getWishlistItemId() {
        return wishlistItemId;
    }

    public void setWishlistItemId(Object wishlistItemId) {
        this.wishlistItemId = wishlistItemId;
    }

    public Object getProductVideo() {
        return productVideo;
    }

    public void setProductVideo(Object productVideo) {
        this.productVideo = productVideo;
    }

    public List<Product> getProductRelated() {
        return productRelated;
    }

    public void setProductRelated(List<Product> productRelated) {
        this.productRelated = productRelated;
    }

    public List<String> getFormattedTierPrice() {
        return formattedTierPrice;
    }

    public void setFormattedTierPrice(List<String> formattedTierPrice) {
        this.formattedTierPrice = formattedTierPrice;
    }

    public List<Product> getProductUpsell() {
        return productUpsell;
    }

    public void setProductUpsell(List<Product> productUpsell) {
        this.productUpsell = productUpsell;
    }

    public AppOptions getAppOptions() {
        return appOptions;
    }

    public void setAppOptions(AppOptions appOptions) {
        this.appOptions = appOptions;
    }
}
