package com.metrotarkari.app.ui.settings.mvp;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.metrotarkari.app.R;
import com.metrotarkari.app.ui.base.BaseView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by samrakchan on 4/27/17.
 */

public class SettingsView extends BaseView {

    @BindView(R.id.toolbar)
    public Toolbar toolbar;

    @BindView(R.id.imageMsgRl)
    public View noItemView;

    @BindView(R.id.progressBarRl)
    public View progressBar;

    @BindView(R.id.imageMsgNoItemTv)
    public AppCompatTextView imageMsgNoItemTv;

    @Nullable
    @BindView(R.id.addressBookRv)
    public RecyclerView addressBookRv;





    private AppCompatActivity activity;

    public SettingsView(@NonNull AppCompatActivity activity) {
        super(activity);
        this.activity = activity;

        ButterKnife.bind(this);

        activity.setSupportActionBar(toolbar);
        activity.getSupportActionBar().setTitle(R.string.action_settings);


    }

    @Override
    public int getContentLayout() {
        return R.layout.activity_address_book;
    }


    public void showNoItem(boolean showItem){
        if(showItem) {
            noItemView.setVisibility(View.VISIBLE);
            imageMsgNoItemTv.setText(R.string.no_order_found);
        }
        else {
            noItemView.setVisibility(View.GONE);
        }
    }

    public void showProgressBar(boolean showing){
        if(showing)
            progressBar.setVisibility(View.VISIBLE);
        else
            progressBar.setVisibility(View.GONE);
    }

}
