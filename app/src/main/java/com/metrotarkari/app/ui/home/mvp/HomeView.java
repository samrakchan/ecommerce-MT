package com.metrotarkari.app.ui.home.mvp;

import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.widget.Toast;

import com.metrotarkari.app.R;
import com.metrotarkari.app.generic.OnItemClickListenerTwo;
import com.metrotarkari.app.model.HomeCategory;
import com.metrotarkari.app.model.Homebanners;
import com.metrotarkari.app.model.Homeproductlist;
import com.metrotarkari.app.model.ItemCategory;
import com.metrotarkari.app.model.Product;
import com.metrotarkari.app.model.ProductArray_;
import com.metrotarkari.app.ui.base.BaseViewFragment;
import com.metrotarkari.app.ui.home.HomeFragment;
import com.metrotarkari.app.utils.AlertUtils;
import com.metrotarkari.app.utils.ViewUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;

/**
 * Created by samrakchan on 4/27/17.
 */

public class HomeView extends BaseViewFragment implements View.OnClickListener, OnItemClickListenerTwo{

    private HomeFragment fragment;

    @BindView(R.id.imageMsgRl)
    public View noItemView;

    @BindView(R.id.progressBarRl)
    public View progressBar;

    @Nullable
    @BindView(R.id.mainRecyclerView)
    public RecyclerView mainRecyclerView;

    @BindView(R.id.swipeRefreshView)
    public SwipeRefreshLayout swipeRefreshLayout;

    private HomeAdapter mMainAdapter;

    private StaggeredGridLayoutManager staggeredGridLayoutManager;

    private final BehaviorSubject<Object> swipeViewBehaviour = BehaviorSubject.create();

    List<Object> objectList;

    public HomeView(HomeFragment fragment){
        super(fragment);
        this.fragment = fragment;

        objectList = new ArrayList<>();

        staggeredGridLayoutManager = new StaggeredGridLayoutManager(ViewUtils.calculateNoOfColumns(getContext()), StaggeredGridLayoutManager.VERTICAL);
        mainRecyclerView.setLayoutManager(staggeredGridLayoutManager);

        DividerItemDecoration diV = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        diV.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.bg_divider));

        DividerItemDecoration diH = new DividerItemDecoration(getContext(), DividerItemDecoration.HORIZONTAL);
        diH.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.bg_divider));

        mainRecyclerView.addItemDecoration(diV);
        mainRecyclerView.addItemDecoration(diH);


        swipeRefreshLayout.setOnRefreshListener(() -> swipeViewBehaviour.onNext(new Object()));

        mMainAdapter = new HomeAdapter(getContext(), this, this);

        mainRecyclerView.setAdapter(mMainAdapter);
    }

    public Observable<Object> getSwipeViewObservable(){
        return swipeViewBehaviour;
    }

    @Override
    protected void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        staggeredGridLayoutManager.setSpanCount(ViewUtils.calculateNoOfColumns(getContext()));
    }

    @Override
    public int getContentLayout() {
        return R.layout.fragment_home;
    }

    public void addBanner(Homebanners homebanners){
        objectList.add(homebanners);
    }

    public void addCategoryAndItsItems(List<HomeCategory> homeCategory){
        for(int i=0; i<homeCategory.size(); i++) {
            HomeCategory hc = homeCategory.get(i);

            ItemCategory itemCategory = new ItemCategory();

            itemCategory.setCategoryId(hc.getCategoryId());
            itemCategory.setCategoryName(hc.getCatName());
            itemCategory.setIcon(false);
            itemCategory.setShowSeeMore(true);

            if(hc.getProductArray() !=null && hc.getProductArray().getProducts()!=null){
                List<Product> products = hc.getProductArray().getProducts();
                boolean hasSubCategory = products.size()>0?true:false;
                if(hasSubCategory) {
                    itemCategory.setSubCategory(hasSubCategory);
                    objectList.add(itemCategory);
                    objectList.addAll(products);
                }
            }
        }
    }

    public void addPopularCategoryAndItems(List<Homeproductlist> list){

       for(int i=0; i<list.size(); i++){
            Homeproductlist hp = list.get(i);
           ProductArray_ productArray_ = hp.getProductArray();
           if( productArray_!=null && !productArray_.getProducts().isEmpty()){
               objectList.add(hp);
           }else if(hp.getTypeCode().equalsIgnoreCase("ads_image")){
               objectList.add(hp);
           }
        }

       // objectList.addAll(list);

        mMainAdapter.addData(objectList);
    }

    public void clearItems(){
        objectList.clear();
    }

    public void showNoItem(boolean showItem){
        if(showItem)
            noItemView.setVisibility(View.VISIBLE);
        else
            noItemView.setVisibility(View.GONE);
    }

    public void showProgressBar(boolean showing){
        if(showing)
            progressBar.setVisibility(View.VISIBLE);
        else
            progressBar.setVisibility(View.GONE);
    }

    private void showAlert(int positiveBtn, int negativeBtn, int msg, Intent intent){
        AlertUtils.createAlert(fragment.getActivity(), positiveBtn, negativeBtn, msg, intent).show();
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.categoryContactCallUsBtn){
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", getContext().getString(R.string.call_us_value).replace("-",""), null));
            showAlert(R.string.call, R.string.cancel, R.string.make_call_text, intent);

        }else if(view.getId() == R.id.categoryContactSalesMarketingBtn){

            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", getContext().getString(R.string.sales_marketing_value).replace("-",""), null));
            showAlert(R.string.call, R.string.cancel, R.string.make_call_text, intent);

        }else if(view.getId() == R.id.categoryEmailBtn){
            try {
                String uriText =
                        "mailto:" +getContext().getString(R.string.email_value)+
                                "?subject=" + Uri.encode("Hello Metro Tarkari!") +
                                "&body=" + Uri.encode("Hi,");
                Uri uri = Uri.parse(uriText);

                Intent sendIntent = new Intent(Intent.ACTION_SENDTO);
                sendIntent.setData(uri);
                getContext().startActivity(Intent.createChooser(sendIntent, "Send email"));
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    protected void hideSwipeView(){
        if(swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void setOnItemClickListener(Object object, int position) {
        Toast.makeText(getContext(), ""+position, Toast.LENGTH_LONG).show();
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("product",  ((Product)object).getEntityId());
        addItemToB2BWishListBehavior.onNext(hashMap);
    }
}
