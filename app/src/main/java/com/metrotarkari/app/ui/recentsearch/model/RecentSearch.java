package com.metrotarkari.app.ui.recentsearch.model;

/**
 * Created by Avinash on 7/6/2017.
 */

public class RecentSearch {

    private String recentSearchItemName;

    public String getRecentSearchItemName() {
        return recentSearchItemName;
    }

    public void setRecentSearchItemName(String recentSearchItemName) {
        this.recentSearchItemName = recentSearchItemName;
    }
}
