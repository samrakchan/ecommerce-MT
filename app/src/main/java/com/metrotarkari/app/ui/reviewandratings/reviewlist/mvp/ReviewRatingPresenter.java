package com.metrotarkari.app.ui.reviewandratings.reviewlist.mvp;

import android.widget.Toast;
import com.metrotarkari.app.R;
import com.metrotarkari.app.model.Count;
import com.metrotarkari.app.model.Error;
import com.metrotarkari.app.model.Review;
import java.util.ArrayList;
import java.util.List;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by samrakchan on 4/27/17.
 */

public class ReviewRatingPresenter {
    private final ReviewRatingView view;
    private final ReviewRatingModel model;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public ReviewRatingPresenter(ReviewRatingView view, ReviewRatingModel model){
        this.view = view;
        this.model = model;

    }

    public void onDestroy(){
        compositeDisposable.dispose();
    }

    public Disposable getProductReview(String productId){
        view.showProgressBar(true);
        return model.getReview(productId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(reviewResponse -> {
                    List<Error> errorResponse = reviewResponse.getErrors();
                    List<Review> review = reviewResponse.getReviews();
                    Count count = reviewResponse.getCount();
                    List<Object> objects = new ArrayList<>();

                    if(errorResponse!=null && !errorResponse.isEmpty()){
                        Error error = errorResponse.get(0);
                        Toast.makeText(view.getContext(),error.getMessage()+"",Toast.LENGTH_LONG).show();
                    }else if (reviewResponse!=null){

                        if (count!=null){
                            //count.setRate(reviewResponse.);
                            count.setRatingCount(reviewResponse.getTotal());
                            objects.add(0,count);
                            view.showProgressBar(false);
                            view.showNoItem(false);
                        }

                        if (review!=null && !review.isEmpty()){
                            objects.addAll(1,review);
                            view.setReviewRatingData(objects);
                            view.showProgressBar(false);
                            view.showNoItem(false);
                        }else {
                            view.showProgressBar(false);
                            view.showNoItem(true);
                        }

                    }else {
                        view.showProgressBar(false);
                        view.showNoItem(true);
                    }
                }, throwable -> {
                    view.showProgressBar(false);
                    view.showNoItem(false);
                    Toast.makeText(view.getContext(), R.string.sww,Toast.LENGTH_LONG).show();
                    throwable.printStackTrace();
                });
    }
}
