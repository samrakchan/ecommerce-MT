package com.metrotarkari.app.ui.list.filter.model;

/**
 * Created by samrakchan on 4/4/17.
 */

public class FilterChild {
    private String childTitle;
    private boolean selected = false;

    public FilterChild(String childTitle){
        this.childTitle = childTitle;
    }

    public String getChildTitle() {
        return childTitle;
    }

    public void setChildTitle(String childTitle) {
        this.childTitle = childTitle;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
