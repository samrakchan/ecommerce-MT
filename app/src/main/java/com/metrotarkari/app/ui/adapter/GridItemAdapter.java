package com.metrotarkari.app.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.metrotarkari.app.R;
import com.metrotarkari.app.generic.OnItemClickListener;
import com.metrotarkari.app.generic.OnItemClickListenerTwo;
import com.metrotarkari.app.model.Product;
import com.metrotarkari.app.ui.productdetail.DetailActivity;
import com.metrotarkari.app.utils.Constant;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by samrakchan on 6/28/17.
 */

public class GridItemAdapter extends RecyclerView.Adapter<GridItemAdapter.CategoryItemList> {
    private List<Product> mData;
    private Context mContext;
    private OnItemClickListenerTwo listener;

    public GridItemAdapter(Context context) {
        mData = new ArrayList<>();
        this.mContext = context;
    }

    public void addData(List<Product> datas) {
        this.mData.addAll(datas);
        notifyDataSetChanged();
    }

    public void setOnItemClickListener(OnItemClickListenerTwo listener){
        this.listener = listener;
    }

    public void removeAllData() {
        mData.clear();
        notifyDataSetChanged();
    }

    @Override
    public GridItemAdapter.CategoryItemList onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_list_item_grid, parent, false);
        return new GridItemAdapter.CategoryItemList(view);
    }

    @Override
    public void onBindViewHolder(GridItemAdapter.CategoryItemList holder, int position) {
        Product product = mData.get(position);
        holder.listItemNameTv.setText(product.getName());

        //String weightValue = product.getMetroWeightTypeValue()!=null?"/"+ product.getMetroWeightTypeValue():"";

       // holder.listItemPriceTv.setText(mContext.getString(R.string.price, Utility.convertToTwoDecimalPlace(product.getFinalPrice()))+weightValue);
        holder.listItemPriceTv.setText(product.getFormattedFinalPrice());
        if(product.getPrice().equalsIgnoreCase(product.getFinalPrice())){
            holder.listItemOldPriceTv.setVisibility(View.GONE);
        }else{
            holder.listItemOldPriceTv.setPaintFlags(holder.listItemOldPriceTv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
         //   holder.listItemOldPriceTv.setText(mContext.getString(R.string.price, Utility.convertToTwoDecimalPlace(product.getPrice())));
            holder.listItemOldPriceTv.setText(product.getFormattedPrice());
            holder.listItemOldPriceTv.setVisibility(View.VISIBLE);
        }
        holder.listItemRatingLl.setVisibility(View.GONE);

        if(product.getDiscountLabel() !=null){
            holder.listItemOfferLabelTv.setText(product.getDiscountLabel());
            holder.listItemOfferLabelTv.setVisibility(View.VISIBLE);
        }else{
            holder.listItemOfferLabelTv.setVisibility(View.GONE);
        }

       /* AppReviews appReviews = product.getAppReviews();
        if(appReviews !=null){
            holder.listItemRatingTv.setText(appReviews.getNumber()+"");
        }

*/
        if (product.getImages() != null && !product.getImages().isEmpty())
            Glide.with(mContext).load(product.getImages().get(0).getUrl()).placeholder(R.mipmap.ic_placeholder_mt).dontAnimate().centerCrop().into(holder.listItemImageIv);

        Log.i("IMG", product.getImages().get(0).getUrl() + "");

    }
    @Override
    public int getItemCount() {
        return mData.size();
    }

    protected class CategoryItemList extends RecyclerView.ViewHolder implements View.OnClickListener {

        private AppCompatImageView listItemImageIv;
        private AppCompatImageView loveIv;
        private AppCompatTextView listItemPriceTv;
        private AppCompatTextView listItemOldPriceTv;
        private AppCompatTextView listItemNameTv;
        private AppCompatTextView listItemRatingTv;
        private AppCompatTextView listItemOfferLabelTv;
        private View listItemRatingLl;
        private AppCompatRatingBar listItemRatingBar;

        public CategoryItemList(View itemView) {
            super(itemView);

            listItemImageIv = (AppCompatImageView) itemView.findViewById(R.id.listItemImageIv);
            loveIv = (AppCompatImageView) itemView.findViewById(R.id.loveIv);
            //int heightPixelValue = (int) mContext.getResources().getDimension(R.dimen.horizontal_product_image_height);
            //listItemImageIv.setMaxHeight(heightPixelValue);
            listItemPriceTv = (AppCompatTextView) itemView.findViewById(R.id.listItemPriceTv);
            listItemOldPriceTv = (AppCompatTextView) itemView.findViewById(R.id.listItemOldPriceTv);
            listItemNameTv = (AppCompatTextView) itemView.findViewById(R.id.listItemNameTv);
            listItemRatingTv = (AppCompatTextView) itemView.findViewById(R.id.listItemRatingTv);
            listItemOfferLabelTv = (AppCompatTextView) itemView.findViewById(R.id.listItemOfferLabelTv);
            listItemRatingLl = itemView.findViewById(R.id.listItemRatingLl);
            listItemRatingBar = (AppCompatRatingBar) itemView.findViewById(R.id.listItemRatingBar);

            loveIv.setOnClickListener(this);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Product product = mData.get(getAdapterPosition());
            if(view.getId() == itemView.getId()){
                Intent intent = new Intent(mContext, DetailActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(Constant.PRODUCT_ID, product.getEntityId());
                mContext.startActivity(intent);
            }else if(view.getId() == R.id.loveIv){
                listener.setOnItemClickListener(product, getAdapterPosition());
            }
        }
    }
}