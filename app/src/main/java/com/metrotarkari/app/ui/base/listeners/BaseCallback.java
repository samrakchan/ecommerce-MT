package com.metrotarkari.app.ui.base.listeners;


/**
 * Created by ahmedeltaher on 3/22/17.
 */

public interface BaseCallback {
    void onSuccess(Object objects);

    void onFail();
}
