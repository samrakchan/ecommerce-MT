package com.metrotarkari.app.ui.order.orderlist.mvp;

import android.support.v7.app.AppCompatActivity;

import com.metrotarkari.app.application.network.DoneNetwork;
import com.metrotarkari.app.ui.order.orderlist.model.OrderListResponse;

import io.reactivex.Observable;


/**
 * Created by samrakchan on 4/30/17.
 */

public class OrderListModel {
    private final AppCompatActivity activity;
    private DoneNetwork network;


    public OrderListModel(AppCompatActivity activity, DoneNetwork network){
        this.activity = activity;
        this.network = network;
    }

    public Observable<OrderListResponse> getOrder(){
        return network.getCustomerOrders();
    }

}
