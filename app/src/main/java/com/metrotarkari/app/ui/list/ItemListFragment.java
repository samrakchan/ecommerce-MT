package com.metrotarkari.app.ui.list;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metrotarkari.app.application.DoneApplication;
import com.metrotarkari.app.ui.list.dagger.DaggerItemListComponent;
import com.metrotarkari.app.ui.list.dagger.ItemListModule;
import com.metrotarkari.app.ui.list.mvp.ItemListPresenter;
import com.metrotarkari.app.ui.list.mvp.ItemListView;

import java.util.HashMap;

import javax.inject.Inject;

/**
 * Created by samrakchan on 6/6/17.
 */

public class ItemListFragment extends Fragment {

    @Inject
    ItemListView itemListView;

    @Inject
    ItemListPresenter itemListPresenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        Bundle bundle = getArguments();
        String catId = bundle.getString("ID");
        String search = bundle.getString("SEARCH");

        HashMap<String, String> map = new HashMap<>();
        if(catId!=null) {
            map.put("filter[cat_id]", catId);
        }
        if(search!=null) {
            map.put("filter[q]", search);
        }


        DaggerItemListComponent.builder().appComponent(DoneApplication.get(this).component())
                .itemListModule(new ItemListModule(this, map)).build().inject(this);


       /* if(catId!=null) {
            //filterQueryWithPagination.setItemParams();
            itemListPresenter.getCategories(catId);
        }*/
        /*else if(search !=null){
            itemListPresenter.searchProduct(search);
        }*/

        return itemListView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }
}
