package com.metrotarkari.app.ui.home.mvp;

import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.metrotarkari.app.R;
import com.metrotarkari.app.application.DoneApplication;
import com.metrotarkari.app.application.network.NoConnectivityException;
import com.metrotarkari.app.db.DoneDbAdapter;
import com.metrotarkari.app.event.CartEvent;
import com.metrotarkari.app.model.Error;
import com.metrotarkari.app.model.Home;
import com.metrotarkari.app.model.HomeCategories;
import com.metrotarkari.app.model.HomeCategory;
import com.metrotarkari.app.model.Home_;
import com.metrotarkari.app.model.Homebanners;
import com.metrotarkari.app.model.Homeproductlists;
import com.metrotarkari.app.utils.ViewUtils;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by samrakchan on 4/27/17.
 */

public class HomePresenter {
    private static final String TAG = HomePresenter.class.getSimpleName();
    private final HomeView view;
    private final HomeModel model;
    private final DoneDbAdapter doneDbAdapter;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private EventBus bus = EventBus.getDefault();

    public HomePresenter(HomeView view, HomeModel model, DoneDbAdapter doneDbAdapter){
        this.view = view;
        this.model = model;
        this.doneDbAdapter = doneDbAdapter;

        compositeDisposable.add(view.getSwipeViewObservable().subscribe(o -> {
            getHomeItems();
        }));
    }

    public void getHomeItems(){
        //determine how many items you want to pull category of home.
        //smaller device will pull 4 where as larger will pull more.

        //We are set for pulling 2 rows, if 2 item in column then its total 4 items
        //if 3 rows and 2 items per column then its 6 items in total

        compositeDisposable.add(getHome(2*ViewUtils.calculateNoOfColumns(view.getContext())));
    }

    private Disposable getHome(int limit){
        view.clearItems();
        view.showProgressBar(true);
        view.showNoItem(false);
        view.showNoInternetConnection(false);
        view.hideSwipeView();
        return model.getHome(limit).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(home -> {

                    displayItems(home);

                }, throwable -> {
                    //view.showNoItem(true);
                    if(throwable instanceof NoConnectivityException){

                        view.showNoInternetConnection(true);

                        doneDbAdapter.openReadableDb();
                        String dbHomeData = doneDbAdapter.getHome();
                        doneDbAdapter.closeDb();

                        if(dbHomeData!=null && dbHomeData.length()>0){
                            Gson gson = new Gson();
                            Home home = gson.fromJson(dbHomeData, Home.class);
                            displayItems(home);
                        }else{
                            view.showNoItem(true);
                        }
                    }else{
                        view.showProgressBar(false);
                        view.showNoItem(false);
                        Toast.makeText(view.getContext(), R.string.sww,Toast.LENGTH_LONG).show();
                        throwable.printStackTrace();
                    }
                });
    }

    private void displayItems(Home home){
        doneDbAdapter.openWritableDb();
        Gson gson = new Gson();
        doneDbAdapter.insetIntoHome(gson.toJson(home));
        doneDbAdapter.closeDb();

        view.showProgressBar(false);
        Home_ home_ = home.getHome();
        List<Error> errorResponse = home.getErrors();

        if(errorResponse!=null && !errorResponse.isEmpty()){
            Error error = errorResponse.get(0);
            Toast.makeText(view.getContext(),error.getMessage()+"",Toast.LENGTH_LONG).show();
        }else if(home_ !=null ) {

            Homebanners homebanners = home_.getHomebanners();

            if(homebanners.getCarTotal()!=null) {
                //EventBus function to notify card items

                Log.i(TAG, TAG+" "+homebanners.getCarTotal());

                CartEvent cartEvent = new CartEvent(homebanners.getCarTotal());
                bus.post(cartEvent);

                if(DoneApplication.getInstance() !=null) {
                    DoneApplication.getInstance().setCartCount(homebanners.getCarTotal());
                }
            }

            view.addBanner(homebanners);
            HomeCategories homeCategories = home_.getHomecategories();

            if (homeCategories != null && homeCategories.getHomecategories() != null) {
                List<HomeCategory> homeCategory = homeCategories.getHomecategories();
                view.addCategoryAndItsItems(homeCategory);
            }

            Homeproductlists homeproductlists = home_.getHomeproductlists();

            if(homeproductlists!=null && homeproductlists.getHomeproductlists()!=null){
                view.addPopularCategoryAndItems(homeproductlists.getHomeproductlists());
            }
        }else {
            view.showNoItem(true);
        }
    }
}
