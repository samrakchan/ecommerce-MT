package com.metrotarkari.app.ui.reviewandratings.reviewpost;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.metrotarkari.app.application.DoneApplication;
import com.metrotarkari.app.ui.base.BaseActivity;
import com.metrotarkari.app.ui.reviewandratings.reviewpost.dragger.DaggerReviewPostComponent;
import com.metrotarkari.app.ui.reviewandratings.reviewpost.dragger.ReviewPostModule;
import com.metrotarkari.app.ui.reviewandratings.reviewpost.mvp.ReviewPostPresenter;
import com.metrotarkari.app.ui.reviewandratings.reviewpost.mvp.ReviewPostView;

import javax.inject.Inject;

/**
 * Created by Avinash on 8/8/2017.
 */

public class ReviewPostActivity extends BaseActivity {

    @Inject
    ReviewPostView view;

    @Inject
    ReviewPostPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DaggerReviewPostComponent.builder().appComponent(DoneApplication.get(this).component())
                .reviewPostModule(new ReviewPostModule(this)).build().inject(this);

        setContentView(view);
        presenter.onCreate();
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }
}
