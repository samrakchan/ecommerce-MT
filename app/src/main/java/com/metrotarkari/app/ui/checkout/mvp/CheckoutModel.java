package com.metrotarkari.app.ui.checkout.mvp;

import com.metrotarkari.app.application.network.DoneNetwork;
import com.metrotarkari.app.model.Order;
import com.metrotarkari.app.model.OrderResponse;
import com.metrotarkari.app.ui.checkout.model.CheckOutOrder;

import java.util.HashMap;

import io.reactivex.Observable;

/**
 * Created by samrakchan on 4/27/17.
 */

public class CheckoutModel {
    private final DoneNetwork doneNetwork;

    public CheckoutModel(DoneNetwork doneNetwork){
        this.doneNetwork = doneNetwork;
    }


    public Observable<Order> postOrder(CheckOutOrder postOrder){
        return doneNetwork.postOrders(postOrder);
    }

    public Observable<OrderResponse> getCheckoutInfos(HashMap<String, Object> items){
        return doneNetwork.getCheckoutInfos(items);
    }
}
