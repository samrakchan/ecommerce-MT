package com.metrotarkari.app.ui.base.mvp;

import io.reactivex.Observable;
import rx.functions.Func1;

/**
 * Created by samrakchan on 5/7/17.
 */

public class BasePresenter {

    private <T> Func1<Throwable,? extends Observable<? extends T>> refreshTokenAndRetry(final Observable<T> toBeResumed) {
        return new Func1<Throwable, Observable<? extends T>>() {
            @Override
            public Observable<? extends T> call(Throwable throwable) {
                // Here check if the error thrown really is a 401
              /*  if (isHttp401Error(throwable)) {
                    return refreshToken().flatMap(new Func1<AuthToken, Observable<? extends T>>() {
                        @Override
                        public Observable<? extends T> call(AuthToken token) {
                            return toBeResumed;
                        }
                    });
                }*/
                // re-throw this error because it's not recoverable from here
                return Observable.error(throwable);
            }
        };
    }
}
