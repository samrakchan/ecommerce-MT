package com.metrotarkari.app.model;

import java.util.List;

/**
 * Created by samrakchan on 3/17/17.
 */

public class CategoryItems {
    private List<CategoryItem> categoryItemList;

    public List<CategoryItem> getCategoryItemList() {
        return categoryItemList;
    }

    public void setCategoryItemList(List<CategoryItem> categoryItemList) {
        this.categoryItemList = categoryItemList;
    }
}
