package com.metrotarkari.app.ui.signup;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.crashlytics.android.Crashlytics;
import com.metrotarkari.app.application.DoneApplication;
import com.metrotarkari.app.ui.signup.dagger.DaggerSignupComponent;
import com.metrotarkari.app.ui.signup.dagger.SignupModule;
import com.metrotarkari.app.ui.signup.mvp.SignupPresenter;
import com.metrotarkari.app.ui.signup.mvp.SignupView;

import javax.inject.Inject;

import io.fabric.sdk.android.Fabric;

/**
 * Created by samrakchan on 5/19/17.
 */

public class SignupActivity extends AppCompatActivity {

    @Inject
    SignupView view;

    @Inject
    SignupPresenter presenter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());


        DaggerSignupComponent.builder()
                .appComponent(DoneApplication.get(this).component())
                .signupModule(new SignupModule(this))
                .build().inject(this);

        setContentView(view);

        presenter.onCreate();

    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

}
