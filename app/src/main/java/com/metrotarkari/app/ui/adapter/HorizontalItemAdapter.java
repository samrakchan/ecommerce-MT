package com.metrotarkari.app.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.metrotarkari.app.R;
import com.metrotarkari.app.model.Product;
import com.metrotarkari.app.ui.productdetail.DetailActivity;
import com.metrotarkari.app.utils.Constant;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by samrakchan on 6/28/17.
 */

public class HorizontalItemAdapter extends RecyclerView.Adapter<HorizontalItemAdapter.CategoryItemList> {
    private List<Product> mData;
    private Context mContext;

    public HorizontalItemAdapter(Context context) {
        mData = new ArrayList<>();
        this.mContext = context;
    }

    public void addData(List<Product> datas) {
        if(datas!=null) {
            this.mData.addAll(datas);
            notifyDataSetChanged();
        }
    }

    public void removeAllData() {
        mData.clear();
        notifyDataSetChanged();
    }


    @Override
    public HorizontalItemAdapter.CategoryItemList onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_item_horizontal_container, parent, false);
            return new HorizontalItemAdapter.CategoryItemList(view);
    }

    @Override
    public void onBindViewHolder(HorizontalItemAdapter.CategoryItemList holder, int position) {
        Product categoryItem = mData.get(position);
        holder.listItemNameTv.setText(categoryItem.getName());

        //String weightValue = categoryItem.getMetroWeightTypeValue()!=null?"/"+ categoryItem.getMetroWeightTypeValue():"";

        Log.i("FORMATTED_PRICE", ""+categoryItem.getFormattedFinalPrice()+" "+categoryItem.getFormattedPrice());

       // holder.listItemPriceTv.setText(mContext.getString(R.string.price, Utility.convertToTwoDecimalPlace(categoryItem.getFinalPrice()))+weightValue);
        holder.listItemPriceTv.setText(categoryItem.getFormattedFinalPrice());
        if(categoryItem.getPrice().equalsIgnoreCase(categoryItem.getFinalPrice())){
            holder.listItemOldPriceTv.setVisibility(View.GONE);
        }else{
            holder.listItemOldPriceTv.setPaintFlags(holder.listItemOldPriceTv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
          //  holder.listItemOldPriceTv.setText(mContext.getString(R.string.price, Utility.convertToTwoDecimalPlace(categoryItem.getPrice())));
            holder.listItemOldPriceTv.setText(categoryItem.getFormattedPrice());
            holder.listItemOldPriceTv.setVisibility(View.VISIBLE);
        }

        if(categoryItem.getDiscountLabel() !=null){
            holder.listItemOfferLabelTv.setText(categoryItem.getDiscountLabel());
            holder.listItemOfferLabelTv.setVisibility(View.VISIBLE);
        }else{
            holder.listItemOfferLabelTv.setVisibility(View.GONE);
        }

        if (categoryItem.getImages() != null && !categoryItem.getImages().isEmpty())
            Glide.with(mContext).load(categoryItem.getImages().get(0).getUrl()).placeholder(R.mipmap.ic_placeholder_mt).dontAnimate().centerCrop().into(holder.listItemImageIv);

//        Log.i("IMG", categoryItem.getImages().get(0).getUrl() + "");

    }
    @Override
    public int getItemCount() {
        return mData.size();
    }

    protected class CategoryItemList extends RecyclerView.ViewHolder {

        private AppCompatImageView listItemImageIv;
        private AppCompatTextView listItemPriceTv;
        private AppCompatTextView listItemOldPriceTv;
        private AppCompatTextView listItemNameTv;
        private AppCompatTextView listItemOfferLabelTv;

        public CategoryItemList(View itemView) {
            super(itemView);

            listItemImageIv = (AppCompatImageView) itemView.findViewById(R.id.listItemImageIv);
            //int heightPixelValue = (int) mContext.getResources().getDimension(R.dimen.horizontal_product_image_height);
            //listItemImageIv.setMaxHeight(heightPixelValue);
            listItemPriceTv = (AppCompatTextView) itemView.findViewById(R.id.listItemPriceTv);
            listItemOldPriceTv = (AppCompatTextView) itemView.findViewById(R.id.listItemOldPriceTv);
            listItemNameTv = (AppCompatTextView) itemView.findViewById(R.id.listItemNameTv);
            listItemOfferLabelTv = (AppCompatTextView) itemView.findViewById(R.id.listItemOfferLabelTv);

            itemView.setOnClickListener(v -> {
                Product product = mData.get(getAdapterPosition());
                Intent intent = new Intent(mContext, DetailActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                intent.putExtra(Constant.PRODUCT_ID, product.getEntityId());
                mContext.startActivity(intent);
            });
        }
    }
}