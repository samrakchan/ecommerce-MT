package com.metrotarkari.app.ui.addressbook.addressentry.dagger;

import android.support.v4.app.Fragment;

import com.metrotarkari.app.application.network.DoneNetwork;
import com.metrotarkari.app.db.DoneDbAdapter;
import com.metrotarkari.app.ui.addressbook.addressentry.mvp.AddressEntryModel;
import com.metrotarkari.app.ui.addressbook.addressentry.mvp.AddressEntryPresenter;
import com.metrotarkari.app.ui.addressbook.addressentry.mvp.AddressEntryView;

import dagger.Module;
import dagger.Provides;

@Module
public class AddAddressModule {

  private final Fragment fragment;

  public AddAddressModule(Fragment fragment) {
    this.fragment = fragment;
  }

  @Provides
  @AddAddressScope
  public AddressEntryView view() {
    return new AddressEntryView(fragment);
  }

  @Provides
  @AddAddressScope
  public AddressEntryModel model(DoneNetwork doneNetwork){
    return new AddressEntryModel(fragment, doneNetwork);
  }

  @Provides
  @AddAddressScope
  public AddressEntryPresenter presenter(AddressEntryView homeView, AddressEntryModel model, DoneDbAdapter doneDbAdapter) {
    return new AddressEntryPresenter(homeView, model, doneDbAdapter);
  }

}
