package com.metrotarkari.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by samrakchan on 5/29/17.
 */

public class AppReviews {
    @SerializedName("rate")
    @Expose
    private Double rate;
    @SerializedName("number")
    @Expose
    private Integer number;
    @SerializedName("5_star_number")
    @Expose
    private Integer _5StarNumber;
    @SerializedName("4_star_number")
    @Expose
    private Integer _4StarNumber;
    @SerializedName("3_star_number")
    @Expose
    private Integer _3StarNumber;
    @SerializedName("2_star_number")
    @Expose
    private Integer _2StarNumber;
    @SerializedName("1_star_number")
    @Expose
    private Integer _1StarNumber;

    @SerializedName("form_add_reviews")
    @Expose
    private List<FormAddReview> formAddReviews = null;
    @SerializedName("review_detail")
    @Expose
    private ReviewDetail reviewDetail;

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Integer get5StarNumber() {
        return _5StarNumber;
    }

    public void set5StarNumber(Integer _5StarNumber) {
        this._5StarNumber = _5StarNumber;
    }

    public Integer get4StarNumber() {
        return _4StarNumber;
    }

    public void set4StarNumber(Integer _4StarNumber) {
        this._4StarNumber = _4StarNumber;
    }

    public Integer get3StarNumber() {
        return _3StarNumber;
    }

    public void set3StarNumber(Integer _3StarNumber) {
        this._3StarNumber = _3StarNumber;
    }

    public Integer get2StarNumber() {
        return _2StarNumber;
    }

    public void set2StarNumber(Integer _2StarNumber) {
        this._2StarNumber = _2StarNumber;
    }

    public Integer get1StarNumber() {
        return _1StarNumber;
    }

    public void set1StarNumber(Integer _1StarNumber) {
        this._1StarNumber = _1StarNumber;
    }

    public List<FormAddReview> getFormAddReviews() {
        return formAddReviews;
    }

    public void setFormAddReviews(List<FormAddReview> formAddReviews) {
        this.formAddReviews = formAddReviews;
    }

    public ReviewDetail getReviewDetail() {
        return reviewDetail;
    }

    public void setReviewDetail(ReviewDetail reviewDetail) {
        this.reviewDetail = reviewDetail;
    }


}
