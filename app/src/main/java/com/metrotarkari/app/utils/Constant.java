package com.metrotarkari.app.utils;

/**
 * Created by samrakchan on 4/23/17.
 */

public class Constant {

    //private static final String BASE = "http://metro.donecommerce.com/";
    public static final String BASE = "https://metrotarkari.com/";

    public static final String BASE_URL =BASE+"doneconnector/rest/";
   // public static final String BASE_URL ="http://metropasal.com/doneconnector/rest/";
   // public static final String BASE_URL ="http://202.51.74.225/doneconnector/rest/";

    //public static final String IMG_BASE_URL = "http://metropasal.com/pub/media/catalog/product/";

    public static final String SHARE_URL= BASE+"catalog/product/view/id/";
    public static final String FORGOT_PASSWORD = BASE+"customer/account/createPassword/";

   // https://axy36.app.goo.gl/?link=http://metro.donecommerce.com/customer/account/createPassword/id/1556/token/28532f887d523c2aefd2b6f92cf06a65/&apn=com.metrotarkari.app

    public static final String DEEP_LINK_PREFIX = "https://axy36.app.goo.gl?link=";
    public static final String DEEP_LINK_SUFFIX = "&apn=com.metrotarkari.app";


    public static String FACEBOOK_URL = "https://www.facebook.com/metrotarkari/";
    public static String FACEBOOK_PAGE_ID = "metrotarkari";


    public static final String PASSWORD_VALIDATION_REG_EXP = "^.{8,}$";
    public static final String PASSWORD_VALIDATION_LOGIN_REG_EXP = "^.{1,}$";
    public static final String NAME_VALIDATION_REG_EXP = "[a-zA-Z\\s]+";
    public static final String PHONE_NUMBER_VALIDATION_REG_EXP = "[+-]?[0-9]{10,14}";
    public static final String AT_LEAST_ONE_CHAR = "[a-zA-Z\\s]+";

    public static final String EMPTY_MESSAGE_VAL_REG_EXP = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*(_|[^\\w])).+$";

    public static final String APP_TOKEN ="13c93fa4e122c8e6ec2da81d4273f16d";

    public static final String ID = "ID";
    public static final String PRODUCT_ID = "PRODUCT_ID";

    public static final String PRODUCT_DETAIL = "PRODUCT_DETAIL";


    public static final String TITLE = "TITLE";
    public static final String URL = "URL";

    public static final int CHECKOUT_ACTIVITY_REQUEST_CODE = 11;
    public static final int CHECKOUT_ACTIVITY_FROM_SIGNUP_REQUEST_CODE = 12;

    public static final String INTENT_FOR = "INTENT_FOR";
    public static final int INTENT_FROM_DRAWER = 23;

    public static final int ADD_ADDRESS = 1;
    public static final int EDIT_ADDRESS = 2;


    public static final String IS_FROM_ADDRESS_BOOK_OR_ADDRESS_ENTRY = "ISFROMADDRESSBOOKORADDRESSENTRY";
    public static final int FROM_ADDRESS_ENTRY = 9;

    public static final int CHANGE_DATE_AND_TIME = 23;
    public static final int IS_FOR_SET_MESSAGE = 25;


    public static final String ADD_ADDRESS_DETAIL = "ADDRESSDETAIL";
    public static final String FCM_BROADCAST = "FCM_BROADCAST";
    public static final String FCM_TOKEN = "FCM_TOKEN";
    public static final int CHECKOUT = 3;
    public static final int ADDRESS_LISTING_ONLY = 4;
    public static final String ADDRESS_ID = "ADDRESS_ID";

    public static final String KEY_VALUE_DATA_ID = "KEYVALUEDATA";

    public static final String SELECTED_DATE = "DATE";
    public static final String SELECTED_TIME = "TIME";

    public static final String IS_FOR_MESSAGE = "MESSAGE";

    public static final int SELECT_ADDRESS = 10;

    public static final String COUPON_CODE = "COUPON_CODE";
    public static final String DATA = "DATA";


}
