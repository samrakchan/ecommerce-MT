
package com.metrotarkari.app.ui.checkout.model;


public class CheckOutOrder {

    private BAddress b_address;
    private SAddress s_address;
    private SMethod s_method;
    private PMethod p_method;
    private String delivery_date;
    private String delivery_time;
    private double orderAmount;

    public BAddress getBAddress() {
        return b_address;
    }

    public void setBAddress(BAddress bAddress) {
        this.b_address = bAddress;
    }

    public SAddress getSAddress() {
        return s_address;
    }

    public void setSAddress(SAddress sAddress) {
        this.s_address = sAddress;
    }

    public SMethod getSMethod() {
        return s_method;
    }

    public void setSMethod(SMethod sMethod) {
        this.s_method = sMethod;
    }

    public PMethod getPMethod() {
        return p_method;
    }

    public void setPMethod(PMethod pMethod) {
        this.p_method = pMethod;
    }

    public String getDeliveryDate() {
        return delivery_date;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.delivery_date = deliveryDate;
    }

    public String getDeliveryTime() {
        return delivery_time;
    }

    public void setDeliveryTime(String deliveryTime) {
        this.delivery_time = deliveryTime;
    }

    public double getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(double orderAmount) {
        this.orderAmount = orderAmount;
    }
}
