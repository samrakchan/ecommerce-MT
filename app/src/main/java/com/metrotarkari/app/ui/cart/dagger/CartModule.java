package com.metrotarkari.app.ui.cart.dagger;

import com.metrotarkari.app.application.network.DoneNetwork;
import com.metrotarkari.app.db.DoneDbAdapter;
import com.metrotarkari.app.ui.cart.CartActivity;
import com.metrotarkari.app.ui.cart.mvp.CartModel;
import com.metrotarkari.app.ui.cart.mvp.CartPresenter;
import com.metrotarkari.app.ui.cart.mvp.CartView;
import com.metrotarkari.app.utils.DoneUserPreferenceManager;

import dagger.Module;
import dagger.Provides;

@Module
public class CartModule {

  private final CartActivity activity;

  public CartModule(CartActivity activity) {
    this.activity = activity;
  }

  @Provides
  @CartScope
  public CartView view() {
    return new CartView(activity);
  }

  @Provides
  @CartScope
  public CartModel model(DoneNetwork doneNetwork){
    return new CartModel(activity, doneNetwork);
  }

  @Provides
  @CartScope
  public CartPresenter presenter(CartView homeView, CartModel model, DoneUserPreferenceManager preferenceManager, DoneDbAdapter dbAdapterd) {
   /* Intent intent = activity.getIntent();
    boolean intentSource= false;
    if (intent != null){

      intentSource = intent.getBooleanExtra(Constant.CALLED_FROM_CHECKOUT,false);
    }*/
    return new CartPresenter(homeView, model, preferenceManager,activity,dbAdapterd);
  }

}
