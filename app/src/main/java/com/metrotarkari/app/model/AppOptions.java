package com.metrotarkari.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by samrakchan on 5/29/17.
 */

public class AppOptions {
    @SerializedName("custom_options")
    @Expose
    private List<CustomOption> customOptions = null;

    public List<CustomOption> getCustomOptions() {
        return customOptions;
    }

    public void setCustomOptions(List<CustomOption> customOptions) {
        this.customOptions = customOptions;
    }
}
