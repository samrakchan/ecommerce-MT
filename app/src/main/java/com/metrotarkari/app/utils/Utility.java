package com.metrotarkari.app.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.util.DisplayMetrics;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicLong;

import static java.security.AccessController.getContext;

/**
 * Created by samrakchan on 6/5/17.
 */

public class Utility {

    private static final AtomicLong LAST_TIME_MS = new AtomicLong();
    private static SecureRandom random = new SecureRandom();


    public static String getSessionId() {
        return new BigInteger(130, random).toString(32);
    }

    public static long getContentValue() {
        long now = System.currentTimeMillis();
        while(true) {
            long lastTime = LAST_TIME_MS.get();
            if (lastTime >= now)
                now = lastTime+1;
            if (LAST_TIME_MS.compareAndSet(lastTime, now))
                return now;
        }
    }

    public static String convertToDecimalPlace(String value, int decimalPlace){
        if(value!=null) {
            try {
                String result = value.substring(0, value.indexOf(".") + (decimalPlace+1));
                return result;
            }catch (ArrayIndexOutOfBoundsException | StringIndexOutOfBoundsException e){
                e.printStackTrace();
                return "";
            }
        }else{
            return "";
        }
    }

    public static void openFacebook(Activity context){
        if (Utility.isAppInstalled(context,"com.facebook.katana")) {
            Intent facebookIntent1 = new Intent(Intent.ACTION_VIEW);
            String facebookUrl = Utility.getFacebookPageURL(context);
            facebookIntent1.setData(Uri.parse(facebookUrl));
            context.startActivity(facebookIntent1);
        }else {
            Intent facebookIntent = Utility.openFacebookIntent(context);
            context.startActivity(facebookIntent);
        }
    }

    public static boolean isAppInstalled(Context context, String packageName) {
        try {
            context.getPackageManager().getApplicationInfo(packageName, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public static Intent openFacebookIntent(Context context) {
        try {
            context.getPackageManager()
                    .getPackageInfo("com.facebook.katana", 0); //Checks if FB is even installed.
            return new Intent(Intent.ACTION_VIEW,
                    Uri.parse("fb://profile/254175194653125")); //Try to make intent with FB's URI
        } catch (Exception e) {
            return new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://www.facebook.com/metrotarkari/")); //catches and opens a url to the desired page
        }
    }

    public static String getFacebookPageURL(Context context) {
        PackageManager packageManager = context.getPackageManager();
        try {
            int versionCode = packageManager.getPackageInfo("com.facebook.katana", 0).versionCode;
            if (versionCode >= 3002850) { //newer versions of fb app
                return "fb://facewebmodal/f?href=" + Constant.FACEBOOK_URL;
            } else { //older versions of fb app
                return "fb://page/" + Constant.FACEBOOK_PAGE_ID;
            }
        } catch (PackageManager.NameNotFoundException e) {
            return Constant.FACEBOOK_URL; //normal web url
        }
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    public static int GetDipsFromPixel(float pixels,Context mContext)
    {
        // Get the screen's density scale
        final float scale = mContext.getResources().getDisplayMetrics().density;
        // Convert the dps to pixels, based on density scale
        return (int) (pixels * scale + 0.5f);
    }


    public static String getDoubleDigit(int value){
        DecimalFormat mFormat= new DecimalFormat("00");
        return mFormat.format(Double.valueOf(value));
    }



    public static int dpToPx(int dp, Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }


    public static String convertDateAndTime(String date){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat output = new SimpleDateFormat("MMM dd, yyyy");
        Date d = null;
        try {
            d = sdf.parse(date);
            return output.format(d);
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (NullPointerException e){
            e.printStackTrace();
        }
        return "";
    }


    public static String convertDateAndTimeForOrderList(String date){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat output = new SimpleDateFormat("MMM dd, yyyy HH:mm");
        Date d = null;
        try {
            d = sdf.parse(date);
            return output.format(d);
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (NullPointerException e){
            e.printStackTrace();
        }
        return "";
    }


    public static String convertDateAndTimeForReviewList(String date){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat output = new SimpleDateFormat("MMM dd, yyyy");
        Date d = null;
        try {
            d = sdf.parse(date);
            return output.format(d);
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (NullPointerException e){
            e.printStackTrace();
        }
        return "";
    }



}
