package com.metrotarkari.app.model;

import com.metrotarkari.app.model.ItemParams;

import java.util.HashMap;

/**
 * Created by samrakchan on 6/11/17.
 */

public class FilterQueryWithPagination {
    private HashMap<String, String> queryValue;
    private ItemParams itemParams;

    public HashMap<String, String> getQueryValue() {
        return queryValue;
    }

    public void setQueryValue(HashMap<String, String> queryValue) {
        this.queryValue = queryValue;
    }

    public ItemParams getItemParams() {
        return itemParams;
    }

    public void setItemParams(ItemParams itemParams) {
        this.itemParams = itemParams;
    }

}
