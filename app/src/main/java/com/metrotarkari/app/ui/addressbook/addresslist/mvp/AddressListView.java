package com.metrotarkari.app.ui.addressbook.addresslist.mvp;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.metrotarkari.app.R;
import com.metrotarkari.app.model.BAddress;
import com.metrotarkari.app.model.PostOrder;
import com.metrotarkari.app.ui.addressbook.addressentry.AddressEntryActivity;
import com.metrotarkari.app.ui.base.BaseView;
import com.metrotarkari.app.utils.Constant;
import com.metrotarkari.app.utils.DoneItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cc.cloudist.acplibrary.ACProgressFlower;
import io.reactivex.subjects.BehaviorSubject;

/**
 * Created by samrakchan on 4/27/17.
 */
public class AddressListView extends BaseView implements DoneItemClickListener{

    @BindView(R.id.toolbar)
    public Toolbar toolbar;

    @BindView(R.id.imageMsgRl)
    public View noItemView;

    @BindView(R.id.progressBarRl)
    public View progressBar;

    @Nullable
    @BindView(R.id.addressBookRv)
    public RecyclerView addressBookRv;

    private AddressListAdapter addressBookAdapter;

    List<Object> objectList;

    private final BehaviorSubject<BAddress> deleteAddressBookBehaviour = BehaviorSubject.create();

    private BAddress address;

    private int intentFor = -1;

    private AppCompatActivity activity;

    private final ACProgressFlower progressDialog = new ACProgressFlower.Builder(getContext())
            .themeColor(Color.WHITE)
            .fadeColor(Color.DKGRAY).build();

    public AddressListView(AppCompatActivity activity){
        super(activity);
        this.activity = activity;

        Intent intent = activity.getIntent();

        //intentFor can be Constant.SELECT_ADDRESS
        intentFor = intent.getIntExtra(Constant.INTENT_FOR ,-1);

        ButterKnife.bind(this);

        objectList = new ArrayList<>();

        addressBookRv = (RecyclerView) findViewById(R.id.addressBookRv);

        activity.setSupportActionBar(toolbar);
        activity.getSupportActionBar().setTitle(R.string.address_book);


        addressBookRv.setLayoutManager(new LinearLayoutManager(getContext()));
        addressBookAdapter = new AddressListAdapter(getContext(),this);
        addressBookRv.setAdapter(addressBookAdapter);

        }

    @Override
    public int getContentLayout() {
        return R.layout.activity_address_book;
    }

    //BAddress data add to the addressBookAdapter
    public void getAddressBookData(List<BAddress> addresses){
        addressBookAdapter.addData(addresses);

    }

    public void showNoItem(boolean showItem){
        if(showItem)
            noItemView.setVisibility(View.VISIBLE);
        else
            noItemView.setVisibility(View.GONE);
    }

    public void showProgressBar(boolean showing){
        if(showing)
            progressBar.setVisibility(View.VISIBLE);
        else
            progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onItemClickListener(int position, int viewId, Object object) {
        address = (BAddress) object;
        PostOrder postOrder = new PostOrder();
        postOrder.setBAddress(address);

        if (viewId == R.id.customerAddressBookDeleteBtn) {
            //to delete address
            AlertDialog.Builder builder = new AlertDialog.Builder(activity)
                    .setMessage(R.string.delete_address)
                    .setCancelable(true)
                    .setPositiveButton(R.string.ok, (dialogInterface, i)->{
                        deleteAddressBookBehaviour.onNext(address);
                    })
                    .setNegativeButton(R.string.cancel, ((dialogInterface, i) -> {
                        dialogInterface.dismiss();
                    }));
            builder.create();
            builder.show();

        } else if (viewId == R.id.customerAddressBookEditBtn) {
            //to edit address
            Intent intent = new Intent(getContext(), AddressEntryActivity.class);
            intent.putExtra(Constant.INTENT_FOR,Constant.EDIT_ADDRESS);
            intent.putExtra(Constant.ADD_ADDRESS_DETAIL,postOrder);
            getContext().startActivity(intent);
            activity.finish();

        } else if (viewId == R.id.addressBookCardView) {

            if(intentFor == Constant.SELECT_ADDRESS){
                Intent intent = new Intent();
                intent.putExtra(Constant.ID, address.getEntityId());
                activity.setResult(Activity.RESULT_OK, intent);
                activity.finish();
            }
        }
    }

    public void showLoading(boolean loading) {
        if (loading) {
            progressDialog.setCancelable(false);
            progressDialog.show();
        } else {
            progressDialog.dismiss();
        }
    }


    public BehaviorSubject<BAddress> deleteAddressBookBehaviour(){
        return deleteAddressBookBehaviour;
    }

}
