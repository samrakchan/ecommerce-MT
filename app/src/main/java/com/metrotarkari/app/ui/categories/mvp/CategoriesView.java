package com.metrotarkari.app.ui.categories.mvp;

import android.content.Intent;
import android.content.res.Configuration;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;

import com.metrotarkari.app.R;
import com.metrotarkari.app.model.ChildCategory;
import com.metrotarkari.app.ui.base.BaseViewFragment;
import com.metrotarkari.app.ui.categories.CategoriesAdapter;
import com.metrotarkari.app.ui.categories.CategoriesFragment;
import com.metrotarkari.app.ui.list.ItemListActivity;
import com.metrotarkari.app.utils.AlertUtils;
import com.metrotarkari.app.utils.Constant;
import com.metrotarkari.app.utils.DoneItemClickListener;
import com.metrotarkari.app.utils.ViewUtils;

import java.util.List;

import butterknife.BindView;
import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;

/**
 * Created by samrakchan on 4/27/17.
 */


public class CategoriesView extends BaseViewFragment implements DoneItemClickListener {

    private CategoriesFragment fragment;

    @BindView(R.id.imageMsgRl)
    public View noItemView;

    @BindView(R.id.progressBarRl)
    public View progressBar;

    @BindView(R.id.swipeRefreshView)
    public SwipeRefreshLayout swipeRefreshLayout;

    @Nullable
    @BindView(R.id.mainRecyclerView)
    public RecyclerView mainRecyclerView;

    private CategoriesAdapter mMainAdapter;

    private StaggeredGridLayoutManager staggeredGridLayoutManager;

    private final BehaviorSubject<Object> swipeViewBehaviour = BehaviorSubject.create();


    public CategoriesView(CategoriesFragment fragment){
        super(fragment);
        this.fragment = fragment;

        staggeredGridLayoutManager = new StaggeredGridLayoutManager(ViewUtils.calculateNoOfColumnsDynamically(getContext(),100), StaggeredGridLayoutManager.VERTICAL);
        mainRecyclerView.setLayoutManager(staggeredGridLayoutManager);

        DividerItemDecoration diV = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        diV.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.bg_divider));

        DividerItemDecoration diH = new DividerItemDecoration(getContext(), DividerItemDecoration.HORIZONTAL);
        diH.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.bg_divider));

        mainRecyclerView.addItemDecoration(diV);
        mainRecyclerView.addItemDecoration(diH);

        swipeRefreshLayout.setOnRefreshListener(() -> swipeViewBehaviour.onNext(new Object()));


        mMainAdapter = new CategoriesAdapter(getContext(),this);
        mainRecyclerView.setAdapter(mMainAdapter);
    }

    @Override
    protected void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        staggeredGridLayoutManager.setSpanCount(ViewUtils.calculateNoOfColumns(getContext()));
    }

    public Observable<Object> getSwifpeViewObservable(){
        return swipeViewBehaviour;
    }

    @Override
    public int getContentLayout() {
        return R.layout.fragment_home;
    }

    public void setCategoriesData(List<Object> objects){
        mMainAdapter.addCategoriesItems(objects);

    }

    public void showNoItem(boolean showItem){
        if(showItem)
            noItemView.setVisibility(View.VISIBLE);
        else
            noItemView.setVisibility(View.GONE);
    }

    public void showProgressBar(boolean showing){
        if(showing)
            progressBar.setVisibility(View.VISIBLE);
        else
            progressBar.setVisibility(View.GONE);
    }

    private void showAlert(int positiveBtn, int negativeBtn, int msg, Intent intent){
        AlertUtils.createAlert(fragment.getActivity(), positiveBtn, negativeBtn, msg, intent).show();

    }


    @Override
    public void onItemClickListener(int position, int viewId, Object object) {
        ChildCategory childCategory = (ChildCategory) object;
        Intent intent = new Intent(fragment.getContext(), ItemListActivity.class);
            intent.putExtra(Constant.ID, childCategory.getId());
            intent.putExtra(Constant.TITLE, childCategory.getName());
            fragment.getActivity().startActivity(intent);
    }

    protected void hideSwipeView(){
        if(swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }
}
