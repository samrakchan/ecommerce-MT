package com.metrotarkari.app.ui.about;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metrotarkari.app.R;
import com.metrotarkari.app.generic.BaseFragment;
import com.metrotarkari.app.utils.AlertUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by samrakchan on 4/19/17.
 */

public class AboutFragment extends BaseFragment implements View.OnClickListener {

    @BindView(R.id.categoryContactCallUsBtn)
    public AppCompatButton categoryContactCallUsBtn;

    @BindView(R.id.categoryContactSalesMarketingBtn)
    public AppCompatButton categoryContactSalesMarketingBtn;

    @BindView(R.id.categoryEmailBtn)
    public AppCompatButton categoryEmailBtn;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

            View view = inflater.inflate(R.layout.fragment_about, container, false);

            ButterKnife.bind(this, view);

            categoryContactCallUsBtn.setOnClickListener(this);
            categoryContactSalesMarketingBtn.setOnClickListener(this);
            categoryEmailBtn.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.categoryContactCallUsBtn){
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", getContext().getString(R.string.call_us_value).replace("-",""), null));
            showAlert(R.string.call, R.string.cancel, R.string.make_call_text, intent);

        }else if(view.getId() == R.id.categoryContactSalesMarketingBtn){

            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", getContext().getString(R.string.sales_marketing_value).replace("-",""), null));
            showAlert(R.string.call, R.string.cancel, R.string.make_call_text, intent);

        }else if(view.getId() == R.id.categoryEmailBtn){
            try {
                String uriText =
                        "mailto:" +getContext().getString(R.string.email_value)+
                                "?subject=" + Uri.encode("Hello Metro Tarkari!") +
                                "&body=" + Uri.encode("Hi,");
                Uri uri = Uri.parse(uriText);

                Intent sendIntent = new Intent(Intent.ACTION_SENDTO);
                sendIntent.setData(uri);
                getContext().startActivity(Intent.createChooser(sendIntent, "Send email"));
            }catch (Exception e){
                e.printStackTrace();
            }
        }

    }

    private void showAlert(int positiveBtn, int negativeBtn, int msg, Intent intent){
        AlertUtils.createAlert((Activity) getContext(), positiveBtn, negativeBtn, msg, intent).show();

    }
}
