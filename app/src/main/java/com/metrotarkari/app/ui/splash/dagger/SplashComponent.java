package com.metrotarkari.app.ui.splash.dagger;

import com.metrotarkari.app.application.dagger.AppComponent;
import com.metrotarkari.app.ui.splash.SplashActivity;
import dagger.Component;

@SplashScope
@Component(modules = { SplashModule.class }, dependencies = AppComponent.class)
public interface SplashComponent {

  void inject(SplashActivity mainActivity);

}
