package com.metrotarkari.app.ui.addressbook.addresslist.dragger;

import android.support.v7.app.AppCompatActivity;

import com.metrotarkari.app.application.network.DoneNetwork;
import com.metrotarkari.app.ui.addressbook.addresslist.mvp.AddressListModel;
import com.metrotarkari.app.ui.addressbook.addresslist.mvp.AddressListPresenter;
import com.metrotarkari.app.ui.addressbook.addresslist.mvp.AddressListView;

import dagger.Module;
import dagger.Provides;

@Module
public class AddressBookModule {

  private final AppCompatActivity activity;

  public AddressBookModule(AppCompatActivity activity) {
    this.activity = activity;
  }

  @Provides
  @AddressBookScope
  public AddressListView view() {
    return new AddressListView(activity);
  }

  @Provides
  @AddressBookScope
  public AddressListModel model(DoneNetwork doneNetwork){
    return new AddressListModel(activity, doneNetwork);
  }

  @Provides
  @AddressBookScope
  public AddressListPresenter presenter(AddressListView view, AddressListModel model) {
    return new AddressListPresenter(view, model);
  }

}
