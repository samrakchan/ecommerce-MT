package com.metrotarkari.app.utils;

import java.util.HashMap;

/**
 * Created by samrakchan on 6/11/17.
 */

public interface QueryListener {

    void onQueryListener(HashMap<String, String> hashMap);
}
