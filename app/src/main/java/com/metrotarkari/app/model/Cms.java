
package com.metrotarkari.app.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Cms {

    @SerializedName("all_ids")
    @Expose
    private List<Object> allIds = null;
    @SerializedName("cmspages")
    @Expose
    private List<Object> cmspages = null;
    @SerializedName("total")
    @Expose
    private Integer total;
    @SerializedName("page_size")
    @Expose
    private Integer pageSize;
    @SerializedName("from")
    @Expose
    private Integer from;
    @SerializedName("total_page")
    @Expose
    private Integer totalPage;
    @SerializedName("current_page")
    @Expose
    private Integer currentPage;
    @SerializedName("car_total")
    @Expose
    private Integer carTotal;

    public List<Object> getAllIds() {
        return allIds;
    }

    public void setAllIds(List<Object> allIds) {
        this.allIds = allIds;
    }

    public List<Object> getCmspages() {
        return cmspages;
    }

    public void setCmspages(List<Object> cmspages) {
        this.cmspages = cmspages;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getFrom() {
        return from;
    }

    public void setFrom(Integer from) {
        this.from = from;
    }

    public Integer getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(Integer totalPage) {
        this.totalPage = totalPage;
    }

    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

    public Integer getCarTotal() {
        return carTotal;
    }

    public void setCarTotal(Integer carTotal) {
        this.carTotal = carTotal;
    }

}
