package com.metrotarkari.app.ui.notifications.mvp;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.metrotarkari.app.utils.DoneUserPreferenceManager;

import io.reactivex.disposables.CompositeDisposable;

import static com.basgeekball.awesomevalidation.ValidationStyle.UNDERLABEL;

/**
 * Created by samrakchan on 4/30/17.
 */

public class NotificationPresenter {

    private final NotificationView view;
    private final NotificationModel model;


    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    private AwesomeValidation awesomeValidation = new AwesomeValidation(UNDERLABEL);
    private DoneUserPreferenceManager doneUserPreferenceManager;

    public NotificationPresenter(NotificationView view, NotificationModel model){
        this.view = view;
        this.model = model;
   //     doneUserPreferenceManager = preferenceManager;
    }


}
