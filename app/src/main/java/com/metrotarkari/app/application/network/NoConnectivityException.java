package com.metrotarkari.app.application.network;

import java.io.IOException;

/**
 * Created by samrakchan on 6/22/17.
 */

public class NoConnectivityException extends IOException {

    @Override
    public String getMessage() {
        return "NoConnectivity";

    }
}
