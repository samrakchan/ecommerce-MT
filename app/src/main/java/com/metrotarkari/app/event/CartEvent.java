package com.metrotarkari.app.event;

/**
 * Created by samrakchan on 6/9/17.
 */

public class CartEvent {

    private final int cartCount;

    public CartEvent(int cartCount){
        this.cartCount = cartCount;
    }

    public int getCartCount(){
        return cartCount;
    }
}
