package com.metrotarkari.app.model;

/**
 * Created by samrakchan on 5/27/17.
 */

public class ItemParams {
    private int page;
    private int limit;
    private String orderBy; //orderBy price, item name etc.
    private String dir; //descending, ascending

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public String getDir() {
        return dir;
    }

    public void setDir(String dir) {
        this.dir = dir;
    }
}
