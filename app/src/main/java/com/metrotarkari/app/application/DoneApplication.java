package com.metrotarkari.app.application;

import android.app.Activity;
import android.app.Service;
import android.support.multidex.MultiDexApplication;
import android.support.v4.app.Fragment;

import com.metrotarkari.app.R;
import com.metrotarkari.app.application.dagger.AppComponent;
import com.metrotarkari.app.application.dagger.DaggerAppComponent;
import com.metrotarkari.app.application.dagger.module.AppModule;

import net.danlew.android.joda.JodaTimeAndroid;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class DoneApplication extends MultiDexApplication {

  public static DoneApplication get(Activity activity) {
	return (DoneApplication) activity.getApplication();
  }

  public static DoneApplication get(Service service) {
	return (DoneApplication) service.getApplication();
  }

  public static DoneApplication get(Fragment fragment){
	return (DoneApplication) fragment.getActivity().getApplicationContext();
  }

    private static DoneApplication singleton;

    public static DoneApplication getInstance(){
        return singleton;
    }

  private AppComponent appComponent;

    private int cartCount = 0;

  @Override
  public void onCreate() {
	super.onCreate();
      singleton = this;

	JodaTimeAndroid.init(this);

	CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
			.setDefaultFontPath("fonts/Roboto-Regular.ttf")
			.setFontAttrId(R.attr.fontPath)
			.build());

	appComponent = DaggerAppComponent.builder()
		.appModule(new AppModule(this))
		.build();
  }

  public AppComponent component() {
	return appComponent;
  }

  public void setCartCount(int cartCount){
      this.cartCount = cartCount;
  }

  public int getCartCount(){
      return cartCount;
  }
}
