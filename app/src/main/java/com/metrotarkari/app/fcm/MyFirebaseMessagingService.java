package com.metrotarkari.app.fcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.metrotarkari.app.R;
import com.metrotarkari.app.ui.dashboard.MainActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

/**
 * Created by samrakchan on 12/28/16.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        //Log.d(TAG, "Message Type: " + remoteMessage.getMessageType());
        //Log.d(TAG, "Click Action: " + remoteMessage.getNotification().getClickAction());


      /*  notification.setNotificationTitle(remoteMessage.getNotification().getTitle());
        notification.setNotificationDescription(remoteMessage.getNotification().getBody());*/

        if (remoteMessage.getData().size() > 0) {

            Map<String, String> data = remoteMessage.getData();
           /* String title = data.containsKey("title")?data.get("title"):"";
            String text = data.containsKey("text")?data.get("text"):"";*/

            Log.i(TAG, TAG+" "+data.toString());

            String message = data.containsKey("message")?data.get("message"):"";
            if(message.length()>0){
                try {
                    JSONObject jsonObject = new JSONObject(message);
                    String noticeTitle ="";String noticeContent="";
                    if(jsonObject.has("notice_title")){
                        noticeTitle = jsonObject.getString("notice_title");
                    }
                    if(jsonObject.has("notice_content")){
                        noticeContent = jsonObject.getString("notice_content");
                    }

                    sendNotification(noticeTitle, noticeContent);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            //sendNotification(title, text);

        }
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
    // [END receive_message]

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     *
     */
    private void sendNotification(String title, String text) {
        int uniqueId = (int)((System.currentTimeMillis()/1000)%3600);

        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Bitmap icon = BitmapFactory.decodeResource(getApplicationContext().getResources(),
                R.mipmap.ic_launcher);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setLargeIcon(icon)
                .setContentTitle(title)
                .setContentText(text)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(uniqueId /* ID of notification */, notificationBuilder.build());
    }


    //Server API Key: AIzaSyDOdSNZgh6L8LXVAa4JEjsf9ZjFWYJ13iM
    //Sender Id: 1012919231966
}
