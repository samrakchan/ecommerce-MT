package com.metrotarkari.app.ui.accounts;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metrotarkari.app.R;
import com.metrotarkari.app.model.IconTextId;
import com.metrotarkari.app.utils.DoneItemClickListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by samrakchan on 4/19/17.
 */

public class AccountsAdapter extends RecyclerView.Adapter<AccountsAdapter.AccountsViewHolder>{
    private Context mContext;
    private List<IconTextId> mData;
    private DoneItemClickListener listener;

    public AccountsAdapter(Context context,DoneItemClickListener listener){
        this.listener = listener;
        mContext = context;
        mData = new ArrayList<>();
    }

    public void addItems(List<IconTextId> iconTextIds){
        mData.addAll(iconTextIds);
        notifyDataSetChanged();
    }

    @Override
    public AccountsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_icon_text_horizontal,parent,false);
        return new AccountsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AccountsViewHolder holder, int position) {
        IconTextId iconTextId = mData.get(position);
        holder.textTv.setText(iconTextId.getText());
        holder.iconIv.setImageResource(iconTextId.getIcon());
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class AccountsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private AppCompatImageView iconIv;
        private AppCompatTextView textTv;

        public AccountsViewHolder(View itemView) {
            super(itemView);

            iconIv = (AppCompatImageView) itemView.findViewById(R.id.iconIv);
            textTv = (AppCompatTextView) itemView.findViewById(R.id.textTv);

            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            IconTextId iconTextId = mData.get(getAdapterPosition());

            listener.onItemClickListener(iconTextId.getId(),view.getId(),null);
        }
    }
}
