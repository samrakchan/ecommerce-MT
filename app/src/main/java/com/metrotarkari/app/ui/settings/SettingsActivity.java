package com.metrotarkari.app.ui.settings;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.metrotarkari.app.application.DoneApplication;
import com.metrotarkari.app.ui.base.BaseActivity;
import com.metrotarkari.app.ui.settings.dagger.DaggerSettingsComponent;
import com.metrotarkari.app.ui.settings.dagger.SettingsModule;
import com.metrotarkari.app.ui.settings.mvp.SettingsPresenter;
import com.metrotarkari.app.ui.settings.mvp.SettingsView;

import javax.inject.Inject;

/**
 * Created by Avinash on 7/13/2017.
 */

public class SettingsActivity extends BaseActivity {

    @Inject
    SettingsView view;

    @Inject
    SettingsPresenter presenter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        DaggerSettingsComponent.builder().appComponent(DoneApplication.get(this).component())
                .settingsModule(new SettingsModule(this)).build().inject(this);

        setContentView(view);

    }
}
