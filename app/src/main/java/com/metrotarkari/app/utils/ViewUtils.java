package com.metrotarkari.app.utils;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.util.DisplayMetrics;
import android.view.Window;

/**
 * Created by samrakchan on 3/30/17.
 */

public class ViewUtils {

    public static int calculateNoOfColumns(Context context){
        int noOfColumns;
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float dpWidth = displayMetrics.widthPixels/displayMetrics.density;
        if (dpWidth <= 420.00){
             noOfColumns = (int)(dpWidth/150);
        }else {
             noOfColumns = (int) (dpWidth / 180);
        }
        if(noOfColumns ==0){
            return 1;
        }
        return noOfColumns;
    }


    public static int calculateNoOfColumnsDynamically(Context context, int layoutWidth){
        int noOfColumns;
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float dpWidth = displayMetrics.widthPixels/displayMetrics.density;
        noOfColumns = (int) dpWidth/layoutWidth;
        return noOfColumns;
    }


    public static int getStatusBarHeight(Activity context) {
        Rect rectangle= new Rect();
        Window window= context.getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(rectangle);
        int StatusBarHeight= rectangle.top;
        return StatusBarHeight;
    }


    /**
     * This method converts dp unit to equivalent pixels, depending on device density.
     *
     * @param dp A value in dp (density independent pixels) unit. Which we need to convert into pixels
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent px equivalent to dp depending on device density
     */
    public static float convertDpToPixel(float dp, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }

    /**
     * This method converts device specific pixels to density independent pixels.
     *
     * @param px A value in px (pixels) unit. Which we need to convert into db
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent dp equivalent to px value
     */
    public static float convertPixelsToDp(float px, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return dp;
    }

    public static int statusBarHeight(Activity context){
        Rect rectangle = new Rect();
        Window window = context.getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(rectangle);
        return rectangle.top;
    }
}
