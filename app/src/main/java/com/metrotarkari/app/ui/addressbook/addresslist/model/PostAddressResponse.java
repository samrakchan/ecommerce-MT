
package com.metrotarkari.app.ui.addressbook.addresslist.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.metrotarkari.app.model.BAddress;
import com.metrotarkari.app.model.ErrorResponse;

public class PostAddressResponse extends ErrorResponse{

    @SerializedName("Address")
    @Expose
    private BAddress bAddress;

    public BAddress getbAddress() {
        return bAddress;
    }

    public void setbAddress(BAddress bAddress) {
        this.bAddress = bAddress;
    }


}
