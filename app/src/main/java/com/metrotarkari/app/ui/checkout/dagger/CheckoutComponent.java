package com.metrotarkari.app.ui.checkout.dagger;

import com.metrotarkari.app.application.dagger.AppComponent;
import com.metrotarkari.app.ui.checkout.CheckoutActivity;

import dagger.Component;

@CheckoutScope
@Component(modules = { CheckoutModule.class }, dependencies = AppComponent.class)
public interface CheckoutComponent {

  void inject(CheckoutActivity activity);

}
