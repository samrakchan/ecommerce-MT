package com.metrotarkari.app.ui.order.orderlist.mvp;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.metrotarkari.app.R;
import com.metrotarkari.app.ui.base.BaseView;
import com.metrotarkari.app.ui.order.orderdetail.OrderDetailActivity;
import com.metrotarkari.app.ui.order.orderlist.OrderListAdapter;
import com.metrotarkari.app.ui.order.orderlist.model.Order;
import com.metrotarkari.app.utils.DoneItemClickListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by samrakchan on 4/27/17.
 */

public class OrderListView extends BaseView implements DoneItemClickListener {

    @BindView(R.id.toolbar)
    public Toolbar toolbar;

    @BindView(R.id.imageMsgRl)
    public View noItemView;

    @BindView(R.id.progressBarRl)
    public View progressBar;

    @BindView(R.id.imageMsgNoItemTv)
    public AppCompatTextView imageMsgNoItemTv;

    @Nullable
    @BindView(R.id.addressBookRv)
    public RecyclerView addressBookRv;



    private OrderListAdapter orderListAdapter;


    private AppCompatActivity activity;

    public OrderListView(@NonNull AppCompatActivity activity) {
        super(activity);
        this.activity = activity;

        ButterKnife.bind(this);

        addressBookRv.setLayoutManager(new LinearLayoutManager(getContext()));
        orderListAdapter = new OrderListAdapter(getContext(),this);


        activity.setSupportActionBar(toolbar);
        activity.getSupportActionBar().setTitle(R.string.order_list);


        addressBookRv.setAdapter(orderListAdapter);

    }

    public void setOrderListData(List<Order> orderList){
        orderListAdapter.addData(orderList);
    }

    @Override
    public int getContentLayout() {
        return R.layout.activity_address_book;
    }


    public void showNoItem(boolean showItem){
        if(showItem) {
            noItemView.setVisibility(View.VISIBLE);
            imageMsgNoItemTv.setText(R.string.no_order_found);
        }
        else {
            noItemView.setVisibility(View.GONE);
        }
    }

    public void showProgressBar(boolean showing){
        if(showing)
            progressBar.setVisibility(View.VISIBLE);
        else
            progressBar.setVisibility(View.GONE);
    }


    @Override
    public void onItemClickListener(int position, int viewId, Object object) {
        Order order = (Order) object;
        //intent navigation to OrderDetailActivity
        Intent intent = new Intent(getContext(), OrderDetailActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("ORDERID", order.getEntityId());
        getContext().startActivity(intent);

    }
}
