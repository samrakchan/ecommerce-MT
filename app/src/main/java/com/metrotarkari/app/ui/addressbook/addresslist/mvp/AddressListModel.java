package com.metrotarkari.app.ui.addressbook.addresslist.mvp;

import android.support.v7.app.AppCompatActivity;

import com.metrotarkari.app.application.network.DoneNetwork;
import com.metrotarkari.app.model.AddressResponse;

import io.reactivex.Observable;

/**
 * Created by samrakchan on 4/27/17.
 */

public class AddressListModel {
    private final AppCompatActivity activity;
    private final DoneNetwork doneNetwork;

    public AddressListModel(AppCompatActivity activity, DoneNetwork doneNetwork){
        this.activity = activity;
        this.doneNetwork = doneNetwork;
    }


    public Observable<AddressResponse> getAddressBook(){
        return doneNetwork.getAddressBook();
    }

    public Observable<AddressResponse> deleteAddress(int addressId){
        return doneNetwork.deleteAddress(addressId);
    }

}
