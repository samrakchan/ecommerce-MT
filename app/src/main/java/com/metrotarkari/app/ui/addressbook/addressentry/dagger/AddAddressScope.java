package com.metrotarkari.app.ui.addressbook.addressentry.dagger;

import javax.inject.Scope;

@Scope
public @interface AddAddressScope {
}
