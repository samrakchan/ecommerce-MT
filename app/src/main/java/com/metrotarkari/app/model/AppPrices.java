package com.metrotarkari.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by samrakchan on 5/29/17.
 */

public class AppPrices {

    @SerializedName("has_special_price")
    @Expose
    private Integer hasSpecialPrice;
    @SerializedName("show_ex_in_price")
    @Expose
    private Integer showExInPrice;
    @SerializedName("price_label")
    @Expose
    private String priceLabel;
    @SerializedName("price")
    @Expose
    private String price;

    public Integer getHasSpecialPrice() {
        return hasSpecialPrice;
    }

    public void setHasSpecialPrice(Integer hasSpecialPrice) {
        this.hasSpecialPrice = hasSpecialPrice;
    }

    public Integer getShowExInPrice() {
        return showExInPrice;
    }

    public void setShowExInPrice(Integer showExInPrice) {
        this.showExInPrice = showExInPrice;
    }

    public String getPriceLabel() {
        return priceLabel;
    }

    public void setPriceLabel(String priceLabel) {
        this.priceLabel = priceLabel;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
