package com.metrotarkari.app.ui.detail.mvp;

import android.content.Context;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metrotarkari.app.R;
import com.metrotarkari.app.model.CustomOption;
import com.metrotarkari.app.utils.Utility;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by samrakchan on 8/22/17.
 */

public class CustomOptionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<CustomOption> mData;
    private Context mContext;

    private static final int CUSTOM_OPTION_TEXT_FIELD_VIEW = 1;
    private static final int CUSTOM_OPTION_TEXT_AREA_VIEW = 2;
    private static final int CUSTOM_OPTION_TEXT_DROPDOWN_VIEW = 3;

    public CustomOptionAdapter(Context mContext) {
        this.mContext = mContext;
        mData = new ArrayList<>();
    }

    public void addAllData(List<CustomOption> objects){
        mData.addAll(objects);
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == CUSTOM_OPTION_TEXT_FIELD_VIEW) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_detail_custom_option_field, parent, false);
            return new CustomOptionAdapter.CustomOptionTextFieldViewHolder(view);
        }else if(viewType == CUSTOM_OPTION_TEXT_AREA_VIEW){
            View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_detail_custom_option_field, parent, false);
            return new CustomOptionAdapter.CustomOptionTextFieldViewHolder(view);
        }else{
            View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_detail_custom_option_drop_down, parent, false);
            return new CustomOptionAdapter.CustomOptionTextFieldViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        CustomOption customOption = mData.get(position);
        if(holder instanceof CustomOptionTextFieldViewHolder && customOption.getType().equalsIgnoreCase("field")){
            CustomOptionTextFieldViewHolder viewHolder = (CustomOptionTextFieldViewHolder)holder;
            viewHolder.layout_custom_option_title_tv.setText(customOption.getTitle());
            viewHolder.layout_custom_option_value_tv.setText(customOption.getValues().toString());
            viewHolder.custom_option_et.setHint(customOption.getTitle());

        }else if(holder instanceof CustomOptionTextFieldViewHolder && customOption.getType().equalsIgnoreCase("area")){
            CustomOptionTextFieldViewHolder viewHolder = (CustomOptionTextFieldViewHolder)holder;
            viewHolder.layout_custom_option_title_tv.setText(customOption.getTitle());
            viewHolder.layout_custom_option_value_tv.setText(customOption.getValues().toString());
            viewHolder.custom_option_et.setHint(customOption.getTitle());
            viewHolder.custom_option_et.setHeight(Utility.dpToPx(40, mContext));
        }else if (holder instanceof CustomOptionSpinnerDropDownViewHolder && customOption.getType().equalsIgnoreCase("drop_down")){
            //drop_down
            CustomOptionSpinnerDropDownViewHolder viewHolder = (CustomOptionSpinnerDropDownViewHolder) holder;
            viewHolder.layout_custom_option_title_tv.setText(customOption.getType()+" values"+customOption.getValues());
            //ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, customOption.getValues());
            //viewHolder.custom_option_spinner.setText(customOption.getType());
        }
    }

    @Override
    public int getItemViewType(int position) {
        CustomOption item = mData.get(position);
        String type = item.getType();
        if (type.equalsIgnoreCase("field")) {
            return CUSTOM_OPTION_TEXT_FIELD_VIEW;
        } else if (type.equalsIgnoreCase("area")) {
            return CUSTOM_OPTION_TEXT_AREA_VIEW;
        }else{
            //drop_down
            return CUSTOM_OPTION_TEXT_DROPDOWN_VIEW;
        }
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    protected class CustomOptionTextFieldViewHolder extends RecyclerView.ViewHolder{
        private AppCompatTextView layout_custom_option_title_tv;
        private AppCompatTextView layout_custom_option_value_tv;
        private AppCompatEditText custom_option_et;

        public CustomOptionTextFieldViewHolder(View itemView) {
            super(itemView);
            layout_custom_option_title_tv = (AppCompatTextView) itemView.findViewById(R.id.layout_custom_option_title_tv);
            layout_custom_option_value_tv = (AppCompatTextView) itemView.findViewById(R.id.layout_custom_option_value_tv);
            custom_option_et = (AppCompatEditText) itemView.findViewById(R.id.custom_option_et);
        }
    }

    protected class CustomOptionSpinnerDropDownViewHolder extends RecyclerView.ViewHolder{
        private AppCompatTextView layout_custom_option_title_tv;
        private AppCompatSpinner custom_option_spinner;

        public CustomOptionSpinnerDropDownViewHolder(View itemView) {
            super(itemView);
            layout_custom_option_title_tv = (AppCompatTextView) itemView.findViewById(R.id.layout_custom_option_title_tv);
            custom_option_spinner = (AppCompatSpinner) itemView.findViewById(R.id.custom_option_spinner);

        }
    }
}
