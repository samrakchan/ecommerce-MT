package com.metrotarkari.app.ui.reviewandratings.reviewpost.mvp;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.google.gson.Gson;
import com.jakewharton.rxbinding2.view.RxView;
import com.metrotarkari.app.R;
import com.metrotarkari.app.model.FormKey;
import com.metrotarkari.app.model.Product_;
import com.metrotarkari.app.model.Rate;
import com.metrotarkari.app.ui.base.BaseView;
import com.metrotarkari.app.ui.reviewandratings.reviewpost.ReviewPostAdapter;
import com.metrotarkari.app.utils.AlertUtils;
import com.metrotarkari.app.utils.Constant;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cc.cloudist.acplibrary.ACProgressFlower;
import io.reactivex.Observable;

/**
 * Created by Avinash on 8/8/17.
 */
public class ReviewPostView extends BaseView{

    @BindView(R.id.postReviewRv)
    public RecyclerView postReviewRv;

    @BindView(R.id.productReviewSubmitBtn)
    public AppCompatButton productReviewSubmitBtn;

    private Product_ product_;

    private ReviewPostAdapter adapter;

    private List<Object> objects;

    private AppCompatActivity activity;

    private LinearLayoutManager linearLayoutManager;

    private final ACProgressFlower progressDialog = new ACProgressFlower.Builder(getContext())
            .themeColor(Color.WHITE)
            .fadeColor(Color.DKGRAY).build();

    public ReviewPostView(AppCompatActivity activity){
        super(activity);
        this.activity = activity;

        ButterKnife.bind(this);

        Intent intent = activity.getIntent();
        String jsonString = intent.getStringExtra(Constant.PRODUCT_DETAIL);

        Gson gson = new Gson();
        product_  = gson.fromJson(jsonString, Product_.class);

        objects = new ArrayList<>();

        objects.addAll(product_.getAppReviews().getFormAddReviews().get(0).getRates());
        objects.addAll(product_.getAppReviews().getFormAddReviews().get(0).getFormReview().getFormKey());

        linearLayoutManager = new LinearLayoutManager(getContext());

        postReviewRv.setLayoutManager(linearLayoutManager);
        adapter = new ReviewPostAdapter(getContext());
        adapter.addPostDataView(objects);
        postReviewRv.setAdapter(adapter);

        activity.getSupportActionBar().setTitle(R.string.post_review);
    }

    @Override
    public int getContentLayout() {
        return R.layout.activity_review_post;
    }


    public Observable<Object> observePostReviewButton() {
        return RxView.clicks(productReviewSubmitBtn);
    }

    public void showLoading(boolean loading) {
        if (loading) {
            progressDialog.setCancelable(false);
            progressDialog.show();
        } else {
            progressDialog.dismiss();
        }
    }

    public void showPostReviewConformation(String conformationMessage){
        AlertUtils.createAlertForPostReview(activity,R.string.ok,conformationMessage).show();
    }

    public HashMap<String, Object> getValues(){
        HashMap<String, Object> postReview = new HashMap<>();
        postReview.put("product_id",Integer.parseInt(product_.getEntityId()));
        HashMap<String, Integer> ratingMap = new HashMap<>();

        for(int i=0; i<adapter.getItemCount(); i++){
            AppCompatRatingBar ratingBar = (AppCompatRatingBar) linearLayoutManager.findViewByPosition(i).findViewById(R.id.adapterPostReviewRb);
            AppCompatEditText ratingTextEt = (AppCompatEditText) linearLayoutManager.findViewByPosition(i).findViewById(R.id.adapterPostReviewDescriptionEt);
            if(ratingBar != null){
                float userGivenRatings = ratingBar.getRating();
                Rate rate = (Rate) ratingBar.getTag();
                ratingMap.put(rate.getRateOptions().get(0).getKey(), (int)userGivenRatings);
            }else if(ratingTextEt !=null){
                String text = ratingTextEt.getText().toString();
                FormKey formKey = (FormKey) ratingTextEt.getTag();
                postReview.put(formKey.getKey(),text);
            }
        }
        postReview.put("ratings", ratingMap);

        return postReview;
    }
}
