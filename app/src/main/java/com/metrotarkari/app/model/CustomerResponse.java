
package com.metrotarkari.app.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CustomerResponse extends ErrorResponse {

    @SerializedName("customer")
    @Expose
    private Customer customer;
    @SerializedName("message")
    @Expose
    private List<String> message = null;

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public List<String> getMessage() {
        return message;
    }

    public void setMessage(List<String> message) {
        this.message = message;
    }

}
