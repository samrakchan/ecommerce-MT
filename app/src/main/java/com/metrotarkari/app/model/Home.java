
package com.metrotarkari.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Home extends ErrorResponse {

    @SerializedName("home")
    @Expose
    private Home_ home;

    public Home_ getHome() {
        return home;
    }

    public void setHome(Home_ home) {
        this.home = home;
    }

}
