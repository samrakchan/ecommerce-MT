package com.metrotarkari.app.ui.dashboard.mvp;

import android.app.Activity;

import com.metrotarkari.app.application.network.DoneNetwork;
import com.metrotarkari.app.model.ConfigResponse;
import com.metrotarkari.app.model.StoreViewResult;

import io.reactivex.Observable;

/**
 * Created by samrakchan on 4/27/17.
 */

public class MainModel {
    private final Activity activity;
    private final DoneNetwork doneNetwork;

    public MainModel(Activity activity, DoneNetwork doneNetwork){
        this.activity = activity;
        this.doneNetwork = doneNetwork;
    }

    public void finishActivity(){
        activity.finish();
    }

    public Observable<ConfigResponse> getCategories(){
        return doneNetwork.getCategories();
    }

    public Observable<StoreViewResult> getStoreViews(){
        return doneNetwork.getStoreViews();
    }
}
