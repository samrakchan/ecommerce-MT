package com.metrotarkari.app.ui.list.sortby;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metrotarkari.app.R;
import com.metrotarkari.app.generic.OnItemClickListener;
import com.metrotarkari.app.model.Sort;
import com.metrotarkari.app.utils.OrderListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by samrakchan on 7/17/17.
 */

public class DrawerSortFragment extends Fragment {
    private RecyclerView filterRv;
    private SortByAdapter sortByAdapter;
    private List<Sort> layerFilters = new ArrayList<>();
    private OrderListener listener;
    private OnItemClickListener itemClickListener;
    private AppCompatButton applyBtn;
    private View backIv;
    private AppCompatTextView categoryTitleTv;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getArguments()!=null) {
            layerFilters.addAll(getArguments().getParcelableArrayList("LIST"));
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.drawer_filter_fragment, container, false);
        filterRv = (RecyclerView) view.findViewById(R.id.filterRv);
        applyBtn = (AppCompatButton)view.findViewById(R.id.applyBtn);
        categoryTitleTv = (AppCompatTextView) view.findViewById(R.id.categoryTitleTv);
        categoryTitleTv.setText(R.string.sort);
        backIv = view.findViewById(R.id.backIv);
        filterRv.setLayoutManager(new LinearLayoutManager(getContext()));
        DividerItemDecoration dividerItemDecorationH = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        dividerItemDecorationH.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.bg_divider));
        filterRv.addItemDecoration(dividerItemDecorationH);
        sortByAdapter = new SortByAdapter(getContext(), layerFilters);
        filterRv.setAdapter(sortByAdapter);

        applyBtn.setOnClickListener(view1 -> {
            if(listener!=null && sortByAdapter !=null){
                listener.onOrderListener(sortByAdapter.getSelectedOrder());
                itemClickListener.setOnItemClickListener(null);
            }
        });

        backIv.setOnClickListener(view1 ->{
            if(itemClickListener!=null){
                itemClickListener.setOnItemClickListener(null);
            }
        });

        return view;
    }

    public void setOrderListener(OrderListener orderListener){
        listener = orderListener;
    }

    public void setOnItemClickListener(OnItemClickListener listener){
        itemClickListener = listener;
    }
}