package com.metrotarkari.app.model;

/**
 * Created by samrakchan on 3/21/17.
 */

public class Thumb {
    private String imageId;
    private String imageUrl;

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
