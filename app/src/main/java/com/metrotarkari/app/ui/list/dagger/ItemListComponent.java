package com.metrotarkari.app.ui.list.dagger;

import com.metrotarkari.app.application.dagger.AppComponent;
import com.metrotarkari.app.ui.list.ItemListFragment;

import dagger.Component;

@ItemListScope
@Component(modules = { ItemListModule.class }, dependencies = AppComponent.class)
public interface ItemListComponent {

  void inject(ItemListFragment fragment);

}
