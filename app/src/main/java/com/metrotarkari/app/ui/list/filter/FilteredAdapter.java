package com.metrotarkari.app.ui.list.filter;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metrotarkari.app.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by samrakchan on 4/4/17.
 */

public class FilteredAdapter extends RecyclerView.Adapter<FilteredAdapter.ViewHolder>{
    private Context mContext;

    private List<String> mData;

    public FilteredAdapter(Context context){
        this.mContext = context;
        mData = new ArrayList<>();
    }

    public void addItems(List<String> items){
        mData.addAll(items);
        notifyDataSetChanged();
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_filtered,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String title = mData.get(position);
        holder.filteredItemTv.setText(title);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        protected AppCompatTextView filteredItemTv;
        protected AppCompatImageView filteredItemCloseIv;

        public ViewHolder(View itemView) {
            super(itemView);
            filteredItemTv = (AppCompatTextView)itemView.findViewById(R.id.filteredItemTv);
            filteredItemCloseIv = (AppCompatImageView)itemView.findViewById(R.id.filteredItemCloseIv);
            filteredItemCloseIv.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            mData.remove(position);
            notifyDataSetChanged();
        }
    }
}
