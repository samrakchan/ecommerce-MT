package com.metrotarkari.app.ui.dashboard.mvp;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.hannesdorfmann.parcelableplease.processor.codegenerator.collection.ParcelableArrayCodeGen;
import com.metrotarkari.app.R;
import com.metrotarkari.app.model.Category;
import com.metrotarkari.app.model.ChildCategory;
import com.metrotarkari.app.utils.DoneItemClickListener;
import com.metrotarkari.app.utils.ViewUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by samrakchan on 6/27/17.
 */

public class MenuChildFragment extends Fragment implements DoneItemClickListener, View.OnClickListener {

    @BindView(R.id.mainChildMenuRv)
    public RecyclerView mainChildMenuRv;

    @BindView(R.id.categoryTitleTv)
    public AppCompatTextView categoryTitleTv;

    private MenuChildAdapter menuAdapter;

    private DoneItemClickListener listener;

    private ArrayList<ChildCategory> childCategories;

    public final static String CATEGORIES_STATE = "categories_state";

    private static Parcelable parcelable;

    private LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());


    @BindView(R.id.menuChildMainMenuLl)
    public View menuChildMainMenuLl;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setRetainInstance(true);
        View view = inflater.inflate(R.layout.fragment_menu_child, container, false);

        ButterKnife.bind(this, view);

        childCategories = new ArrayList<>();

   /*     LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lp.setMargins(0, ViewUtils.statusBarHeight(getActivity()), 0, 0);
        menuChildMainMenuLl.setLayoutParams(lp);*/

        menuAdapter = new MenuChildAdapter(getContext());
        menuAdapter.addItemClickListener(this);
        mainChildMenuRv.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecorationH = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        dividerItemDecorationH.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.bg_divider));
        mainChildMenuRv.addItemDecoration(dividerItemDecorationH);
        mainChildMenuRv.setAdapter(menuAdapter);

        menuChildMainMenuLl.setOnClickListener(this);

        Bundle bundle = getArguments();
        if(bundle!=null) {
            Category category = bundle.getParcelable("CATEGORY");
            if (category != null) {
                addItemToAdapter(category.getChildCategory());
                categoryTitleTv.setText(category.getName() + "");
            }
        }

        return view;
    }

    public void addItemToAdapter(List<ChildCategory> categories){
        if(this.childCategories!=null && categories!=null) {
            this.childCategories.clear();
            this.childCategories.addAll(categories);
            menuAdapter.addData(this.childCategories);
        }
    }

    public void setItemClickListener(DoneItemClickListener listener){
        this.listener = listener;
    }

    @Override
    public void onItemClickListener(int position, int viewId, Object object) {
        listener.onItemClickListener(position, viewId, object);
    }

    @Override
    public void onClick(View view) {
        listener.onItemClickListener(-1, menuChildMainMenuLl.getId(), null);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        parcelable = layoutManager.onSaveInstanceState();
        outState.putParcelable(CATEGORIES_STATE, parcelable);

    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if(savedInstanceState !=null){
            savedInstanceState.getParcelable(CATEGORIES_STATE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(parcelable!=null){
            layoutManager.onRestoreInstanceState(parcelable);
        }
    }
}
