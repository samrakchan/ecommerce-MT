package com.metrotarkari.app.ui.productdetail.mvp;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Build;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.bumptech.glide.Glide;
import com.freegeek.android.materialbanner.MaterialBanner;
import com.freegeek.android.materialbanner.holder.Holder;
import com.freegeek.android.materialbanner.view.indicator.CirclePageIndicator;
import com.metrotarkari.app.R;
import com.metrotarkari.app.model.AppReviews;
import com.metrotarkari.app.model.CustomOption;
import com.metrotarkari.app.model.Homeproductlist;
import com.metrotarkari.app.model.Image;
import com.metrotarkari.app.model.Product;
import com.metrotarkari.app.model.Product_;
import com.metrotarkari.app.model.Tier;
import com.metrotarkari.app.ui.adapter.HorizontalItemAdapter;
import com.metrotarkari.app.ui.list.ItemListActivity;
import com.metrotarkari.app.utils.Constant;
import com.metrotarkari.app.utils.DoneItemClickListener;
import com.metrotarkari.app.utils.Utility;
import com.ms.square.android.expandabletextview.ExpandableTextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.basgeekball.awesomevalidation.ValidationStyle.UNDERLABEL;

/**
 * Created by samrakchan on 8/23/17.
 */

public class DetailAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Object> mData;
    private Context mContext;
    private String mTitle;

    private AwesomeValidation awesomeValidation = new AwesomeValidation(UNDERLABEL);

    private static final int PRODUCT_DETAIL_VIEW = 1;
    private static final int ADD_TO_CART_VIEW = 2;
    private static final int TIRE_PRICE_VIEW = 3;
    private static final int CUSTOM_OPTION_TEXT_FIELD_VIEW = 4;
    private static final int CUSTOM_OPTION_TEXT_AREA_VIEW = 5;
    private static final int CUSTOM_OPTION_TEXT_DROPDOWN_VIEW = 6;
    private static final int HORIZONTAL_LIST_VIEW = 7;
    private static final int PRODUCT_REVIEW_VIEW = 8;
    private static final int PRODUCT_DESCRIPTION = 9;

    private DoneItemClickListener listener;

    public DetailAdapter(Context mContext) {
        this.awesomeValidation.setContext(mContext);
        this.mContext = mContext;
        mData = new ArrayList<>();
        mTitle = "";
    }

    public void addAllData(List<Object> objects){
        mData.addAll(objects);
        notifyDataSetChanged();
    }

    public void addData(Object object){
        mData.add(object);
    }

    public void addData(Object object, String title){
        mData.add(object);
        mTitle = title;
    }

    public void addData(int position, Object object, String title){
        mData.add(position, object);
        mTitle = title;
        notifyDataSetChanged();
    }

    public void setListener(DoneItemClickListener listener){
        this.listener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == PRODUCT_DETAIL_VIEW) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_detail_product, parent, false);
            return new DetailAdapter.ProductDetailViewHolder(view);
        }else if(viewType == TIRE_PRICE_VIEW){
            View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_detail_tier_price, parent, false);
            return new TierPriceViewHolder(view);
        }else if(viewType == ADD_TO_CART_VIEW){
            View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_detail_add_to_cart, parent, false);
            return new DetailAdapter.AddToCartViewHolder(view);
        }else if(viewType == CUSTOM_OPTION_TEXT_FIELD_VIEW) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_detail_custom_option_field, parent, false);
            return new CustomOptionTextFieldViewHolder(view);
        }else if(viewType == CUSTOM_OPTION_TEXT_AREA_VIEW){
            View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_detail_custom_option_field, parent, false);
            return new CustomOptionTextFieldViewHolder(view);
        }else if(viewType == CUSTOM_OPTION_TEXT_DROPDOWN_VIEW){
            View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_detail_custom_option_drop_down, parent, false);
            return new CustomOptionTextFieldViewHolder(view);
        }else if(viewType == HORIZONTAL_LIST_VIEW) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_label_recycler, parent, false);
            return new LabelHorizontalRecyclerViewHolder(view);
        }else if(viewType == PRODUCT_REVIEW_VIEW) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_detail_product_review, parent, false);
            return new AppReviewViewHolder(view);
        }else /*if(viewType == PRODUCT_DESCRIPTION)*/ {
            View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_detail_product_description, parent, false);
            return new DescriptionViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Object object = mData.get(position);
        if(holder instanceof ProductDetailViewHolder && object instanceof Product_){
            ProductDetailViewHolder viewHolder = (ProductDetailViewHolder) holder;
            Product_ product = (Product_) object;

            viewHolder.itemDetailItemNameTv.setText(product.getName());
            viewHolder.itemDetailItemPriceTv.setText(product.getFormattedFinalPrice());

            if(!product.getFinalPrice().equalsIgnoreCase(product.getPrice())) {
                viewHolder.itemDetailItemPriceCutTv.setText(product.getFormattedPrice());
                viewHolder.itemDetailItemPriceCutTv.setPaintFlags(viewHolder.itemDetailItemPriceCutTv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                viewHolder.itemDetailItemPriceCutTv.setVisibility(View.VISIBLE);
            }else{
                viewHolder.itemDetailItemPriceCutTv.setVisibility(View.GONE);
            }

            viewHolder.itemDetailSKUTv.setText(mContext.getString(R.string.sku)+" "+product.getSku());

            //product discount for not null
            if(product.getDiscountLabel() != null && !product.getDiscountLabel().isEmpty()){
                viewHolder.listItemOfferLabelTv.setText(product.getDiscountLabel());
                viewHolder.listItemOfferLabelTv.setVisibility(View.VISIBLE);
            }else{
                //if no product discount hiding product lable discount TextView
                viewHolder.listItemOfferLabelTv.setVisibility(View.GONE);
            }

            if(product.getImages()!=null){
                if(!product.getImages().isEmpty()){
                    CirclePageIndicator circlePageIndicator = new CirclePageIndicator(mContext);
                    circlePageIndicator.setStrokeColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
                    circlePageIndicator.setFillColor(ContextCompat.getColor(mContext, R.color.colorGreenLight));
                    circlePageIndicator.setRadius(MaterialBanner.dip2Pix(mContext,3));
                    circlePageIndicator.setBetween(20);
                    viewHolder.itemDetailIv.setIndicatorInside(false);
                    viewHolder.itemDetailIv.setPages(() -> new SliderViewHolder(), product.getImages()).setIndicator(circlePageIndicator);
                }
            }

        }else if(object instanceof Tier && holder instanceof TierPriceViewHolder){
            Tier tier = (Tier) object;
            ((TierPriceViewHolder) holder).adapterTierPriceTv.setText(tier.getText());
        }else if(object instanceof CustomOption) {
            CustomOption customOption = (CustomOption) object;
            if((customOption.getType().equalsIgnoreCase("field") || customOption.getType().equalsIgnoreCase("area")) && holder instanceof CustomOptionTextFieldViewHolder){
                CustomOptionTextFieldViewHolder viewHolder = (CustomOptionTextFieldViewHolder)holder;
                viewHolder.layout_custom_option_title_tv.setText(customOption.getTitle());
                List<HashMap<String, Object>> values = customOption.getValues();
                if(values.size()>0) {
                    StringBuilder stringBuilder = new StringBuilder();
                    for (String key : values.get(0).keySet() ) {
                        stringBuilder.append(key+" "+values.get(0).get(key));
                    }
                    viewHolder.layout_custom_option_value_tv.setText(stringBuilder.toString());
                }
                viewHolder.custom_option_et.setHint(customOption.getTitle());
                if(customOption.getType().equalsIgnoreCase("area")) {
                    viewHolder.custom_option_et.setHeight(Utility.dpToPx(70, mContext));
                }


                if(customOption.getIsRequired().equalsIgnoreCase("1")){
                    this.awesomeValidation.addValidation(viewHolder.custom_option_et, Constant.AT_LEAST_ONE_CHAR, mContext.getString(R.string.field_missing));
                }

            }else if (customOption.getType().equalsIgnoreCase("drop_down") && holder instanceof CustomOptionSpinnerDropDownViewHolder){
                //drop_down
                CustomOptionSpinnerDropDownViewHolder viewHolder = (CustomOptionSpinnerDropDownViewHolder) holder;
                viewHolder.layout_custom_option_title_tv.setText(customOption.getTitle());
                List<HashMap<String, Object>> list = customOption.getValues();
                //viewHolder.custom_option_spinner.setAdapter();

            }
        }else if(object instanceof String && holder instanceof AddToCartViewHolder){
            //do nothing for now
        }else if(holder instanceof LabelHorizontalRecyclerViewHolder){
            List<Product> products = (List<Product>) object;
            LabelHorizontalRecyclerViewHolder labelHorizontalRecyclerViewHolder = (LabelHorizontalRecyclerViewHolder)holder;
            labelHorizontalRecyclerViewHolder.categoryHeaderTv.setText(mTitle);
            if(labelHorizontalRecyclerViewHolder.itemListAdapter.getItemCount()==0) {
                labelHorizontalRecyclerViewHolder.itemListAdapter.addData(products);
            }
        }else if(holder instanceof AppReviewViewHolder && object instanceof AppReviews){
            AppReviewViewHolder appReviewViewHolder = (AppReviewViewHolder) holder;
            AppReviews appReviews = (AppReviews) object;
            if (appReviews!=null){
                if (appReviews.getRate() == 0){
                    appReviewViewHolder.itemDetailBeTheFirstToReviewTv.setVisibility(View.VISIBLE);
                    appReviewViewHolder.itemDetailBeTheFirstToReviewTv.setText(R.string.first_to_review_product);
                    appReviewViewHolder.itemDetailRatingLl.setVisibility(View.INVISIBLE);
                    appReviewViewHolder.itemDetailReviewNumberTv.setVisibility(View.INVISIBLE);
                }else {
                    appReviewViewHolder.itemDetailBeTheFirstToReviewTv.setVisibility(View.GONE);
                    appReviewViewHolder.itemDetailRatingLl.setVisibility(View.VISIBLE);
                    String rate = appReviews.getRate().toString();
                    appReviewViewHolder.itemDetailRatingTv.setText(Utility.convertToDecimalPlace(rate, 1));
                    appReviewViewHolder.itemDetailReviewNumberTv.setText(mContext.getString(R.string.item_review_number,appReviews.getNumber()));
                }
            }
        }else if(object instanceof String && holder instanceof DescriptionViewHolder){
            DescriptionViewHolder viewHolder = (DescriptionViewHolder)holder;
            Spanned spanned = null;
            String description = (String) object;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                spanned = Html.fromHtml(description, Html.FROM_HTML_MODE_COMPACT);
            }else{
                spanned = Html.fromHtml(description);
            }
            //if(viewHolder.expandable_text!=null) {
                viewHolder.expandable_text.setText(spanned.toString());
            //}
        }
    }

    @Override
    public int getItemViewType(int position) {
        Object object = mData.get(position);
        if(object instanceof Product_) {
            return PRODUCT_DETAIL_VIEW;
        }else if(object instanceof Tier){
            return TIRE_PRICE_VIEW;
        }else if(object instanceof CustomOption){
            String type = ((CustomOption) object).getType();
            if (type.equalsIgnoreCase("field")) {
                return CUSTOM_OPTION_TEXT_FIELD_VIEW;
            } else if (type.equalsIgnoreCase("area")) {
                return CUSTOM_OPTION_TEXT_AREA_VIEW;
            }else /*if(type.equalsIgnoreCase("drop_down"))*/{
                //drop_down
                return CUSTOM_OPTION_TEXT_DROPDOWN_VIEW;
            }
        }else if(object instanceof String && ((String)object).equalsIgnoreCase("AddToCart")){
            return ADD_TO_CART_VIEW;
        }else if(object instanceof AppReviews){
            return PRODUCT_REVIEW_VIEW;
        }else if(object instanceof String){
            return PRODUCT_DESCRIPTION;
        }else{
            return HORIZONTAL_LIST_VIEW;
        }

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    protected class ProductDetailViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private MaterialBanner itemDetailIv;
        private FloatingActionButton listItemShareFAB;
        private AppCompatTextView itemDetailItemNameTv;
        private AppCompatTextView listItemOfferLabelTv;
        private AppCompatTextView itemDetailItemPriceTv;
        private AppCompatTextView itemDetailItemPriceCutTv;
        private AppCompatTextView itemDetailSKUTv;

        public ProductDetailViewHolder(View itemView) {
            super(itemView);
            itemDetailIv = (MaterialBanner) itemView.findViewById(R.id.itemDetailIv);
            listItemShareFAB = (FloatingActionButton) itemView.findViewById(R.id.listItemShareFAB);
            itemDetailItemNameTv = (AppCompatTextView) itemView.findViewById(R.id.itemDetailItemNameTv);
            listItemOfferLabelTv = (AppCompatTextView) itemView.findViewById(R.id.listItemOfferLabelTv);
            itemDetailItemPriceTv = (AppCompatTextView) itemView.findViewById(R.id.itemDetailItemPriceTv);
            itemDetailItemPriceCutTv = (AppCompatTextView) itemView.findViewById(R.id.itemDetailItemPriceCutTv);
            itemDetailSKUTv = (AppCompatTextView) itemView.findViewById(R.id.itemDetailSKUTv);

            listItemShareFAB.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            listener.onItemClickListener(getAdapterPosition(), view.getId(), null);
        }
    }

    protected class AddToCartViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private AppCompatButton itemDetailAddToCartBtn;
        private AppCompatImageView img_minus;
        private AppCompatImageView img_plus;
        private AppCompatTextView txt_quantity;

        public AddToCartViewHolder(View itemView) {
            super(itemView);
            itemDetailAddToCartBtn = (AppCompatButton) itemView.findViewById(R.id.itemDetailAddToCartBtn);
            img_minus = (AppCompatImageView) itemView.findViewById(R.id.img_minus);
            img_plus = (AppCompatImageView) itemView.findViewById(R.id.img_plus);
            txt_quantity = (AppCompatTextView) itemView.findViewById(R.id.txt_quantity);

            itemDetailAddToCartBtn.setOnClickListener(this);
            img_minus.setOnClickListener(this);
            img_plus.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if(view.getId() == R.id.img_plus){
                updateQuantity(getQuantity()+1);
            }else if(view.getId() == R.id.img_minus){
                if(getQuantity()>1) {
                    updateQuantity(getQuantity() -1);
                }
            }else if(view.getId() == R.id.itemDetailAddToCartBtn){
                if (awesomeValidation.validate()) {
                    listener.onItemClickListener(getAdapterPosition(), view.getId(), getQuantity());
                }
            }
        }

        private void updateQuantity(int qty){
            txt_quantity.setText(qty+"");
        }

        private int getQuantity(){
            return Integer.valueOf(txt_quantity.getText().toString().trim());
        }
    }

    protected class TierPriceViewHolder extends RecyclerView.ViewHolder{
        private AppCompatTextView adapterTierPriceTv;

        public TierPriceViewHolder(View itemView) {
            super(itemView);
            adapterTierPriceTv = (AppCompatTextView) itemView.findViewById(R.id.adapterTierPriceTv);
        }
    }

    protected class DescriptionViewHolder extends RecyclerView.ViewHolder{
        private ExpandableTextView expandable_text;

        public DescriptionViewHolder(View itemView) {
            super(itemView);
            expandable_text =  (ExpandableTextView) itemView.findViewById(R.id.expand_text_view);

            expandable_text.setOnExpandStateChangeListener((textView, isExpanded) -> {
                View expandCollapseView= expandable_text.findViewById(R.id.expand_collapse);
                expandCollapseView.setBackground(isExpanded?null:ContextCompat.getDrawable(mContext, R.drawable.tv_gradient_more));
            });
        }
    }

    protected class AppReviewViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private AppCompatTextView itemDetailBeTheFirstToReviewTv;
        private View itemDetailRatingLl;
        private AppCompatTextView itemDetailReviewNumberTv;
        private AppCompatTextView itemDetailRatingTv;
        private View itemDetailAppReviewCv;

        public AppReviewViewHolder(View itemView) {
            super(itemView);
            itemDetailBeTheFirstToReviewTv = (AppCompatTextView) itemView.findViewById(R.id.itemDetailBeTheFirstToReviewTv);
            itemDetailRatingLl =  itemView.findViewById(R.id.itemDetailRatingLl);
            itemDetailReviewNumberTv = (AppCompatTextView) itemView.findViewById(R.id.itemDetailReviewNumberTv);
            itemDetailRatingTv = (AppCompatTextView) itemView.findViewById(R.id.itemDetailRatingTv);
            itemDetailAppReviewCv = itemView.findViewById(R.id.itemDetailAppReviewCv);
            itemDetailAppReviewCv.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            listener.onItemClickListener(getAdapterPosition(), view.getId(), null);
        }
    }


    protected class CustomOptionTextFieldViewHolder extends RecyclerView.ViewHolder{
        private AppCompatTextView layout_custom_option_title_tv;
        private AppCompatTextView layout_custom_option_value_tv;
        private AppCompatEditText custom_option_et;

        public CustomOptionTextFieldViewHolder(View itemView) {
            super(itemView);
            layout_custom_option_title_tv = (AppCompatTextView) itemView.findViewById(R.id.layout_custom_option_title_tv);
            layout_custom_option_value_tv = (AppCompatTextView) itemView.findViewById(R.id.layout_custom_option_value_tv);
            custom_option_et = (AppCompatEditText) itemView.findViewById(R.id.custom_option_et);

        }
    }

    protected class CustomOptionSpinnerDropDownViewHolder extends RecyclerView.ViewHolder{
        private AppCompatTextView layout_custom_option_title_tv;
        private AppCompatSpinner custom_option_spinner;

        public CustomOptionSpinnerDropDownViewHolder(View itemView) {
            super(itemView);
            layout_custom_option_title_tv = (AppCompatTextView) itemView.findViewById(R.id.layout_custom_option_title_tv);
            custom_option_spinner = (AppCompatSpinner) itemView.findViewById(R.id.custom_option_spinner);

        }
    }

    protected class LabelHorizontalRecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private AppCompatTextView categoryHeaderTv;
        private AppCompatButton categorySeeMoreBtn;
        private RecyclerView labelRecyclerRv;
        private HorizontalItemAdapter itemListAdapter;

        public LabelHorizontalRecyclerViewHolder(View itemView) {
            super(itemView);
            categoryHeaderTv = (AppCompatTextView)itemView.findViewById(R.id.categoryHeaderTv);
            categorySeeMoreBtn = (AppCompatButton) itemView.findViewById(R.id.categorySeeMoreBtn);
            labelRecyclerRv = (RecyclerView) itemView.findViewById(R.id.labelRecyclerRv);
            labelRecyclerRv.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mContext, DividerItemDecoration.HORIZONTAL);
            dividerItemDecoration.setDrawable(ContextCompat.getDrawable(mContext, R.drawable.bg_divider));
            labelRecyclerRv.addItemDecoration(dividerItemDecoration);
            itemListAdapter = new HorizontalItemAdapter(mContext);
            labelRecyclerRv.setAdapter(itemListAdapter);
            categorySeeMoreBtn.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            try {
                Intent intent = new Intent(mContext, ItemListActivity.class);
                Homeproductlist itemCategory = (Homeproductlist)mData.get(getAdapterPosition());
                intent.putExtra(Constant.ID, itemCategory.getEntityId());
                mContext.startActivity(intent);

            }catch (ClassCastException|NullPointerException e){
                e.printStackTrace();
            }
        }
    }

    protected class SliderViewHolder implements Holder<Image> {
        private AppCompatImageView sliderIv;

        @Override
        public View createView(Context context) {
            View view = LayoutInflater.from(context).inflate(R.layout.detail_slider,null);
            sliderIv = (AppCompatImageView) view.findViewById(R.id.sliderIv);
            return view;
        }

        @Override
        public void UpdateUI(Context context, int i, Image banners) {
            Glide.with(context)
                    .load(banners.getUrl())
                    .dontAnimate()
                    .fitCenter()
                    .into(sliderIv);
        }
    }
}
