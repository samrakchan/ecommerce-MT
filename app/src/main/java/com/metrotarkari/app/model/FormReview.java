
package com.metrotarkari.app.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FormReview {

    @SerializedName("key_1")
    @Expose
    private String key1;
    @SerializedName("key_2")
    @Expose
    private String key2;
    @SerializedName("key_3")
    @Expose
    private String key3;
    @SerializedName("form_key")
    @Expose
    private List<FormKey> formKey = null;

    public String getKey1() {
        return key1;
    }

    public void setKey1(String key1) {
        this.key1 = key1;
    }

    public String getKey2() {
        return key2;
    }

    public void setKey2(String key2) {
        this.key2 = key2;
    }

    public String getKey3() {
        return key3;
    }

    public void setKey3(String key3) {
        this.key3 = key3;
    }

    public List<FormKey> getFormKey() {
        return formKey;
    }

    public void setFormKey(List<FormKey> formKey) {
        this.formKey = formKey;
    }

}
