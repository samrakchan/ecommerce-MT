package com.metrotarkari.app.ui.list.dagger;

import javax.inject.Scope;

@Scope
public @interface ItemListScope {
}
