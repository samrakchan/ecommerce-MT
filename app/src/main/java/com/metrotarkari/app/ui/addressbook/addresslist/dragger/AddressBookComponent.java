package com.metrotarkari.app.ui.addressbook.addresslist.dragger;

import com.metrotarkari.app.application.dagger.AppComponent;
import com.metrotarkari.app.ui.addressbook.addresslist.mvp.AddressListActivity;

import dagger.Component;

@AddressBookScope
@Component(modules = { AddressBookModule.class }, dependencies = AppComponent.class)
public interface AddressBookComponent {

  void inject(AddressListActivity addressBookActivity);

}
