package com.metrotarkari.app.ui.notifications.mvp;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.metrotarkari.app.R;
import com.metrotarkari.app.model.ChildCategory;
import com.metrotarkari.app.ui.base.BaseView;
import com.metrotarkari.app.ui.notifications.NotificationAdapter;
import com.metrotarkari.app.utils.ViewUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by samrakchan on 4/27/17.
 */

public class NotificationView extends BaseView {

    @BindView(R.id.toolbar)
    public Toolbar toolbar;

    @BindView(R.id.imageMsgRl)
    public View noItemView;

    @BindView(R.id.progressBarRl)
    public View progressBar;

    @Nullable
    @BindView(R.id.addressBookRv)
    public RecyclerView addressBookRv;

    private StaggeredGridLayoutManager staggeredGridLayoutManager;

    private NotificationAdapter notificationAdapter;


    private AppCompatActivity activity;

    public NotificationView(@NonNull AppCompatActivity activity) {
        super(activity);
        this.activity = activity;

        ButterKnife.bind(this);

        staggeredGridLayoutManager = new StaggeredGridLayoutManager(ViewUtils.calculateNoOfColumns(getContext()), StaggeredGridLayoutManager.VERTICAL);
        addressBookRv.setLayoutManager(staggeredGridLayoutManager);
        notificationAdapter = new NotificationAdapter(getContext());

        showProgressBar(false);
        showNoItem(false);

        activity.setSupportActionBar(toolbar);
        activity.getSupportActionBar().setTitle(R.string.notifications);

        notificationAdapter.addNotificationData(getObjectData());

        addressBookRv.setAdapter(notificationAdapter);

    }

    @Override
    public int getContentLayout() {
        return R.layout.activity_address_book;
    }


    public void showNoItem(boolean showItem){
        if(showItem)
            noItemView.setVisibility(View.VISIBLE);
        else
            noItemView.setVisibility(View.GONE);
    }

    public void showProgressBar(boolean showing){
        if(showing)
            progressBar.setVisibility(View.VISIBLE);
        else
            progressBar.setVisibility(View.GONE);
    }


    public List<Object> getObjectData(){
        List<Object> objects = new ArrayList<>();

        for(int i=0;i<=5;i++){
            ChildCategory cc = new ChildCategory();
            cc.setName("The is Bumper discount");
            cc.setImage("Upto 70% off +10% off HDFC credit card");
            objects.add(cc);

        }

        return objects;
    }
}
