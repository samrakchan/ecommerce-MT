package com.metrotarkari.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by samrakchan on 5/29/17.
 */

public class Detail extends ErrorResponse {

    @SerializedName("product")
    @Expose
    private Product_ product;


    public Product_ getProduct() {
        return product;
    }

    public void setProduct(Product_ product) {
        this.product = product;
    }

}
