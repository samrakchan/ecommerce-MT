package com.metrotarkari.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by samrakchan on 6/6/17.
 */

public class Homebanner {

    @SerializedName("banner_id")
    @Expose
    private String bannerId;
    @SerializedName("banner_name")
    @Expose
    private String bannerName;
    @SerializedName("banner_url")
    @Expose
    private String bannerUrl;
    @SerializedName("banner_name_tablet")
    @Expose
    private String bannerNameTablet;
    @SerializedName("banner_title")
    @Expose
    private String bannerTitle;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("website_id")
    @Expose
    private String websiteId;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("sort_order")
    @Expose
    private String sortOrder;
    @SerializedName("entity_id")
    @Expose
    private String entityId;
    @SerializedName("content_type")
    @Expose
    private String contentType;
    @SerializedName("item_id")
    @Expose
    private String itemId;
    @SerializedName("store_view_id")
    @Expose
    private String storeViewId;
    @SerializedName("width")
    @Expose
    private Integer width;
    @SerializedName("height")
    @Expose
    private Integer height;
    @SerializedName("width_tablet")
    @Expose
    private Integer widthTablet;
    @SerializedName("height_tablet")
    @Expose
    private Integer heightTablet;
    @SerializedName("has_children")
    @Expose
    private Boolean hasChildren;
    @SerializedName("cat_name")
    @Expose
    private String catName;

    public String getBannerId() {
        return bannerId;
    }

    public void setBannerId(String bannerId) {
        this.bannerId = bannerId;
    }

    public String getBannerName() {
        return bannerName;
    }

    public void setBannerName(String bannerName) {
        this.bannerName = bannerName;
    }

    public Object getBannerUrl() {
        return bannerUrl;
    }

    public void setBannerUrl(String bannerUrl) {
        this.bannerUrl = bannerUrl;
    }

    public String getBannerNameTablet() {
        return bannerNameTablet;
    }

    public void setBannerNameTablet(String bannerNameTablet) {
        this.bannerNameTablet = bannerNameTablet;
    }

    public String getBannerTitle() {
        return bannerTitle;
    }

    public void setBannerTitle(String bannerTitle) {
        this.bannerTitle = bannerTitle;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Object getWebsiteId() {
        return websiteId;
    }

    public void setWebsiteId(String websiteId) {
        this.websiteId = websiteId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getStoreViewId() {
        return storeViewId;
    }

    public void setStoreViewId(String storeViewId) {
        this.storeViewId = storeViewId;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getWidthTablet() {
        return widthTablet;
    }

    public void setWidthTablet(Integer widthTablet) {
        this.widthTablet = widthTablet;
    }

    public Integer getHeightTablet() {
        return heightTablet;
    }

    public void setHeightTablet(Integer heightTablet) {
        this.heightTablet = heightTablet;
    }

    public Boolean getHasChildren() {
        return hasChildren;
    }

    public void setHasChildren(Boolean hasChildren) {
        this.hasChildren = hasChildren;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }
}
