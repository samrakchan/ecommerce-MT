package com.metrotarkari.app.ui.productdetail.mvp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.metrotarkari.app.R;

import java.util.HashMap;
import java.util.List;

/**
 * Created by samrakchan on 8/25/17.
 */

public class CustomOptionSpinnerAdapter extends ArrayAdapter<Object> {
    private LayoutInflater inflater;

    public CustomOptionSpinnerAdapter(Context context, List<Object> objects) {
        super(context, R.layout.adapter_custom_option_spinner, objects);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {

        HashMap<String, Object> map = (HashMap<String, Object>) getItem(position);

        View view = inflater.inflate(R.layout.adapter_custom_option_spinner, parent, false);
        AppCompatTextView tv = (AppCompatTextView) view.findViewById(R.id.customOptionSpinnerTv);


        return view;
    }
}
