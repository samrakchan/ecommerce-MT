package com.metrotarkari.app.ui.addressbook.addressentry.dagger;

import com.metrotarkari.app.application.dagger.AppComponent;
import com.metrotarkari.app.ui.addressbook.addressentry.AddressEntryFragment;

import dagger.Component;

@AddAddressScope
@Component(modules = { AddAddressModule.class }, dependencies = AppComponent.class)
public interface AddAddressComponent {

  void inject(AddressEntryFragment fragment);

}
