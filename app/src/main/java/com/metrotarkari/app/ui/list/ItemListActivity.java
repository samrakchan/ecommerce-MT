package com.metrotarkari.app.ui.list;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.metrotarkari.app.R;
import com.metrotarkari.app.application.DoneApplication;
import com.metrotarkari.app.event.CartEvent;
import com.metrotarkari.app.search.SearchActivity;
import com.metrotarkari.app.ui.base.NonDraggerBaseActivity;
import com.metrotarkari.app.ui.cart.CartActivity;
import com.metrotarkari.app.utils.Constant;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import berlin.volders.badger.BadgeShape;
import berlin.volders.badger.Badger;
import berlin.volders.badger.CountBadge;

/**
 * Created by samrakchan on 3/18/17.
 */

public class ItemListActivity extends NonDraggerBaseActivity{

    private CountBadge.Factory circleFactory;

    private Menu menu;

    private EventBus eventBus = EventBus.getDefault();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        circleFactory = new CountBadge.Factory(this, BadgeShape.circle(.5f, Gravity.END | Gravity.TOP));

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        Intent intent = getIntent();

        ItemListFragment listFragment = new ItemListFragment();
        Bundle bundle = new Bundle();
        bundle.putString(Constant.ID, intent.getStringExtra(Constant.ID));
        listFragment.setArguments(bundle);

        if(toolbar!=null && intent.getStringExtra(Constant.TITLE)!=null){
            toolbar.setTitle(intent.getStringExtra(Constant.TITLE));
        }

        fragmentTransaction.replace(R.id.frameLayout, listFragment).commit();
    }

    @Override
    protected int getToolbarTitle() {
        return R.string.app_name;
    }

    @Override
    protected int getContentLayout() {
        return R.layout.activity_fragment_holder;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_detail, menu);
        this.menu = menu;

        if(DoneApplication.getInstance()!=null) {
            updateCartInMenu(DoneApplication.getInstance().getCartCount());
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        if(item.getItemId() == R.id.action_cart){
            Intent intent = new Intent(getApplicationContext(), CartActivity.class);
            startActivity(intent);
            return true;
        }else if (item.getItemId() == R.id.action_search){
            Intent intent = new Intent(getApplicationContext(), SearchActivity.class);
            startActivity(intent);
            return true;

        }
        return false;
    }


    public void updateCartInMenu(int count) {
        if (menu != null){
            Badger.sett(menu.findItem(R.id.action_cart), circleFactory).setCount(count);
        }
    }

    //change color of icons to white
    private void updateMenuColorToWhite(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if(menu!=null) {
                for (int i = 0; i < menu.size(); i++) {
                    boolean isCartIcon = (this.menu.getItem(i).getItemId() == R.id.action_cart);
                    if(isCartIcon) {
                        Drawable drawable = ContextCompat.getDrawable(this, R.mipmap.ic_toolbar_cart);
                        drawable.setColorFilter(ContextCompat.getColor(this, R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);
                        menu.getItem(i).setIcon(drawable);
                    }
                }
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onStart() {
        super.onStart();
        eventBus.register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (isFinishing()){
            //overridePendingTransition(R.anim.exit_to_left, R.anim.enter_from_right);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        eventBus.unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(DoneApplication.getInstance()!=null) {
            updateMenuColorToWhite(); //for older version only
            updateCartInMenu(DoneApplication.getInstance().getCartCount());
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(CartEvent event){
        updateCartInMenu(event.getCartCount());
    }
}

