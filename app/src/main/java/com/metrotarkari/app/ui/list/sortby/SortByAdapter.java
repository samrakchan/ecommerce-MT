package com.metrotarkari.app.ui.list.sortby;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bignerdranch.expandablerecyclerview.ChildViewHolder;
import com.metrotarkari.app.R;
import com.metrotarkari.app.dialogue.filter.model.FilterChild;
import com.metrotarkari.app.model.Sort;

import java.util.List;

/**
 * Created by samrakchan on 4/5/17.
 */

public class SortByAdapter extends RecyclerView.Adapter<SortByAdapter.ViewHolder>{
    private List<Sort> mData;
    private Context mContext;
    private Sort selectedOrder;

    public SortByAdapter(Context context, List<Sort> data){
        this.mData = data;
        this.mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.dialog_adapter_filter_content,parent,false);
        return new SortByAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Sort order = mData.get(position);
        holder.bind(order);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    protected class ViewHolder extends ChildViewHolder<FilterChild> implements View.OnClickListener{

        private AppCompatTextView dialogFilterContentTitleTv;
        private AppCompatRadioButton dialogFilterContentSelectIndicatorRb;
        private AppCompatTextView dialogFilterContentItemCountTv;

        /**
         * Default constructor.
         *
         * @param itemView The {@link View} being hosted in this ViewHolder
         */
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            dialogFilterContentTitleTv = (AppCompatTextView) itemView.findViewById(R.id.dialogFilterContentTitleTv);
            dialogFilterContentSelectIndicatorRb = (AppCompatRadioButton) itemView.findViewById(R.id.dialogFilterContentSelectIndicatorRb);
            dialogFilterContentItemCountTv = (AppCompatTextView) itemView.findViewById(R.id.dialogFilterContentItemCountTv);



        }

        public void bind(@NonNull Sort order) {
                dialogFilterContentTitleTv.setText(order.getValue());
            dialogFilterContentItemCountTv.setText(order.getDirection());
                dialogFilterContentSelectIndicatorRb.setChecked(order.getDefault().equalsIgnoreCase("1"));
                if(order.getDefault().equalsIgnoreCase("1")){
                    selectedOrder = order;
                }
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            Sort order = mData.get(getAdapterPosition());

            for(int i=0; i<mData.size(); i++){
                if(i!=getAdapterPosition()) {
                    mData.get(i).setDefault("0");
                }
            }

            mData.get(getAdapterPosition()).setDefault(order.getDefault().equalsIgnoreCase("1")?"0":"1");
            if(mData.get(getAdapterPosition()).getDefault().equalsIgnoreCase("1")){
                selectedOrder = order;
            }
            notifyDataSetChanged();
        }
    }

    public Sort getSelectedOrder(){
        return selectedOrder;
    }

}
