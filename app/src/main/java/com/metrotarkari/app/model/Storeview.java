
package com.metrotarkari.app.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Storeview {

    @SerializedName("base")
    @Expose
    private Base base;
    @SerializedName("cms")
    @Expose
    private Cms cms;
    @SerializedName("category_cmspages")
    @Expose
    private List<Object> categoryCmspages = null;
    @SerializedName("mixpanel_config")
    @Expose
    private MixpanelConfig mixpanelConfig;
    @SerializedName("country")
    @Expose
    private List<Country> country = null;
    @SerializedName("categories")
    @Expose
    private List<Category> categories = null;
    @SerializedName("shipping")
    @Expose
    private List<Shipping> shipping = null;
    @SerializedName("payment")
    @Expose
    private List<Payment> payment = null;
    @SerializedName("stores")
    @Expose
    private Stores stores;

    public Base getBase() {
        return base;
    }

    public void setBase(Base base) {
        this.base = base;
    }

    public Cms getCms() {
        return cms;
    }

    public void setCms(Cms cms) {
        this.cms = cms;
    }

    public List<Object> getCategoryCmspages() {
        return categoryCmspages;
    }

    public void setCategoryCmspages(List<Object> categoryCmspages) {
        this.categoryCmspages = categoryCmspages;
    }

    public MixpanelConfig getMixpanelConfig() {
        return mixpanelConfig;
    }

    public void setMixpanelConfig(MixpanelConfig mixpanelConfig) {
        this.mixpanelConfig = mixpanelConfig;
    }

    public List<Country> getCountry() {
        return country;
    }

    public void setCountry(List<Country> country) {
        this.country = country;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public List<Shipping> getShipping() {
        return shipping;
    }

    public void setShipping(List<Shipping> shipping) {
        this.shipping = shipping;
    }

    public List<Payment> getPayment() {
        return payment;
    }

    public void setPayment(List<Payment> payment) {
        this.payment = payment;
    }

    public Stores getStores() {
        return stores;
    }

    public void setStores(Stores stores) {
        this.stores = stores;
    }

}
