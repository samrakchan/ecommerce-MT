package com.metrotarkari.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by samrakchan on 5/29/17.
 */

public class QuantityAndStockStatus {

    @SerializedName("is_in_stock")
    @Expose
    private Boolean isInStock;
    @SerializedName("qty")
    @Expose
    private Integer qty;

    public Boolean getIsInStock() {
        return isInStock;
    }

    public void setIsInStock(Boolean isInStock) {
        this.isInStock = isInStock;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }
}
