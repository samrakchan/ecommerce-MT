package com.metrotarkari.app.ui.dashboard.mvp;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;

import com.metrotarkari.app.R;
import com.metrotarkari.app.model.Category;
import com.metrotarkari.app.model.ChildCategory;
import com.metrotarkari.app.search.SearchActivity;
import com.metrotarkari.app.ui.about.AboutFragment;
import com.metrotarkari.app.ui.accounts.AccountsFragment;
import com.metrotarkari.app.ui.b2b.view.B2bActivity;
import com.metrotarkari.app.ui.categories.CategoriesFragment;
import com.metrotarkari.app.ui.home.HomeFragment;
import com.metrotarkari.app.ui.list.ItemListActivity;
import com.metrotarkari.app.ui.login.LoginActivity2;
import com.metrotarkari.app.utils.Constant;
import com.metrotarkari.app.utils.DoneItemClickListener;
import com.metrotarkari.app.utils.DoneUserPreferenceManager;
import com.metrotarkari.app.utils.Utility;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by samrakchan on 4/27/17.
 */
public class MainView extends FrameLayout implements NavigationView.OnNavigationItemSelectedListener,
        View.OnClickListener, DoneItemClickListener /*ExpandableListView.OnChildClickListener,
        ExpandableListView.OnGroupClickListener*/{

    @BindView(R.id.appBarLayout)
    public AppBarLayout appBarLayout;

    @BindView(R.id.toolbar)
    public Toolbar toolbar;

    @BindView(R.id.tabs)
    public TabLayout tabLayout;

    /*@BindView(R.id.viewpager)
    public ViewPager viewPager;*/

    @BindView(R.id.searchView)
    public View searchView;

    @BindView(R.id.drawer_layout)
    public DrawerLayout drawerLayout;

    @BindView(R.id.nav_view)
    public NavigationView nav_view;

    public AppCompatTextView headerGuestNameTv;

    private View menuContainerView;


   /* public ExpandableListView menuExpendedLv;
    private ExpendedMenuAdapter expendedMenuAdapter;*/

    //private ViewPagerAdapter adapter;

    private AppCompatActivity appCompatActivity;
    /*private AppCompatTextView navInitalsTv;

    public AppCompatButton navHeaderLoginBtn;*/
    public AppCompatImageButton menuFacebookIb;
    public AppCompatImageButton menuAboutIb;
    public AppCompatImageButton menuSettingsIb;
    public AppCompatImageButton menuBtoBIb;

     public DoneUserPreferenceManager doneUserPreferenceManager;

    private FragmentManager fragmentManager;

    private HomeFragment homeFragment;
    private CategoriesFragment categoriesFragment;
    private AccountsFragment accountsFragment;
    private AboutFragment aboutFragment;

    MenuParentFragment fragmentMenuMain;


    public MainView(AppCompatActivity activity){
        super(activity);
        this.appCompatActivity = activity;

        fragmentManager = activity.getSupportFragmentManager();

        inflate(getContext(), R.layout.activity_main, this);

        ButterKnife.bind(this);

        doneUserPreferenceManager = new DoneUserPreferenceManager(getContext());

        searchView.setOnClickListener(this);

        appCompatActivity.setSupportActionBar(toolbar);
        appCompatActivity.getSupportActionBar().setTitle(R.string.app_name);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        final ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                activity, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        //initDrawerMenuTypeOneItems();

        headerGuestNameTv = (AppCompatTextView) nav_view.findViewById(R.id.headerGuestNameTv);
        /*navInitalsTv = (AppCompatTextView) nav_view.findViewById(R.id.navInitalsTv);
        navHeaderLoginBtn = (AppCompatButton) nav_view.findViewById(R.id.navHeaderLoginBtn);*/
        menuFacebookIb = (AppCompatImageButton) nav_view.findViewById(R.id.menuFacebookIb);
        menuAboutIb = (AppCompatImageButton) nav_view.findViewById(R.id.menuAboutIb);
        menuSettingsIb = (AppCompatImageButton) nav_view.findViewById(R.id.menuSettingsIb);
        menuBtoBIb = (AppCompatImageButton) nav_view.findViewById(R.id.menuBtoBIb);

      /*  if (doneUserPreferenceManager.isLoggedInUser()){
            navHeaderLoginBtn.setVisibility(View.INVISIBLE);
        }

        navHeaderLoginBtn.setOnClickListener(this);*/

        menuFacebookIb.setOnClickListener(this);
        menuAboutIb.setOnClickListener(this);
        menuSettingsIb.setOnClickListener(this);
        menuBtoBIb.setOnClickListener(this);

        //setupViewPager();

        /*if(doneUserPreferenceManager.isB2bUser()){
            menuBtoBIb.setVisibility(View.GONE);
        }*/

        tabLayout.addTab(tabLayout.newTab().setText(R.string.home).setIcon(R.mipmap.ic_tab_home));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.categories).setIcon(R.mipmap.ic_tab_categories));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.account).setIcon(R.mipmap.ic_tab_account));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.about).setIcon(R.mipmap.ic_tab_about));


        showFragment(new HomeFragment(), 0);
        tabLayout.getTabAt(0).select();


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            tabLayout.getTabAt(0).getIcon().setColorFilter(ContextCompat.getColor(getContext(),R.color.colorGreen), PorterDuff.Mode.SRC_IN);
            tabLayout.getTabAt(1).getIcon().setColorFilter(ContextCompat.getColor(getContext(),R.color.colorGrey), PorterDuff.Mode.SRC_IN);
            tabLayout.getTabAt(2).getIcon().setColorFilter(ContextCompat.getColor(getContext(),R.color.colorGrey), PorterDuff.Mode.SRC_IN);
            tabLayout.getTabAt(3).getIcon().setColorFilter(ContextCompat.getColor(getContext(),R.color.colorGrey), PorterDuff.Mode.SRC_IN);
        }

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                tab.getIcon().setColorFilter(ContextCompat.getColor(getContext(),R.color.colorGreen), PorterDuff.Mode.SRC_IN);
                CharSequence title = tab.getText();
                if(title.equals(getContext().getString(R.string.home))){
                    activity.getSupportActionBar().setTitle(R.string.app_name);
                }else {
                    activity.getSupportActionBar().setTitle(title);
                }

                int position = tab.getPosition();
                displayFragment(position);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                tab.getIcon().setColorFilter(ContextCompat.getColor(getContext(),R.color.colorGrey), PorterDuff.Mode.SRC_IN);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }

        });

        initDrawer();


        //viewPager.getCurrentItem();
    }

    private void displayFragment(int position){
        //navigation to the different fragment according to the postion in the tablayout
        if(position == 0){
            if(homeFragment==null) {
                homeFragment = new HomeFragment();
            }
            showFragment(homeFragment, position);
        }else if(position == 1){
            if(categoriesFragment == null) {
                categoriesFragment = new CategoriesFragment();
            }
            showFragment(categoriesFragment,position);
        }else if(position ==2){
            if(accountsFragment == null) {
                accountsFragment = new AccountsFragment();
            }
            showFragment(accountsFragment,position);
        }else if(position == 3){
            if(aboutFragment == null) {
                aboutFragment = new AboutFragment();
            }
            showFragment(aboutFragment,position);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

/*    private void setupViewPager() {
        adapter = new ViewPagerAdapter(appCompatActivity.getSupportFragmentManager());
        adapter.addFragment(new HomeFragment(), appCompatActivity.getString(R.string.home));
        adapter.addFragment(new CategoriesFragment(), appCompatActivity.getString(R.string.categories));
        adapter.addFragment(new AccountsFragment(), appCompatActivity.getString(R.string.account));
        adapter.addFragment(new AboutFragment(), appCompatActivity.getString(R.string.about));

        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(4);

//        appCompatActivity.getSupportActionBar().setTitle(adapter.getPageTitle(viewPager.getCurrentItem()));
    }*/


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            //Search
            case R.id.searchView:
                Intent intent = new Intent(getContext(), SearchActivity.class);
                ActivityOptionsCompat options = ActivityOptionsCompat.
                        makeSceneTransitionAnimation(appCompatActivity, searchView, "search");
                appCompatActivity.startActivity(intent, options.toBundle());
                break;
            case R.id.navHeaderLoginBtn:
                Intent intent2 = new Intent(getContext(), LoginActivity2.class);
                appCompatActivity.startActivity(intent2);
                break;
            case R.id.menuFacebookIb:
                Utility.openFacebook(appCompatActivity);
                break;
            case R.id.menuAboutIb:
                tabLayout.getTabAt(3).select();
                toggleDrawer();
                break;
            case R.id.menuSettingsIb:
                tabLayout.getTabAt(2).select();
                toggleDrawer();
                break;
            case R.id.menuBtoBIb:
                Intent intent3 = new Intent(getContext(), B2bActivity.class);
                appCompatActivity.startActivity(intent3);
                toggleDrawer();
                break;
        }
    }

/*    @Override
    public boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i1, long l) {

        Intent intent = new Intent(getContext(), ItemListActivity.class);
        ChildCategory childCategory = (ChildCategory) expendedMenuAdapter.getChild(i, i1);

        if(childCategory!=null) {
            intent.putExtra("ID", childCategory.getIds());
            intent.putExtra("NAME", childCategory.getName());
            appCompatActivity.startActivity(intent);
        }

        return false;
    }

    @Override
    public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {
        //Intent intent = new Intent(getContext(), ItemListActivity.class);
        //appCompatActivity.startActivity(intent);
        return false;
    }*/
/*
    private class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }*/

/*    private void initDrawerMenuTypeOneItems(){
        menuExpendedLv = (ExpandableListView) nav_view.findViewById(R.id.menuExpendedLv);
        headerGuestNameTv = (AppCompatTextView) nav_view.getHeaderView(0).findViewById(R.id.headerGuestNameTv);
        navInitalsTv = (AppCompatTextView) nav_view.getHeaderView(0).findViewById(R.id.navInitalsTv);
        navHeaderLoginBtn = (AppCompatButton) nav_view.getHeaderView(0).findViewById(R.id.navHeaderLoginBtn);
        menuFacebookIb = (AppCompatImageButton) nav_view.findViewById(R.id.menuFacebookIb);
        menuExpendedLv.setOnChildClickListener(this);
        menuExpendedLv.setOnGroupClickListener(this);
        expendedMenuAdapter = new ExpendedMenuAdapter(getContext());
        menuExpendedLv.setAdapter(expendedMenuAdapter);

    }*/

    public void addItemToDrawer(List<Category> categories){
        //expendedMenuAdapter.addItems(categories);
        if(fragmentMenuMain != null) {
            fragmentMenuMain.addItemToAdapter(categories);
        }
    }

    public void setUserName(String userName){
//        headerGuestNameTv.setText(getContext().getString(R.string.welcome)+" "+userName);
    }

    public void setNavInitialTv(String name){
        //navInitalsTv.setText(name.substring(0,1).toUpperCase());
    }

    private void showFragment(Fragment fragment, int position){
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.replace(R.id.mainFragmentContainer, fragment, position + "");
        ft.commit();
    }

    private void initDrawer(){
        menuContainerView = nav_view.findViewById(R.id.menuContainerFl);
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentMenuMain = new MenuParentFragment();
        fragmentMenuMain.setItemClickListener(this);
        fragmentTransaction.add(menuContainerView.getId(), fragmentMenuMain, "HE");
        fragmentTransaction.commit();
    }


    @Override
    public void onItemClickListener(int position, int viewId, Object object) {
        //Menu Drawer item Click listener.
        //this is for back click listener
        if(viewId == R.id.menuChildMainMenuLl){
            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

        }else if(object instanceof Category){

            Category category = (Category) object;

            if(category.getChildCategory()!=null && !category.getChildCategory().isEmpty()) {
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                MenuChildFragment fragmentSubMenuMain = new MenuChildFragment();
                Bundle bundle = new Bundle();
                bundle.putParcelable("CATEGORY", category);
                fragmentSubMenuMain.setArguments(bundle);
                fragmentSubMenuMain.setItemClickListener(this);
                fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                fragmentTransaction.add(menuContainerView.getId(), fragmentSubMenuMain, "HELLO");
                fragmentTransaction.addToBackStack("HELLO");
                fragmentTransaction.commit();
            }else {
                Intent intent = new Intent(getContext(), ItemListActivity.class);
                intent.putExtra(Constant.ID, category.getId());
                intent.putExtra(Constant.TITLE, category.getName());
                appCompatActivity.startActivity(intent);
            }

        }else if(object instanceof ChildCategory){

            toggleDrawer();

            ChildCategory childCategory = (ChildCategory) object;
            Intent intent = new Intent(getContext(), ItemListActivity.class);
            intent.putExtra(Constant.ID, childCategory.getId());
            intent.putExtra(Constant.TITLE, childCategory.getName());
            appCompatActivity.startActivity(intent);
        }
    }

    public void toggleDrawer(){
        if (drawerLayout.isDrawerOpen(Gravity.LEFT)) {
            drawerLayout.closeDrawer(Gravity.LEFT);
        } else {
            drawerLayout.openDrawer(Gravity.LEFT);
        }
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        return super.onSaveInstanceState();
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        super.onRestoreInstanceState(state);
    }
}
