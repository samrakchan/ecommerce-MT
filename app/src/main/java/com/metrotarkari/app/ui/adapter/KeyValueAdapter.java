package com.metrotarkari.app.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.metrotarkari.app.R;
import com.metrotarkari.app.model.KeyValue;

import java.util.List;

/**
 * Created by samrakchan on 7/10/17.
 */

public class KeyValueAdapter extends ArrayAdapter<KeyValue> {
    private LayoutInflater layoutInflater;
    private List<KeyValue> list;

    public KeyValueAdapter(@NonNull Context context, List<KeyValue> list) {
        super(context, R.layout.adapter_spinner, list);
        this.list = list;
        layoutInflater =  (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        KeyValue keyValue = list.get(position);
        View row = layoutInflater.inflate(R.layout.adapter_spinner, parent, false);
        ((AppCompatTextView)row.findViewById(R.id.spinnerTextTv)).setText(keyValue.getValue());
        return row;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        KeyValue keyValue = list.get(position);
        View row = layoutInflater.inflate(R.layout.adapter_spinner, parent, false);
        ((AppCompatTextView)row.findViewById(R.id.spinnerTextTv)).setText(keyValue.getValue());
        return row;
    }
}
