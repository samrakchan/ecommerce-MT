package com.metrotarkari.app.ui.cart;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.metrotarkari.app.R;
import com.metrotarkari.app.model.BestSell;
import com.metrotarkari.app.model.Quoteitem;
import com.metrotarkari.app.ui.adapter.HorizontalItemAdapter;
import com.metrotarkari.app.ui.productdetail.DetailActivity;
import com.metrotarkari.app.utils.Constant;
import com.metrotarkari.app.utils.DoneItemClickListener;
import com.metrotarkari.app.utils.Utility;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by samrakchan on 3/21/17.
 */

public class CartAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Object> mData;
    private Context mContext;
    private DoneItemClickListener listener;

    private static final int CART_ITEMS_VIEW = 1;
    private static final int HEADING_VIEW = 2;
    private static final int HORIZONTAL_ITEM_LIST_VIEW = 3;

    public CartAdapter(Context context) {
        mData = new ArrayList<>();
        this.mContext = context;
    }

    public void addData(List<Object> datas) {
        this.mData.addAll(datas);
        notifyDataSetChanged();
    }

    public void clearItem(){
        this.mData.clear();
        notifyDataSetChanged();
    }

    public void addOnItemClickListener(DoneItemClickListener listener){
        this.listener = listener;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == CART_ITEMS_VIEW){

            View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_cart_list, parent, false);
            return new CartAdapter.CategoryItemList(view);

        }else if (viewType == HORIZONTAL_ITEM_LIST_VIEW){
            View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_label_recycler, parent, false);
            return new CartAdapter.CartHorizontalItemList(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Object object = mData.get(position);
        if(object instanceof Quoteitem && holder instanceof CartAdapter.CategoryItemList) {
            CartAdapter.CategoryItemList categoryItemListHolder = (CartAdapter.CategoryItemList) holder;
            Quoteitem categoryItem = (Quoteitem) mData.get(position);
            categoryItemListHolder.listItemNameTv.setText(categoryItem.getName());
            categoryItemListHolder.listItemPriceTv.setText(categoryItem.getFormattedBaseRowTotal());
            categoryItemListHolder.listItemQtyTv.setText(categoryItem.getQty()+"");
            Log.i("URL", categoryItem.getImage());
            Glide.with(mContext)
                    .load(categoryItem.getImage())
                    .centerCrop()
                    .placeholder(R.mipmap.ic_placeholder_mt)
                    .dontAnimate()
                    .into(categoryItemListHolder.listItemImageIv);

        }else if (object instanceof BestSell && holder instanceof CartAdapter.CartHorizontalItemList){
            CartAdapter.CartHorizontalItemList cartHorizontalItemList = (CartAdapter.CartHorizontalItemList) holder;
            BestSell bestSell = (BestSell) mData.get(position);
            if (bestSell!= null && bestSell.getProducts()!=null && !bestSell.getProducts().isEmpty()) {
                cartHorizontalItemList.categoryHeaderTv.setText(R.string.best_sell);
                cartHorizontalItemList.relatedProductAdapter.addData(bestSell.getProducts());
                cartHorizontalItemList.labelRecyclerRv.setAdapter(cartHorizontalItemList.relatedProductAdapter);
            }
        }
    }

   /* @Override
    public void onBindViewHolder(CartAdapter.CategoryItemList holder, int position) {
        Quoteitem categoryItem = mData.get(position);
        holder.listItemNameTv.setText(categoryItem.getName());
        holder.listItemPriceTv.setText(mContext.getString(R.string.price, Utility.convertToTwoDecimalPlace(categoryItem.getBaseRowTotal()+"")));
        holder.listItemQtyTv.setText(categoryItem.getQty()+"");
        Log.i("URL", categoryItem.getImage());
        Glide.with(mContext)
                .load(categoryItem.getImage()).placeholder(R.mipmap.ic_placeholder_mt)
                .into(holder.listItemImageIv);

    }*/

    @Override
    public int getItemCount() {
        return mData.size();
    }

    @Override
    public int getItemViewType(int position) {
        Object items = mData.get(position);

        if (items instanceof Quoteitem){
            return CART_ITEMS_VIEW;
        }else if (items instanceof String){
            return HEADING_VIEW;
        }else if (items instanceof BestSell){
            return HORIZONTAL_ITEM_LIST_VIEW;
        }
        return -1;

    }

    protected class CategoryItemList extends RecyclerView.ViewHolder  implements View.OnClickListener{

        private AppCompatImageView listItemImageIv;
        private AppCompatImageView imageMinus;
        private AppCompatImageView imagePlus;
        private AppCompatTextView textQty;
        private AppCompatTextView listItemPriceTv;
        private AppCompatTextView listItemNameTv;
        private AppCompatTextView listItemQtyTv;
        private AppCompatButton listItemDeleteIv;

        public CategoryItemList(View itemView) {
            super(itemView);

            listItemImageIv = (AppCompatImageView) itemView.findViewById(R.id.listItemImageIv);
            imageMinus = (AppCompatImageView)itemView.findViewById(R.id.img_minus);
            imagePlus = (AppCompatImageView)itemView.findViewById(R.id.img_plus) ;
            textQty = (AppCompatTextView) itemView.findViewById(R.id.txt_quantity);
            listItemPriceTv = (AppCompatTextView) itemView.findViewById(R.id.listItemPriceTv);
            listItemNameTv = (AppCompatTextView) itemView.findViewById(R.id.listItemNameTv);
            listItemQtyTv = (AppCompatTextView) itemView.findViewById(R.id.txt_quantity);
            listItemDeleteIv = (AppCompatButton) itemView.findViewById(R.id.listItemDeleteIv);
            listItemDeleteIv.setOnClickListener(this);
            imageMinus.setOnClickListener(this);
            imagePlus.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Quoteitem quoteitem = (Quoteitem) mData.get(getAdapterPosition());
            if(view.getId() == R.id.listItemDeleteIv){
                //To delete Item make item count zero
                quoteitem.setItemQtyChangedTo(0);
                listener.onItemClickListener(getAdapterPosition(), view.getId(), quoteitem);

            }else if (view.getId() == R.id.img_minus){
                quoteitem.setItemQtyChangedTo(quoteitem.getQty()-1);
                listener.onItemClickListener(getAdapterPosition(), view.getId(), quoteitem);
            }else if (view.getId() == R.id.img_plus){
                quoteitem.setItemQtyChangedTo(quoteitem.getQty()+1);
                listener.onItemClickListener(getAdapterPosition(), view.getId(), quoteitem);
            }else{
                Intent intent = new Intent(mContext, DetailActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(Constant.PRODUCT_ID, quoteitem.getProductId());
                mContext.startActivity(intent);
            }
        }
    }


    protected class CartHorizontalItemList extends RecyclerView.ViewHolder{

        private AppCompatTextView categoryHeaderTv;
        private RecyclerView labelRecyclerRv;
        private HorizontalItemAdapter relatedProductAdapter;

        public CartHorizontalItemList(View itemView) {
            super(itemView);
            categoryHeaderTv = (AppCompatTextView) itemView.findViewById(R.id.categoryHeaderTv);
            labelRecyclerRv = (RecyclerView) itemView.findViewById(R.id.labelRecyclerRv);
            relatedProductAdapter = new HorizontalItemAdapter(mContext);
            labelRecyclerRv.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mContext,DividerItemDecoration.HORIZONTAL);
            dividerItemDecoration.setDrawable(ContextCompat.getDrawable(mContext, R.drawable.bg_divider));
            labelRecyclerRv.addItemDecoration(dividerItemDecoration);
            labelRecyclerRv.setHasFixedSize(true);
            int marginHeight = Utility.dpToPx((int)mContext.getResources().getDimension(R.dimen.content_padding_md), mContext);
            ViewGroup.MarginLayoutParams marginLayoutParams =(ViewGroup.MarginLayoutParams) labelRecyclerRv.getLayoutParams();
            marginLayoutParams.setMargins(0, 0, 0, marginHeight);
            labelRecyclerRv.setLayoutParams(marginLayoutParams);
        }
    }
}