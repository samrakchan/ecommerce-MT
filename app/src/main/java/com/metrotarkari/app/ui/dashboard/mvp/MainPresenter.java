package com.metrotarkari.app.ui.dashboard.mvp;

import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.metrotarkari.app.R;
import com.metrotarkari.app.application.network.NoConnectivityException;
import com.metrotarkari.app.db.DoneDbAdapter;
import com.metrotarkari.app.model.CustomerResponse;
import com.metrotarkari.app.model.Error;
import com.metrotarkari.app.model.StoreViewResult;
import com.metrotarkari.app.model.Storeview;
import com.metrotarkari.app.utils.DoneUserPreferenceManager;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static android.content.ContentValues.TAG;

/**
 * Created by samrakchan on 4/27/17.
 */

public class MainPresenter {
    private final MainView view;
    private final MainModel model;
    private final DoneDbAdapter doneDbAdapter;
    private DoneUserPreferenceManager doneUserPreferenceManager;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();


    public MainPresenter(MainView view, MainModel model, DoneDbAdapter doneDbAdapter, DoneUserPreferenceManager preferenceManager){
        this.view = view;
        this.model = model;
        this.doneDbAdapter = doneDbAdapter;
        //compositeDisposable.add(getCategories());
        compositeDisposable.add(getStoreViews());
        doneUserPreferenceManager = preferenceManager;

        if (doneUserPreferenceManager.isLoggedInUser()) {
            getName();
        }

    }

    public void onDestroy(){
        compositeDisposable.dispose();
    }

    private void getName(){
        doneDbAdapter.openReadableDb();
        String jsonString = doneDbAdapter.getCustomer();
        doneDbAdapter.closeDb();
        Log.i(TAG, "JSON "+jsonString);
        if(jsonString!=null) {
            Gson gson = new Gson();
            CustomerResponse customerResponse = gson.fromJson(jsonString, CustomerResponse.class);
            if (customerResponse != null) {
                List<Error> errorResponse = customerResponse.getErrors();

                if (errorResponse != null && !errorResponse.isEmpty()) {
                    Error error = errorResponse.get(0);
                    Toast.makeText(view.getContext(), error.getMessage() + "", Toast.LENGTH_LONG).show();
                }
            }
        }

    }

    private Disposable getStoreViews(){
        return model.getStoreViews().observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(storeViewResult -> {
                    List<Error> errorResponse = storeViewResult.getErrors();
                    if(errorResponse!=null && !errorResponse.isEmpty()){
                        Error error = errorResponse.get(0);
                        Toast.makeText(view.getContext(),error.getMessage()+"",Toast.LENGTH_LONG).show();
                    }else if(storeViewResult!=null) {
                        Log.i("MainPresenter", storeViewResult.toString());
                        Storeview storeview = storeViewResult.getStoreview();
                        if(storeview!=null) {
                            addItemToDrawer(storeViewResult);
                            doneDbAdapter.openWritableDb();
                            Gson gson = new Gson();
                            doneDbAdapter.insetIntoStoreView(gson.toJson(storeViewResult));
                            doneDbAdapter.closeDb();
                        }
                    }
                }, throwable -> {
                    if(throwable instanceof NoConnectivityException){
                        doneDbAdapter.openReadableDb();
                        Gson gson = new Gson();
                        StoreViewResult storeViewResult = gson.fromJson(doneDbAdapter.getStoreView(), StoreViewResult.class);
                        addItemToDrawer(storeViewResult);
                        doneDbAdapter.closeDb();
                    }else{
                        throwable.printStackTrace();
                    }
                });
    }

    private void addItemToDrawer(StoreViewResult storeViewResult){
        if(storeViewResult!=null && storeViewResult.getStoreview() !=null) {
            view.addItemToDrawer(storeViewResult.getStoreview().getCategories());
        }
    }



    private Disposable getCategories(){
        return model.getCategories().observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(configResponse -> {
                    List<Error> errorResponse = configResponse.getErrors();
                    if(errorResponse!=null && !errorResponse.isEmpty()){
                        Error error = errorResponse.get(0);
                        Toast.makeText(view.getContext(),error.getMessage()+"",Toast.LENGTH_LONG).show();
                    }else if(configResponse!=null) {
                        Log.i("MainPresenter", configResponse.toString());
                        view.addItemToDrawer(configResponse.getCategories());

                        doneDbAdapter.openWritableDb();
                        Gson gson = new Gson();
                        doneDbAdapter.insetIntoStoreView(gson.toJson(configResponse));
                        doneDbAdapter.closeDb();

                    }
                }, throwable -> {
                    throwable.printStackTrace();
                    if(throwable.hashCode()==503){
                        Toast.makeText(view.getContext(), R.string.sww, Toast.LENGTH_LONG).show();
                    }
                });
    }


}
