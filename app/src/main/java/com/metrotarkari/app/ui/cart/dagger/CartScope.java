package com.metrotarkari.app.ui.cart.dagger;

import javax.inject.Scope;

@Scope
public @interface CartScope {
}
