package com.metrotarkari.app.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

/**
 * Created by samrakchan on 6/22/17.
 */

public class NetworkBroadcastReceiver extends BroadcastReceiver {
    public static final String TAG = NetworkBroadcastReceiver.class.getSimpleName();
    public static final String NETWORK_AVAILABLE_ACTION = "com.metrotarkari.app.NetworkAvailable";
    public static final String IS_NETWORK_AVAILABLE = "isNetworkAvailable";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(TAG, "Network Receiver");
        Intent networkStateIntent = new Intent(NETWORK_AVAILABLE_ACTION);
        networkStateIntent.putExtra(IS_NETWORK_AVAILABLE, NetworkUtils.isConnected(context));
        LocalBroadcastManager.getInstance(context).sendBroadcast(networkStateIntent);
    }
}
