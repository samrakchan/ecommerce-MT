package com.metrotarkari.app.ui.cart.mvp;

import android.app.Activity;

import com.metrotarkari.app.application.network.DoneNetwork;
import com.metrotarkari.app.model.AddressResponse;
import com.metrotarkari.app.model.CartQuoteContent;
import com.metrotarkari.app.model.ItemParams;

import java.util.HashMap;

import io.reactivex.Observable;

/**
 * Created by samrakchan on 4/27/17.
 */

public class CartModel {
    private final Activity activity;
    private final DoneNetwork doneNetwork;

    public CartModel(Activity activity, DoneNetwork doneNetwork){
        this.activity = activity;
        this.doneNetwork = doneNetwork;
    }

    public void finishActivity(){
        activity.finish();
    }

    public Observable<CartQuoteContent> getCartQuote(ItemParams itemParams){
        return doneNetwork.getQuoteItem(itemParams.getPage(), itemParams.getLimit(), itemParams.getOrderBy(), itemParams.getDir());
    }

    public Observable<CartQuoteContent> putCartQuote(HashMap<String, Integer> itemQty){
        return doneNetwork.putQuoteItem(itemQty);
    }

    public Observable<AddressResponse> getAddressBook(){
        return doneNetwork.getAddressBook();
    }
}
