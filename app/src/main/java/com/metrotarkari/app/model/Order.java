package com.metrotarkari.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by samrakchan on 6/12/17.
 */

public class Order extends ErrorResponse{

    @SerializedName("order")
    @Expose
    private Order_ order;

    public Order_ getOrder() {
        return order;
    }

    public void setOrder(Order_ order) {
        this.order = order;
    }

}
