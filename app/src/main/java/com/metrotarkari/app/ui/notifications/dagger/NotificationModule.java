package com.metrotarkari.app.ui.notifications.dagger;

import com.metrotarkari.app.application.network.DoneNetwork;
import com.metrotarkari.app.ui.notifications.NotificationActivity;
import com.metrotarkari.app.ui.notifications.mvp.NotificationModel;
import com.metrotarkari.app.ui.notifications.mvp.NotificationPresenter;
import com.metrotarkari.app.ui.notifications.mvp.NotificationView;

import dagger.Module;
import dagger.Provides;

@Module
public class NotificationModule {

  private final NotificationActivity activity;

  public NotificationModule(NotificationActivity activity) {
    this.activity = activity;
  }

  @Provides
  @NotificationScope
  public NotificationView view() {
    return new NotificationView(activity);
  }

  @Provides
  @NotificationScope
  public NotificationModel model(DoneNetwork doneNetwork){
    return new NotificationModel(activity, doneNetwork);
  }

  @Provides
  @NotificationScope
  public NotificationPresenter notificationPresenter(NotificationView homeView, NotificationModel model) {
    return new NotificationPresenter(homeView, model);
  }

}
