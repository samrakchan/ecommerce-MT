package com.metrotarkari.app.ui.order.orderdetail.mvp;


import android.widget.Toast;

import com.metrotarkari.app.R;
import com.metrotarkari.app.model.Error;
import com.metrotarkari.app.ui.order.orderdetail.model.Order;
import com.metrotarkari.app.ui.order.orderdetail.model.OrderHeader;
import com.metrotarkari.app.ui.order.orderdetail.model.OrderI;
import com.metrotarkari.app.utils.DoneUserPreferenceManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Avinash on 6/7/2017.
 */

public class OrderDetailPresenter {
    private final OrderDetailView view;
    private final OrderDetailModel model;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private DoneUserPreferenceManager doneUserPreferenceManager;

    public OrderDetailPresenter(OrderDetailView view, OrderDetailModel model,String orderID) {
        this.view = view;
        this.model = model;
        doneUserPreferenceManager = new DoneUserPreferenceManager(view.getContext());

        compositeDisposable.add(getOrderDetail(orderID));
    }


    private Disposable getOrderDetail(String orderID) {
        view.showNoItem(false);
        view.showProgressBar(true);
        return model.getOrderDetail(orderID).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(orderResponse -> {
                    if (orderResponse != null) {
                        Order orderDetailResponse = orderResponse.getOrder();
                        List<Error> errorResponse = orderResponse.getErrors();

                        if(errorResponse!=null && !errorResponse.isEmpty()){
                            Error error = errorResponse.get(0);
                            Toast.makeText(view.getContext(),error.getMessage()+"",Toast.LENGTH_LONG).show();
                        }else if (orderDetailResponse.equals(null)){
                            view.showNoItem(true);
                            }else {

                                view.showProgressBar(false);
                                view.showNoItem(false);

                                List<OrderHeader> orderHeaderList = new ArrayList<>();

                                OrderHeader orderHeader1 = new OrderHeader();
                                orderHeader1.setTitle(view.getContext().getString(R.string.order_id,orderDetailResponse.getIncrementId()));

                                OrderHeader orderHeaderAddress = new OrderHeader();
                                orderHeaderAddress.setTitle(view.getContext().getString(R.string.address_information));

                                OrderHeader paymentAndShippingOrderHeader = new OrderHeader();
                                paymentAndShippingOrderHeader.setTitle(view.getContext().getString(R.string.payment_and_shipping));

                                OrderHeader orderHeaderProductDetail = new OrderHeader();
                                orderHeaderProductDetail.setTitle(view.getContext().getString(R.string.product_detail,orderDetailResponse.getTotalItemCount()));

                                orderHeaderList.add(orderHeader1);
                                orderHeaderList.add(orderHeaderAddress);
                                orderHeaderList.add(paymentAndShippingOrderHeader);
                                orderHeaderList.add(orderHeaderProductDetail);

                                HashMap<OrderHeader, Object> listHashMap = new HashMap<>();
                                List<String> list = new ArrayList<String>();
                                list.add(view.getContext().getString(R.string.order_cust_name,orderDetailResponse.getCustomerFirstname(),orderDetailResponse.getCustomerLastname()));
                                list.add(view.getContext().getString(R.string.order_cust_email,orderDetailResponse.getCustomerEmail()));
                                list.add(view.getContext().getString(R.string.order_delivery_date,orderDetailResponse.getDeliveryDate()));
                                list.add(view.getContext().getString(R.string.order_status,orderDetailResponse.getStatus()));

                                List<String> addressDetailList = new ArrayList<String>();

                                addressDetailList.add(view.getContext().getString(R.string.order_billing_address,
                                        orderDetailResponse.getBillingAddress().getFirstname(),orderDetailResponse.getBillingAddress().getLastname(),
                                        orderDetailResponse.getBillingAddress().getCompany(),
                                        orderDetailResponse.getBillingAddress().getCountryId(),orderDetailResponse.getBillingAddress().getRegion(),orderDetailResponse.getBillingAddress().getCity(),orderDetailResponse.getBillingAddress().getCusArea(),orderDetailResponse.getBillingAddress().getStreet(),
                                        orderDetailResponse.getBillingAddress().getTelephone()
                                ));
                                addressDetailList.add(view.getContext().getString(R.string.order_shipping_address,
                                        orderDetailResponse.getShippingAddress().getFirstname(),orderDetailResponse.getShippingAddress().getLastname(),
                                        orderDetailResponse.getShippingAddress().getCompany(),
                                        orderDetailResponse.getShippingAddress().getCountryId(),orderDetailResponse.getShippingAddress().getRegion(),orderDetailResponse.getShippingAddress().getCity(),orderDetailResponse.getShippingAddress().getCusArea(),orderDetailResponse.getShippingAddress().getStreet(),
                                        orderDetailResponse.getShippingAddress().getTelephone()
                                ));

                                List<String> paymentAndShippingList = new ArrayList<String>();

                                paymentAndShippingList.add(view.getContext().getString(R.string.order_payment_information));
                                paymentAndShippingList.add(view.getContext().getString(R.string.order_payment_method,orderDetailResponse.getPaymentMethod()));

                                paymentAndShippingList.add(view.getContext().getString(R.string.order_shipping_and_handling));
                                paymentAndShippingList.add(view.getContext().getString(R.string.order_time_slot,orderDetailResponse.getMetroTimeSlot()));
                                paymentAndShippingList.add(view.getContext().getString(R.string.order_delivery_date_time,orderDetailResponse.getDeliveryDate()));

                                ArrayList<Object> orderItemDetailList = new ArrayList<Object>();

                                listHashMap.put(orderHeader1, list);
                                listHashMap.put(orderHeaderAddress, addressDetailList);
                                listHashMap.put(paymentAndShippingOrderHeader,paymentAndShippingList);
                                if(orderDetailResponse.getOrderItems() !=null && !orderDetailResponse.getOrderItems().isEmpty()) {
                                    OrderI orderI = new OrderI();
                                    orderI.setOrderItems(orderDetailResponse.getOrderItems());
                                    orderItemDetailList.add(orderI);
                                    listHashMap.put(orderHeaderProductDetail,orderItemDetailList);
                                }

                                view.addItemToOrderDetailAdapter(orderHeaderList, listHashMap);
                            }

                    }else {
                        view.showNoItem(true);
                    }
                }, throwable -> {
                    throwable.printStackTrace();
                    Toast.makeText(view.getContext(), R.string.sww,Toast.LENGTH_LONG).show();
                    view.showProgressBar(false);
                    view.showNoItem(false);
                });
    }



}


