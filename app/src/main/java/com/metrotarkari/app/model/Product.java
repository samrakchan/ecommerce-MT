
package com.metrotarkari.app.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Product  {

    @SerializedName("entity_id")
    private String entityId;

    @SerializedName("type_id")
    private String typeId;

    @SerializedName("sku")
    private String sku;

    @SerializedName("has_options")
    private String hasOptions;

    @SerializedName("required_options")
    private String requiredOptions;

    @SerializedName("status")
    private String status;

    @SerializedName("price")
    private String price;

    @SerializedName("tax_class_id")
    private String taxClassId;

    @SerializedName("final_price")
    private String finalPrice;

    @SerializedName("tier_price")
    private Object tierPrice;

    @SerializedName("cat_index_position")
    private String catIndexPosition;

    @SerializedName("is_salable")
    private String isSalable;

    @SerializedName("visibility")
    private String visibility;

    @SerializedName("metro_weight_type")
    private String metroWeightType;

    @SerializedName("name")
    private String name;

    @SerializedName("image")
    private String image;

    @SerializedName("small_image")
    private String smallImage;

    @SerializedName("thumbnail")
    private String thumbnail;

    @SerializedName("options_container")
    private String optionsContainer;

    @SerializedName("metro_weight_type_value")
    private String metroWeightTypeValue;

    @SerializedName("meta_keyword")
    private String metaKeyword;

    @SerializedName("description")
    private String description;

    @SerializedName("images")
    private List<Image> images = null;

    @SerializedName("formatted_final_price")
    private String formattedFinalPrice;

    @SerializedName("formatted_price")
    private String formattedPrice;

    @SerializedName("discount_label")
    private String discountLabel;

    @SerializedName("app_prices")
    private AppPrices appPrices;

    @SerializedName("app_reviews")
    private AppReviews appReviews;


    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }


    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getHasOptions() {
        return hasOptions;
    }

    public void setHasOptions(String hasOptions) {
        this.hasOptions = hasOptions;
    }

    public String getRequiredOptions() {
        return requiredOptions;
    }

    public void setRequiredOptions(String requiredOptions) {
        this.requiredOptions = requiredOptions;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTaxClassId() {
        return taxClassId;
    }

    public void setTaxClassId(String taxClassId) {
        this.taxClassId = taxClassId;
    }

    public String getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(String finalPrice) {
        this.finalPrice = finalPrice;
    }

    public Object getTierPrice() {
        return tierPrice;
    }

    public void setTierPrice(Object tierPrice) {
        this.tierPrice = tierPrice;
    }

    public String getCatIndexPosition() {
        return catIndexPosition;
    }

    public void setCatIndexPosition(String catIndexPosition) {
        this.catIndexPosition = catIndexPosition;
    }

    public String getIsSalable() {
        return isSalable;
    }

    public void setIsSalable(String isSalable) {
        this.isSalable = isSalable;
    }

    public String getVisibility() {
        return visibility;
    }

    public void setVisibility(String visibility) {
        this.visibility = visibility;
    }

  /*  public String getQuantityAndStockStatus() {
        return quantityAndStockStatus;
    }

    public void setQuantityAndStockStatus(String quantityAndStockStatus) {
        this.quantityAndStockStatus = quantityAndStockStatus;
    }*/

    public String getMetroWeightType() {
        return metroWeightType;
    }

    public void setMetroWeightType(String metroWeightType) {
        this.metroWeightType = metroWeightType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getSmallImage() {
        return smallImage;
    }

    public void setSmallImage(String smallImage) {
        this.smallImage = smallImage;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getOptionsContainer() {
        return optionsContainer;
    }

    public void setOptionsContainer(String optionsContainer) {
        this.optionsContainer = optionsContainer;
    }

    public String getMetaKeyword() {
        return metaKeyword;
    }

    public void setMetaKeyword(String metaKeyword) {
        this.metaKeyword = metaKeyword;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public String getFormattedFinalPrice() {
        return formattedFinalPrice;
    }

    public void setFormattedFinalPrice(String formattedFinalPrice) {
        this.formattedFinalPrice = formattedFinalPrice;
    }

    public String getFormattedPrice() {
        return formattedPrice;
    }

    public void setFormattedPrice(String formattedPrice) {
        this.formattedPrice = formattedPrice;
    }

    public AppPrices getAppPrices() {
        return appPrices;
    }

    public void setAppPrices(AppPrices appPrices) {
        this.appPrices = appPrices;
    }

    public AppReviews getAppReviews() {
        return appReviews;
    }

    public void setAppReviews(AppReviews appReviews) {
        this.appReviews = appReviews;
    }

    public String getMetroWeightTypeValue() {
        return metroWeightTypeValue;
    }

    public void setMetroWeightTypeValue(String metroWeightTypeValue) {
        this.metroWeightTypeValue = metroWeightTypeValue;
    }

    public String getDiscountLabel() {
        return discountLabel;
    }

    public void setDiscountLabel(String discountLabel) {
        this.discountLabel = discountLabel;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
