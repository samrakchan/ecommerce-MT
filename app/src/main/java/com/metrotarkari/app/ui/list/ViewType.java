package com.metrotarkari.app.ui.list;

/**
 * Created by samrakchan on 3/21/17.
 */

public enum ViewType {
    GRID(0),
    LIST(1),
    PROGRESS(2);

    private final int type;

    ViewType(final int viewType) {
        type = viewType;
    }

    public int getViewType() { return type; }
}
