package com.metrotarkari.app.model;

import java.util.List;

/**
 * Created by samrakchan on 3/17/17.
 */

public class ItemCategory {
    private String categoryId;
    private String categoryName;
    private String categoryTag;
    private boolean subCategory;
    private boolean icon;
    private boolean showSeeMore;
    private String viewType;
    private List<Product> products;

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryTag() {
        return categoryTag;
    }

    public void setCategoryTag(String categoryTag) {
        this.categoryTag = categoryTag;
    }

    public boolean hasSubCategory() {
        return subCategory;
    }

    public void setSubCategory(boolean subCategory) {
        this.subCategory = subCategory;
    }

    public boolean hasIcon() {
        return icon;
    }

    public void setIcon(boolean icon) {
        this.icon = icon;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public boolean isSubCategory() {
        return subCategory;
    }

    public boolean isIcon() {
        return icon;
    }

    public boolean isShowSeeMore() {
        return showSeeMore;
    }

    public void setShowSeeMore(boolean showSeeMore) {
        this.showSeeMore = showSeeMore;
    }

    public String getViewType() {
        return viewType;
    }

    public void setViewType(String viewType) {
        this.viewType = viewType;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }
}
