package com.metrotarkari.app.model;

/**
 * Created by samrakchan on 6/2/17.
 */

public class CartQuote {
    //product id
    private String product;
    private String qty;

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }
}
