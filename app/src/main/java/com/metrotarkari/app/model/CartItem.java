
package com.metrotarkari.app.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CartItem {

    @SerializedName("quoteitems")
    @Expose
    private List<Quoteitem> quoteitems = null;
    @SerializedName("total")
    @Expose
    private Total total;
    @SerializedName("page_size")
    @Expose
    private Integer pageSize;
    @SerializedName("from")
    @Expose
    private Integer from;
    @SerializedName("total_page")
    @Expose
    private Integer totalPage;
    @SerializedName("current_page")
    @Expose
    private Integer currentPage;
    @SerializedName("car_total")
    @Expose
    private Integer carTotal;
    @SerializedName("cart_total")
    @Expose
    private Integer cartTotal;
    @SerializedName("quote_id")
    @Expose
    private String quoteId;

    public List<Quoteitem> getQuoteitems() {
        return quoteitems;
    }

    public void setQuoteitems(List<Quoteitem> quoteitems) {
        this.quoteitems = quoteitems;
    }

    public Total getTotal() {
        return total;
    }

    public void setTotal(Total total) {
        this.total = total;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getFrom() {
        return from;
    }

    public void setFrom(Integer from) {
        this.from = from;
    }

    public Integer getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(Integer totalPage) {
        this.totalPage = totalPage;
    }

    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

    public Integer getCarTotal() {
        return carTotal;
    }

    public void setCarTotal(Integer carTotal) {
        this.carTotal = carTotal;
    }

    public Integer getCartTotal() {
        return cartTotal;
    }

    public void setCartTotal(Integer cartTotal) {
        this.cartTotal = cartTotal;
    }

    public String getQuoteId() {
        return quoteId;
    }

    public void setQuoteId(String quoteId) {
        this.quoteId = quoteId;
    }

}
