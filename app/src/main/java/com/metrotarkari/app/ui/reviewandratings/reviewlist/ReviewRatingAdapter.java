package com.metrotarkari.app.ui.reviewandratings.reviewlist;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.metrotarkari.app.R;
import com.metrotarkari.app.model.Count;
import com.metrotarkari.app.model.Review;
import com.metrotarkari.app.utils.Utility;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Avinash on 7/26/2017.
 */

public class ReviewRatingAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Object> mData;
    private Context mContext;
    protected BarData data;


    private static final int VIEW_RATING_HEADING_AND_BAR_TYPE = 1;
    private static final int VIEW_REVIEW_DETAIL_TYPE = 2;

    public ReviewRatingAdapter(Context mContext) {
        this.mContext = mContext;
        mData = new ArrayList<>();
    }

    public void addAllRatingReviewData(List<Object> items) {
        mData.addAll(items);
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_REVIEW_DETAIL_TYPE) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_rating_review_layout, parent, false);
            return new ReviewRatingAdapter.ReviewListViewHolder(view);
        } else {
            View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_rating_heading_and_bar_layout, parent, false);
            return new ReviewRatingAdapter.RatingHeadingAndBarViewHolder(view);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Object obj = mData.get(position);
        if (obj instanceof Review && holder instanceof ReviewListViewHolder) {
            Review item = (Review) obj;
            ReviewListViewHolder reviewListViewHolder = (ReviewListViewHolder) holder;
            reviewListViewHolder.adapterReviewTitleTv.setText(item.getTitle());
            reviewListViewHolder.adapterReviewNameAndDateTv.setText(mContext.getString(R.string.item_review_name_date, item.getNickname(), Utility.convertDateAndTimeForReviewList(item.getCreatedAt())));
            reviewListViewHolder.adapterReviewDescriptionTv.setText(item.getDetail());
            reviewListViewHolder.ratingCountNumberTv.setText(item.getRatePoints() + "");
            reviewListViewHolder.mRatingBar.setRating(Float.valueOf(String.valueOf(item.getRatePoints())));

        } else {

            Count count = (Count) obj;
            RatingHeadingAndBarViewHolder ratingHeadingAndBarViewHolder = (RatingHeadingAndBarViewHolder) holder;
           // ratingHeadingAndBarViewHolder.ratingTv.setText(count.getRte());
           // ratingHeadingAndBarViewHolder.reviewNumberTv.setText(count.getRatingount());

            setData(5,8f,count);
            ratingHeadingAndBarViewHolder.chart.setData(data);
            ratingHeadingAndBarViewHolder.chart.setDescription(null);    // Hide the description
            ratingHeadingAndBarViewHolder.chart.getAxisLeft().setDrawLabels(false);
            ratingHeadingAndBarViewHolder.chart.getAxisRight().setDrawLabels(false);
            ratingHeadingAndBarViewHolder.chart.getXAxis().setDrawLabels(true);
            ratingHeadingAndBarViewHolder.chart.getAxisRight().setDrawGridLines(false);
            ratingHeadingAndBarViewHolder.chart.getAxisRight().setDrawLimitLinesBehindData(false);
            ratingHeadingAndBarViewHolder.chart.getXAxis().setDrawGridLines(false);
            ratingHeadingAndBarViewHolder.chart.getAxisRight().setDrawAxisLine(false);

            ArrayList<String> labels = new ArrayList<String>();
            labels.add(mContext.getString(R.string.start_5));
            labels.add(mContext.getString(R.string.star_4));
            labels.add(mContext.getString(R.string.start_3));
            labels.add(mContext.getString(R.string.start_2));
            labels.add(mContext.getString(R.string.star_1));

                ArrayList<Integer> l = new ArrayList();
                l.add(count.get1Star());
                l.add(count.get2Star());
                l.add(count.get3Star());
                l.add(count.get4Star());
                l.add(count.get5Star());

               int max =  Collections.max(l);

            XAxis xAxis = ratingHeadingAndBarViewHolder.chart.getXAxis();
            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
            xAxis.setDrawGridLines(false);
            xAxis.setDrawLabels(true);
            xAxis.setLabelCount(5);
            xAxis.setAxisLineColor(Color.WHITE);
            xAxis.setTextSize(10f);
            xAxis.setTextColor(ContextCompat.getColor(mContext,R.color.text_color));
            xAxis.setDrawGridLines(false);
            xAxis.setSpaceMin(data.getBarWidth() / 2f);
            xAxis.setSpaceMax(data.getBarWidth() / 2f);
            xAxis.setValueFormatter(new IndexAxisValueFormatter(labels));

            YAxis yAxis = ratingHeadingAndBarViewHolder.chart.getAxisLeft();
            yAxis.setDrawGridLines(false);
            yAxis.setAxisLineColor(Color.WHITE);
            yAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)
            yAxis.setAxisMaximum(max+(max/2.5f));

            YAxis yr = ratingHeadingAndBarViewHolder.chart.getAxisRight();
            yr.setAxisMinimum(0f); // this replaces setStartAtZero(true)
            yr.setAxisMaximum(10f);

            ratingHeadingAndBarViewHolder.chart.getLegend().setEnabled(false);
            ratingHeadingAndBarViewHolder.chart.setFitBars(true);
            ratingHeadingAndBarViewHolder.chart.invalidate();
            ratingHeadingAndBarViewHolder.chart.setPinchZoom(false);
            ratingHeadingAndBarViewHolder.chart.setTouchEnabled(false);
            ratingHeadingAndBarViewHolder.chart.setDoubleTapToZoomEnabled(false);
        }

    }

    @Override
    public int getItemViewType(int position) {
        Object object = mData.get(position);
        if (object instanceof Review) {
            return VIEW_REVIEW_DETAIL_TYPE;
        } else {
            return VIEW_RATING_HEADING_AND_BAR_TYPE;
        }
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    protected class RatingHeadingAndBarViewHolder extends RecyclerView.ViewHolder {
        private AppCompatTextView ratingTv;
        private AppCompatTextView reviewNumberTv;
        protected HorizontalBarChart chart;

        public RatingHeadingAndBarViewHolder(View itemView) {
            super(itemView);

            ratingTv = (AppCompatTextView) itemView.findViewById(R.id.ratingTv);
            reviewNumberTv = (AppCompatTextView) itemView.findViewById(R.id.reviewNumberTv);
            chart = (HorizontalBarChart) itemView.findViewById(R.id.ratingChartCCV);
        }
    }

    protected class ReviewListViewHolder extends RecyclerView.ViewHolder {

        private AppCompatTextView adapterReviewTitleTv;
        private AppCompatTextView adapterReviewNameAndDateTv;
        private AppCompatTextView adapterReviewDescriptionTv;
        private AppCompatRatingBar mRatingBar;
        private AppCompatTextView ratingCountNumberTv;

        public ReviewListViewHolder(View itemView) {
            super(itemView);
            adapterReviewTitleTv = (AppCompatTextView) itemView.findViewById(R.id.adapterReviewTitleTv);
            adapterReviewNameAndDateTv = (AppCompatTextView) itemView.findViewById(R.id.adapterReviewNameAndDateTv);
            adapterReviewDescriptionTv = (AppCompatTextView) itemView.findViewById(R.id.adapterReviewDescriptionTv);
            mRatingBar = (AppCompatRatingBar) itemView.findViewById(R.id.rating);
            ratingCountNumberTv = (AppCompatTextView) itemView.findViewById(R.id.ratingCountNumberTv);
        }
    }


    private void setData(int count, float range,Count counts) {

        ArrayList<Integer> l = new ArrayList();
        l.add(counts.get5Star());
        l.add(counts.get4Star());
        l.add(counts.get3Star());
        l.add(counts.get2Star());
        l.add(counts.get1Star());

        float barWidth = 0.55f;
        float spaceForBar = 1f;
        ArrayList<BarEntry> yVals1 = new ArrayList<BarEntry>();

        for (int i = 0; i < count; i++) {
            yVals1.add(new BarEntry(i * spaceForBar,l.get(i),ContextCompat.getDrawable(mContext,R.drawable.a_wt_1)));
        }

            BarDataSet set1;
            set1 = new BarDataSet(yVals1, "DataSet 1");
            set1.setDrawIcons(false);

            ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
            dataSets.add(set1);

            data = new BarData(dataSets);
            data.setValueTextSize(10f);
            data.setValueFormatter(new IValueFormatter() {
                @Override
                public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
                    return "" + ((int) value);

                }
            });
            data.setBarWidth(barWidth);
    }
}
