package com.metrotarkari.app.ui.splash.mvp;

import android.app.Activity;
import android.support.v7.widget.AppCompatImageView;
import android.widget.FrameLayout;

import com.metrotarkari.app.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by samrakchan on 4/27/17.
 */

public class SplashView extends FrameLayout {

    @BindView(R.id.logo)
    AppCompatImageView logo;

    public SplashView(Activity activity){
        super(activity);

        inflate(getContext(), R.layout.activity_splash, this);

        ButterKnife.bind(this);
    }
}
