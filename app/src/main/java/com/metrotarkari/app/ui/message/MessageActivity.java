package com.metrotarkari.app.ui.message;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.metrotarkari.app.R;
import com.metrotarkari.app.ui.base.NonDraggerBaseActivity;
import com.metrotarkari.app.utils.Constant;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.basgeekball.awesomevalidation.ValidationStyle.UNDERLABEL;

/**
 * Created by Avinash on 7/20/2017.
 */

public class MessageActivity extends NonDraggerBaseActivity implements View.OnClickListener,TextWatcher {

    @BindView(R.id.messageEt)
    public AppCompatEditText messageEt;

    @BindView(R.id.setMessageBtn)
    public AppCompatButton setMessageBtn;

    private String message="" ;

    private AwesomeValidation awesomeValidation = new AwesomeValidation(UNDERLABEL);



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ButterKnife.bind(this);

        setMessageBtn.setOnClickListener(this);
        messageEt.addTextChangedListener(this);

    }

    @Override
    protected int getToolbarTitle() {
        return R.string.message;
    }

    @Override
    protected int getContentLayout() {
        return R.layout.activity_message;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.setMessageBtn) {
            message = messageEt.getText().toString().trim();
            if (message.equals("")) {
                messageEt.setError(getString(R.string.error_message_text));
            } else {
                Intent intent = new Intent();
                intent.putExtra(Constant.IS_FOR_MESSAGE, message);
                setResult(Activity.RESULT_OK, intent);
                finish();
            }
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        messageEt.setError(null);
    }
}
