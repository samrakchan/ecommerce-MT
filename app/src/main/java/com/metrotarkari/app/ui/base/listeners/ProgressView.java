package com.metrotarkari.app.ui.base.listeners;

/**
 * Created by samrakchan on 4/23/17.
 */

public interface ProgressView {

    void showProgress(String progressMessage);

    void hideProgress();

}
