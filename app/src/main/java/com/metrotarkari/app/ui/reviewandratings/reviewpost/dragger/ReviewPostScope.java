package com.metrotarkari.app.ui.reviewandratings.reviewpost.dragger;

import javax.inject.Scope;

@Scope
public @interface ReviewPostScope {
}
