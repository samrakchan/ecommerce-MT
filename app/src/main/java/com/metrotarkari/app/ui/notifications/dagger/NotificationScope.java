package com.metrotarkari.app.ui.notifications.dagger;

import javax.inject.Scope;

@Scope
public @interface NotificationScope {
}
