package com.metrotarkari.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by samrakchan on 8/4/17.
 */

public class CheckoutShipping{

    @SerializedName("shipping")
    @Expose
    private ArrayList<Shipping> shipping = null;

    public ArrayList<Shipping> getShipping() {
        return shipping;
    }

    public void setShipping(ArrayList<Shipping> shipping) {
        this.shipping = shipping;
    }
}
