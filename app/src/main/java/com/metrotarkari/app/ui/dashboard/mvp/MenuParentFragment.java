package com.metrotarkari.app.ui.dashboard.mvp;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;
import com.metrotarkari.app.R;
import com.metrotarkari.app.db.DoneDbAdapter;
import com.metrotarkari.app.model.Category;
import com.metrotarkari.app.model.CustomerResponse;
import com.metrotarkari.app.model.Error;
import com.metrotarkari.app.ui.login.LoginActivity2;
import com.metrotarkari.app.utils.Constant;
import com.metrotarkari.app.utils.DoneItemClickListener;
import com.metrotarkari.app.utils.DoneUserPreferenceManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.ContentValues.TAG;

/**
 * Created by samrakchan on 6/27/17.
 */

public class MenuParentFragment extends Fragment implements DoneItemClickListener, View.OnClickListener{

    @BindView(R.id.mainMenuRv)
    public RecyclerView mainMenuRv;

    @BindView(R.id.navHeaderLoginBtn)
    public AppCompatButton navHeaderLoginBtn;

    @BindView(R.id.navInitalsTv)
    public AppCompatTextView navInitalsTv;

    @BindView(R.id.headerGuestNameTv)
    public AppCompatTextView headerGuestNameTv;

    @BindView(R.id.topll)
    public View topll;

    private MenuParentAdapter menuAdapter;

    private DoneUserPreferenceManager doneUserPreferenceManager;

    private DoneItemClickListener listener;

    private DoneDbAdapter doneDbAdapter;

    private List<Category> categories;

    public final static String CATEGORIES_STATE = "categories_state";

    private static Parcelable parcelable;

    private LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setRetainInstance(true);
        View view = inflater.inflate(R.layout.fragment_menu_main, container, false);

        categories= new ArrayList<>();
        ButterKnife.bind(this, view);

        doneUserPreferenceManager = new DoneUserPreferenceManager(getContext());

        if (doneUserPreferenceManager.isLoggedInUser()){
            navHeaderLoginBtn.setVisibility(View.GONE);
            getName();
        }else {
            navHeaderLoginBtn.setVisibility(View.VISIBLE);
        }

        menuAdapter = new MenuParentAdapter(getContext());
        menuAdapter.addItemClickListener(this);
        mainMenuRv.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecorationH = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        dividerItemDecorationH.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.bg_divider));
        mainMenuRv.addItemDecoration(dividerItemDecorationH);
        mainMenuRv.setAdapter(menuAdapter);
        navHeaderLoginBtn.setOnClickListener(this);

        return view;
    }

    private void getName(){
        doneDbAdapter = new DoneDbAdapter(getContext());
        doneDbAdapter.openReadableDb();
        String jsonString = doneDbAdapter.getCustomer();
        Log.i(TAG, "JSON "+jsonString);
        if(jsonString!=null){
            Gson gson = new Gson();
            CustomerResponse customerResponse = gson.fromJson(jsonString, CustomerResponse.class);

            if(customerResponse!=null) {
                List<Error> errorResponse = customerResponse.getErrors();
                String firstName = customerResponse.getCustomer().getFirstname();
                headerGuestNameTv.setText(getContext().getString(R.string.welcome)+" "+firstName);
                navInitalsTv.setText(firstName.substring(0,1).toUpperCase());
                if(errorResponse!=null && !errorResponse.isEmpty()){
                    Error error = errorResponse.get(0);
                    Toast.makeText(getContext(),error.getMessage()+"",Toast.LENGTH_LONG).show();
                }
            }
        }
        doneDbAdapter.closeDb();
    }

    public void addItemToAdapter(List<Category> categories){
        this.categories.clear();
        this.categories.addAll(categories);
        menuAdapter.addData(this.categories);
    }

    public void setItemClickListener(DoneItemClickListener listener){
        this.listener = listener;
    }

    @Override
    public void onItemClickListener(int position, int viewId, Object object) {
        listener.onItemClickListener(position, viewId, object);
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(getActivity(), LoginActivity2.class);
        intent.putExtra(Constant.INTENT_FOR, Constant.INTENT_FROM_DRAWER);
        startActivity(intent);
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        parcelable = layoutManager.onSaveInstanceState();
        outState.putParcelable(CATEGORIES_STATE, parcelable);

    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if(savedInstanceState !=null){
            savedInstanceState.getParcelable(CATEGORIES_STATE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(parcelable!=null){
            layoutManager.onRestoreInstanceState(parcelable);
        }
    }

}
