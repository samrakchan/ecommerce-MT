package com.metrotarkari.app.ui.shippingdatetime;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.metrotarkari.app.R;
import com.metrotarkari.app.model.Shipping;
import com.metrotarkari.app.ui.base.NonDraggerBaseActivity;
import com.metrotarkari.app.utils.Constant;
import com.metrotarkari.app.utils.DoneItemClickListener;
import com.metrotarkari.app.utils.MyToast;
import com.metrotarkari.app.utils.Utility;

import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Avinash on 7/19/2017.
 */

public class ShippingDateAndTimeActivity extends NonDraggerBaseActivity implements View.OnClickListener, DoneItemClickListener {
    @BindView(R.id.deliveryDateEt)
    public AppCompatEditText deliveryDateEt;

    @BindView(R.id.dateSelectorsIb)
    public AppCompatImageButton dateSelectorsIb;

    @BindView(R.id.dateTimeSelectorTimeRv)
    public RecyclerView dateTimeSelectorTimeRv;

    @BindView(R.id.dateTimeSetBtn)
    public AppCompatButton dateTimeSetBtn;

    private int mYear;
    private int mMonth;
    private int mDay;

    private String selectedDate = "";
    private Shipping selectedShipping = null;

    private List<Shipping> shippings;

    private ShippingTimeAdapter timeAdapter ;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ButterKnife.bind(this);

        Intent intent = getIntent();
        shippings = intent.getParcelableArrayListExtra(Constant.KEY_VALUE_DATA_ID);


        dateTimeSelectorTimeRv.setLayoutManager(new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL, false));
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getApplicationContext(),DividerItemDecoration.VERTICAL);
        dividerItemDecoration.setDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.bg_divider));
        dateTimeSelectorTimeRv.addItemDecoration(dividerItemDecoration);
        timeAdapter = new ShippingTimeAdapter(this);
        timeAdapter.addOnItemClickListener(this);
        if(shippings!=null) {
            timeAdapter.addData(shippings);
        }else{
            MyToast.displayToast(getApplicationContext(), "No Data for time selection", Toast.LENGTH_LONG);
        }
        dateTimeSelectorTimeRv.setAdapter(timeAdapter);

        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth= c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        selectedDate = dateInString(mMonth, mDay, mYear);
        deliveryDateEt.setText(selectedDate);

        dateSelectorsIb.setOnClickListener(this);
        dateTimeSetBtn.setOnClickListener(this);
    }

    @Override
    protected int getToolbarTitle() {
        return R.string.delivery_date_and_time;
    }

    @Override
    protected int getContentLayout() {
        return R.layout.activity_date_time;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.dateSelectorsIb) {
            DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                    (view1, year, monthOfYear, dayOfMonth) -> {
                        selectedDate = dateInString(monthOfYear, dayOfMonth, year);
                        deliveryDateEt.setText(selectedDate);

                    }, mYear, mMonth, mDay);
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            datePickerDialog.getDatePicker().setMaxDate((long) (System.currentTimeMillis()+3.154e+10));
            datePickerDialog.show();
        }else if (v.getId() == R.id.dateTimeSetBtn){

            Intent intent = new Intent();
            intent.putExtra(Constant.SELECTED_DATE,selectedDate);
            intent.putExtra(Constant.SELECTED_TIME,selectedShipping);
            setResult(Activity.RESULT_OK,intent);
            finish();
        }
    }


    private String dateInString(int monthOfYear, int dayOfMonth,  int year){
        return (year+"-"+ Utility.getDoubleDigit(monthOfYear + 1)+"-"+ Utility.getDoubleDigit(dayOfMonth));
    }

    @Override
    public void onItemClickListener(int position, int viewId, Object object) {
        if(object != null) {
            selectedShipping = (Shipping) object;
        }
    }
}
