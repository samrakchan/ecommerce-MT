package com.metrotarkari.app.ui.settings.dagger;

import javax.inject.Scope;

@Scope
public @interface SettingsScope {
}
