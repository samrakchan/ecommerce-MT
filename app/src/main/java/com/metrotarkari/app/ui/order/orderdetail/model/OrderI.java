package com.metrotarkari.app.ui.order.orderdetail.model;

import java.util.ArrayList;

/**
 * Created by Avinash on 7/10/2017.
 */

public class OrderI {

    ArrayList<OrderItem> orderItems;

    public ArrayList<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(ArrayList<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }
}
