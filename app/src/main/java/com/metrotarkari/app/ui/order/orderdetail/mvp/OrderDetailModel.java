package com.metrotarkari.app.ui.order.orderdetail.mvp;

import android.app.Activity;

import com.metrotarkari.app.application.network.DoneNetwork;
import com.metrotarkari.app.ui.order.orderdetail.model.OrderDetailResponse;

import io.reactivex.Observable;

/**
 * Created by Avinash on 6/7/2017.
 */

public class OrderDetailModel {
    private final Activity activity;
    private final DoneNetwork doneNetwork;

    public OrderDetailModel(Activity activity, DoneNetwork doneNetwork){
        this.activity = activity;
        this.doneNetwork = doneNetwork;
    }

    public Observable<OrderDetailResponse> getOrderDetail(String orderID){
        return doneNetwork.getOrderDetail(orderID);
    }
}
