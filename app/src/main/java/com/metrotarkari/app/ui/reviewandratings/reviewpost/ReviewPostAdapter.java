package com.metrotarkari.app.ui.reviewandratings.reviewpost;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metrotarkari.app.R;
import com.metrotarkari.app.model.FormKey;
import com.metrotarkari.app.model.Rate;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Avinash on 8/10/2017.
 */

public class ReviewPostAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Object> mData;
    private Context mContext;

    private static final int POST_REVIEW_RATING_VIEW = 1;
    private static final int POST_REVIEW_EDIT_TEXT_VIEW = 3;

    private RatingBarViewHolder ratingViewHolder;
    private RatingEditTextViewHolder ratingEditTextViewHolder;


    public ReviewPostAdapter(Context mContext) {
        this.mContext = mContext;
        mData = new ArrayList<>();
    }

    public void addPostDataView(List<Object> objects){
        mData.addAll(objects);
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == POST_REVIEW_RATING_VIEW){
            View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_post_review_rating_bar, parent, false);
            return new RatingBarViewHolder(view);

        }else if (viewType == POST_REVIEW_EDIT_TEXT_VIEW){
            View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_post_review_edit_text, parent, false);
            return new RatingEditTextViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Object object = mData.get(position);
        if (object instanceof Rate && holder instanceof RatingBarViewHolder){
             ratingViewHolder = (RatingBarViewHolder) holder;
            Rate rate = (Rate)object;
            ratingViewHolder.adapterPostReviewRatingBarNameTv.setText(rate.getRateCode());
            ratingViewHolder.adapterPostReviewRb.setTag(rate);
        }else {
            ratingEditTextViewHolder = (RatingEditTextViewHolder)holder;
            FormKey formKey = (FormKey)object;
            ratingEditTextViewHolder.adapterPostReviewDescriptionEt.setTag(formKey);
            //ratingEditTextViewHolder.adapterPostReviewDescriptionEt.setHint(formKey.getValue());
            ratingEditTextViewHolder.adapterPostReviewTil.setHint(formKey.getValue());
        }
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    @Override
    public int getItemViewType(int position) {
        Object items = mData.get(position);
        if (items instanceof Rate){
            return POST_REVIEW_RATING_VIEW;
        }else if (items instanceof FormKey){
            return POST_REVIEW_EDIT_TEXT_VIEW;
        }
        return -1;
    }

    protected class RatingBarViewHolder extends RecyclerView.ViewHolder{
        private AppCompatTextView adapterPostReviewRatingBarNameTv;
        private AppCompatRatingBar adapterPostReviewRb;

        public RatingBarViewHolder(View itemView) {
            super(itemView);
            adapterPostReviewRatingBarNameTv = (AppCompatTextView) itemView.findViewById(R.id.adapterPostReviewRatingBarNameTv);
            adapterPostReviewRb = (AppCompatRatingBar) itemView.findViewById(R.id.adapterPostReviewRb);
        }
    }

    protected class RatingEditTextViewHolder extends RecyclerView.ViewHolder{
        private AppCompatEditText adapterPostReviewDescriptionEt;
        private TextInputLayout adapterPostReviewTil;

        public RatingEditTextViewHolder(View itemView) {
            super(itemView);
            adapterPostReviewTil = (TextInputLayout) itemView.findViewById(R.id.adapterPostReviewTil);
            adapterPostReviewDescriptionEt = (AppCompatEditText) itemView.findViewById(R.id.adapterPostReviewDescriptionEt);
        }
    }
}
