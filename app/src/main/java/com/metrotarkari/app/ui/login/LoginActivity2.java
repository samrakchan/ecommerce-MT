package com.metrotarkari.app.ui.login;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.crashlytics.android.Crashlytics;
import com.metrotarkari.app.R;
import com.metrotarkari.app.application.DoneApplication;
import com.metrotarkari.app.ui.login.dagger.DaggerLogin2Component;
import com.metrotarkari.app.ui.login.dagger.LoginModule;
import com.metrotarkari.app.ui.login.mvp.LoginPresenter;
import com.metrotarkari.app.ui.login.mvp.LoginView;

import javax.inject.Inject;

import io.fabric.sdk.android.Fabric;

/**
 * Created by samrakchan on 5/19/17.
 */

public class LoginActivity2 extends AppCompatActivity {

    @Inject
    LoginView view;

    @Inject
    LoginPresenter presenter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());


        DaggerLogin2Component.builder()
                .appComponent(DoneApplication.get(this).component())
                .loginModule(new LoginModule(this))
                .build().inject(this);

        setContentView(view);

        presenter.onCreate();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

}
