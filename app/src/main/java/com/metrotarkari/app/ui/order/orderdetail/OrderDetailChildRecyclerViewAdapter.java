package com.metrotarkari.app.ui.order.orderdetail;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.metrotarkari.app.R;
import com.metrotarkari.app.ui.order.orderdetail.model.OrderItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Avinash on 7/3/2017.
 */

public class OrderDetailChildRecyclerViewAdapter extends RecyclerView.Adapter<OrderDetailChildRecyclerViewAdapter.orderDetailChildRecyclerViewViewHolder> {

    private Context mContext ;
    private List<OrderItem> mData;

    public OrderDetailChildRecyclerViewAdapter(Context mContext) {
        mData = new ArrayList<>();
        this.mContext = mContext;
    }

    public void addProductLineData(List<OrderItem> productLinesData){
        this.mData.addAll(productLinesData);
    }

    @Override
    public orderDetailChildRecyclerViewViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_item_detail_for_product_detail,parent,false);
        return new OrderDetailChildRecyclerViewAdapter.orderDetailChildRecyclerViewViewHolder(view);
    }

    @Override
    public void onBindViewHolder(orderDetailChildRecyclerViewViewHolder holder, int position) {
        OrderItem orderItemsData = mData.get(position);
        holder.adapterItemDetailForProductNameTv.setText(orderItemsData.getName());
        holder.adapterItemDetailForProductSkuTv.setText(orderItemsData.getSku());
        holder.adapterItemDetailForProductQuantityTv.setText(orderItemsData.getQtyOrdered());
        holder.adapterItemDetailForProductPriceTv.setText(orderItemsData.getBasePriceInclTax());
        holder.adapterItemDetailForProductTotalTv.setText(orderItemsData.getBaseRowTotalInclTax());
        holder.adapterItemDetailForProductAttributeTv.setText(orderItemsData.getBasePrice());
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    protected class orderDetailChildRecyclerViewViewHolder extends RecyclerView.ViewHolder{

        private AppCompatTextView adapterItemDetailForProductNameTv;
        private AppCompatTextView adapterItemDetailForProductSkuTv;
        private AppCompatTextView adapterItemDetailForProductQuantityTv;
        private AppCompatTextView adapterItemDetailForProductPriceTv;
        private AppCompatTextView adapterItemDetailForProductTotalTv;
        private AppCompatTextView adapterItemDetailForProductAttributeTv;

        public orderDetailChildRecyclerViewViewHolder(View itemView) {
            super(itemView);

            adapterItemDetailForProductNameTv = (AppCompatTextView) itemView.findViewById(R.id.adapterItemDetailForProductNameTv);
            adapterItemDetailForProductSkuTv = (AppCompatTextView) itemView.findViewById(R.id.adapterItemDetailForProductSkuTv);
            adapterItemDetailForProductQuantityTv = (AppCompatTextView) itemView.findViewById(R.id.adapterItemDetailForProductQuantityTv);
            adapterItemDetailForProductPriceTv = (AppCompatTextView) itemView.findViewById(R.id.adapterItemDetailForProductPriceTv);
            adapterItemDetailForProductTotalTv = (AppCompatTextView) itemView.findViewById(R.id.adapterItemDetailForProductTotalTv);
            adapterItemDetailForProductAttributeTv = (AppCompatTextView) itemView.findViewById(R.id.adapterItemDetailForProductAttributeTv);
        }
    }
}
