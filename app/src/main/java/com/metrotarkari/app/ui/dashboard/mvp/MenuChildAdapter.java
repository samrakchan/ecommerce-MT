package com.metrotarkari.app.ui.dashboard.mvp;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metrotarkari.app.R;
import com.metrotarkari.app.model.Category;
import com.metrotarkari.app.model.ChildCategory;
import com.metrotarkari.app.utils.DoneItemClickListener;
import com.metrotarkari.app.utils.ViewUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by samrakchan on 6/27/17.
 */

public class MenuChildAdapter extends RecyclerView.Adapter<MenuChildAdapter.MenuItemViewHolder>{

    private List<ChildCategory> mData;
    private Context mContext;
    private DoneItemClickListener listener;

    public MenuChildAdapter(Context context){
        mData = new ArrayList<>();
        mContext = context;
    }

    public void addItemClickListener(DoneItemClickListener listener){
        this.listener = listener;
    }

    public void addData(List<ChildCategory> categories){
        mData.addAll(categories);
    }

    public void clearData(){
        mData.clear();
    }

    @Override
    public MenuItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_menu_item,parent,false);
        return new MenuChildAdapter.MenuItemViewHolder(view);

    }

    @Override
    public void onBindViewHolder(MenuItemViewHolder holder, int position) {
        ChildCategory category = mData.get(position);
        holder.menuItemTv.setText(category.getName());

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    protected class MenuItemViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener {

        private AppCompatTextView menuItemTv;
        private AppCompatImageView menuChevronIv;

        public MenuItemViewHolder(View itemView) {
            super(itemView);
            menuItemTv = (AppCompatTextView) itemView.findViewById(R.id.menuItemTv);
            menuItemTv.setPadding((int)ViewUtils.convertDpToPixel(30, mContext),0,0,0);
            menuChevronIv = (AppCompatImageView) itemView.findViewById(R.id.menuChevronIv);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            ChildCategory category = mData.get(position);
            listener.onItemClickListener(position, view.getId(), category);
        }
    }
}
