package com.metrotarkari.app.ui.home.mvp;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.freegeek.android.materialbanner.MaterialBanner;
import com.freegeek.android.materialbanner.view.indicator.CirclePageIndicator;
import com.metrotarkari.app.R;
import com.metrotarkari.app.generic.OnItemClickListenerTwo;
import com.metrotarkari.app.model.Contacts;
import com.metrotarkari.app.model.Homebanners;
import com.metrotarkari.app.model.Homeproductlist;
import com.metrotarkari.app.model.Image;
import com.metrotarkari.app.model.Product;
import com.metrotarkari.app.model.ProductArray_;
import com.metrotarkari.app.ui.adapter.GridItemAdapter;
import com.metrotarkari.app.ui.adapter.HorizontalItemAdapter;
import com.metrotarkari.app.ui.list.ItemListActivity;
import com.metrotarkari.app.ui.productdetail.DetailActivity;
import com.metrotarkari.app.utils.Constant;
import com.metrotarkari.app.utils.ViewUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by samrakchan on 3/17/17.
 */

public class HomeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Object> mData;
    private Context mContext;

    private static final int VIEW_TYPE_CATEGORY_HEADING = 1;
    private static final int VIEW_TYPE_CATEGORY_CONTENT =2;
    private static final int VIEW_TYPE_CATEGORY_VIEW_PAGER =3;
    private static final int VIEW_TYPE_CATEGORY_CONTACT =4;
    //
    private static final int VIEW_TYPE_CATEGORY_HORIZONTAL_LIST = 5;
    private static final int VIEW_TYPE_CATEGORY_ADS = 6;
    private static final int VIEW_TYPE_CATEGORY_SINGLE = 8;
    private static final int VIEW_TYPE_CATEGORY_GRID_LIST = 9;

    private CirclePageIndicator circlePageIndicator;
    private View.OnClickListener listener;
    private OnItemClickListenerTwo listenerTwo;

    public HomeAdapter(Context context, View.OnClickListener listener, OnItemClickListenerTwo listenerTwo){
        mData = new ArrayList<>();
        this.listener = listener;
        this.listenerTwo = listenerTwo;
        this.mContext = context;
        initIndicator();
    }

    public void addData(List<Object> datas){
        mData.clear();
        this.mData.addAll(datas);
        notifyDataSetChanged();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == VIEW_TYPE_CATEGORY_CONTACT){
            View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_main_category_contacts,parent,false);
            return new ContactViewHolder(view);
        }else if(viewType == VIEW_TYPE_CATEGORY_HORIZONTAL_LIST) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_label_recycler, parent, false);
            return new LabelHorizontalRecyclerViewHolder(view);
        }else if(viewType == VIEW_TYPE_CATEGORY_GRID_LIST) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_label_recycler, parent, false);
            return new LabelGridRecyclerViewHolder(view, listenerTwo);
        }else if(viewType == VIEW_TYPE_CATEGORY_ADS){
            View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_ads, parent, false);
            return new AdsViewHolder(view);
        } else if(viewType == VIEW_TYPE_CATEGORY_SINGLE){
            View view = LayoutInflater.from(mContext).inflate(R.layout.layout_item_large, parent, false);
            return new SingleImageViewHolder(view);
        }
        else if(viewType == VIEW_TYPE_CATEGORY_VIEW_PAGER){
            View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_main_category_view_pager,parent,false);
            return new SliderViewPagerViewHolder(view);
        }else{
            View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_main_category_view_pager,parent,false);
            return new SliderViewPagerViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) holder.itemView.getLayoutParams();
        layoutParams.setFullSpan(true);
        Object obj = mData.get(position);
        if(obj instanceof Homeproductlist){
            Homeproductlist itemCategory = (Homeproductlist) obj;
            ProductArray_ productArray = itemCategory.getProductArray();
            if(holder instanceof LabelHorizontalRecyclerViewHolder){
                if(itemCategory!=null && productArray!=null && !productArray.getProducts().isEmpty()){
                    LabelHorizontalRecyclerViewHolder labelHorizontalRecyclerViewHolder = (LabelHorizontalRecyclerViewHolder)holder;
                    labelHorizontalRecyclerViewHolder.categoryHeaderTv.setText(itemCategory.getListTitle());
                    if(labelHorizontalRecyclerViewHolder.itemListAdapter.getItemCount()==0) {
                        labelHorizontalRecyclerViewHolder.itemListAdapter.addData(productArray.getProducts());
                    }
                }
            }else if(obj instanceof Homeproductlist && holder instanceof LabelGridRecyclerViewHolder){
                if(itemCategory!=null && productArray!=null && !productArray.getProducts().isEmpty()){
                    LabelGridRecyclerViewHolder labelGridRecyclerViewHolder = (LabelGridRecyclerViewHolder)holder;
                    labelGridRecyclerViewHolder.categoryHeaderTv.setText(itemCategory.getListTitle());
                    if(labelGridRecyclerViewHolder.itemListAdapter.getItemCount() == 0) {
                        labelGridRecyclerViewHolder.itemListAdapter.addData(productArray.getProducts());
                    }
                }
            }else if(obj instanceof Homeproductlist && holder instanceof AdsViewHolder){
                AdsViewHolder categoryViewHolder = (AdsViewHolder) holder;
                Glide.with(mContext)
                        .load(itemCategory.getListImage())
                        .dontAnimate()
                        .into(categoryViewHolder.listItemImageIv);

            } else if(obj instanceof Homeproductlist && holder instanceof SingleImageViewHolder) {
                  List<Product> products = productArray.getProducts();
                  SingleImageViewHolder singelImageViewHolder = (SingleImageViewHolder) holder;
                  if (!products.isEmpty()) {
                      singelImageViewHolder.categoryHeaderTv.setText(itemCategory.getListTitle());
                      singelImageViewHolder.listItemNameTv.setText(products.get(0).getName());
                    //  singelImageViewHolder.listItemPriceTv.setText(mContext.getString(R.string.price, Utility.convertToTwoDecimalPlace(products.get(0).getPrice())));
                      singelImageViewHolder.listItemPriceTv.setText(products.get(0).getFormattedPrice());
                    //  singelImageViewHolder.listItemPriceTv.setText(mContext.getString(R.string.price, Utility.convertToTwoDecimalPlace(products.get(0).getFinalPrice())));
                      singelImageViewHolder.listItemPriceTv.setText(products.get(0).getFormattedFinalPrice());
                      if(products.get(0).getPrice().equalsIgnoreCase(products.get(0).getFinalPrice())){
                          singelImageViewHolder.listItemOldPriceTv.setVisibility(View.GONE);
                      }else{
                          singelImageViewHolder.listItemOldPriceTv.setPaintFlags(singelImageViewHolder.listItemOldPriceTv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                        //  singelImageViewHolder.listItemOldPriceTv.setText(mContext.getString(R.string.price, Utility.convertToTwoDecimalPlace(products.get(0).getPrice())));
                          singelImageViewHolder.listItemOldPriceTv.setText(products.get(0).getFormattedPrice());
                          singelImageViewHolder.listItemOldPriceTv.setVisibility(View.VISIBLE);
                      }

                      if(products.get(0).getDiscountLabel() !=null){
                          singelImageViewHolder.listItemOfferLabelTv.setText(products.get(0).getDiscountLabel());
                          singelImageViewHolder.listItemOfferLabelTv.setVisibility(View.VISIBLE);
                      }else{
                          singelImageViewHolder.listItemOfferLabelTv.setVisibility(View.GONE);
                      }

                      List<Image> images = products.get(0).getImages();
                        if(!images.isEmpty()) {
                            Glide.with(mContext)
                                    .load(images.get(0).getUrl())
                                    .placeholder(R.mipmap.ic_placeholder_mt)
                                    .dontAnimate()
                                    .centerCrop()
                                    .into(singelImageViewHolder.listItemImageIv);
                        }
                  }
          }
        }else if(obj instanceof Homebanners && holder instanceof SliderViewPagerViewHolder){
            Homebanners homebanners = ((Homebanners) obj);
            SliderViewPagerViewHolder viewHolder = ((SliderViewPagerViewHolder) holder);
            if(homebanners!=null && homebanners.getHomebanners()!=null){
                viewHolder.materialBanner.setPages(() -> new SliderViewHolder(), homebanners.getHomebanners()).setIndicator(circlePageIndicator);
                viewHolder.materialBanner.startTurning(4000);
            }

        }else if(obj instanceof Contacts && holder instanceof ContactViewHolder){

        }
    }

    /**
     * materialBanner.setPages(new ViewHolderCreator() {
    @Override
    public Object createHolder() {
    return new SliderViewHolder();
    }
    }, )
     * @return
     */

    @Override
    public int getItemCount() {
        return mData.size();
    }

    @Override
    public int getItemViewType(int position) {
        Object object = mData.get(position);
        if(object instanceof Homeproductlist){
            Homeproductlist itemCategory = (Homeproductlist) object;
            if(itemCategory.getTypeCode()!=null){
                String typeCode = itemCategory.getTypeCode();
                if(typeCode.equalsIgnoreCase("product_list_scroll")){
                    return VIEW_TYPE_CATEGORY_HORIZONTAL_LIST;
                }else if(typeCode.equalsIgnoreCase("ads_image")){
                    return VIEW_TYPE_CATEGORY_ADS;
                }else if(typeCode.equalsIgnoreCase("single_product")){
                    return VIEW_TYPE_CATEGORY_SINGLE;
                }else if(typeCode.equalsIgnoreCase("category_product_list")){
                    return VIEW_TYPE_CATEGORY_GRID_LIST;
                }else if(typeCode.equalsIgnoreCase("product_list")){
                    return VIEW_TYPE_CATEGORY_GRID_LIST;
                }else if(typeCode.equalsIgnoreCase("category_product_list_scroll")){
                    return VIEW_TYPE_CATEGORY_HORIZONTAL_LIST;
                }
            }
        }else if(object instanceof Contacts) {
            return VIEW_TYPE_CATEGORY_CONTACT;
        }else if(object instanceof Homebanners){
            return VIEW_TYPE_CATEGORY_VIEW_PAGER;
        }else if(object instanceof Product){
            return VIEW_TYPE_CATEGORY_GRID_LIST;
        }
        return -1;
    }

    protected class CategoryItemViewHolder extends RecyclerView.ViewHolder{

        private AppCompatImageView listItemImageIv;
        private AppCompatTextView listItemPriceTv;
        private AppCompatTextView listItemNameTv;

        public CategoryItemViewHolder(View itemView) {
            super(itemView);

            listItemImageIv = (AppCompatImageView)itemView.findViewById(R.id.listItemImageIv);
            listItemPriceTv = (AppCompatTextView)itemView.findViewById(R.id.listItemPriceTv);
            listItemNameTv = (AppCompatTextView)itemView.findViewById(R.id.listItemNameTv);

            itemView.setOnClickListener(v -> {

                try {
                    Intent intent = new Intent(mContext, DetailActivity.class);
                    Product product = (Product) mData.get(getAdapterPosition());
                    intent.putExtra("PRODUCT_ID", product.getEntityId()+"");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(intent);
                }catch (ClassCastException|NullPointerException e){
                    e.printStackTrace();
                }

            });
        }
    }

    protected class ContactViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private AppCompatButton categoryContactCallUsBtn;
        private AppCompatButton categoryContactSalesMarketingBtn;
        private AppCompatButton categoryEmailBtn;

        public ContactViewHolder(View itemView) {
            super(itemView);
            categoryContactCallUsBtn = (AppCompatButton) itemView.findViewById(R.id.categoryContactCallUsBtn);
            categoryContactSalesMarketingBtn = (AppCompatButton) itemView.findViewById(R.id.categoryContactSalesMarketingBtn);
            categoryEmailBtn = (AppCompatButton) itemView.findViewById(R.id.categoryEmailBtn);

            categoryContactCallUsBtn.setOnClickListener(this);
            categoryContactSalesMarketingBtn.setOnClickListener(this);
            categoryEmailBtn.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            listener.onClick(view);
        }
    }

    protected class CategoryHeaderViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private AppCompatTextView categoryHeaderTv;
        private AppCompatButton categorySeeMoreBtn;

        public CategoryHeaderViewHolder(View itemView) {
            super(itemView);

            categoryHeaderTv = (AppCompatTextView)itemView.findViewById(R.id.categoryHeaderTv);
            categorySeeMoreBtn = (AppCompatButton) itemView.findViewById(R.id.categorySeeMoreBtn);
            categorySeeMoreBtn.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            //see more clicked
            try {
                Intent intent = new Intent(mContext, ItemListActivity.class);
                Homeproductlist itemCategory = (Homeproductlist)mData.get(getAdapterPosition());
                intent.putExtra(Constant.ID, itemCategory.getEntityId());
                mContext.startActivity(intent);

            }catch (ClassCastException|NullPointerException e){
                e.printStackTrace();
            }
        }
    }

    protected class SingleImageViewHolder extends RecyclerView.ViewHolder{

        private AppCompatTextView categoryHeaderTv;
            private AppCompatImageView listItemImageIv;
            private AppCompatTextView listItemPriceTv;
            private AppCompatTextView listItemOldPriceTv;
            private AppCompatTextView listItemNameTv;
        private AppCompatTextView listItemOfferLabelTv;

            public SingleImageViewHolder(View itemView) {
                super(itemView);

                categoryHeaderTv = (AppCompatTextView) itemView.findViewById(R.id.categoryHeaderTv);
                listItemImageIv = (AppCompatImageView)itemView.findViewById(R.id.listItemImageIv);
                listItemPriceTv = (AppCompatTextView)itemView.findViewById(R.id.listItemPriceTv);
                listItemOldPriceTv = (AppCompatTextView)itemView.findViewById(R.id.listItemOldPriceTv);
                listItemNameTv = (AppCompatTextView)itemView.findViewById(R.id.listItemNameTv);
                listItemOfferLabelTv = (AppCompatTextView)itemView.findViewById(R.id.listItemOfferLabelTv);

                itemView.setOnClickListener(v -> {

                    try {
                        Intent intent = new Intent(mContext, DetailActivity.class);
                        Homeproductlist homeproductlist = (Homeproductlist) mData.get(getAdapterPosition());
                        ProductArray_ productArray = homeproductlist.getProductArray();
                        List<Product> products = productArray.getProducts();
                        if (!products.isEmpty()) {
                            intent.putExtra("PRODUCT_ID", products.get(0).getEntityId() + "");
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            mContext.startActivity(intent);
                        }
                    }catch (ClassCastException|NullPointerException e){
                        e.printStackTrace();
                    }

                });
            }
    }

    protected class AdsViewHolder extends RecyclerView.ViewHolder{
        private AppCompatImageView listItemImageIv;

        public AdsViewHolder(View itemView) {
            super(itemView);
            listItemImageIv = (AppCompatImageView)itemView.findViewById(R.id.adsIv);
        }
    }

    protected class LabelHorizontalRecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private AppCompatTextView categoryHeaderTv;
        private AppCompatButton categorySeeMoreBtn;
        private RecyclerView labelRecyclerRv;
        private HorizontalItemAdapter itemListAdapter;

        public LabelHorizontalRecyclerViewHolder(View itemView) {
            super(itemView);
            categoryHeaderTv = (AppCompatTextView)itemView.findViewById(R.id.categoryHeaderTv);
            categorySeeMoreBtn = (AppCompatButton) itemView.findViewById(R.id.categorySeeMoreBtn);
            labelRecyclerRv = (RecyclerView) itemView.findViewById(R.id.labelRecyclerRv);
            labelRecyclerRv.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mContext, DividerItemDecoration.HORIZONTAL);
            dividerItemDecoration.setDrawable(ContextCompat.getDrawable(mContext, R.drawable.bg_divider));
            labelRecyclerRv.addItemDecoration(dividerItemDecoration);
            itemListAdapter = new HorizontalItemAdapter(mContext);
            labelRecyclerRv.setAdapter(itemListAdapter);
            categorySeeMoreBtn.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            try {
                Intent intent = new Intent(mContext, ItemListActivity.class);
                Homeproductlist itemCategory = (Homeproductlist)mData.get(getAdapterPosition());
                intent.putExtra(Constant.ID, itemCategory.getEntityId());
                mContext.startActivity(intent);

            }catch (ClassCastException|NullPointerException e){
                e.printStackTrace();
            }
        }
    }

    protected class LabelGridRecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private AppCompatTextView categoryHeaderTv;
        private AppCompatButton categorySeeMoreBtn;
        private RecyclerView labelRecyclerRv;
        private GridItemAdapter itemListAdapter;

        public LabelGridRecyclerViewHolder(View itemView, OnItemClickListenerTwo listenerTwo) {
            super(itemView);
            categoryHeaderTv = (AppCompatTextView)itemView.findViewById(R.id.categoryHeaderTv);
            categorySeeMoreBtn = (AppCompatButton) itemView.findViewById(R.id.categorySeeMoreBtn);
            labelRecyclerRv = (RecyclerView) itemView.findViewById(R.id.labelRecyclerRv);
            labelRecyclerRv.setLayoutManager(new GridLayoutManager(mContext, ViewUtils.calculateNoOfColumns(mContext), LinearLayoutManager.VERTICAL, false));
            DividerItemDecoration dividerItemDecorationH = new DividerItemDecoration(mContext, DividerItemDecoration.HORIZONTAL);
            DividerItemDecoration dividerItemDecorationV = new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL);
            dividerItemDecorationH.setDrawable(ContextCompat.getDrawable(mContext, R.drawable.bg_divider));
            dividerItemDecorationV.setDrawable(ContextCompat.getDrawable(mContext, R.drawable.bg_divider));
            labelRecyclerRv.addItemDecoration(dividerItemDecorationH);
            labelRecyclerRv.addItemDecoration(dividerItemDecorationV);
            itemListAdapter = new GridItemAdapter(mContext);
            itemListAdapter.setOnItemClickListener(listenerTwo);
            labelRecyclerRv.setAdapter(itemListAdapter);
            categorySeeMoreBtn.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            try {
                Intent intent = new Intent(mContext, ItemListActivity.class);
                Homeproductlist itemCategory = (Homeproductlist)mData.get(getAdapterPosition());
                intent.putExtra(Constant.ID, itemCategory.getEntityId());
                mContext.startActivity(intent);

            }catch (ClassCastException|NullPointerException e){
                e.printStackTrace();
            }
        }
    }

    protected class SliderViewPagerViewHolder extends RecyclerView.ViewHolder{
        private MaterialBanner materialBanner;

        public SliderViewPagerViewHolder(View itemView) {
            super(itemView);
            //slider = (BannerSlider) itemView.findViewById(R.id.mainBannerSlider);
            materialBanner = (MaterialBanner) itemView.findViewById(R.id.material_banner);
            materialBanner.setIndicatorInside(false);
        }
    }
    private void initIndicator(){
        circlePageIndicator = new CirclePageIndicator(mContext);
        circlePageIndicator.setStrokeColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
        circlePageIndicator.setFillColor(ContextCompat.getColor(mContext, R.color.colorGreenLight));
        circlePageIndicator.setRadius(MaterialBanner.dip2Pix(mContext,3));
        circlePageIndicator.setBetween(20);
    }
}
