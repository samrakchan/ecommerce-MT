package com.metrotarkari.app.ui.b2b.add;

import com.metrotarkari.app.application.network.DoneNetwork;
import com.metrotarkari.app.ui.b2b.add.mvp.B2bAddModel;
import com.metrotarkari.app.ui.b2b.add.mvp.B2bAddPresenter;
import com.metrotarkari.app.ui.home.mvp.HomeView;

import dagger.Module;
import dagger.Provides;

@Module
public class B2bAddModule {

  @Provides
  public B2bAddModel model(DoneNetwork doneNetwork){
    return new B2bAddModel(doneNetwork);
  }

  @Provides
  public B2bAddPresenter presenter(HomeView view, B2bAddModel model) {
    return new B2bAddPresenter(view, model);
  }

}
