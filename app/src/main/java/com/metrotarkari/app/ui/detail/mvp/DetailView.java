package com.metrotarkari.app.ui.detail.mvp;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.bumptech.glide.Glide;
import com.freegeek.android.materialbanner.MaterialBanner;
import com.freegeek.android.materialbanner.holder.Holder;
import com.freegeek.android.materialbanner.view.indicator.CirclePageIndicator;
import com.google.gson.Gson;
import com.jakewharton.rxbinding2.view.RxView;
import com.metrotarkari.app.R;
import com.metrotarkari.app.application.DoneApplication;
import com.metrotarkari.app.model.AppReviews;
import com.metrotarkari.app.model.CustomOption;
import com.metrotarkari.app.model.Image;
import com.metrotarkari.app.model.Product;
import com.metrotarkari.app.model.Product_;
import com.metrotarkari.app.ui.adapter.HorizontalItemAdapter;
import com.metrotarkari.app.ui.base.BaseView;
import com.metrotarkari.app.ui.cart.CartActivity;
import com.metrotarkari.app.ui.detail.DetailActivity;
import com.metrotarkari.app.ui.reviewandratings.reviewlist.ReviewRatingsActivity;
import com.metrotarkari.app.ui.reviewandratings.reviewpost.ReviewPostActivity;
import com.metrotarkari.app.utils.Constant;
import com.metrotarkari.app.utils.Utility;
import com.ms.square.android.expandabletextview.ExpandableTextView;

import java.util.List;

import butterknife.BindView;
import io.reactivex.Observable;

/**
 * Created by samrakchan on 4/27/17.
 */

public class DetailView extends BaseView implements View.OnClickListener{

    @Nullable
    @BindView(R.id.appBarLayoutTransparent)
    AppBarLayout appBarLayout;

    @Nullable
    @BindView(R.id.itemDetailIv)
    public MaterialBanner bannerSlider;

    @Nullable
    @BindView(R.id.nestedSV)
    public ScrollView nestedSV;

    @BindView(R.id.itemDetailItemNameTv)
    public AppCompatTextView itemDetailItemNameTv;

    @BindView(R.id.itemDetailItemPriceTv)
    public AppCompatTextView itemDetailItemPriceTv;

    @BindView(R.id.itemDetailItemPriceCutTv)
    public AppCompatTextView itemDetailItemPriceCutTv;

    @BindView(R.id.expand_text_view)
    public ExpandableTextView itemDetailDescriptionTv;

    @BindView(R.id.itemDetailSKUTv)
    public AppCompatTextView itemDetailSKUTv;

    @BindView(R.id.imageMsgRl)
    public View noItemView;

    @BindView(R.id.progressBarRl)
    public View progressBar;

    @BindView(R.id.itemDetailAddToCartBtn)
    public View itemDetailAddToCartBtn;

    @BindView(R.id.txt_quantity)
    public AppCompatTextView txtQuantity;

    @BindView(R.id.img_minus)
    public AppCompatImageView img_minus;

    @BindView(R.id.img_plus)
    public AppCompatImageView img_plus;

    @BindView(R.id.categoryHeaderTv)
    public AppCompatTextView categoryHeaderTv;

    @BindView(R.id.categorySeeMoreBtn)
    public AppCompatButton categorySeeMoreBtn;

    @BindView(R.id.labelRecyclerRv)
    public RecyclerView itemDetailRelatedRecyclerRv;

    @BindView(R.id.includedYouMayBeInterestedProducts)
    public View youMayBeInterestedView;

    @BindView(R.id.itemDetailBeTheFirstToReviewTv)
    public AppCompatTextView itemDetailBeTheFirstToReviewTv;

    @BindView(R.id.listItemOfferLabelTv)
    public AppCompatTextView listItemOfferLabelTv;

    @BindView(R.id.itemDetailRelatedProductCv)
    public View itemDetailRelatedProductCv;

    @BindView(R.id.itemDetailYouMayInterestedInCv)
    public View itemDetailYouMayInterestedInCv;

    @BindView(R.id.includedAppReview)
    public View includedAppReview;

    @BindView(R.id.itemDetailDescriptionCv)
    public CardView itemDetailDescriptionCv;

    @BindView(R.id.itemDetailRatingTv)
    public AppCompatTextView itemDetailRatingTv;

    @BindView(R.id.itemDetailReviewNumberTv)
    public AppCompatTextView itemDetailReviewNumberTv;

    @BindView(R.id.itemDetailReviewComIconIv)
    public AppCompatImageView itemDetailReviewComIconIv;

    @BindView(R.id.itemDetailAppReviewCv)
    public CardView itemDetailAppReviewCv;

    @BindView(R.id.imageMsgNoItemTv)
    public AppCompatTextView imageMsgNoItemTv;

    @BindView(R.id.listItemShareFAB)
    public FloatingActionButton listItemShareFAB;

    @BindView(R.id.itemDetailRatingLl)
    public LinearLayout itemDetailRatingLl;

    public RecyclerView relatedProductRv;
    public RecyclerView youMayBeInterestedRv;

    @BindView(R.id.detailTierPriceRv)
    protected RecyclerView detailTierPriceRv;

    @BindView(R.id.detailCustomOptionsRv)
    protected RecyclerView detailCustomOptionsRv;

    private HorizontalItemAdapter relatedProductAdapter;
    private HorizontalItemAdapter youMayBeInterestedAdapter;
    private CustomOptionAdapter customOptionAdapter;

    private Product_ productDetail = null;
    private DetailActivity activity;
    private DividerItemDecoration dividerItemDecoration;

    private CirclePageIndicator circlePageIndicator;
    private Drawable backArrowDrawable;
    private int menuColor = R.color.colorGrey;
    private int currentMenuColor = 0;
    private int menuSize = 0;

    public DetailView(@NonNull DetailActivity activity) {
        super(activity);
        this.activity = activity;

        initIndicator();

        dividerItemDecoration = new DividerItemDecoration(getContext(), DividerItemDecoration.HORIZONTAL);
        dividerItemDecoration.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.bg_divider));

        backArrowDrawable = toolbar.getNavigationIcon();

        itemDetailReviewComIconIv.setOnClickListener(this);
        itemDetailAppReviewCv.setOnClickListener(this);


        /*nestedSV.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {

            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                int intColorCode = scrollY;
                if(intColorCode>255){
                    intColorCode = 255;
                }
                //double toolbarAlpha = scale(intColorCode, 0, 255, 0, 1);
                //Log.i("FLOAT VALUE", toolbarAlpha+" sx"+scrollY+" osx"+oldScrollY+" intColorCode "+intColorCode);
                int color = ColorUtils.setAlphaComponent(Color.parseColor("#21920f"),intColorCode);
                toolbar.setBackgroundColor(color);

                if(intColorCode>=100){
                    menuColor = R.color.colorWhite;
                }else{
                    menuColor = R.color.colorGrey;
                }
                changeMenuIconColor(menuColor);
            }
        });*/

        activity.getSupportActionBar().setTitle("");

        itemDetailDescriptionTv.setOnExpandStateChangeListener((textView, isExpanded) -> {
            View expandCollapseView= itemDetailDescriptionTv.findViewById(R.id.expand_collapse);
            expandCollapseView.setBackground(isExpanded?null:ContextCompat.getDrawable(getContext(), R.drawable.tv_gradient_more));
        });

        initRelatedProductView();
        initYouMayBeInterestedInView();
        changeMenuIconColor(menuColor);
    }

    public Observable<Object> plusBtn(){
        return RxView.clicks(img_plus);
    }

    public Observable<Object> minusBtn(){
        return RxView.clicks(img_minus);
    }

    public Observable<Object> shareBtn(){
        return RxView.clicks(listItemShareFAB);
    }

    public Observable<Object> addToCartBtn() {
        return RxView.clicks(itemDetailAddToCartBtn);
    }

    public void setQuantity(int qty){
        txtQuantity.setText(qty+"");
    }
    public void setQudantity(int qty){
        txtQuantity.setText(qty+"");
    }

    public int getQuantity(){
        return Integer.valueOf(txtQuantity.getText().toString().trim());
    }

    @Override
    public int getContentLayout() {
        return R.layout.activity_item_detail_;
    }

    public void setDetail(Product_ detail){
        productDetail = detail;

        itemDetailItemNameTv.setText(detail.getName());
        Log.i("FORMATTED PRICE",detail.getFormattedFinalPrice()+","+detail.getFormattedPrice());
     //   itemDetailItemPriceTv.setText(getContext().getString(R.string.price, Utility.convertToTwoDecimalPlace(detail.getFinalPrice()))+" "+weightValue);
        itemDetailItemPriceTv.setText(detail.getFormattedFinalPrice());
        //checking if final price is not equal to price i.e. discount on product available
        if(!detail.getFinalPrice().equalsIgnoreCase(detail.getPrice())) {
        //    itemDetailItemPriceCutTv.setText(getContext().getString(R.string.price, Utility.convertToTwoDecimalPlace(detail.getPrice())));
            itemDetailItemPriceCutTv.setText(detail.getFormattedPrice());
            itemDetailItemPriceCutTv.setPaintFlags(itemDetailItemPriceCutTv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            itemDetailItemPriceCutTv.setVisibility(View.VISIBLE);
        }else{
            //final price equal to price i.e. no discount available on product ,hide the pricecut textview
            itemDetailItemPriceCutTv.setVisibility(View.GONE);
        }
        Spanned spanned = null;

           //Produce detail description check for not null
        if (detail.getDescription()!=null && !detail.getDescription().isEmpty()){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                spanned = Html.fromHtml(detail.getDescription(), Html.FROM_HTML_MODE_COMPACT);
            }else{
                spanned = Html.fromHtml(detail.getDescription());
            }
        }else {
            //if product detail description is null hiding the item decription containing card view
            itemDetailDescriptionCv.setVisibility(View.GONE);
        }
        itemDetailDescriptionTv.setText(spanned);

            //product discount for not null
        if(productDetail.getDiscountLabel() != null && !productDetail.getDiscountLabel().isEmpty()){
            listItemOfferLabelTv.setText(productDetail.getDiscountLabel());
            listItemOfferLabelTv.setVisibility(View.VISIBLE);
        }else{
            //if no product discount hiding product lable discount TextView
            listItemOfferLabelTv.setVisibility(View.GONE);
        }


        //itemDetailDescriptionTv.setText(detail.getDescription());
        itemDetailSKUTv.setText(getContext().getString(R.string.sku)+" "+detail.getSku());

        if(detail.getImages()!=null){
            if(!detail.getImages().isEmpty()){
                    bannerSlider.setIndicatorInside(false);
                    bannerSlider.setPages(() -> new SliderViewHolder(), detail.getImages()).setIndicator(circlePageIndicator);
            }
        }
    }

    public AlertDialog showCartAlertDialog(){
        return new AlertDialog.Builder(activity)
                .setMessage(R.string.successfully_added_to_cart)
                .setPositiveButton(R.string.continue_shopping, ((dialogInterface, i) -> {
                    dialogInterface.dismiss();
                }))
                .setNegativeButton(R.string.view_cart, (dialogInterface, i) -> {
                    dialogInterface.dismiss();
                    Intent intent = new Intent(getContext(), CartActivity.class);
                    getContext().startActivity(intent);
                    activity.finish();
                }).create();
    }

    public String getProductId(){
        if(productDetail!=null)
            return productDetail.getEntityId();
        else
            return "";
    }

    public String getProductQty(){
        return txtQuantity.getText().toString().trim();
    }


    public void showProgressBar(boolean showItem){
        if(showItem)
            progressBar.setVisibility(View.VISIBLE);
        else
            progressBar.setVisibility(View.GONE);
    }

    public void showNoItem(boolean showing,boolean fromNormalOrFromException){
        if(showing) {
            noItemView.setVisibility(View.VISIBLE);
            if (fromNormalOrFromException){
                imageMsgNoItemTv.setText(R.string.sww);
            }
        }
        else {
            noItemView.setVisibility(View.GONE);
        }
    }

    public double scale(final double valueIn, final double baseMin, final double baseMax, final double limitMin, final double limitMax) {
        return ((limitMax - limitMin) * (valueIn - baseMin) / (baseMax - baseMin)) + limitMin;
    }


    //initializing the related product view
    private void initRelatedProductView(){
        ((AppCompatTextView) itemDetailRelatedProductCv.findViewById(R.id.categoryHeaderTv)).setText(R.string.related_product);
        relatedProductAdapter = new HorizontalItemAdapter(getContext());
        relatedProductRv = (RecyclerView) itemDetailRelatedProductCv.findViewById(R.id.labelRecyclerRv);
        relatedProductRv.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        //ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(getContext(), R.dimen.item_offset_1, true);
        relatedProductRv.addItemDecoration(dividerItemDecoration);
        relatedProductRv.setHasFixedSize(true);
        relatedProductRv.setAdapter(relatedProductAdapter);
    }

    //initializing the interested product view
    private void initYouMayBeInterestedInView(){
        ((AppCompatTextView) youMayBeInterestedView.findViewById(R.id.categoryHeaderTv)).setText(R.string.you_may_also_like);
        //youMayBeInterestedView.findViewById(R.id.categorySeeMoreBtn);
        youMayBeInterestedAdapter = new HorizontalItemAdapter(getContext());
        youMayBeInterestedRv = (RecyclerView)youMayBeInterestedView.findViewById(R.id.labelRecyclerRv);
        youMayBeInterestedRv.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        youMayBeInterestedRv.addItemDecoration(dividerItemDecoration);
        youMayBeInterestedRv.setHasFixedSize(true);
        youMayBeInterestedRv.setAdapter(youMayBeInterestedAdapter);
    }

    public void addRelatedProducts(List<Product> productList){
        if(productList!=null && !productList.isEmpty()){
            relatedProductAdapter.addData(productList);
            itemDetailRelatedProductCv.setVisibility(View.VISIBLE);
        }else {
            itemDetailRelatedProductCv.setVisibility(View.GONE);
        }
    }

    public void addAppReview(AppReviews appReviews){
        if (appReviews!=null){
            if (appReviews.getRate() == 0){
                itemDetailAppReviewCv.setVisibility(View.VISIBLE);
                itemDetailBeTheFirstToReviewTv.setVisibility(View.VISIBLE);
                itemDetailBeTheFirstToReviewTv.setText(R.string.first_to_review_product);
                itemDetailRatingLl.setVisibility(View.INVISIBLE);
                itemDetailReviewNumberTv.setVisibility(View.INVISIBLE);
            }else {
                itemDetailBeTheFirstToReviewTv.setVisibility(View.GONE);
                itemDetailRatingLl.setVisibility(View.VISIBLE);
                itemDetailAppReviewCv.setVisibility(View.VISIBLE);
                String rate = appReviews.getRate().toString();
                itemDetailRatingTv.setText(Utility.convertToDecimalPlace(rate, 1));
                itemDetailReviewNumberTv.setText(getContext().getString(R.string.item_review_number,appReviews.getNumber()));
            }
        }else {
            itemDetailAppReviewCv.setVisibility(View.GONE);
        }

    }

    public void addYouMayBeInterestedProducts(List<Product> productList){
        //checking product list for null and empty
        if(productList!=null && !productList.isEmpty()){
            //if product list not null add the product list to the youMayBeInterestedAdapter
            youMayBeInterestedAdapter.addData(productList);
            itemDetailYouMayInterestedInCv.setVisibility(View.VISIBLE);
        }else {
            itemDetailYouMayInterestedInCv.setVisibility(View.GONE);
        }
    }

    private void initIndicator(){
        circlePageIndicator = new CirclePageIndicator(getContext());
        circlePageIndicator.setStrokeColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
        circlePageIndicator.setFillColor(ContextCompat.getColor(getContext(), R.color.colorGreenLight));
        circlePageIndicator.setRadius(MaterialBanner.dip2Pix(getContext(),3));
        circlePageIndicator.setBetween(20);
    }

    public void initCustomOptions(List<CustomOption> customOptions) {
        if(!customOptions.isEmpty()) {
            customOptionAdapter = new CustomOptionAdapter(getContext());
            detailCustomOptionsRv.setLayoutManager(new LinearLayoutManager(getContext()));
            detailCustomOptionsRv.setNestedScrollingEnabled(false);
            detailCustomOptionsRv.setAdapter(customOptionAdapter);
            customOptionAdapter.addAllData(customOptions);
            detailCustomOptionsRv.setVisibility(View.VISIBLE);

        }else{
            detailCustomOptionsRv.setVisibility(View.INVISIBLE);
        }
    }

    public class SliderViewHolder implements Holder<Image> {
        private AppCompatImageView sliderIv;

        @Override
        public View createView(Context context) {
            View view = LayoutInflater.from(context).inflate(R.layout.detail_slider,null);
            sliderIv = (AppCompatImageView) view.findViewById(R.id.sliderIv);
            return view;
        }

        @Override
        public void UpdateUI(Context context, int i, Image banners) {
            Glide.with(context)
                    .load(banners.getUrl())
                    .dontAnimate()
                    .fitCenter()
                    .into(sliderIv);
        }
    }

    public void changeMenuIconColor(int color){
        if(activity.getMenu()!=null) {
            menuSize = this.activity.getMenu().size();

            if (currentMenuColor != color) {
                for (int i = 0; i < menuSize; i++) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        activity.getMenu().getItem(i).getIcon().setTint(ContextCompat.getColor(activity, color));//.setColorFilter(ContextCompat.getColor(activity, color), PorterDuff.Mode.SRC_ATOP);
                    }else{
                        boolean isCartIcon = (activity.getMenu().getItem(i).getItemId() == R.id.action_cart);
                        if(isCartIcon) {
                            //activity.getMenu().getItem(i).getIcon().mutate().setColorFilter(ContextCompat.getColor(getContext(), color), PorterDuff.Mode.SRC_IN);
                            Drawable drawable = ContextCompat.getDrawable(getContext(), R.mipmap.ic_toolbar_cart);
                            drawable.setColorFilter(ContextCompat.getColor(activity, color), PorterDuff.Mode.SRC_ATOP);
                            activity.getMenu().getItem(i).setIcon(drawable);

                        }else{
                        activity.getMenu().getItem(i).getIcon().mutate().setColorFilter(ContextCompat.getColor(getContext(), color), PorterDuff.Mode.SRC_IN);
                        }
                    }
                    backArrowDrawable.setColorFilter(ContextCompat.getColor(activity, color), PorterDuff.Mode.SRC_ATOP);
                }
                currentMenuColor = color;
                activity.updateCartInMenu(DoneApplication.getInstance().getCartCount());
            }
        }
    }

    @Override
    public void onClick(View v) {
        Gson gson = new Gson();
        String product =  gson.toJson(productDetail, Product_.class);
        String productID = productDetail.getEntityId();
        if (v.getId() == R.id.itemDetailAppReviewCv){
                if (productDetail.getAppReviews().getNumber() == 0){
                    Intent intentToReviewPost = new Intent(getContext(), ReviewPostActivity.class);
                    intentToReviewPost.putExtra(Constant.PRODUCT_DETAIL,product);
                    activity.startActivity(intentToReviewPost);
                }else {
                    //  Toast.makeText(getContext(),"Navigate To ReviewRating Page",Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(getContext(), ReviewRatingsActivity.class);
                    intent.putExtra(Constant.PRODUCT_DETAIL, product);
                    activity.startActivity(intent);
                }
        }
    }

    public void initTierPrice(List<String> data){
        if(data!=null && !data.isEmpty()) {
            detailTierPriceRv.setLayoutManager(new LinearLayoutManager(getContext()));
            DividerItemDecoration diH = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
            diH.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.bg_divider_tier));
            detailTierPriceRv.addItemDecoration(diH);
            detailTierPriceRv.setNestedScrollingEnabled(false);
            TierPriceAdapter tierPriceAdapter = new TierPriceAdapter(getContext());
            tierPriceAdapter.addAllData(data);
            detailTierPriceRv.setAdapter(tierPriceAdapter);
            detailTierPriceRv.setHasFixedSize(true);
            detailTierPriceRv.setVisibility(View.VISIBLE);
        }else{
            detailTierPriceRv.setVisibility(View.GONE);
        }
    }
}
