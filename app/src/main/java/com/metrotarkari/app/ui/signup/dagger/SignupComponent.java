package com.metrotarkari.app.ui.signup.dagger;

import com.metrotarkari.app.application.dagger.AppComponent;
import com.metrotarkari.app.ui.signup.SignupActivity;

import dagger.Component;

@SingnupScope
@Component(modules = { SignupModule.class }, dependencies = AppComponent.class)
public interface SignupComponent {

  void inject(SignupActivity activity);

}
