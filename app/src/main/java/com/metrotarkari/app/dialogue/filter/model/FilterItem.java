package com.metrotarkari.app.dialogue.filter.model;

import com.bignerdranch.expandablerecyclerview.model.Parent;

import java.util.List;

/**
 * Created by samrakchan on 4/4/17.
 */

public class FilterItem implements Parent<FilterChild> {

    private List<FilterChild> filterChilds;
    private String filterHeading;
    private boolean initiallyExpended = false;


    public FilterItem(String filterHeading, List<FilterChild> filterChilds){
        this.filterChilds = filterChilds;
        this.filterHeading = filterHeading;
    }

    public String getFilterTitle(int position){
        return filterHeading;
    }


    @Override
    public List<FilterChild> getChildList() {
        return filterChilds;
    }

    public void setInitiallyExpended(boolean initiallyExpended){
        this.initiallyExpended = initiallyExpended;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return initiallyExpended;
    }
}
