package com.metrotarkari.app.ui.checkout.mvp;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.jakewharton.rxbinding2.view.RxView;
import com.metrotarkari.app.R;
import com.metrotarkari.app.model.Address;
import com.metrotarkari.app.model.Shipping;
import com.metrotarkari.app.ui.addressbook.addresslist.mvp.AddressListActivity;
import com.metrotarkari.app.ui.base.BaseView;
import com.metrotarkari.app.ui.checkout.CheckoutAdapter;
import com.metrotarkari.app.ui.checkout.model.CouponCode;
import com.metrotarkari.app.ui.dashboard.MainActivity;
import com.metrotarkari.app.utils.AlertUtils;
import com.metrotarkari.app.utils.Constant;
import com.metrotarkari.app.utils.DoneItemClickListener;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;

/**
 * Created by samrakchan on 4/27/17.
 */

public class CheckoutView extends BaseView implements DoneItemClickListener {

    @BindView(R.id.chekoutRecyclerView)
    public RecyclerView chekoutRecyclerView;

    @BindView(R.id.imageMsgRl)
    public View noItemView;

    @BindView(R.id.progressBarRl)
    public View progressBar;

    private LinearLayoutManager mLayoutManager;
    private CheckoutAdapter mAdapter;

    @BindView(R.id.swipeRefreshView)
    public SwipeRefreshLayout swipeRefreshView;

    @BindView(R.id.checkoutBtn)
    AppCompatButton checkoutBtn;

    private final BehaviorSubject<Map<String, String>> couponCodeObservable = BehaviorSubject.create();

    private AppCompatActivity activity;
    private int totalItemsCount;

    public CheckoutView(@NonNull AppCompatActivity activity) {
        super(activity);
        this.activity = activity;

        mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        mAdapter = new CheckoutAdapter(activity);
        mAdapter.addOnItemClickListener(this);
        DividerItemDecoration diH = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        diH.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.bg_divider));
        chekoutRecyclerView.addItemDecoration(diH);
        chekoutRecyclerView.setHasFixedSize(true);
        chekoutRecyclerView.setLayoutManager(mLayoutManager);
        chekoutRecyclerView.setAdapter(mAdapter);

        swipeRefreshView.setEnabled(false);
        swipeRefreshView.setRefreshing(false);

        activity.getSupportActionBar().setTitle(R.string.checkout);

    }

    public Observable<Object> observerCheckoutButton() {
        return RxView.clicks(checkoutBtn);
    }

    public void addItemToCheckoutAdapter(List<Object> items) {
        mAdapter.addData(items);
    }

    public void setTotalItems(int totalItems){
        totalItemsCount = totalItems;
    }

    public int getTotalItems(){
        return totalItemsCount;
    }

    public void showInvoice(String invoice) {
        //show alert by displaying the ordered invoice number and intent to MainActivity
        Intent intent = new Intent(getContext(), MainActivity.class);
        AlertUtils.createAlert(activity, R.string.ok, getContext().getString(R.string.invoice_message) + " " + invoice, intent).show();
    }


    @Override
    public int getContentLayout() {
        return R.layout.activity_checkout;
    }


    public void showNoItem(boolean showItem){
        if(showItem)
            noItemView.setVisibility(View.VISIBLE);
        else
            noItemView.setVisibility(View.GONE);
    }

    public void showProgressBar(boolean showing){
        if(showing)
            progressBar.setVisibility(View.VISIBLE);
        else
            progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onItemClickListener(int position, int viewId, Object object) {
        //Change Address of Checkout
        if(object instanceof Address){
            //Intent for selecting address
            Intent intent = new Intent(getContext(), AddressListActivity.class);
            intent.putExtra(Constant.INTENT_FOR, Constant.SELECT_ADDRESS);
            activity.startActivityForResult(intent, Constant.SELECT_ADDRESS);

        }else if(object instanceof CouponCode) {
            //coupon code
            Map<String, String> map = new HashMap<>();
            CouponCode cc = (CouponCode)object;
            map.put(Constant.COUPON_CODE, cc!=null? cc.getCoupenCode():"");
            couponCodeObservable.onNext(map);
        }
    }


    public BehaviorSubject<Map<String, String>> getCouponCodeBehaviorSubject(){
        return couponCodeObservable;
    }

    public void removeItemFromAdapter(){
        mAdapter.clear();
    }


    public void setDateTime(String date,Shipping time){
        //setting the selected date and time to the checkout adapter
        mAdapter.setSelectedDate(date);
        mAdapter.setSelectedTime(time);
        mAdapter.notifyDataSetChanged();
    }

    public void setMessage(String message){
        //setting the selected message to the checkout adapter
        mAdapter.setMessage(message);
        mAdapter.notifyDataSetChanged();
    }

    public String getSelectedDate(){
        return mAdapter.getSelectedDate();
    }

    public Shipping getSelectedTime(){
        return mAdapter.getSelectedTime();
    }

    public String getAddressId(){
        return mAdapter.getAddressId();
    }

}