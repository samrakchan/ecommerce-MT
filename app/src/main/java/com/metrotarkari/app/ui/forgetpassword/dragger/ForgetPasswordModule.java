package com.metrotarkari.app.ui.forgetpassword.dragger;

import android.support.v7.app.AppCompatActivity;

import com.metrotarkari.app.application.network.DoneNetwork;
import com.metrotarkari.app.ui.forgetpassword.mvp.ForgetPasswordModel;
import com.metrotarkari.app.ui.forgetpassword.mvp.ForgetPasswordPresenter;
import com.metrotarkari.app.ui.forgetpassword.mvp.ForgetPasswordView;

import dagger.Module;
import dagger.Provides;

@Module
public class ForgetPasswordModule {

  private final AppCompatActivity activity;

  public ForgetPasswordModule(AppCompatActivity activity) {
    this.activity = activity;
  }

  @Provides
  @ForgetPasswordScope
  public ForgetPasswordView view() {
    return new ForgetPasswordView(activity);
  }

  @Provides
  @ForgetPasswordScope
  public ForgetPasswordModel model(DoneNetwork doneNetwork){
    return new ForgetPasswordModel(activity, doneNetwork);
  }

  @Provides
  @ForgetPasswordScope
  public ForgetPasswordPresenter forgetPasswordPresenter(ForgetPasswordView homeView, ForgetPasswordModel model) {
    return new ForgetPasswordPresenter(activity, homeView, model);
  }

}
