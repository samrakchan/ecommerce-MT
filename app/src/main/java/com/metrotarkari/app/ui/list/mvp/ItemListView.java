package com.metrotarkari.app.ui.list.mvp;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metrotarkari.app.R;
import com.metrotarkari.app.dialogue.filter.FilteredAdapter;
import com.metrotarkari.app.generic.OnItemClickListener;
import com.metrotarkari.app.model.FilterQueryWithPagination;
import com.metrotarkari.app.model.ItemParams;
import com.metrotarkari.app.model.LayerFilter;
import com.metrotarkari.app.model.Product;
import com.metrotarkari.app.model.Sort;
import com.metrotarkari.app.ui.base.BaseViewFragment;
import com.metrotarkari.app.ui.list.ItemListAdapter;
import com.metrotarkari.app.ui.list.ViewType;
import com.metrotarkari.app.ui.list.filter.DrawerFilterFragment;
import com.metrotarkari.app.ui.list.sortby.DrawerSortFragment;
import com.metrotarkari.app.utils.OrderListener;
import com.metrotarkari.app.utils.QueryListener;
import com.metrotarkari.app.utils.ViewUtils;
import com.paginate.Paginate;
import com.paginate.recycler.LoadingListItemCreator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;

/**
 * Created by samrakchan on 4/27/17.
 */

public class ItemListView extends BaseViewFragment implements View.OnClickListener, Paginate.Callbacks, QueryListener, OrderListener, OnItemClickListener{

    @BindView(R.id.listRecyclerView)
    public RecyclerView listRecyclerView;

    @BindView(R.id.layoutFilteredRv)
    public RecyclerView layoutFilteredRv;

    @BindView(R.id.listViewTooggleBtn)
    public AppCompatImageButton listViewTooggleBtn;

    @BindView(R.id.itemListFilterIv)
    public AppCompatImageView itemListFilterIv;

    @BindView(R.id.itemListSortIv)
    public AppCompatImageView itemListSortIv;

    @BindView(R.id.imageMsgRl)
    public View noItemView;

    @BindView(R.id.progressBarRl)
    public View progressBar;

    @BindView(R.id.itemListFilterTv)
    public AppCompatTextView itemListFilterTv;

    @BindView(R.id.nav_view)
    public NavigationView navView;

    @BindView(R.id.drawer_layout)
    public DrawerLayout drawerLayout;

    @BindView(R.id.itemListSortLl)
    public View itemListSortLl;

    @BindView(R.id.itemListFilterLl)
    public View itemListFilterLl;

    @BindView(R.id.listActionRl)
    public View listActionRl;


    /*@BindView(R.id.swipeRefreshView)
    public SwipeRefreshLayout swipeRefreshLayout;*/

    public ItemListAdapter mProductAdapter;

    private FilteredAdapter mFilteredAdapter;

    private GridLayoutManager mGridLayoutManager;

    private FragmentManager fragmentManager;
    private View drawerContainerView;


    private Paginate paginate;

    private ArrayList<LayerFilter> filters;
    private ArrayList<Sort> orders;

    //private final BehaviorSubject<ItemParams> paginationItemParamsBehaviorSubject = BehaviorSubject.create();
    private final BehaviorSubject<FilterQueryWithPagination> filterQueryBehaviorSubject = BehaviorSubject.create();
    private ItemParams paginationParams = new ItemParams();

    private FilterQueryWithPagination filterQueryWithPagination = new FilterQueryWithPagination();

    private int page = 0;
    private boolean loading = false;
    protected int threshold = 4;
    protected int totalPages = 1;
    protected int itemsPerPage = 20;
    protected boolean addLoadingRow = true;
    protected boolean customLoadingListItem = false;
    private HashMap<String, String> map = new HashMap<>();


    public ItemListView(Fragment fragment) {
        super(fragment);

        if (paginate != null) {
            paginate.unbind();
        }

        drawerContainerView = navView.findViewById(R.id.frameLayout);
        fragmentManager = fragment.getChildFragmentManager();

        filters = new ArrayList<>();
        orders = new ArrayList<>();

        paginationParams.setDir("");
        paginationParams.setOrderBy("");

        listViewTooggleBtn.setOnClickListener(this);
        itemListFilterLl.setOnClickListener(this);
        itemListSortLl.setOnClickListener(this);

        layoutFilteredRv.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        mFilteredAdapter = new FilteredAdapter(getContext());
        layoutFilteredRv.setAdapter(mFilteredAdapter);

        mProductAdapter = new com.metrotarkari.app.ui.list.ItemListAdapter(getContext());
        mGridLayoutManager = new GridLayoutManager(getContext(), ViewUtils.calculateNoOfColumns(getContext()));
        listRecyclerView.setLayoutManager(mGridLayoutManager);
        DividerItemDecoration dividerItemDecorationH = new DividerItemDecoration(getContext(), DividerItemDecoration.HORIZONTAL);
        DividerItemDecoration dividerItemDecorationV = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        dividerItemDecorationH.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.bg_divider));
        dividerItemDecorationV.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.bg_divider));
        //ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(getContext(), R.dimen.item_offset);
        listRecyclerView.addItemDecoration(dividerItemDecorationH);
        listRecyclerView.addItemDecoration(dividerItemDecorationV);
        listRecyclerView.setHasFixedSize(true);
        listRecyclerView.setAdapter(mProductAdapter);

        loading = false;
        page = 0;

        paginate = Paginate.with(listRecyclerView, this)
                .setLoadingTriggerThreshold(threshold)
                .addLoadingListItem(addLoadingRow)
                .setLoadingListItemCreator(customLoadingListItem ? new CustomLoadingListItemCreator() : null)
                .setLoadingListItemSpanSizeLookup(() -> mGridLayoutManager.getSpanCount())
                .build();

    }

    public void setTotalPage(int totalPages){
        this.totalPages = totalPages;
    }

    @Override
    protected void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mGridLayoutManager.setSpanCount(ViewUtils.calculateNoOfColumns(getContext()));
    }

    @Override
    public int getContentLayout() {
        return R.layout.fragment_item_list;
    }

    public void addItemToProductAdapter(List<Product> products){
        loading = false;
        mProductAdapter.addData(products);
    }

    public void showNoItem(boolean showItem){
        if(showItem)
            noItemView.setVisibility(View.VISIBLE);
        else
            noItemView.setVisibility(View.GONE);
    }

    public void showProgressBar(boolean showing){
        if(showing)
            progressBar.setVisibility(View.VISIBLE);
        else
            progressBar.setVisibility(View.GONE);
    }

    public void setPageIncrement(){
        page = page+1;
    }

    public int getCurrentItemSize(){
        return mProductAdapter.getItemCount();
    }

    public int getItemPerPage(){
        return itemsPerPage;
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.listViewTooggleBtn){

            if(mProductAdapter.getCurrentViewType()== ViewType.GRID){
                //product listing in grid view
                mGridLayoutManager.setSpanCount(1);
                mProductAdapter.setViewType(ViewType.LIST);
                listViewTooggleBtn.setImageResource(R.mipmap.ic_list_grid);

            }else if(mProductAdapter.getCurrentViewType() == ViewType.LIST){
                //product listing in list view
                mGridLayoutManager.setSpanCount(ViewUtils.calculateNoOfColumns(getContext()));
                mProductAdapter.setViewType(ViewType.GRID);
                listViewTooggleBtn.setImageResource(R.mipmap.ic_list_list);
            }
        }else if(v.getId() == R.id.itemListFilterLl){
            //for product filtering by price and category
            initFilterFragment();
        }else if(v.getId() == R.id.itemListSortLl){
            //for product sorting in order i.e. ascending or descending
            initSortFragment();
        }
    }

    private void initFilterFragment(){
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("LIST", filters);
        DrawerFilterFragment drawerFilterFragment = new DrawerFilterFragment();
        drawerFilterFragment.setQueryListener(this);
        drawerFilterFragment.setOnItemClickListener(this);
        drawerFilterFragment.setArguments(bundle);
        loadDrawerFragment(drawerFilterFragment);
        toggleDrawer();
    }

    private void initSortFragment(){
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("LIST", orders);
        DrawerSortFragment drawerSortFragment = new DrawerSortFragment();
        drawerSortFragment.setOrderListener(this);
        drawerSortFragment.setOnItemClickListener(this);
        drawerSortFragment.setArguments(bundle);
        loadDrawerFragment(drawerSortFragment);
        toggleDrawer();
    }


    @Override
    public void onLoadMore() {
        Log.d("Paginate", "onLoadMore");
        loading = true;

        paginationParams.setPage(page);
        paginationParams.setLimit(itemsPerPage);

        filterQueryWithPagination.setItemParams(paginationParams);
        filterQueryWithPagination.setQueryValue(map);

        filterQueryBehaviorSubject.onNext(filterQueryWithPagination);
    }

    public Observable<FilterQueryWithPagination> getFilterObservable(){
        return filterQueryBehaviorSubject;
    }

    public void setFilterAttributes(List<LayerFilter> layerFilters){
        if(filters.size()>0){
            filters.clear();
        }
        if(layerFilters.size()>0){
            layerFilters.get(0).setInitiallyExpended(true);
        }

        filters.addAll(layerFilters);
    }

    public void setOrder(List<Sort> orders){
        this.orders.clear();
        this.orders.addAll(orders);
    }

    @Override
    public boolean isLoading() {
        return loading;
    }

    @Override
    public boolean hasLoadedAllItems() {
        return page == totalPages;
    }

    @Override
    public void onQueryListener(HashMap<String, String> hashMap) {
        toggleDrawer();
        page = 0;
        paginationParams.setPage(page);
        filterQueryWithPagination.getQueryValue().putAll(hashMap);

        mProductAdapter.removeAllData();

        //filterQueryBehaviorSubject.onNext(filterQueryWithPagination);
    }

    /**
     * short by
     * @param order
     */

    @Override
    public void onOrderListener(Sort order) {
        if (order != null){
            page = 0;
            paginationParams.setPage(page);
            paginationParams.setDir(order.getDirection());
            paginationParams.setOrderBy(order.getKey());

            mProductAdapter.removeAllData();
        }
    }

    @Override
    public void setOnItemClickListener(Object object) {
        toggleDrawer();
    }

    private class CustomLoadingListItemCreator implements LoadingListItemCreator {
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View view = inflater.inflate(R.layout.layout_progress, parent, false);
            return new VH(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

            if (listRecyclerView.getLayoutManager() instanceof StaggeredGridLayoutManager) {
                StaggeredGridLayoutManager.LayoutParams params = (StaggeredGridLayoutManager.LayoutParams) holder.itemView.getLayoutParams();
                params.setFullSpan(true);
            }
        }
    }

    static class VH extends RecyclerView.ViewHolder {

        public VH(View itemView) {
            super(itemView);
        }
    }

    public void toggleDrawer(){
        if (drawerLayout.isDrawerOpen(Gravity.END)) {
            drawerLayout.closeDrawer(Gravity.END);
        } else {
            drawerLayout.openDrawer(Gravity.END);
        }
    }

    private void loadDrawerFragment(Fragment fragment){
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(drawerContainerView.getId(), fragment, "HE");
        fragmentTransaction.commit();
    }

    public void showListActionView(boolean show){
        if(show){
            listActionRl.setVisibility(View.VISIBLE);
        }else{
            listActionRl.setVisibility(View.GONE);
        }
    }
}
