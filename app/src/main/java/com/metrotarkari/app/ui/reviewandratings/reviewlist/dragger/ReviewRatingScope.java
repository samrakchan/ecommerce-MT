package com.metrotarkari.app.ui.reviewandratings.reviewlist.dragger;

import javax.inject.Scope;

@Scope
public @interface ReviewRatingScope {
}
