package com.metrotarkari.app.ui.cart.mvp;

import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.metrotarkari.app.R;
import com.metrotarkari.app.application.DoneApplication;
import com.metrotarkari.app.db.DoneDbAdapter;
import com.metrotarkari.app.event.CartEvent;
import com.metrotarkari.app.model.BAddress;
import com.metrotarkari.app.model.Base;
import com.metrotarkari.app.model.CartQuoteContent;
import com.metrotarkari.app.model.Error;
import com.metrotarkari.app.model.ItemParams;
import com.metrotarkari.app.model.Quoteitem;
import com.metrotarkari.app.model.StoreViewResult;
import com.metrotarkari.app.model.Total;
import com.metrotarkari.app.utils.AlertUtils;
import com.metrotarkari.app.utils.DoneUserPreferenceManager;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by samrakchan on 4/27/17.
 */

public class CartPresenter {
    private static final String TAG = CartPresenter.class.getSimpleName();

    private final CartView view;
    private final CartModel model;
    private double cartTotalAmount = 0.0;

    private DoneDbAdapter doneDbAdapter;

    private boolean proceedToCheckout = false;

    private AppCompatActivity activity;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private DoneUserPreferenceManager doneUserPreferenceManager;
    private EventBus bus = EventBus.getDefault();

    public CartPresenter(CartView view, CartModel model, DoneUserPreferenceManager preferenceManager, AppCompatActivity activity,DoneDbAdapter doneDbAdapter){
        this.view = view;
        this.model = model;
        this.doneUserPreferenceManager = preferenceManager;
        this.doneDbAdapter = doneDbAdapter;

        this.activity = activity;

        compositeDisposable.add(view.getOnScrollObservable().subscribe(
                itemParams -> {
                    compositeDisposable.add(getQuote(itemParams));
                }
        ));

        view.onAlertDeleteObservable().throttleFirst(2, TimeUnit.SECONDS)
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(aVoid -> {
                        Disposable disposable = changeItemQty(aVoid);
                        compositeDisposable.add(disposable);
                })
        ;

        view.proceedToCheckOutClickObservable().throttleFirst(2, TimeUnit.SECONDS)
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(aVoid -> {
                    makeOrder();
                    if (proceedToCheckout) {
                        if (doneUserPreferenceManager.isLoggedInUser()) {
                            compositeDisposable.add(getAddressBook());
                        } else {
                            view.navigateToLoginActivity();
                        }
                    }
                });
    }

    private Disposable changeItemQty(HashMap<String, Integer> map){
        view.showProgressDialog();
        return model.putCartQuote(map).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(cartQuoteContent -> {
                    List<Error> errorResponse = cartQuoteContent.getErrors();
                    if (errorResponse != null && !errorResponse.isEmpty()) {
                        Error error = errorResponse.get(0);
                        Toast.makeText(view.getContext(), error.getMessage() + "", Toast.LENGTH_LONG).show();
                        view.hideProgressDialog();
                    } else {
                        view.hideProgressDialog();
                        view.clearItemAndChangePageToZero();

                        loadItem(cartQuoteContent);
                    }

                }, throwable -> {
                    view.hideProgressDialog();
                    Toast.makeText(view.getContext(),R.string.sww,Toast.LENGTH_LONG).show();
                    throwable.printStackTrace();
                });
    }

    private Disposable getQuote(ItemParams itemParams){
        if(view.getCurrentItemSize()==0) {
            view.showProgressBar(true);
        }
        itemParams.setPage(itemParams.getPage()+1);

        view.setPageIncrement();
        view.showNoItem(false);
        return model.getCartQuote(itemParams)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(cartQuoteContent -> {
                    proceedToCheckout = false;
                    List<Error> errorResponse = cartQuoteContent.getErrors();
                    if(errorResponse!=null && !errorResponse.isEmpty()){
                        Error error = errorResponse.get(0);
                        if(error.getCode()==4){
                            //code for Invalid Method
                            //this appears when user tries to load items by scrolling
                            Log.i(TAG, error.getMessage());
                        }else {
                            Toast.makeText(view.getContext(), error.getMessage() + "", Toast.LENGTH_LONG).show();
                        }
                        view.showProgressBar(false);
                    }else{
                        loadItem(cartQuoteContent);
                    }
                }, throwable -> {
                    throwable.printStackTrace();
                    view.showProgressBar(false);
                    view.showNoItem(true);
                    Toast.makeText(view.getContext(),R.string.sww,Toast.LENGTH_LONG).show();
                });
    }

    private void loadItem(CartQuoteContent cartQuoteContent){
        if(cartQuoteContent!=null){
            if(cartQuoteContent.getCartTotal() == 0){
                view.showNoItem(true);
                view.showProgressBar(false);
                if(DoneApplication.getInstance()!=null){
                    DoneApplication.getInstance().setCartCount(cartQuoteContent.getCartTotal());
                }
            }else {
                if(DoneApplication.getInstance()!=null){
                    DoneApplication.getInstance().setCartCount(cartQuoteContent.getCartTotal());
                }
                view.showNoItem(false);

                List<Quoteitem> quoteItems = cartQuoteContent.getQuoteitems();
                List<Object> objects = new ArrayList<>();
                if (quoteItems != null) {
                    if (quoteItems.isEmpty()) {
                        view.setTotalPage(0);
                        view.showNoItem(true);
                    } else {
                        Total total = cartQuoteContent.getTotal();

                        view.setTotal(total.getFormattedGrandTotalExclTax());
                        view.setSubTotal(total.getFormattedSubtotalExclTax());
                        view.setTax(total.getFormattedTax());
                        if (total.getDiscount()!=null) {
                            view.setDiscount(total.getFormattedDiscount());
                        }

                        objects.addAll(quoteItems);

                        if(cartQuoteContent.getBestSell()!=null){
                            objects.add(cartQuoteContent.getBestSell());
                        }
                        if (total.getDiscount()!=null) {
                            view.grandTotalViewHideShow(total.getGrandTotalExclTax(), total.getTax(), total.getSubtotalExclTax(), total.getDiscount());
                        }else {
                            view.grandTotalViewHideShow(total.getGrandTotalExclTax(), total.getTax(), total.getSubtotalExclTax(),0.0);

                        }
                        cartTotalAmount = cartQuoteContent.getTotal().getGrandTotalExclTax();
                        view.setTotalPage(cartQuoteContent.getTotalPage());
                        view.addItemToProductAdapter(objects);
                    }
                    //Cart Item Notification
                    if(DoneApplication.getInstance()!=null && cartQuoteContent.getCartTotal() !=null) {
                        CartEvent cartEvent = new CartEvent(cartQuoteContent.getCartTotal());
                        bus.post(cartEvent);
                        DoneApplication.getInstance().setCartCount(cartQuoteContent.getCartTotal());
                    }
                } else {
                    view.setTotalPage(0);
                    view.showNoItem(true);
                }
                view.showProgressBar(false);
                view.showNoItem(false);
            }
        }
    }

    public void destroy(){
        compositeDisposable.dispose();
    }


    private Disposable getAddressBook(){
        view.showLoading(true);
        return model.getAddressBook()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(addressResponse -> {

                    List<BAddress> addresses = addressResponse.getAddresses();
                    List<Error> errorResponse = addressResponse.getErrors();
                    view.showLoading(false);

                    if(errorResponse!=null && !errorResponse.isEmpty()){
                        Error error = errorResponse.get(0);
                        Toast.makeText(view.getContext(),error.getMessage()+"",Toast.LENGTH_LONG).show();
                        view.showLoading(false);
                    }else if (addressResponse!=null){
                        if (addresses!=null && !addresses.isEmpty()){
                            view.navigateToCheckout(addresses.get(0).getEntityId());
                        }else {
                            view.navigateToAddressEntry();
                        }
                    }
                }, throwable -> {
                    view.showLoading(false);
                    view.showProgressBar(false);
                    throwable.printStackTrace();
                });
    }

    private void makeOrder(){
        doneDbAdapter.openReadableDb();
        String jsonString = doneDbAdapter.getStoreView();

        if(jsonString!=null) {
            Gson gson = new Gson();
            StoreViewResult configResponse = gson.fromJson(jsonString, StoreViewResult.class);
            if(configResponse!=null && configResponse.getStoreview()!=null) {
                List<Error> errorResponse = configResponse.getErrors();
                Base base = configResponse.getStoreview().getBase();
                if(errorResponse!=null && !errorResponse.isEmpty()){
                    Error error = errorResponse.get(0);
                    Toast.makeText(view.getContext(),error.getMessage()+"",Toast.LENGTH_LONG).show();
                }else if (base!=null){

                    if(!checkIfSatisfyMinimumOrderRule(base, cartTotalAmount)){
                        AlertUtils.createAlert(activity, R.string.ok, base.getMinOrderAmountMessage()).show();
                    }else{
                        proceedToCheckout = true;
                    }
                }else {
                    Toast.makeText(view.getContext(), R.string.sww,Toast.LENGTH_LONG).show();
                }
            }
        }

        doneDbAdapter.closeDb();
    }

    private boolean checkIfSatisfyMinimumOrderRule(Base base, double orderedAmount){
        // if min order amount == 1 is assumed minimum order rule is enabled.
        if(base.getMinOrderAmountEnable().equals("1")){
            String minimumOrderAmount = base.getMinOrderAmount();
            if(Double.parseDouble(minimumOrderAmount)>orderedAmount){
                return false;
            }
            return true;
        }else{
            return true;
        }
    }
}
