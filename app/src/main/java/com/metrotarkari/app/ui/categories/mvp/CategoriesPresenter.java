package com.metrotarkari.app.ui.categories.mvp;

import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.metrotarkari.app.R;
import com.metrotarkari.app.db.DoneDbAdapter;
import com.metrotarkari.app.model.Category;
import com.metrotarkari.app.model.ChildCategory;
import com.metrotarkari.app.model.Error;
import com.metrotarkari.app.model.StoreViewResult;
import com.metrotarkari.app.utils.DoneUserPreferenceManager;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.disposables.CompositeDisposable;

import static com.metrotarkari.app.utils.NetworkBroadcastReceiver.TAG;

/**
 * Created by samrakchan on 4/27/17.
 */

public class CategoriesPresenter {
    private final CategoriesView view;
    private final CategoriesModel model;
    private DoneDbAdapter doneDbAdapter;


    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private DoneUserPreferenceManager doneUserPreferenceManager;
    private EventBus bus = EventBus.getDefault();

    public CategoriesPresenter(CategoriesView view, CategoriesModel model){
        this.view = view;
        this.model = model;
        doneUserPreferenceManager = new DoneUserPreferenceManager(view.getContext());

        compositeDisposable.add(view.getSwifpeViewObservable().subscribe(o -> {
            getCategoriesData();
        }));


    }

    public void getCategoriesData(){
        view.showProgressBar(true);
        view.hideSwipeView();
        doneDbAdapter = new DoneDbAdapter(view.getContext());
        //determine how many items you want to pull category of home.
        //smaller device will pull 4 where as larger will pull more.

        //We are set for pulling 2 rows, if 2 item in column then its total 4 items
        //if 3 rows and 2 items per column then its 6 items in total

       // compositeDisposable.add(getHome(2*ViewUtils.calculateNoOfColumns(view.getContext())));

        doneDbAdapter.openReadableDb();
        String jsonString = doneDbAdapter.getStoreView();
        Log.i(TAG, "JSON "+jsonString);
        if(jsonString!=null){
            Gson gson = new Gson();
            StoreViewResult configResponse = gson.fromJson(jsonString, StoreViewResult.class);

            if(configResponse!=null && configResponse.getStoreview()!=null) {
                List<Error> errorResponse = configResponse.getErrors();
                List<Category> categories = configResponse.getStoreview().getCategories();

                if(errorResponse!=null && !errorResponse.isEmpty()){
                    Error error = errorResponse.get(0);
                    Toast.makeText(view.getContext(),error.getMessage()+"",Toast.LENGTH_LONG).show();
                }else if (categories!=null && !categories.isEmpty()){
                        view.showProgressBar(false);
                        view.showNoItem(false);
                        List<Object> objects  = new ArrayList<>();
                        for (Category c: categories){
                               objects.add(c);
                            if (c.getChildCategory()!=null) {
                                for (ChildCategory cC : c.getChildCategory()) {
                                    objects.add(cC);
                                }
                            }
                        }
                        view.setCategoriesData(objects);
                    }else {
                        Toast.makeText(view.getContext(), R.string.sww,Toast.LENGTH_LONG).show();
                        view.showProgressBar(false);
                        view.showNoItem(true);
                    }
            }
        }
        doneDbAdapter.closeDb();
    }

}
