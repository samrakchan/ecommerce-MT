package com.metrotarkari.app.ui.addressbook.addressentry.mvp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.metrotarkari.app.model.Region;

import java.util.List;

/**
 * Created by samrakchan on 6/14/17.
 */

public class AutoCompleteRegionAdapter extends ArrayAdapter<Region> {
    private LayoutInflater inflater;


    public AutoCompleteRegionAdapter(@NonNull Context context, List<Region> list) {
        super(context, android.R.layout.simple_dropdown_item_1line, list);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = inflater.inflate(android.R.layout.simple_dropdown_item_1line, parent);
        Region object = getItem(position);
        TextView textView = (TextView) view.findViewById(android.R.id.text1);
        textView.setText(object.getName()+"");
        textView.setTag(object);
        return view;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = inflater.inflate(android.R.layout.simple_dropdown_item_1line, parent);
        Region object = getItem(position);
        TextView textView = (TextView) view.findViewById(android.R.id.text1);
        textView.setText(object.getName()+"");
        textView.setTag(object);

        return view;
    }
}
