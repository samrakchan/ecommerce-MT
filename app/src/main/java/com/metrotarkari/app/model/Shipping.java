
package com.metrotarkari.app.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Shipping implements Parcelable {

    @SerializedName("s_method_id")
    @Expose
    private String sMethodId;
    @SerializedName("s_method_code")
    @Expose
    private String sMethodCode;
    @SerializedName("s_method_title")
    @Expose
    private String sMethodTitle;
    @SerializedName("s_method_fee")
    @Expose
    private Integer sMethodFee;
    @SerializedName("s_method_name")
    @Expose
    private String sMethodName;
    @SerializedName("s_method_selected")
    @Expose
    private Boolean sMethodSelected;
    @SerializedName("s_carrier_code")
    @Expose
    private String sCarrierCode;
    @SerializedName("s_carrier_title")
    @Expose
    private String sCarrierTitle;
    @SerializedName("s_options")
    @Expose
    private List<String> sOptions ;

    private Boolean isSelected = false;

    public final static Creator<Shipping> CREATOR = new Creator<Shipping>() {

        @SuppressWarnings({
                "unchecked"
        })
        public Shipping createFromParcel(Parcel in) {
            Shipping instance = new Shipping();
            instance.sMethodId = ((String) in.readValue((String.class.getClassLoader())));
            instance.sMethodCode = ((String) in.readValue((String.class.getClassLoader())));
            instance.sMethodTitle = ((String) in.readValue((String.class.getClassLoader())));
            instance.sMethodFee = ((Integer) in.readValue((Integer.class.getClassLoader())));
            instance.sMethodName = ((String) in.readValue((String.class.getClassLoader())));
            instance.sMethodSelected = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
            instance.sCarrierCode = ((String) in.readValue((String.class.getClassLoader())));
            instance.sCarrierTitle = ((String) in.readValue((String.class.getClassLoader())));
            instance.sOptions = (List<String>) in.readValue((String.class.getClassLoader()));
            instance.isSelected = (Boolean) in.readValue((Boolean.class.getClassLoader()));
            return instance;
        }

        public Shipping[] newArray(int size) {
            return (new Shipping[size]);
        }

    };

    public String getSMethodId() {
        return sMethodId;
    }

    public void setSMethodId(String sMethodId) {
        this.sMethodId = sMethodId;
    }

    public String getSMethodCode() {
        return sMethodCode;
    }

    public void setSMethodCode(String sMethodCode) {
        this.sMethodCode = sMethodCode;
    }

    public String getSMethodTitle() {
        return sMethodTitle;
    }

    public void setSMethodTitle(String sMethodTitle) {
        this.sMethodTitle = sMethodTitle;
    }

    public Integer getSMethodFee() {
        return sMethodFee;
    }

    public void setSMethodFee(Integer sMethodFee) {
        this.sMethodFee = sMethodFee;
    }

    public String getSMethodName() {
        return sMethodName;
    }

    public String getTimeTitle(int position){
        return sMethodName;
    }

    public void setSMethodName(String sMethodName) {
        this.sMethodName = sMethodName;
    }

    public Boolean getSMethodSelected() {
        return sMethodSelected;
    }

    public void setSMethodSelected(Boolean sMethodSelected) {
        this.sMethodSelected = sMethodSelected;
    }

    public String getSCarrierCode() {
        return sCarrierCode;
    }

    public void setSCarrierCode(String sCarrierCode) {
        this.sCarrierCode = sCarrierCode;
    }

    public String getSCarrierTitle() {
        return sCarrierTitle;
    }

    public void setSCarrierTitle(String sCarrierTitle) {
        this.sCarrierTitle = sCarrierTitle;
    }

    public List<String> getSOptions() {
        return sOptions;
    }

    public void setSOptions(List<String> sOptions) {
        this.sOptions = sOptions;
    }

    public Boolean getSelected() {
        return isSelected;
    }

    public void setSelected(Boolean selected) {
        isSelected = selected;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(sMethodId);
        dest.writeValue(sMethodCode);
        dest.writeValue(sMethodTitle);
        dest.writeValue(sMethodFee);
        dest.writeValue(sMethodName);
        dest.writeValue(sMethodSelected);
        dest.writeValue(sCarrierCode);
        dest.writeValue(sCarrierTitle);
        dest.writeValue(sOptions);
        dest.writeValue(isSelected);
    }
}
