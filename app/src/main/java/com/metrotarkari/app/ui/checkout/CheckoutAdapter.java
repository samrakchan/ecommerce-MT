package com.metrotarkari.app.ui.checkout;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.metrotarkari.app.R;
import com.metrotarkari.app.model.Address;
import com.metrotarkari.app.model.Payment;
import com.metrotarkari.app.model.Quoteitem;
import com.metrotarkari.app.model.Shipping;
import com.metrotarkari.app.model.Total;
import com.metrotarkari.app.ui.checkout.model.CouponCode;
import com.metrotarkari.app.ui.shippingdatetime.ShippingDateAndTimeActivity;
import com.metrotarkari.app.ui.message.MessageActivity;
import com.metrotarkari.app.utils.Constant;
import com.metrotarkari.app.utils.DoneItemClickListener;
import com.metrotarkari.app.utils.Utility;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by samrakchan on 3/21/17.
 */

public class CheckoutAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = CheckoutAdapter.class.getSimpleName();
    private List<Object> mData;
    private AppCompatActivity mActivity;
    private DoneItemClickListener listener;


//    private int mYear;
//    private int mMonth;
//    private int mDay;

    private String selectedDate = "";
    private Shipping selectedTime = null;
    private String message = "";

    private static final int CART_ITEMS_VIEW = 1;
    private static final int CART_TOTAL_VIEW = 3;
    private static final int ADDRESS_VIEW = 4;
    private static final int SHIPPING_TIME_VIEW = 5;
    private static final int PAYMENT_OPTION_VIEW = 6;
    private static final int CHECKOUT_MESSAGE_VIEW = 7;
    private static final int PAYMENT_METHOD_VIEW = 8;
    private static final int COUPEN_CODE_VIEW = 9;

    public CheckoutAdapter(AppCompatActivity context) {
        mData = new ArrayList<>();
        this.mActivity = context;

//        final Calendar c = Calendar.getInstance();
//        mYear = c.get(Calendar.YEAR);
//        mMonth= c.get(Calendar.MONTH);
//        mDay = c.get(Calendar.DAY_OF_MONTH);
    }

    public void addData(List<Object> datas) {
        this.mData.clear();
        this.mData.addAll(datas);
        notifyDataSetChanged();
    }

    public void addOnItemClickListener(DoneItemClickListener listener){
        this.listener = listener;
    }

    public void clear(){
        mData.clear();
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == CART_ITEMS_VIEW) {
            View view = LayoutInflater.from(mActivity).inflate(R.layout.adapter_cart_list, parent, false);
            return new CategoryItemList(view);
        }else if(viewType == CART_TOTAL_VIEW){
            View view = LayoutInflater.from(mActivity).inflate(R.layout.layout_cart_summary, parent, false);
            return new CartSummaryViewHolder(view);
        }else if(viewType == ADDRESS_VIEW){
            View view = LayoutInflater.from(mActivity).inflate(R.layout.layout_shipping_address, parent, false);
            return new AddressViewHolder(view);
        }else if(viewType == SHIPPING_TIME_VIEW){
            View view = LayoutInflater.from(mActivity).inflate(R.layout.layout_shipping_time, parent, false);
            return new ShippingDateTimeViewHolder(view);
        }else if (viewType == CHECKOUT_MESSAGE_VIEW){
            View view = LayoutInflater.from(mActivity).inflate(R.layout.adapter_checkout_message, parent, false);
            return new CheckoutMessageViewHolder(view);
        }else if (viewType == COUPEN_CODE_VIEW){
            View view = LayoutInflater.from(mActivity).inflate(R.layout.adapter_checkout_coupen_code, parent, false);
            return new CheckoutCoupenCodeViewHolder(view);
        } else if (viewType == PAYMENT_METHOD_VIEW){
            View view = LayoutInflater.from(mActivity).inflate(R.layout.adapter_checkout_payment_method, parent, false);
            return new CheckoutPaymentMethodViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Object object = mData.get(position);

        if(object instanceof Quoteitem && holder instanceof CategoryItemList) {

            CategoryItemList categoryItemListHolder = (CategoryItemList) holder;

            Quoteitem categoryItem = (Quoteitem) mData.get(position);
            categoryItemListHolder.listItemNameTv.setText(categoryItem.getName());
            categoryItemListHolder.listItemPriceTv.setText(categoryItem.getFormattedBaseRowTotal()+"");
            //categoryItemListHolder.listItemPriceTv.setText(mActivity.getString(R.string.price, Utility.convertToTwoDecimalPlace(categoryItem.getPrice()+"")));
            categoryItemListHolder.listItemQtyTv.setText(categoryItem.getQty() + "");
            Glide.with(mActivity)
                    .load(categoryItem.getImage())
                    .dontAnimate()
                    .into(categoryItemListHolder.listItemImageIv);
        }else if(object instanceof Total && holder instanceof CartSummaryViewHolder){
            CartSummaryViewHolder cartSummaryViewHolder = (CartSummaryViewHolder) holder;
            Total total = (Total) object;
            if(total!=null) {
                if (total.getTax() == 0.0) {
                    cartSummaryViewHolder.cartTaxLl.setVisibility(View.GONE);
                } else {
                    cartSummaryViewHolder.cartTaxLl.setVisibility(View.VISIBLE);
                }
                if (total.getFormattedDiscount() != null) {
                    cartSummaryViewHolder.cartDiscountLl.setVisibility(View.VISIBLE);
                    cartSummaryViewHolder.cartDiscountTv.setText(total.getFormattedDiscount());
                } else {
                    cartSummaryViewHolder.cartDiscountLl.setVisibility(View.GONE);
                }

                cartSummaryViewHolder.cartTotalTv.setText(total.getFormattedGrandTotalExclTax());
                cartSummaryViewHolder.cartTaxTv.setText(total.getFormattedTax());
                cartSummaryViewHolder.cartSubTotalTv.setText(total.getFormattedSubtotalExclTax());
            }
        }else if(object instanceof Address && holder instanceof AddressViewHolder){
            AddressViewHolder addressViewHolder = (AddressViewHolder) holder;
            Address address = (Address) object;
            addressViewHolder.addressTv.setText(getFormattedText(address.getFirstname())+" "+getFormattedText(address.getLastname())+"\n"+getFormattedText(address.getStreet())+", "+getFormattedText(address.getCity())+"\n"+getFormattedText(address.getTelephone()));
        }else if (object instanceof String && holder instanceof CheckoutMessageViewHolder ){
            CheckoutMessageViewHolder checkoutMessageViewHolder = (CheckoutMessageViewHolder)holder;
            checkoutMessageViewHolder.checkoutMesssageTv.setText(message);
        }else if (object instanceof Integer && holder instanceof CheckoutPaymentMethodViewHolder){
            CheckoutPaymentMethodViewHolder checkoutPaymentMethodViewHolder = (CheckoutPaymentMethodViewHolder)holder;
            checkoutPaymentMethodViewHolder.paymentTv.setText(R.string.cash_on_delivery);
        }else if (object instanceof CouponCode && holder instanceof CheckoutCoupenCodeViewHolder){
            CouponCode couponCodes = (CouponCode)object;
            CheckoutCoupenCodeViewHolder checkoutCoupenCodeViewHolder = (CheckoutCoupenCodeViewHolder)holder;

            if (couponCodes.getIsCoupenApplied()){
                checkoutCoupenCodeViewHolder.coupenCodeApplyBtn.setText(R.string.cancle);
                checkoutCoupenCodeViewHolder.coupenCodeApplyBtn.setTag("CANCEL");

                checkoutCoupenCodeViewHolder.coupenCodeEt.setText(couponCodes.getCoupenCode());
                checkoutCoupenCodeViewHolder.coupenCodeEt.setEnabled(false);
                checkoutCoupenCodeViewHolder.coupenCodeEt.getBackground().clearColorFilter();


            }else {
                checkoutCoupenCodeViewHolder.coupenCodeApplyBtn.setText(R.string.apply);
                checkoutCoupenCodeViewHolder.coupenCodeApplyBtn.setTag("APPLY");
                checkoutCoupenCodeViewHolder.coupenCodeEt.setEnabled(true);

                checkoutCoupenCodeViewHolder.coupenCodeEt.setText(couponCodes.getCoupenCode());
            }

        }else {
            ShippingDateTimeViewHolder deliveryDateTimeViewHolder = (ShippingDateTimeViewHolder) holder;
            if(selectedTime!=null) {
                deliveryDateTimeViewHolder.deliveryTimeEt.setText(selectedTime.getSMethodTitle());
            }
            deliveryDateTimeViewHolder.deliveryDateEt.setText(selectedDate);
        }
    }


    @Override
    public int getItemViewType(int position) {
        Object item = mData.get(position);
        if(item instanceof String){
            return CHECKOUT_MESSAGE_VIEW;
        }else if(item instanceof Quoteitem){
            return CART_ITEMS_VIEW;
        }else if(item instanceof Integer){
            return PAYMENT_METHOD_VIEW;
        }else if (item instanceof CouponCode){
            return COUPEN_CODE_VIEW;
        } else if(item instanceof Address){
            return ADDRESS_VIEW;
        }else if(item instanceof Payment){
            return PAYMENT_OPTION_VIEW;
        }else if(item instanceof Total){
            return CART_TOTAL_VIEW;
        }else{
            //Shipping
            return SHIPPING_TIME_VIEW;
        }
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    protected class AddressViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public AppCompatTextView addressTv;
        protected View view;

        public AddressViewHolder(View itemView) {
            super(itemView);
            addressTv = (AppCompatTextView) itemView.findViewById(R.id.addressTv);
            view =  itemView.findViewById(R.id.addressLl);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            listener.onItemClickListener(getAdapterPosition(), view.getId(), mData.get(getAdapterPosition()));
        }
    }

    protected class ShippingDateTimeViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public AppCompatTextView deliveryDateEt;
        public AppCompatTextView deliveryTimeEt;
        private View shippingTimeRootRl;

        public ShippingDateTimeViewHolder(View itemView) {
            super(itemView);
            deliveryDateEt = (AppCompatTextView) itemView.findViewById(R.id.deliveryDateEt);
            deliveryTimeEt = (AppCompatTextView) itemView.findViewById(R.id.deliveryTimeEt);
            shippingTimeRootRl = itemView.findViewById(R.id.shippingTimeRootRl);

            shippingTimeRootRl.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            //in position 2 we have expectally added Shipping object
            ArrayList<Shipping> checkoutShipping = (ArrayList<Shipping>) mData.get(getAdapterPosition());
            Intent intent = new Intent(mActivity, ShippingDateAndTimeActivity.class);
            intent.putParcelableArrayListExtra(Constant.KEY_VALUE_DATA_ID,checkoutShipping);
            mActivity.startActivityForResult(intent,Constant.CHANGE_DATE_AND_TIME);
        }
    }

    protected class CartSummaryViewHolder extends RecyclerView.ViewHolder{

        public AppCompatTextView cartTotalTv;
        public AppCompatTextView cartTaxTv;
        public AppCompatTextView cartSubTotalTv;
        public AppCompatTextView cartDiscountTv;
        public LinearLayout  cartDiscountLl;
        public LinearLayout cartTaxLl;

        public CartSummaryViewHolder(View itemView) {
            super(itemView);
            cartTotalTv = (AppCompatTextView) itemView.findViewById(R.id.cartTotalTv);
            cartTaxTv = (AppCompatTextView) itemView.findViewById(R.id.cartTaxTv);
            cartSubTotalTv = (AppCompatTextView) itemView.findViewById(R.id.cartSubTotalTv);
            cartDiscountTv = (AppCompatTextView) itemView.findViewById(R.id.cartDiscountTv);
            cartDiscountLl = (LinearLayout) itemView.findViewById(R.id.cartDiscountLl);
            cartTaxLl = (LinearLayout) itemView.findViewById(R.id.cartTaxLl);
        }
    }


    protected class CategoryItemList extends RecyclerView.ViewHolder {

        private AppCompatImageView listItemImageIv;
        private AppCompatImageView imageMinus;
        private AppCompatImageView imagePlus;
        private AppCompatTextView listItemPriceTv;
        private AppCompatTextView listItemNameTv;
        private AppCompatTextView listItemQtyTv;
        private AppCompatButton listItemDeleteIv;
        private View listItemDivider;

        public CategoryItemList(View itemView) {
            super(itemView);

            listItemImageIv = (AppCompatImageView) itemView.findViewById(R.id.listItemImageIv);
            imageMinus = (AppCompatImageView)itemView.findViewById(R.id.img_minus);
            imagePlus = (AppCompatImageView)itemView.findViewById(R.id.img_plus) ;
            listItemPriceTv = (AppCompatTextView) itemView.findViewById(R.id.listItemPriceTv);
            listItemNameTv = (AppCompatTextView) itemView.findViewById(R.id.listItemNameTv);
            listItemQtyTv = (AppCompatTextView) itemView.findViewById(R.id.txt_quantity);
            listItemDeleteIv = (AppCompatButton) itemView.findViewById(R.id.listItemDeleteIv);
            listItemDivider = (View) itemView.findViewById(R.id.listItemDivider);
            listItemDeleteIv.setVisibility(View.GONE);
            imageMinus.setVisibility(View.GONE);
            imagePlus.setVisibility(View.GONE);
            listItemDivider.setVisibility(View.GONE);
        }
    }

    protected class CheckoutMessageViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private AppCompatTextView checkoutMesssageTv;

        public CheckoutMessageViewHolder(View itemView) {
            super(itemView);

            checkoutMesssageTv = (AppCompatTextView) itemView.findViewById(R.id.checkoutMesssageTv);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(mActivity, MessageActivity.class);
            mActivity.startActivityForResult(intent,Constant.IS_FOR_SET_MESSAGE);
        }
    }


    protected class CheckoutCoupenCodeViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private AppCompatEditText coupenCodeEt;
        private AppCompatButton coupenCodeApplyBtn;

        public CheckoutCoupenCodeViewHolder(View itemView) {
            super(itemView);
            coupenCodeEt = (AppCompatEditText) itemView.findViewById(R.id.coupenCodeEt);
            coupenCodeApplyBtn = (AppCompatButton) itemView.findViewById(R.id.coupenCodeApplyBtn);

            coupenCodeApplyBtn.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position  = getAdapterPosition();
            if(mData.get(position) instanceof CouponCode){
                CouponCode couponCode = new CouponCode();
                //couponCode.setIsCoupenApplied(((CouponCode) mData.get(position)).getIsCoupenApplied());
                if(v.getTag()!=null && v.getTag().equals("CANCEL")){
                    couponCode.setCoupenCode("");
                }else{
                    couponCode.setCoupenCode(coupenCodeEt.getText().toString().trim());
                }

                listener.onItemClickListener(getAdapterPosition(),v.getId(),couponCode);
            }
            listener.onItemClickListener(getAdapterPosition(),v.getId(),null);
        }
    }


    protected class CheckoutPaymentMethodViewHolder extends RecyclerView.ViewHolder{
        private AppCompatTextView paymentTv;

        public CheckoutPaymentMethodViewHolder(View itemView) {
            super(itemView);

            paymentTv = (AppCompatTextView) itemView.findViewById(R.id.paymentTv);
        }
    }

    private String dateInString(int monthOfYear, int dayOfMonth,  int year){
        return (year+"-"+ Utility.getDoubleDigit(monthOfYear + 1)+"-"+ Utility.getDoubleDigit(dayOfMonth));
    }

    public String getAddressId(){
        for(Object obj : mData){
            if(obj instanceof Address){
                String id = ((Address)obj).getEntityId();
                return  id;
            }
        }
        return "";
    }


    public void setSelectedDate(String selectedDate){
        this.selectedDate = selectedDate;
    }

    public void setSelectedTime(Shipping selectedTime){
        this.selectedTime = selectedTime;
    }

    public void setMessage(String message){
        this.message = message;
    }

    public String getSelectedDate(){
        return selectedDate;
    }

    public Shipping getSelectedTime(){
        return selectedTime;
    }

    public String getMessage(){
        return  message;
    }

    private String getFormattedText(String text){
        return TextUtils.isEmpty(text)?"":text.trim();
    }
}
