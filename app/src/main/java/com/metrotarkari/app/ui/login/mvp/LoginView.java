package com.metrotarkari.app.ui.login.mvp;

import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;

import com.jakewharton.rxbinding2.view.RxView;
import com.metrotarkari.app.R;
import com.metrotarkari.app.ui.dashboard.MainActivity;
import com.metrotarkari.app.ui.login.LoginActivity2;
import com.metrotarkari.app.utils.Constant;

import butterknife.BindView;
import butterknife.ButterKnife;
import cc.cloudist.acplibrary.ACProgressFlower;
import io.reactivex.Observable;

/**
 * Created by samrakchan on 4/27/17.
 */

public class LoginView extends FrameLayout {

    @Nullable
    @BindView(R.id.input_email)
    AppCompatEditText input_email;

    @Nullable
    @BindView(R.id.input_password)
    AppCompatEditText input_password;

    @Nullable
    @BindView(R.id.loginLoginBtn)
    AppCompatButton loginLoginBtn;

    @Nullable
    @BindView(R.id.loginSkipTv)
    AppCompatTextView loginSkipTv;

    @Nullable
    @BindView(R.id.loginForgetPasswordTv)
    AppCompatTextView loginForgetPasswordTv;

    @Nullable
    @BindView(R.id.loginSignUpBtn)
    AppCompatButton loginSignUpBtn;

    @Nullable
    @BindView(R.id.loginCrossIv)
    View loginCrossIv;

   // private DoneUserPreferenceManager doneUserPreferenceManager;
    public int intentSource = -1;
    private LoginActivity2 activity;

    private final ACProgressFlower progressDialog = new ACProgressFlower.Builder(getContext())
            .themeColor(Color.WHITE)
            .fadeColor(Color.DKGRAY).build();

    public LoginView(LoginActivity2 activity){
        super(activity);
        this.activity = activity;

        Intent intent = activity.getIntent();
        intentSource = intent.getIntExtra(Constant.INTENT_FOR, -1);

        inflate(activity, R.layout.activity_login, this);
       // doneUserPreferenceManager = new DoneUserPreferenceManager(getContext());

        ButterKnife.bind(this);

        if((intentSource == Constant.CHECKOUT || intentSource == Constant.INTENT_FROM_DRAWER)){
            loginSkipTv.setVisibility(View.INVISIBLE);
            loginCrossIv.setVisibility(View.VISIBLE);
        }else{
            loginCrossIv.setVisibility(View.INVISIBLE);
        }
    }

    public Observable<Object> observeLoginButton() {
        return RxView.clicks(loginLoginBtn);
    }

    public Observable<Object> observeSkipTv(){
        return RxView.clicks(loginSkipTv);
    }

    public Observable<Object> observeForgetPasswordTv(){
        return RxView.clicks(loginForgetPasswordTv);
    }

    public Observable<Object> observeLoginCrossIv(){
        return RxView.clicks(loginCrossIv);
    }

    public Observable<Object> observeSignUpTv(){
        return RxView.clicks(loginSignUpBtn);
    }

    public EditText getEmailEt(){
        return input_email;
    }

    public EditText getPasswordEt(){
        return input_password;
    }

    public String getEmailText() {
        return input_email.getText().toString().trim();
    }

    public String getPasswordText(){
        return input_password.getText().toString();
    }

    public void showLoading(boolean loading){
        if(loading){
            progressDialog.setCancelable(false);
            progressDialog.show();
        }else{
            progressDialog.dismiss();
        }
    }

    public void navigateToTheDestination(){
        //doneUserPreferenceManager.setLoggedInUser(true);

        //checking intent call from checkout
        if(intentSource == Constant.CHECKOUT){
            //if intent result if from checkout go back to Checkout
            activity.setResult(activity.RESULT_OK);
            activity.finish();
        }else{
            //if intent not from checkout go to MainActivity(MainScreen)
            Intent intentMain = new Intent(getContext(), MainActivity.class);
            intentMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            getContext().startActivity(intentMain);
        }
    }
}
