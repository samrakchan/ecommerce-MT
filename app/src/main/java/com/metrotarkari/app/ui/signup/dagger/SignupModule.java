package com.metrotarkari.app.ui.signup.dagger;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import com.metrotarkari.app.application.network.DoneNetwork;
import com.metrotarkari.app.ui.signup.mvp.SignupModel;
import com.metrotarkari.app.ui.signup.mvp.SignupPresenter;
import com.metrotarkari.app.ui.signup.mvp.SignupView;
import com.metrotarkari.app.utils.Constant;

import dagger.Module;
import dagger.Provides;

@Module
public class SignupModule {

  private final AppCompatActivity activity;

  public SignupModule(AppCompatActivity activity) {
    this.activity = activity;
  }

  @Provides
  @SingnupScope
  public SignupView view() {
    return new SignupView(activity);
  }

  @Provides
  @SingnupScope
  public SignupModel model(DoneNetwork doneNetwork){
    return new SignupModel(activity, doneNetwork);
  }

  @Provides
  @SingnupScope
  public SignupPresenter signupPresenter(SignupView homeView, SignupModel model) {
    Intent intent = activity.getIntent();
    boolean isFromCheckOut = false;
    if (intent.getIntExtra(Constant.INTENT_FOR,-1) == Constant.CHECKOUT){
        isFromCheckOut = true;
    }
    return new SignupPresenter(homeView, model,isFromCheckOut);
  }

}
