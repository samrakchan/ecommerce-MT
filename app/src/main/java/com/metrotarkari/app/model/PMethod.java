
package com.metrotarkari.app.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PMethod implements Parcelable
{

    @SerializedName("method")
    @Expose
    private String method;
    public final static Creator<PMethod> CREATOR = new Creator<PMethod>() {


        @SuppressWarnings({
            "unchecked"
        })
        public PMethod createFromParcel(Parcel in) {
            PMethod instance = new PMethod();
            instance.method = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public PMethod[] newArray(int size) {
            return (new PMethod[size]);
        }

    }
    ;

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(method);
    }

    public int describeContents() {
        return  0;
    }

}
