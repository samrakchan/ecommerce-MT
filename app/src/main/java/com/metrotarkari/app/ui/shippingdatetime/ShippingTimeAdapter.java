package com.metrotarkari.app.ui.shippingdatetime;

import android.content.Context;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metrotarkari.app.R;
import com.metrotarkari.app.model.Shipping;
import com.metrotarkari.app.utils.DoneItemClickListener;

import java.util.ArrayList;
import java.util.List;


public class ShippingTimeAdapter extends RecyclerView.Adapter<ShippingTimeAdapter.TimeSelectionViewHolder>{

    private List<Shipping> mData;
    private Context mContext;
    private DoneItemClickListener listener;

    public ShippingTimeAdapter(Context context) {
        mData = new ArrayList<>();
        this.mContext = context;
    }

    public void addOnItemClickListener(DoneItemClickListener listener){
        this.listener = listener;
    }

    public void addData(List<Shipping> datas) {
        this.mData.addAll(datas);
        notifyDataSetChanged();
    }

    public void clearItem(){
        this.mData.clear();
        notifyDataSetChanged();
    }

    @Override
    public TimeSelectionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_time, parent, false);
        return new TimeSelectionViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TimeSelectionViewHolder holder, int position) {
        Shipping shipping = mData.get(position);
        holder.shippingTimeTitle.setText(shipping.getSMethodTitle());
        holder.shippingTimeMethod.setText(shipping.getSMethodName()+" ("+shipping.getSMethodFee()+")");
        holder.shippingTimeRb.setChecked(shipping.getSelected());
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    protected class TimeSelectionViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private AppCompatTextView shippingTimeTitle;
        private AppCompatTextView shippingTimeMethod;
        private AppCompatRadioButton shippingTimeRb;
        private View timeLl;


        public TimeSelectionViewHolder(View itemView) {
            super(itemView);
            shippingTimeTitle = (AppCompatTextView) itemView.findViewById(R.id.shippingTimeTitleTv);
            shippingTimeMethod = (AppCompatTextView)itemView.findViewById(R.id.shippingTimeMethod);
            shippingTimeRb = (AppCompatRadioButton) itemView.findViewById(R.id.shippingTimeRb);
            timeLl = itemView.findViewById(R.id.timeLl);
            timeLl.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            try {
                unSelectAll();
                mData.get(getAdapterPosition()).setSelected(true);
                listener.onItemClickListener(getAdapterPosition(), view.getId(), mData.get(getAdapterPosition()));
                notifyDataSetChanged();
            }catch (ArrayIndexOutOfBoundsException e){
                e.printStackTrace();
            }
        }
    }

    private void unSelectAll(){
        for(int i=0; i<mData.size(); i++){
            mData.get(i).setSelected(false);
        }
    }
}



