package com.metrotarkari.app.ui.categories.mvp;

import android.support.v4.app.Fragment;

import com.metrotarkari.app.application.network.DoneNetwork;

/**
 * Created by samrakchan on 4/27/17.
 */

public class CategoriesModel {
    private final Fragment fragment;
    private final DoneNetwork doneNetwork;

    public CategoriesModel(Fragment fragment, DoneNetwork doneNetwork){
        this.fragment = fragment;
        this.doneNetwork = doneNetwork;
    }

}
