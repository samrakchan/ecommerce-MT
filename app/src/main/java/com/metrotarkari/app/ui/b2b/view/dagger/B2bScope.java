package com.metrotarkari.app.ui.b2b.view.dagger;

import javax.inject.Scope;

@Scope
public @interface B2bScope {
}
