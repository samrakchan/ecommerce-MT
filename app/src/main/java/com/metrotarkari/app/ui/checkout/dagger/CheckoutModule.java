package com.metrotarkari.app.ui.checkout.dagger;

import android.support.v7.app.AppCompatActivity;

import com.metrotarkari.app.application.network.DoneNetwork;
import com.metrotarkari.app.db.DoneDbAdapter;
import com.metrotarkari.app.ui.checkout.CheckoutActivity;
import com.metrotarkari.app.ui.checkout.mvp.CheckoutModel;
import com.metrotarkari.app.ui.checkout.mvp.CheckoutPresenter;
import com.metrotarkari.app.ui.checkout.mvp.CheckoutView;

import dagger.Module;
import dagger.Provides;

@Module
public class CheckoutModule {

  private final CheckoutActivity activity;

  public CheckoutModule(CheckoutActivity activity) {
    this.activity = activity;
  }

  @Provides
  @CheckoutScope
  public CheckoutView view() {
    return new CheckoutView(activity);
  }

  @Provides
  @CheckoutScope
  public CheckoutModel model(DoneNetwork doneNetwork){
    return new CheckoutModel(doneNetwork);
  }

  @Provides
  @CheckoutScope
  public CheckoutPresenter presenter(CheckoutView homeView, CheckoutModel model, DoneDbAdapter dbAdapter) {
    return new CheckoutPresenter(homeView, model, activity, dbAdapter);
  }

}
