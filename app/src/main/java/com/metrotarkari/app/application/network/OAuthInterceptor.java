package com.metrotarkari.app.application.network;

import android.util.Log;

import com.google.gson.Gson;
import com.metrotarkari.app.utils.Constant;
import com.metrotarkari.app.utils.DoneTokenPreferenceManager;
import com.metrotarkari.app.utils.DoneUserPreferenceManager;

import java.io.IOException;
import java.util.Set;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

import static com.metrotarkari.app.utils.Constant.BASE_URL;

/**
 * Created by samrakchan on 7/18/17.
 */

public class OAuthInterceptor implements Interceptor {
    private static final String TAG = OAuthInterceptor.class.getSimpleName();

    private DoneUserPreferenceManager userPreferenceManager;
    private DoneTokenPreferenceManager doneTokenPreferenceManager;
    private static final String AUTH_HEADER = "Authorization";
    private static int logoutCount = 0;

    public OAuthInterceptor(DoneUserPreferenceManager userPreferenceManager, DoneTokenPreferenceManager doneTokenPreferenceManager){
        this.userPreferenceManager = userPreferenceManager;
        this.doneTokenPreferenceManager = doneTokenPreferenceManager;
    }

    private String getAppToken(){
        return Constant.APP_TOKEN;
    }

    //Perform action like logout.
    private void unauthorizedUser(){
        Log.i(TAG, TAG+" Logout");
    }

    private String getRefreshToken(){
        return doneTokenPreferenceManager.getRefreshToken();
    }

    @Override
    public Response intercept(Chain chain) throws IOException {



        Request.Builder requestBuilder = chain.request().newBuilder()
                .header("Content-Type", "application/json")
                .header(AUTH_HEADER, "Bearer "+getAppToken())
                .header("Cache-Control", "no-cache")
                //.header(AUTH_HEADER, doneTokenPreferenceManager.getTokenType() + " " + doneTokenPreferenceManager.getAccessToken())
                .header("Cookie", "mage-messages=%5B%5D; private_content_version=" + doneTokenPreferenceManager.getPrivateContentVersion() + "; PHPSESSID=" + doneTokenPreferenceManager.getPhpSessionId() + ";")
                ;

        if(!userPreferenceManager.getUsername().equals("") && !userPreferenceManager.getPassword().equals("")){
            requestBuilder.header("email", userPreferenceManager.getUsername());
            requestBuilder.header("password", userPreferenceManager.getPassword());
        }


        Request request = requestBuilder.build();

        final Response originalResponse = chain.proceed(request);
        return originalResponse;
    }
        /*
        long tokenExpiresIn = doneTokenPreferenceManager.getTokenExpireTimestamp();
        if(tokenExpiresIn>System.currentTimeMillis()){
            Log.i(TAG, TAG+" Time Expired");
            return refreshToken(request, chain);
        }else{
            Log.i(TAG, TAG+" Time Not Expired");
            if (originalResponse.code() == 200) {
                //if response is absolutely right
                Log.i(TAG, TAG+" Okay success");
                return originalResponse;
            } else if (originalResponse.code() == 401) {
                //if response is UnAuthorized
                Log.i(TAG, TAG+" 1. AUTH 401");
                //if response is UnAuthorized and there is no refreshToken.
                if (getRefreshToken().equalsIgnoreCase("")) {
                    if(!userPreferenceManager.isLoggedInUser()) {
                        Log.i(TAG, TAG+" 2. AUTH Not Logged in User 401");
                        return makeRequestForAuthAndVerifyResponse(request, chain);
                    }else{
                        Log.i(TAG, TAG+" 3. AUTH Logged in User 401");
                        unauthorizedUser();
                    }
                } else {
                    //case 401 when there is refresh token in preference
                    Log.i(TAG, TAG+" 4. AUTH yes refresh token 401");
                    return refreshToken(request, chain);
                }
            }
        }
        return originalResponse;
    }

    private Response refreshToken(Request request, Chain chain) throws IOException{
        Log.i(TAG, TAG+" 5. Refresh Token");
        String authHeader = doneTokenPreferenceManager.getTokenType() + " " + doneTokenPreferenceManager.getRefreshToken();
        final Request refreshTokenRequest = chain.request().newBuilder()
                .removeHeader(AUTH_HEADER)
                .header("Refresh", authHeader)
                .url(BASE_URL + "v2/token")
                .build();

        final Response response = chain.proceed(refreshTokenRequest);
        //save response, access token, refresh token and expire time.
        if (response.code() == 401) {
            Log.i(TAG, TAG+" 6. Refresh Token 401");
            if(!userPreferenceManager.isLoggedInUser()){
                return requestWithAuthorizationHeader(request, chain);
            }else {
                unauthorizedUser();
            }
        } else if (response.code() == 200) {
            Log.i(TAG, TAG+" 7. Refresh Token success 200");
            String jsonData = response.body().string();
            saveAuthResponse(jsonData);

            return requestWithAuthorizationHeader(request, chain);
        }
        return response;
    }

    private void saveAuthResponse(String jsonData){
        Log.i(TAG, TAG+" 9. Saving Token");
        Gson gson = new Gson();
        DoneAuthResponse doneAuthResponse = gson.fromJson(jsonData, DoneAuthResponse.class);
        if (doneAuthResponse != null && !doneAuthResponse.getAccessToken().equalsIgnoreCase("")) {
            doneTokenPreferenceManager.setDoneAuthResponse(doneAuthResponse);
        }
    }

    private Response requestWithAuthorizationHeader(Request request, Chain chain) throws IOException{
        String authHeader = doneTokenPreferenceManager.getTokenType() + " " + doneTokenPreferenceManager.getAccessToken();
        final Request accessTokenizeRequest = request.newBuilder()
                .header(AUTH_HEADER, authHeader)
                .build();

        Log.i(TAG, TAG+" 8. Request Auth Header");

        Log.i("AuthHeader", authHeader);

        return chain.proceed(accessTokenizeRequest);
    }

    private Response makeRequestForAuthAndVerifyResponse(Request request, Chain chain) throws IOException{
        Log.i(TAG, TAG+" 10. AuthAndVerify");

        final Request tokenizeRequest = chain.request().newBuilder()
                .header("token", getAppToken())
                .url(BASE_URL + "v2/auth")
                .build();

        final Response response = chain.proceed(tokenizeRequest);

        if (response.code() == 401) {
            //explectily unauthorizedUser
            Log.i(TAG, TAG+" 11. v2/auth Unauthorized Users 401");
            unauthorizedUser();
            return intercept(chain);

        } else if (response.code() == 200) {
            Log.i(TAG, TAG+" 12. v2/auth Authorized Users success");
            //save response, access token, refresh token and expire time.
            String jsonData = response.body().string();
            saveAuthResponse(jsonData);
            return requestWithAuthorizationHeader(request, chain);
        } else if (response.code() == 404) {
            //not found, server unreachable, do nothing.
            Log.i(TAG, TAG+" v2/auth Not Found 404");
            return response;
        } else {
            return response;
        }
    }

    private static int unAuthorizedCounter(){
        return logoutCount += 1;
    }

    */

}
