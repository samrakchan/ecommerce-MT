package com.metrotarkari.app.ui.base;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.FrameLayout;

import com.metrotarkari.app.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import cc.cloudist.acplibrary.ACProgressFlower;

/**
 * Created by samrakchan on 4/23/17.
 */

public abstract class BaseView extends FrameLayout {

    @Nullable
    @BindView(R.id.toolbar)
    public Toolbar toolbar;

    public final ACProgressFlower progressDialog = new ACProgressFlower.Builder(getContext())
            .themeColor(Color.WHITE)
            .fadeColor(Color.DKGRAY).build();


    public BaseView(@NonNull AppCompatActivity activity) {
        super(activity);
        inflate(getContext(), getContentLayout(), this);

        ButterKnife.bind(this);

        activity.setSupportActionBar(toolbar);

        ActionBar actionBar =activity.getSupportActionBar();
        if(actionBar!=null) {
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    public abstract int getContentLayout();

    public void showProgressDialog(){
        progressDialog.show();
    }

    public void hideProgressDialog(){
        progressDialog.dismiss();
    }


}
