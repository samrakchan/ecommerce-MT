package com.metrotarkari.app.ui.order.orderdetail.dragger;



import com.metrotarkari.app.application.network.DoneNetwork;
import com.metrotarkari.app.ui.order.orderdetail.OrderDetailActivity;
import com.metrotarkari.app.ui.order.orderdetail.mvp.OrderDetailModel;
import com.metrotarkari.app.ui.order.orderdetail.mvp.OrderDetailPresenter;
import com.metrotarkari.app.ui.order.orderdetail.mvp.OrderDetailView;

import dagger.Module;
import dagger.Provides;

@Module
public class OrderDetailModule {

  private final OrderDetailActivity activity;

  public OrderDetailModule(OrderDetailActivity activity) {
    this.activity = activity;
  }

  @Provides
  @OrderListScope
  public OrderDetailView view() {
    return new OrderDetailView(activity);
  }

  @Provides
  @OrderListScope
  public OrderDetailModel model(DoneNetwork doneNetwork){
    return new OrderDetailModel(activity, doneNetwork);
  }

  @Provides
  @OrderListScope
  public OrderDetailPresenter presenter(OrderDetailView homeView, OrderDetailModel model) {
      String orderId = activity.getIntent().getStringExtra("ORDERID");
    return new OrderDetailPresenter(homeView, model, (orderId==null)?"":orderId);
  }

}
