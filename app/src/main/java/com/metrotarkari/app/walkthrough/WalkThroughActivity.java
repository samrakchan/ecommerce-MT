package com.metrotarkari.app.walkthrough;

import android.animation.ArgbEvaluator;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.view.View;

import com.metrotarkari.app.R;
import com.metrotarkari.app.ui.dashboard.MainActivity;
import com.metrotarkari.app.ui.login.LoginActivity2;
import com.metrotarkari.app.utils.DoneUserPreferenceManager;

import me.relex.circleindicator.CircleIndicator;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by samrakchan on 5/9/17.
 */

public class WalkThroughActivity extends AppCompatActivity implements View.OnClickListener {

    private ViewPager mViewPager;
    private ViewPagerAdapter mViewPagerAdapter;
    ArgbEvaluator argbEvaluator = new ArgbEvaluator();
    private AppCompatButton walkThroughSkipBtn;
    private AppCompatButton walkThroughNextBtn;
    private DoneUserPreferenceManager userPreferenceManager;

    //Integer[] colors = null;


    //private static final Integer COLOR[] = {Color.MAGENTA, Color.YELLOW, Color.BLUE};
    private static final Integer COLOR[] = {Color.parseColor("#241D38"), Color.parseColor("#DBA409"), Color.parseColor("#026AD1")};

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_walkthrough);

        walkThroughSkipBtn = (AppCompatButton)findViewById(R.id.walkThroughSkipBtn);
        walkThroughNextBtn = (AppCompatButton)findViewById(R.id.walkThroughNextBtn);
        walkThroughSkipBtn.setOnClickListener(this);
        walkThroughNextBtn.setOnClickListener(this);

        mViewPager = (ViewPager) findViewById(R.id.viewPager);
        mViewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mViewPagerAdapter);

        userPreferenceManager = new DoneUserPreferenceManager(getApplicationContext());

        CircleIndicator indicator = (CircleIndicator) findViewById(R.id.indicator);
        mViewPager.setAdapter(mViewPagerAdapter);
        indicator.setViewPager(mViewPager);
        mViewPager.addOnPageChangeListener(new CustomOnPageChangeListener());
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.walkThroughNextBtn){
            if(mViewPager.getCurrentItem() == COLOR.length-1){
                startLoginActivity();
            }else{
                mViewPager.setCurrentItem(mViewPager.getCurrentItem()+1);
            }
        }else if(view.getId() == R.id.walkThroughSkipBtn){
            startLoginActivity();
        }
    }

    private void  startLoginActivity(){
        userPreferenceManager.setWalkThroughPreference(false);
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private static class ViewPagerAdapter extends FragmentPagerAdapter {

        public ViewPagerAdapter(final FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(final int position) {
            return PageFragment.newInstance(position);
        }

        @Override
        public int getCount() {
            return COLOR.length;
        }
    }

    private class CustomOnPageChangeListener implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            if(position < (mViewPagerAdapter.getCount() -1) && position < (COLOR.length - 1)) {
                mViewPager.setBackgroundColor((Integer) argbEvaluator.evaluate(positionOffset, COLOR[position], COLOR[position+1]));
            } else {
                // the last page color
               // mViewPager.setBackgroundColor(colors[colors.length - 1]);
                mViewPager.setBackgroundColor(COLOR[position]);
            }
        }

        @Override
        public void onPageSelected(int position) {
        }

        @Override
        public void onPageScrollStateChanged(int i) {
        }

    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
