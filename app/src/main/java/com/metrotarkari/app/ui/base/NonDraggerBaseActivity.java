package com.metrotarkari.app.ui.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;

import com.metrotarkari.app.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by samrakchan on 6/6/17.
 */

public abstract class NonDraggerBaseActivity extends BaseActivity {

    @Nullable
    @BindView(R.id.toolbar)
    public Toolbar toolbar;

    private Unbinder unbinder;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getContentLayout());

        unbinder = ButterKnife.bind(this);
        initializeToolbar();
    }


    protected void initializeToolbar(){
        if(toolbar!=null){
            setSupportActionBar(toolbar);
            if(getToolbarTitle()!=0) {
                getSupportActionBar().setTitle(getToolbarTitle());
            }
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

    }


    @Override
    protected void onDestroy() {
        unbinder.unbind();
        super.onDestroy();
    }

    protected abstract int getToolbarTitle();

    protected abstract int getContentLayout();
}
