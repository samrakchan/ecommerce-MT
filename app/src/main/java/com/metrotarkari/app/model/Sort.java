package com.metrotarkari.app.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by samrakchan on 6/6/17.
 */

public class Sort implements Parcelable{
    @SerializedName("key")
    @Expose
    private String key;
    @SerializedName("value")
    @Expose
    private String value;
    @SerializedName("direction")
    @Expose
    private String direction;

    //default is selected item.
    @SerializedName("default")
    @Expose
    private String _default;

    public final static Parcelable.Creator<Sort> CREATOR = new Creator<Sort>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Sort createFromParcel(Parcel in) {
            Sort instance = new Sort();
            instance.key = ((String) in.readValue((String.class.getClassLoader())));
            instance.value = ((String) in.readValue((String.class.getClassLoader())));
            instance.direction = ((String) in.readValue((String.class.getClassLoader())));
            instance._default = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public Sort[] newArray(int size) {
            return (new Sort[size]);
        }

    }
            ;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getDefault() {
        return _default;
    }

    public void setDefault(String _default) {
        this._default = _default;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(key);
        dest.writeValue(value);
        dest.writeValue(direction);
        dest.writeValue(_default);
    }

    public int describeContents() {
        return 0;
    }
}
