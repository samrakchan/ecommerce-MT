package com.metrotarkari.app.ui.categories.dagger;

import javax.inject.Scope;

@Scope
public @interface CategoriesScope {
}
