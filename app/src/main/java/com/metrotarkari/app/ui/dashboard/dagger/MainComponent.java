package com.metrotarkari.app.ui.dashboard.dagger;

import com.metrotarkari.app.application.dagger.AppComponent;
import com.metrotarkari.app.ui.dashboard.MainActivity;

import dagger.Component;

@MainScope
@Component(modules = { MainModule.class }, dependencies = AppComponent.class)
public interface MainComponent {

  void inject(MainActivity mainActivity);

}
