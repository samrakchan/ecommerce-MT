package com.metrotarkari.app.ui.forgetpassword.mvp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.widget.Toast;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.metrotarkari.app.R;
import com.metrotarkari.app.model.Customer;
import com.metrotarkari.app.model.Error;
import com.metrotarkari.app.ui.login.LoginActivity2;
import com.metrotarkari.app.utils.AlertUtils;
import com.metrotarkari.app.utils.Constant;
import com.metrotarkari.app.utils.MyToast;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.basgeekball.awesomevalidation.ValidationStyle.UNDERLABEL;

/**
 * Created by samrakchan on 4/30/17.
 */

public class ForgetPasswordPresenter {

    private final ForgetPasswordView view;
    private final ForgetPasswordModel model;
    private final AppCompatActivity activity;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    private AwesomeValidation awesomeValidation = new AwesomeValidation(UNDERLABEL);

    public ForgetPasswordPresenter(AppCompatActivity activity, ForgetPasswordView view, ForgetPasswordModel model){
        this.view = view;
        this.model = model;
        this.activity = activity;

        view.observeCrossButton().subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(aVoid->{
                    view.finish();
                });
    }

    public void verifyIdToken(HashMap<String, String> idTokenMap){
        view.showPasswordEntryForm(false);
        view.showForgotPasswordForm(false);

        Disposable disposable = verifyCustomerToken(idTokenMap);
        compositeDisposable.add(disposable);
    }

    public void initForgetPassword() {
        view.showPasswordEntryForm(false);
        view.showForgotPasswordForm(true);
        this.awesomeValidation.setContext(view.getContext());
        this.awesomeValidation.addValidation(view.getEmailEt(), Patterns.EMAIL_ADDRESS, view.getContext().getString(R.string.error_email_address));
        view.observeForgetPasswordButton().throttleFirst(5, TimeUnit.SECONDS)
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(aVoid -> {
                    if(awesomeValidation.validate()) {
                        Disposable disposable = sendMailForGetPassword(view.getEmailText());
                        compositeDisposable.add(disposable);
                    }
                });

    }

    public void initResetMyPassword(HashMap<String, String> idToken) {
        view.showForgotPasswordForm(false);
        view.showPasswordEntryForm(true);

        this.awesomeValidation.setContext(view.getContext());
        this.awesomeValidation.addValidation(view.getResetPasswordEt(), Constant.PASSWORD_VALIDATION_REG_EXP, view.getContext().getString(R.string.error_password_length));
        this.awesomeValidation.addValidation(view.getResetConfirmPasswordEt(),view.getResetPasswordEt(),view.getContext().getString(R.string.error_password_length));

        view.observeResentPasswordButton().throttleFirst(5, TimeUnit.SECONDS)
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(aVoid -> {
                    if(awesomeValidation.validate()) {
                        HashMap<String, String> hashMap1 = new HashMap<>();
                        hashMap1.put("new_password", view.getResetPassword());
                        hashMap1.put("com_password", view.getResetConfirmPassword());
                        Disposable disposable = changePassword(idToken, hashMap1);
                        compositeDisposable.add(disposable);
                    }
                });

        view.observeCrossButton().subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(aVoid->{
                    view.finish();
                });
    }

    public void onDestroy(){
        if (compositeDisposable!=null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    private Disposable verifyCustomerToken(final HashMap<String, String> hashMap){
        view.showProgressDialogue(true);
        return model.verifyCustomerToken(hashMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(customer_ -> {
                    view.showProgressDialogue(false);
                    if(customer_!=null) {
                        List<Error> errorResponse = customer_.getErrors();
                        if (errorResponse != null && !errorResponse.isEmpty()) {
                            Error error = errorResponse.get(0);
                            Toast.makeText(view.getContext(), error.getMessage() + "", Toast.LENGTH_LONG).show();
                        } else if (customer_.getCustomer() != null) {
                            Customer customer = customer_.getCustomer();
                            if (customer.getEntityId() != null) {
                                initResetMyPassword(hashMap);
                            } else {
                                if (customer_.getMessage() != null) {
                                    AlertUtils.createAlert(activity, R.string.ok, "" + customer_.getMessage().get(0)).show();
                                } else {
                                    MyToast.displayToast(view.getContext(), R.string.sww, Toast.LENGTH_LONG);
                                }
                            }
                        }
                    }
                }, throwable -> {
                    view.showProgressDialogue(false);
                });
    }

    private Disposable changePassword(HashMap<String, String> idToken, HashMap<String, String> newPassword){
        view.showProgressDialogue(true);
        return model.changePassword(idToken, newPassword)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(customer_ -> {
                    view.showProgressDialogue(false);

                    if(customer_!=null) {
                        List<Error> errorResponse = customer_.getErrors();
                        if (errorResponse != null && !errorResponse.isEmpty()) {
                            Error error = errorResponse.get(0);
                            Toast.makeText(view.getContext(), error.getMessage() + "", Toast.LENGTH_LONG).show();
                        }else if (customer_.getCustomer() != null) {
                            Customer customer = customer_.getCustomer();
                            if (customer.getEntityId() != null) {
                                Intent intent = new Intent(activity, LoginActivity2.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                AlertUtils.createAlert(activity, R.string.ok, customer_.getMessage().get(0)+" "+view.getContext().getString(R.string.successfully_reset_password), intent).show();

                            } else {
                                AlertUtils.createAlert(activity, R.string.ok, view.getContext().getString(R.string.user_not_found)).show();
                            }
                        }else{
                            Toast.makeText(view.getContext(), "Hello ", Toast.LENGTH_LONG).show();
                        }
                    }else{
                        Toast.makeText(view.getContext(), "Hello 2 ", Toast.LENGTH_LONG).show();
                    }
                }, throwable -> {
                    view.showProgressDialogue(false);
                    MyToast.displayToast(view.getContext(), R.string.sww, Toast.LENGTH_LONG);
                });
    }

    private Disposable sendMailForGetPassword(String email){
        view.showProgressDialogue(true);
        return model.sendMailForGetPassword(email)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(forgetPasswordResponse -> {
                    view.showProgressDialogue(false);
                    List<String> message = forgetPasswordResponse.getMessage();
                    List<Error> errorResponse = forgetPasswordResponse.getErrors();
                    if(errorResponse!=null && !errorResponse.isEmpty()){
                        Error error = errorResponse.get(0);
                        Toast.makeText(view.getContext(),error.getMessage()+"",Toast.LENGTH_LONG).show();
                    }else if(message!=null && !message.isEmpty()){
                        String messages = message.get(0);
                        view.getEmailEt().setText("");
                        view.onForgetPasswordSuccess(messages);
                    }else{
                        Toast.makeText(view.getContext(), R.string.sww, Toast.LENGTH_LONG).show();
                    }
                }, throwable -> {
                    view.showProgressDialogue(false);
                    Toast.makeText(view.getContext(),R.string.sww,Toast.LENGTH_LONG).show();
                    throwable.printStackTrace();
                });

    }

}
