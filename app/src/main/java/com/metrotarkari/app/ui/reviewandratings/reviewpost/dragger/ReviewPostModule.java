package com.metrotarkari.app.ui.reviewandratings.reviewpost.dragger;

import android.support.v7.app.AppCompatActivity;

import com.metrotarkari.app.application.network.DoneNetwork;
import com.metrotarkari.app.ui.reviewandratings.reviewpost.mvp.ReviewPostModel;
import com.metrotarkari.app.ui.reviewandratings.reviewpost.mvp.ReviewPostPresenter;
import com.metrotarkari.app.ui.reviewandratings.reviewpost.mvp.ReviewPostView;

import dagger.Module;
import dagger.Provides;

@Module
public class ReviewPostModule {

  private final AppCompatActivity activity;

  public ReviewPostModule(AppCompatActivity activity) {
    this.activity = activity;
  }

  @Provides
  @ReviewPostScope
  public ReviewPostView view() {
    return new ReviewPostView(activity);
  }

  @Provides
  @ReviewPostScope
  public ReviewPostModel model(DoneNetwork doneNetwork){
    return new ReviewPostModel(activity, doneNetwork);
  }

  @Provides
  @ReviewPostScope
  public ReviewPostPresenter presenter(ReviewPostView view, ReviewPostModel model) {
    return new ReviewPostPresenter(view, model);
  }

}
