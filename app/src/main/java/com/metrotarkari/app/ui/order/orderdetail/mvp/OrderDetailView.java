package com.metrotarkari.app.ui.order.orderdetail.mvp;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ExpandableListView;

import com.metrotarkari.app.R;
import com.metrotarkari.app.ui.base.BaseView;
import com.metrotarkari.app.ui.order.orderdetail.OrderDetailAdapter;
import com.metrotarkari.app.ui.order.orderdetail.model.Order;
import com.metrotarkari.app.ui.order.orderdetail.model.OrderHeader;
import com.metrotarkari.app.utils.Utility;
import com.paginate.Paginate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;

/**
 * Created by Avinash on 6/7/2017.
 */

public class OrderDetailView extends BaseView implements Paginate.Callbacks,ExpandableListView.OnGroupExpandListener {

    @BindView(R.id.orderItemDetailExpandableListView)
    public ExpandableListView orderItemDetailExpandableListView;

    @BindView(R.id.appBarLayout)
    public AppBarLayout appBarLayout;

    @BindView(R.id.toolbar)
    public Toolbar toolbar;

    @BindView(R.id.imageMsgNoItemTv)
    public AppCompatTextView imageMsgNoItemTv;

    private OrderDetailAdapter orderDetailAdapter;

    private AppCompatActivity appCompatActivity;

    private Paginate paginate;

    @BindView(R.id.progressBarRl)
    public View progressBar;

    @BindView(R.id.imageMsgRl)
    public View noItemView;

    private int width;

    List<Order> orderItemListHeader;


    public OrderDetailView(@NonNull AppCompatActivity activity) {
        super(activity);
        this.appCompatActivity = activity;

        if (paginate != null) {
            paginate.unbind();
        }

        appCompatActivity.setSupportActionBar(toolbar);
        appCompatActivity.getSupportActionBar().setTitle(R.string.order_detail);
        orderItemListHeader = new ArrayList<Order>();
        orderDetailAdapter = new OrderDetailAdapter(getContext());
        orderItemDetailExpandableListView.setAdapter(orderDetailAdapter);

        DisplayMetrics metrics = new DisplayMetrics();
        ((Activity) getContext()).getWindowManager().getDefaultDisplay().getMetrics(metrics);
        width = metrics.widthPixels;
        orderItemDetailExpandableListView.setIndicatorBounds(width - Utility.GetDipsFromPixel(50,getContext()), width - Utility.GetDipsFromPixel(10,getContext()));
    }

    @Override
    public int getContentLayout() {
        return R.layout.activity_order_item_detail;
    }

    @Override
    public void onLoadMore() {

    }

    @Override
    public boolean isLoading() {
        return false;
    }

    @Override
    public boolean hasLoadedAllItems() {
        return false;
    }

    @Override
    public void onGroupExpand(int groupPosition) {
      /*  Toast.makeText(getContext(),orderItemListHeader
                .get(groupPosition) + " List Expanded.",
                Toast.LENGTH_SHORT).show();*/
    }

    public void addItemToOrderDetailAdapter(List<OrderHeader> orderHeaders, HashMap<OrderHeader,Object> orderItemListDetail){
        orderDetailAdapter.addItems(orderHeaders,orderItemListDetail);
        orderItemDetailExpandableListView.expandGroup(0);
    }


    public void showNoItem(boolean showItem){
        if(showItem) {
            noItemView.setVisibility(View.VISIBLE);
            imageMsgNoItemTv.setText(R.string.no_order_detail_found);
        }
        else {
            noItemView.setVisibility(View.GONE);
        }
    }

    public void showProgressBar(boolean showing){
        if(showing)
            progressBar.setVisibility(View.VISIBLE);
        else
            progressBar.setVisibility(View.GONE);
    }


}
