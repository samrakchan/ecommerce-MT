package com.metrotarkari.app.ui.shippingdatetime.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Avinash on 7/31/2017.
 */

public class SOption implements Parcelable{
    private String stringOptionValue;

    public final static Creator<SOption> CREATOR = new Creator<SOption>() {


        @SuppressWarnings({
                "unchecked"
        })
        public SOption createFromParcel(Parcel in) {
            SOption instance = new SOption();
            instance.stringOptionValue = ((String) in.readValue((String.class.getClassLoader())));

            return instance;
        }

        public SOption[] newArray(int size) {
            return (new SOption[size]);
        }

    };



    public String getStringOptionValue() {
        return stringOptionValue;
    }

    public void setStringOptionValue(String stringOptionValue) {
        this.stringOptionValue = stringOptionValue;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(stringOptionValue);

    }

    public int describeContents() {
        return  0;
    }
}
