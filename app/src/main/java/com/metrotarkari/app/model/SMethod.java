
package com.metrotarkari.app.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SMethod implements Parcelable
{

    @SerializedName("method")
    @Expose
    private String method;
    public final static Creator<SMethod> CREATOR = new Creator<SMethod>() {


        @SuppressWarnings({
            "unchecked"
        })
        public SMethod createFromParcel(Parcel in) {
            SMethod instance = new SMethod();
            instance.method = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public SMethod[] newArray(int size) {
            return (new SMethod[size]);
        }

    }
    ;

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(method);
    }

    public int describeContents() {
        return  0;
    }

}
