
package com.metrotarkari.app.ui.order.orderdetail.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderItem {

    @SerializedName("option")
    @Expose
    private List<Object> option = null;
    @SerializedName("item_id")
    @Expose
    private String itemId;
    @SerializedName("order_id")
    @Expose
    private String orderId;
    @SerializedName("parent_item_id")
    @Expose
    private Object parentItemId;
    @SerializedName("quote_item_id")
    @Expose
    private String quoteItemId;
    @SerializedName("store_id")
    @Expose
    private String storeId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("product_type")
    @Expose
    private String productType;
    @SerializedName("product_options")
    @Expose
    private ProductOptions productOptions;
    @SerializedName("weight")
    @Expose
    private Object weight;
    @SerializedName("is_virtual")
    @Expose
    private String isVirtual;
    @SerializedName("sku")
    @Expose
    private String sku;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private Object description;
    @SerializedName("applied_rule_ids")
    @Expose
    private Object appliedRuleIds;
    @SerializedName("additional_data")
    @Expose
    private Object additionalData;
    @SerializedName("is_qty_decimal")
    @Expose
    private String isQtyDecimal;
    @SerializedName("no_discount")
    @Expose
    private String noDiscount;
    @SerializedName("qty_backordered")
    @Expose
    private Object qtyBackordered;
    @SerializedName("qty_canceled")
    @Expose
    private String qtyCanceled;
    @SerializedName("qty_invoiced")
    @Expose
    private String qtyInvoiced;
    @SerializedName("qty_ordered")
    @Expose
    private String qtyOrdered;
    @SerializedName("qty_refunded")
    @Expose
    private String qtyRefunded;
    @SerializedName("qty_shipped")
    @Expose
    private String qtyShipped;
    @SerializedName("base_cost")
    @Expose
    private Object baseCost;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("base_price")
    @Expose
    private String basePrice;
    @SerializedName("original_price")
    @Expose
    private String originalPrice;
    @SerializedName("base_original_price")
    @Expose
    private Object baseOriginalPrice;
    @SerializedName("tax_percent")
    @Expose
    private String taxPercent;
    @SerializedName("tax_amount")
    @Expose
    private String taxAmount;
    @SerializedName("base_tax_amount")
    @Expose
    private String baseTaxAmount;
    @SerializedName("tax_invoiced")
    @Expose
    private String taxInvoiced;
    @SerializedName("base_tax_invoiced")
    @Expose
    private String baseTaxInvoiced;
    @SerializedName("discount_percent")
    @Expose
    private String discountPercent;
    @SerializedName("discount_amount")
    @Expose
    private String discountAmount;
    @SerializedName("base_discount_amount")
    @Expose
    private String baseDiscountAmount;
    @SerializedName("discount_invoiced")
    @Expose
    private String discountInvoiced;
    @SerializedName("base_discount_invoiced")
    @Expose
    private String baseDiscountInvoiced;
    @SerializedName("amount_refunded")
    @Expose
    private String amountRefunded;
    @SerializedName("base_amount_refunded")
    @Expose
    private String baseAmountRefunded;
    @SerializedName("row_total")
    @Expose
    private String rowTotal;
    @SerializedName("base_row_total")
    @Expose
    private String baseRowTotal;
    @SerializedName("row_invoiced")
    @Expose
    private String rowInvoiced;
    @SerializedName("base_row_invoiced")
    @Expose
    private String baseRowInvoiced;
    @SerializedName("row_weight")
    @Expose
    private String rowWeight;
    @SerializedName("base_tax_before_discount")
    @Expose
    private Object baseTaxBeforeDiscount;
    @SerializedName("tax_before_discount")
    @Expose
    private Object taxBeforeDiscount;
    @SerializedName("ext_order_item_id")
    @Expose
    private Object extOrderItemId;
    @SerializedName("locked_do_invoice")
    @Expose
    private Object lockedDoInvoice;
    @SerializedName("locked_do_ship")
    @Expose
    private Object lockedDoShip;
    @SerializedName("price_incl_tax")
    @Expose
    private String priceInclTax;
    @SerializedName("base_price_incl_tax")
    @Expose
    private String basePriceInclTax;
    @SerializedName("row_total_incl_tax")
    @Expose
    private String rowTotalInclTax;
    @SerializedName("base_row_total_incl_tax")
    @Expose
    private String baseRowTotalInclTax;
    @SerializedName("discount_tax_compensation_amount")
    @Expose
    private String discountTaxCompensationAmount;
    @SerializedName("base_discount_tax_compensation_amount")
    @Expose
    private String baseDiscountTaxCompensationAmount;
    @SerializedName("discount_tax_compensation_invoiced")
    @Expose
    private Object discountTaxCompensationInvoiced;
    @SerializedName("base_discount_tax_compensation_invoiced")
    @Expose
    private Object baseDiscountTaxCompensationInvoiced;
    @SerializedName("discount_tax_compensation_refunded")
    @Expose
    private Object discountTaxCompensationRefunded;
    @SerializedName("base_discount_tax_compensation_refunded")
    @Expose
    private Object baseDiscountTaxCompensationRefunded;
    @SerializedName("tax_canceled")
    @Expose
    private Object taxCanceled;
    @SerializedName("discount_tax_compensation_canceled")
    @Expose
    private Object discountTaxCompensationCanceled;
    @SerializedName("tax_refunded")
    @Expose
    private Object taxRefunded;
    @SerializedName("base_tax_refunded")
    @Expose
    private Object baseTaxRefunded;
    @SerializedName("discount_refunded")
    @Expose
    private Object discountRefunded;
    @SerializedName("base_discount_refunded")
    @Expose
    private Object baseDiscountRefunded;
    @SerializedName("free_shipping")
    @Expose
    private String freeShipping;
    @SerializedName("gift_message_id")
    @Expose
    private Object giftMessageId;
    @SerializedName("gift_message_available")
    @Expose
    private Object giftMessageAvailable;
    @SerializedName("weee_tax_applied")
    @Expose
    private Object weeeTaxApplied;
    @SerializedName("weee_tax_applied_amount")
    @Expose
    private Object weeeTaxAppliedAmount;
    @SerializedName("weee_tax_applied_row_amount")
    @Expose
    private Object weeeTaxAppliedRowAmount;
    @SerializedName("weee_tax_disposition")
    @Expose
    private Object weeeTaxDisposition;
    @SerializedName("weee_tax_row_disposition")
    @Expose
    private Object weeeTaxRowDisposition;
    @SerializedName("base_weee_tax_applied_amount")
    @Expose
    private Object baseWeeeTaxAppliedAmount;
    @SerializedName("base_weee_tax_applied_row_amnt")
    @Expose
    private Object baseWeeeTaxAppliedRowAmnt;
    @SerializedName("base_weee_tax_disposition")
    @Expose
    private Object baseWeeeTaxDisposition;
    @SerializedName("base_weee_tax_row_disposition")
    @Expose
    private Object baseWeeeTaxRowDisposition;
    @SerializedName("image")
    @Expose
    private String image;

    public List<Object> getOption() {
        return option;
    }

    public void setOption(List<Object> option) {
        this.option = option;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Object getParentItemId() {
        return parentItemId;
    }

    public void setParentItemId(Object parentItemId) {
        this.parentItemId = parentItemId;
    }

    public String getQuoteItemId() {
        return quoteItemId;
    }

    public void setQuoteItemId(String quoteItemId) {
        this.quoteItemId = quoteItemId;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public ProductOptions getProductOptions() {
        return productOptions;
    }

    public void setProductOptions(ProductOptions productOptions) {
        this.productOptions = productOptions;
    }

    public Object getWeight() {
        return weight;
    }

    public void setWeight(Object weight) {
        this.weight = weight;
    }

    public String getIsVirtual() {
        return isVirtual;
    }

    public void setIsVirtual(String isVirtual) {
        this.isVirtual = isVirtual;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getDescription() {
        return description;
    }

    public void setDescription(Object description) {
        this.description = description;
    }

    public Object getAppliedRuleIds() {
        return appliedRuleIds;
    }

    public void setAppliedRuleIds(Object appliedRuleIds) {
        this.appliedRuleIds = appliedRuleIds;
    }

    public Object getAdditionalData() {
        return additionalData;
    }

    public void setAdditionalData(Object additionalData) {
        this.additionalData = additionalData;
    }

    public String getIsQtyDecimal() {
        return isQtyDecimal;
    }

    public void setIsQtyDecimal(String isQtyDecimal) {
        this.isQtyDecimal = isQtyDecimal;
    }

    public String getNoDiscount() {
        return noDiscount;
    }

    public void setNoDiscount(String noDiscount) {
        this.noDiscount = noDiscount;
    }

    public Object getQtyBackordered() {
        return qtyBackordered;
    }

    public void setQtyBackordered(Object qtyBackordered) {
        this.qtyBackordered = qtyBackordered;
    }

    public String getQtyCanceled() {
        return qtyCanceled;
    }

    public void setQtyCanceled(String qtyCanceled) {
        this.qtyCanceled = qtyCanceled;
    }

    public String getQtyInvoiced() {
        return qtyInvoiced;
    }

    public void setQtyInvoiced(String qtyInvoiced) {
        this.qtyInvoiced = qtyInvoiced;
    }

    public String getQtyOrdered() {
        return qtyOrdered;
    }

    public void setQtyOrdered(String qtyOrdered) {
        this.qtyOrdered = qtyOrdered;
    }

    public String getQtyRefunded() {
        return qtyRefunded;
    }

    public void setQtyRefunded(String qtyRefunded) {
        this.qtyRefunded = qtyRefunded;
    }

    public String getQtyShipped() {
        return qtyShipped;
    }

    public void setQtyShipped(String qtyShipped) {
        this.qtyShipped = qtyShipped;
    }

    public Object getBaseCost() {
        return baseCost;
    }

    public void setBaseCost(Object baseCost) {
        this.baseCost = baseCost;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(String basePrice) {
        this.basePrice = basePrice;
    }

    public String getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(String originalPrice) {
        this.originalPrice = originalPrice;
    }

    public Object getBaseOriginalPrice() {
        return baseOriginalPrice;
    }

    public void setBaseOriginalPrice(Object baseOriginalPrice) {
        this.baseOriginalPrice = baseOriginalPrice;
    }

    public String getTaxPercent() {
        return taxPercent;
    }

    public void setTaxPercent(String taxPercent) {
        this.taxPercent = taxPercent;
    }

    public String getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(String taxAmount) {
        this.taxAmount = taxAmount;
    }

    public String getBaseTaxAmount() {
        return baseTaxAmount;
    }

    public void setBaseTaxAmount(String baseTaxAmount) {
        this.baseTaxAmount = baseTaxAmount;
    }

    public String getTaxInvoiced() {
        return taxInvoiced;
    }

    public void setTaxInvoiced(String taxInvoiced) {
        this.taxInvoiced = taxInvoiced;
    }

    public String getBaseTaxInvoiced() {
        return baseTaxInvoiced;
    }

    public void setBaseTaxInvoiced(String baseTaxInvoiced) {
        this.baseTaxInvoiced = baseTaxInvoiced;
    }

    public String getDiscountPercent() {
        return discountPercent;
    }

    public void setDiscountPercent(String discountPercent) {
        this.discountPercent = discountPercent;
    }

    public String getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(String discountAmount) {
        this.discountAmount = discountAmount;
    }

    public String getBaseDiscountAmount() {
        return baseDiscountAmount;
    }

    public void setBaseDiscountAmount(String baseDiscountAmount) {
        this.baseDiscountAmount = baseDiscountAmount;
    }

    public String getDiscountInvoiced() {
        return discountInvoiced;
    }

    public void setDiscountInvoiced(String discountInvoiced) {
        this.discountInvoiced = discountInvoiced;
    }

    public String getBaseDiscountInvoiced() {
        return baseDiscountInvoiced;
    }

    public void setBaseDiscountInvoiced(String baseDiscountInvoiced) {
        this.baseDiscountInvoiced = baseDiscountInvoiced;
    }

    public String getAmountRefunded() {
        return amountRefunded;
    }

    public void setAmountRefunded(String amountRefunded) {
        this.amountRefunded = amountRefunded;
    }

    public String getBaseAmountRefunded() {
        return baseAmountRefunded;
    }

    public void setBaseAmountRefunded(String baseAmountRefunded) {
        this.baseAmountRefunded = baseAmountRefunded;
    }

    public String getRowTotal() {
        return rowTotal;
    }

    public void setRowTotal(String rowTotal) {
        this.rowTotal = rowTotal;
    }

    public String getBaseRowTotal() {
        return baseRowTotal;
    }

    public void setBaseRowTotal(String baseRowTotal) {
        this.baseRowTotal = baseRowTotal;
    }

    public String getRowInvoiced() {
        return rowInvoiced;
    }

    public void setRowInvoiced(String rowInvoiced) {
        this.rowInvoiced = rowInvoiced;
    }

    public String getBaseRowInvoiced() {
        return baseRowInvoiced;
    }

    public void setBaseRowInvoiced(String baseRowInvoiced) {
        this.baseRowInvoiced = baseRowInvoiced;
    }

    public String getRowWeight() {
        return rowWeight;
    }

    public void setRowWeight(String rowWeight) {
        this.rowWeight = rowWeight;
    }

    public Object getBaseTaxBeforeDiscount() {
        return baseTaxBeforeDiscount;
    }

    public void setBaseTaxBeforeDiscount(Object baseTaxBeforeDiscount) {
        this.baseTaxBeforeDiscount = baseTaxBeforeDiscount;
    }

    public Object getTaxBeforeDiscount() {
        return taxBeforeDiscount;
    }

    public void setTaxBeforeDiscount(Object taxBeforeDiscount) {
        this.taxBeforeDiscount = taxBeforeDiscount;
    }

    public Object getExtOrderItemId() {
        return extOrderItemId;
    }

    public void setExtOrderItemId(Object extOrderItemId) {
        this.extOrderItemId = extOrderItemId;
    }

    public Object getLockedDoInvoice() {
        return lockedDoInvoice;
    }

    public void setLockedDoInvoice(Object lockedDoInvoice) {
        this.lockedDoInvoice = lockedDoInvoice;
    }

    public Object getLockedDoShip() {
        return lockedDoShip;
    }

    public void setLockedDoShip(Object lockedDoShip) {
        this.lockedDoShip = lockedDoShip;
    }

    public String getPriceInclTax() {
        return priceInclTax;
    }

    public void setPriceInclTax(String priceInclTax) {
        this.priceInclTax = priceInclTax;
    }

    public String getBasePriceInclTax() {
        return basePriceInclTax;
    }

    public void setBasePriceInclTax(String basePriceInclTax) {
        this.basePriceInclTax = basePriceInclTax;
    }

    public String getRowTotalInclTax() {
        return rowTotalInclTax;
    }

    public void setRowTotalInclTax(String rowTotalInclTax) {
        this.rowTotalInclTax = rowTotalInclTax;
    }

    public String getBaseRowTotalInclTax() {
        return baseRowTotalInclTax;
    }

    public void setBaseRowTotalInclTax(String baseRowTotalInclTax) {
        this.baseRowTotalInclTax = baseRowTotalInclTax;
    }

    public String getDiscountTaxCompensationAmount() {
        return discountTaxCompensationAmount;
    }

    public void setDiscountTaxCompensationAmount(String discountTaxCompensationAmount) {
        this.discountTaxCompensationAmount = discountTaxCompensationAmount;
    }

    public String getBaseDiscountTaxCompensationAmount() {
        return baseDiscountTaxCompensationAmount;
    }

    public void setBaseDiscountTaxCompensationAmount(String baseDiscountTaxCompensationAmount) {
        this.baseDiscountTaxCompensationAmount = baseDiscountTaxCompensationAmount;
    }

    public Object getDiscountTaxCompensationInvoiced() {
        return discountTaxCompensationInvoiced;
    }

    public void setDiscountTaxCompensationInvoiced(Object discountTaxCompensationInvoiced) {
        this.discountTaxCompensationInvoiced = discountTaxCompensationInvoiced;
    }

    public Object getBaseDiscountTaxCompensationInvoiced() {
        return baseDiscountTaxCompensationInvoiced;
    }

    public void setBaseDiscountTaxCompensationInvoiced(Object baseDiscountTaxCompensationInvoiced) {
        this.baseDiscountTaxCompensationInvoiced = baseDiscountTaxCompensationInvoiced;
    }

    public Object getDiscountTaxCompensationRefunded() {
        return discountTaxCompensationRefunded;
    }

    public void setDiscountTaxCompensationRefunded(Object discountTaxCompensationRefunded) {
        this.discountTaxCompensationRefunded = discountTaxCompensationRefunded;
    }

    public Object getBaseDiscountTaxCompensationRefunded() {
        return baseDiscountTaxCompensationRefunded;
    }

    public void setBaseDiscountTaxCompensationRefunded(Object baseDiscountTaxCompensationRefunded) {
        this.baseDiscountTaxCompensationRefunded = baseDiscountTaxCompensationRefunded;
    }

    public Object getTaxCanceled() {
        return taxCanceled;
    }

    public void setTaxCanceled(Object taxCanceled) {
        this.taxCanceled = taxCanceled;
    }

    public Object getDiscountTaxCompensationCanceled() {
        return discountTaxCompensationCanceled;
    }

    public void setDiscountTaxCompensationCanceled(Object discountTaxCompensationCanceled) {
        this.discountTaxCompensationCanceled = discountTaxCompensationCanceled;
    }

    public Object getTaxRefunded() {
        return taxRefunded;
    }

    public void setTaxRefunded(Object taxRefunded) {
        this.taxRefunded = taxRefunded;
    }

    public Object getBaseTaxRefunded() {
        return baseTaxRefunded;
    }

    public void setBaseTaxRefunded(Object baseTaxRefunded) {
        this.baseTaxRefunded = baseTaxRefunded;
    }

    public Object getDiscountRefunded() {
        return discountRefunded;
    }

    public void setDiscountRefunded(Object discountRefunded) {
        this.discountRefunded = discountRefunded;
    }

    public Object getBaseDiscountRefunded() {
        return baseDiscountRefunded;
    }

    public void setBaseDiscountRefunded(Object baseDiscountRefunded) {
        this.baseDiscountRefunded = baseDiscountRefunded;
    }

    public String getFreeShipping() {
        return freeShipping;
    }

    public void setFreeShipping(String freeShipping) {
        this.freeShipping = freeShipping;
    }

    public Object getGiftMessageId() {
        return giftMessageId;
    }

    public void setGiftMessageId(Object giftMessageId) {
        this.giftMessageId = giftMessageId;
    }

    public Object getGiftMessageAvailable() {
        return giftMessageAvailable;
    }

    public void setGiftMessageAvailable(Object giftMessageAvailable) {
        this.giftMessageAvailable = giftMessageAvailable;
    }

    public Object getWeeeTaxApplied() {
        return weeeTaxApplied;
    }

    public void setWeeeTaxApplied(Object weeeTaxApplied) {
        this.weeeTaxApplied = weeeTaxApplied;
    }

    public Object getWeeeTaxAppliedAmount() {
        return weeeTaxAppliedAmount;
    }

    public void setWeeeTaxAppliedAmount(Object weeeTaxAppliedAmount) {
        this.weeeTaxAppliedAmount = weeeTaxAppliedAmount;
    }

    public Object getWeeeTaxAppliedRowAmount() {
        return weeeTaxAppliedRowAmount;
    }

    public void setWeeeTaxAppliedRowAmount(Object weeeTaxAppliedRowAmount) {
        this.weeeTaxAppliedRowAmount = weeeTaxAppliedRowAmount;
    }

    public Object getWeeeTaxDisposition() {
        return weeeTaxDisposition;
    }

    public void setWeeeTaxDisposition(Object weeeTaxDisposition) {
        this.weeeTaxDisposition = weeeTaxDisposition;
    }

    public Object getWeeeTaxRowDisposition() {
        return weeeTaxRowDisposition;
    }

    public void setWeeeTaxRowDisposition(Object weeeTaxRowDisposition) {
        this.weeeTaxRowDisposition = weeeTaxRowDisposition;
    }

    public Object getBaseWeeeTaxAppliedAmount() {
        return baseWeeeTaxAppliedAmount;
    }

    public void setBaseWeeeTaxAppliedAmount(Object baseWeeeTaxAppliedAmount) {
        this.baseWeeeTaxAppliedAmount = baseWeeeTaxAppliedAmount;
    }

    public Object getBaseWeeeTaxAppliedRowAmnt() {
        return baseWeeeTaxAppliedRowAmnt;
    }

    public void setBaseWeeeTaxAppliedRowAmnt(Object baseWeeeTaxAppliedRowAmnt) {
        this.baseWeeeTaxAppliedRowAmnt = baseWeeeTaxAppliedRowAmnt;
    }

    public Object getBaseWeeeTaxDisposition() {
        return baseWeeeTaxDisposition;
    }

    public void setBaseWeeeTaxDisposition(Object baseWeeeTaxDisposition) {
        this.baseWeeeTaxDisposition = baseWeeeTaxDisposition;
    }

    public Object getBaseWeeeTaxRowDisposition() {
        return baseWeeeTaxRowDisposition;
    }

    public void setBaseWeeeTaxRowDisposition(Object baseWeeeTaxRowDisposition) {
        this.baseWeeeTaxRowDisposition = baseWeeeTaxRowDisposition;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

}
