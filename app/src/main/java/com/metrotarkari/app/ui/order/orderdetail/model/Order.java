
package com.metrotarkari.app.ui.order.orderdetail.model;

import java.util.ArrayList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.metrotarkari.app.model.BAddress;
import com.metrotarkari.app.model.SAddress;

public class Order {

    @SerializedName("entity_id")
    @Expose
    private String entityId;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("coupon_code")
    @Expose
    private Object couponCode;
    @SerializedName("protect_code")
    @Expose
    private String protectCode;
    @SerializedName("shipping_description")
    @Expose
    private Object shippingDescription;
    @SerializedName("is_virtual")
    @Expose
    private String isVirtual;
    @SerializedName("store_id")
    @Expose
    private String storeId;
    @SerializedName("customer_id")
    @Expose
    private String customerId;
    @SerializedName("base_discount_amount")
    @Expose
    private String baseDiscountAmount;
    @SerializedName("base_discount_canceled")
    @Expose
    private Object baseDiscountCanceled;
    @SerializedName("base_discount_invoiced")
    @Expose
    private Object baseDiscountInvoiced;
    @SerializedName("base_discount_refunded")
    @Expose
    private Object baseDiscountRefunded;
    @SerializedName("base_grand_total")
    @Expose
    private String baseGrandTotal;
    @SerializedName("base_shipping_amount")
    @Expose
    private String baseShippingAmount;
    @SerializedName("base_shipping_canceled")
    @Expose
    private Object baseShippingCanceled;
    @SerializedName("base_shipping_invoiced")
    @Expose
    private Object baseShippingInvoiced;
    @SerializedName("base_shipping_refunded")
    @Expose
    private Object baseShippingRefunded;
    @SerializedName("base_shipping_tax_amount")
    @Expose
    private String baseShippingTaxAmount;
    @SerializedName("base_shipping_tax_refunded")
    @Expose
    private Object baseShippingTaxRefunded;
    @SerializedName("base_subtotal")
    @Expose
    private String baseSubtotal;
    @SerializedName("base_subtotal_canceled")
    @Expose
    private Object baseSubtotalCanceled;
    @SerializedName("base_subtotal_invoiced")
    @Expose
    private Object baseSubtotalInvoiced;
    @SerializedName("base_subtotal_refunded")
    @Expose
    private Object baseSubtotalRefunded;
    @SerializedName("base_tax_amount")
    @Expose
    private String baseTaxAmount;
    @SerializedName("base_tax_canceled")
    @Expose
    private Object baseTaxCanceled;
    @SerializedName("base_tax_invoiced")
    @Expose
    private Object baseTaxInvoiced;
    @SerializedName("base_tax_refunded")
    @Expose
    private Object baseTaxRefunded;
    @SerializedName("base_to_global_rate")
    @Expose
    private String baseToGlobalRate;
    @SerializedName("base_to_order_rate")
    @Expose
    private String baseToOrderRate;
    @SerializedName("base_total_canceled")
    @Expose
    private Object baseTotalCanceled;
    @SerializedName("base_total_invoiced")
    @Expose
    private Object baseTotalInvoiced;
    @SerializedName("base_total_invoiced_cost")
    @Expose
    private Object baseTotalInvoicedCost;
    @SerializedName("base_total_offline_refunded")
    @Expose
    private Object baseTotalOfflineRefunded;
    @SerializedName("base_total_online_refunded")
    @Expose
    private Object baseTotalOnlineRefunded;
    @SerializedName("base_total_paid")
    @Expose
    private Object baseTotalPaid;
    @SerializedName("base_total_qty_ordered")
    @Expose
    private Object baseTotalQtyOrdered;
    @SerializedName("base_total_refunded")
    @Expose
    private Object baseTotalRefunded;
    @SerializedName("discount_amount")
    @Expose
    private String discountAmount;
    @SerializedName("discount_canceled")
    @Expose
    private Object discountCanceled;
    @SerializedName("discount_invoiced")
    @Expose
    private Object discountInvoiced;
    @SerializedName("discount_refunded")
    @Expose
    private Object discountRefunded;
    @SerializedName("grand_total")
    @Expose
    private String grandTotal;
    @SerializedName("shipping_amount")
    @Expose
    private String shippingAmount;
    @SerializedName("shipping_canceled")
    @Expose
    private Object shippingCanceled;
    @SerializedName("shipping_invoiced")
    @Expose
    private Object shippingInvoiced;
    @SerializedName("shipping_refunded")
    @Expose
    private Object shippingRefunded;
    @SerializedName("shipping_tax_amount")
    @Expose
    private String shippingTaxAmount;
    @SerializedName("shipping_tax_refunded")
    @Expose
    private Object shippingTaxRefunded;
    @SerializedName("store_to_base_rate")
    @Expose
    private String storeToBaseRate;
    @SerializedName("store_to_order_rate")
    @Expose
    private String storeToOrderRate;
    @SerializedName("subtotal")
    @Expose
    private String subtotal;
    @SerializedName("subtotal_canceled")
    @Expose
    private Object subtotalCanceled;
    @SerializedName("subtotal_invoiced")
    @Expose
    private Object subtotalInvoiced;
    @SerializedName("subtotal_refunded")
    @Expose
    private Object subtotalRefunded;
    @SerializedName("tax_amount")
    @Expose
    private String taxAmount;
    @SerializedName("tax_canceled")
    @Expose
    private Object taxCanceled;
    @SerializedName("tax_invoiced")
    @Expose
    private Object taxInvoiced;
    @SerializedName("tax_refunded")
    @Expose
    private Object taxRefunded;
    @SerializedName("total_canceled")
    @Expose
    private Object totalCanceled;
    @SerializedName("total_invoiced")
    @Expose
    private Object totalInvoiced;
    @SerializedName("total_offline_refunded")
    @Expose
    private Object totalOfflineRefunded;
    @SerializedName("total_online_refunded")
    @Expose
    private Object totalOnlineRefunded;
    @SerializedName("total_paid")
    @Expose
    private Object totalPaid;
    @SerializedName("total_qty_ordered")
    @Expose
    private String totalQtyOrdered;
    @SerializedName("total_refunded")
    @Expose
    private Object totalRefunded;
    @SerializedName("can_ship_partially")
    @Expose
    private Object canShipPartially;
    @SerializedName("can_ship_partially_item")
    @Expose
    private Object canShipPartiallyItem;
    @SerializedName("customer_is_guest")
    @Expose
    private String customerIsGuest;
    @SerializedName("customer_note_notify")
    @Expose
    private String customerNoteNotify;
    @SerializedName("billing_address_id")
    @Expose
    private String billingAddressId;
    @SerializedName("customer_group_id")
    @Expose
    private String customerGroupId;
    @SerializedName("edit_increment")
    @Expose
    private Object editIncrement;
    @SerializedName("email_sent")
    @Expose
    private String emailSent;
    @SerializedName("send_email")
    @Expose
    private String sendEmail;
    @SerializedName("forced_shipment_with_invoice")
    @Expose
    private Object forcedShipmentWithInvoice;
    @SerializedName("payment_auth_expiration")
    @Expose
    private Object paymentAuthExpiration;
    @SerializedName("quote_address_id")
    @Expose
    private Object quoteAddressId;
    @SerializedName("quote_id")
    @Expose
    private String quoteId;
    @SerializedName("shipping_address_id")
    @Expose
    private String shippingAddressId;
    @SerializedName("adjustment_negative")
    @Expose
    private Object adjustmentNegative;
    @SerializedName("adjustment_positive")
    @Expose
    private Object adjustmentPositive;
    @SerializedName("base_adjustment_negative")
    @Expose
    private Object baseAdjustmentNegative;
    @SerializedName("base_adjustment_positive")
    @Expose
    private Object baseAdjustmentPositive;
    @SerializedName("base_shipping_discount_amount")
    @Expose
    private String baseShippingDiscountAmount;
    @SerializedName("base_subtotal_incl_tax")
    @Expose
    private Object baseSubtotalInclTax;
    @SerializedName("base_total_due")
    @Expose
    private String baseTotalDue;
    @SerializedName("payment_authorization_amount")
    @Expose
    private Object paymentAuthorizationAmount;
    @SerializedName("shipping_discount_amount")
    @Expose
    private String shippingDiscountAmount;
    @SerializedName("subtotal_incl_tax")
    @Expose
    private String subtotalInclTax;
    @SerializedName("total_due")
    @Expose
    private String totalDue;
    @SerializedName("weight")
    @Expose
    private String weight;
    @SerializedName("customer_dob")
    @Expose
    private Object customerDob;
    @SerializedName("increment_id")
    @Expose
    private String incrementId;
    @SerializedName("applied_rule_ids")
    @Expose
    private Object appliedRuleIds;
    @SerializedName("base_currency_code")
    @Expose
    private String baseCurrencyCode;
    @SerializedName("customer_email")
    @Expose
    private String customerEmail;
    @SerializedName("customer_firstname")
    @Expose
    private String customerFirstname;
    @SerializedName("customer_lastname")
    @Expose
    private String customerLastname;
    @SerializedName("customer_middlename")
    @Expose
    private Object customerMiddlename;
    @SerializedName("customer_prefix")
    @Expose
    private Object customerPrefix;
    @SerializedName("customer_suffix")
    @Expose
    private Object customerSuffix;
    @SerializedName("customer_taxvat")
    @Expose
    private Object customerTaxvat;
    @SerializedName("discount_description")
    @Expose
    private Object discountDescription;
    @SerializedName("ext_customer_id")
    @Expose
    private Object extCustomerId;
    @SerializedName("ext_order_id")
    @Expose
    private Object extOrderId;
    @SerializedName("global_currency_code")
    @Expose
    private String globalCurrencyCode;
    @SerializedName("hold_before_state")
    @Expose
    private Object holdBeforeState;
    @SerializedName("hold_before_status")
    @Expose
    private Object holdBeforeStatus;
    @SerializedName("order_currency_code")
    @Expose
    private String orderCurrencyCode;
    @SerializedName("original_increment_id")
    @Expose
    private Object originalIncrementId;
    @SerializedName("relation_child_id")
    @Expose
    private Object relationChildId;
    @SerializedName("relation_child_real_id")
    @Expose
    private Object relationChildRealId;
    @SerializedName("relation_parent_id")
    @Expose
    private Object relationParentId;
    @SerializedName("relation_parent_real_id")
    @Expose
    private Object relationParentRealId;
    @SerializedName("remote_ip")
    @Expose
    private String remoteIp;
    @SerializedName("shipping_method")
    @Expose
    private Object shippingMethod;
    @SerializedName("store_currency_code")
    @Expose
    private String storeCurrencyCode;
    @SerializedName("store_name")
    @Expose
    private String storeName;
    @SerializedName("x_forwarded_for")
    @Expose
    private Object xForwardedFor;
    @SerializedName("customer_note")
    @Expose
    private Object customerNote;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("total_item_count")
    @Expose
    private String totalItemCount;
    @SerializedName("customer_gender")
    @Expose
    private Object customerGender;
    @SerializedName("discount_tax_compensation_amount")
    @Expose
    private String discountTaxCompensationAmount;
    @SerializedName("base_discount_tax_compensation_amount")
    @Expose
    private String baseDiscountTaxCompensationAmount;
    @SerializedName("shipping_discount_tax_compensation_amount")
    @Expose
    private String shippingDiscountTaxCompensationAmount;
    @SerializedName("base_shipping_discount_tax_compensation_amnt")
    @Expose
    private Object baseShippingDiscountTaxCompensationAmnt;
    @SerializedName("discount_tax_compensation_invoiced")
    @Expose
    private Object discountTaxCompensationInvoiced;
    @SerializedName("base_discount_tax_compensation_invoiced")
    @Expose
    private Object baseDiscountTaxCompensationInvoiced;
    @SerializedName("discount_tax_compensation_refunded")
    @Expose
    private Object discountTaxCompensationRefunded;
    @SerializedName("base_discount_tax_compensation_refunded")
    @Expose
    private Object baseDiscountTaxCompensationRefunded;
    @SerializedName("shipping_incl_tax")
    @Expose
    private String shippingInclTax;
    @SerializedName("base_shipping_incl_tax")
    @Expose
    private String baseShippingInclTax;
    @SerializedName("coupon_rule_name")
    @Expose
    private Object couponRuleName;
    @SerializedName("paypal_ipn_customer_notified")
    @Expose
    private String paypalIpnCustomerNotified;
    @SerializedName("gift_message_id")
    @Expose
    private Object giftMessageId;
    @SerializedName("delivery_date")
    @Expose
    private String deliveryDate;
    @SerializedName("delivery_comment")
    @Expose
    private String deliveryComment;
    @SerializedName("fee_amount")
    @Expose
    private String feeAmount;
    @SerializedName("base_fee_amount")
    @Expose
    private String baseFeeAmount;
    @SerializedName("fee_amount_refunded")
    @Expose
    private String feeAmountRefunded;
    @SerializedName("base_fee_amount_refunded")
    @Expose
    private String baseFeeAmountRefunded;
    @SerializedName("fee_amount_invoiced")
    @Expose
    private String feeAmountInvoiced;
    @SerializedName("base_fee_amount_invoiced")
    @Expose
    private String baseFeeAmountInvoiced;
    @SerializedName("metro_time_slot")
    @Expose
    private String metroTimeSlot;
    @SerializedName("payment_method")
    @Expose
    private String paymentMethod;
    @SerializedName("billing_address")
    @Expose
    private BAddress billingAddress;
    @SerializedName("shipping_address")
    @Expose
    private SAddress shippingAddress;
    @SerializedName("order_items")
    @Expose
    private ArrayList<OrderItem> orderItems = null;
    @SerializedName("total")
    @Expose
    private Total total;

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Object getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(Object couponCode) {
        this.couponCode = couponCode;
    }

    public String getProtectCode() {
        return protectCode;
    }

    public void setProtectCode(String protectCode) {
        this.protectCode = protectCode;
    }

    public Object getShippingDescription() {
        return shippingDescription;
    }

    public void setShippingDescription(Object shippingDescription) {
        this.shippingDescription = shippingDescription;
    }

    public String getIsVirtual() {
        return isVirtual;
    }

    public void setIsVirtual(String isVirtual) {
        this.isVirtual = isVirtual;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getBaseDiscountAmount() {
        return baseDiscountAmount;
    }

    public void setBaseDiscountAmount(String baseDiscountAmount) {
        this.baseDiscountAmount = baseDiscountAmount;
    }

    public Object getBaseDiscountCanceled() {
        return baseDiscountCanceled;
    }

    public void setBaseDiscountCanceled(Object baseDiscountCanceled) {
        this.baseDiscountCanceled = baseDiscountCanceled;
    }

    public Object getBaseDiscountInvoiced() {
        return baseDiscountInvoiced;
    }

    public void setBaseDiscountInvoiced(Object baseDiscountInvoiced) {
        this.baseDiscountInvoiced = baseDiscountInvoiced;
    }

    public Object getBaseDiscountRefunded() {
        return baseDiscountRefunded;
    }

    public void setBaseDiscountRefunded(Object baseDiscountRefunded) {
        this.baseDiscountRefunded = baseDiscountRefunded;
    }

    public String getBaseGrandTotal() {
        return baseGrandTotal;
    }

    public void setBaseGrandTotal(String baseGrandTotal) {
        this.baseGrandTotal = baseGrandTotal;
    }

    public String getBaseShippingAmount() {
        return baseShippingAmount;
    }

    public void setBaseShippingAmount(String baseShippingAmount) {
        this.baseShippingAmount = baseShippingAmount;
    }

    public Object getBaseShippingCanceled() {
        return baseShippingCanceled;
    }

    public void setBaseShippingCanceled(Object baseShippingCanceled) {
        this.baseShippingCanceled = baseShippingCanceled;
    }

    public Object getBaseShippingInvoiced() {
        return baseShippingInvoiced;
    }

    public void setBaseShippingInvoiced(Object baseShippingInvoiced) {
        this.baseShippingInvoiced = baseShippingInvoiced;
    }

    public Object getBaseShippingRefunded() {
        return baseShippingRefunded;
    }

    public void setBaseShippingRefunded(Object baseShippingRefunded) {
        this.baseShippingRefunded = baseShippingRefunded;
    }

    public String getBaseShippingTaxAmount() {
        return baseShippingTaxAmount;
    }

    public void setBaseShippingTaxAmount(String baseShippingTaxAmount) {
        this.baseShippingTaxAmount = baseShippingTaxAmount;
    }

    public Object getBaseShippingTaxRefunded() {
        return baseShippingTaxRefunded;
    }

    public void setBaseShippingTaxRefunded(Object baseShippingTaxRefunded) {
        this.baseShippingTaxRefunded = baseShippingTaxRefunded;
    }

    public String getBaseSubtotal() {
        return baseSubtotal;
    }

    public void setBaseSubtotal(String baseSubtotal) {
        this.baseSubtotal = baseSubtotal;
    }

    public Object getBaseSubtotalCanceled() {
        return baseSubtotalCanceled;
    }

    public void setBaseSubtotalCanceled(Object baseSubtotalCanceled) {
        this.baseSubtotalCanceled = baseSubtotalCanceled;
    }

    public Object getBaseSubtotalInvoiced() {
        return baseSubtotalInvoiced;
    }

    public void setBaseSubtotalInvoiced(Object baseSubtotalInvoiced) {
        this.baseSubtotalInvoiced = baseSubtotalInvoiced;
    }

    public Object getBaseSubtotalRefunded() {
        return baseSubtotalRefunded;
    }

    public void setBaseSubtotalRefunded(Object baseSubtotalRefunded) {
        this.baseSubtotalRefunded = baseSubtotalRefunded;
    }

    public String getBaseTaxAmount() {
        return baseTaxAmount;
    }

    public void setBaseTaxAmount(String baseTaxAmount) {
        this.baseTaxAmount = baseTaxAmount;
    }

    public Object getBaseTaxCanceled() {
        return baseTaxCanceled;
    }

    public void setBaseTaxCanceled(Object baseTaxCanceled) {
        this.baseTaxCanceled = baseTaxCanceled;
    }

    public Object getBaseTaxInvoiced() {
        return baseTaxInvoiced;
    }

    public void setBaseTaxInvoiced(Object baseTaxInvoiced) {
        this.baseTaxInvoiced = baseTaxInvoiced;
    }

    public Object getBaseTaxRefunded() {
        return baseTaxRefunded;
    }

    public void setBaseTaxRefunded(Object baseTaxRefunded) {
        this.baseTaxRefunded = baseTaxRefunded;
    }

    public String getBaseToGlobalRate() {
        return baseToGlobalRate;
    }

    public void setBaseToGlobalRate(String baseToGlobalRate) {
        this.baseToGlobalRate = baseToGlobalRate;
    }

    public String getBaseToOrderRate() {
        return baseToOrderRate;
    }

    public void setBaseToOrderRate(String baseToOrderRate) {
        this.baseToOrderRate = baseToOrderRate;
    }

    public Object getBaseTotalCanceled() {
        return baseTotalCanceled;
    }

    public void setBaseTotalCanceled(Object baseTotalCanceled) {
        this.baseTotalCanceled = baseTotalCanceled;
    }

    public Object getBaseTotalInvoiced() {
        return baseTotalInvoiced;
    }

    public void setBaseTotalInvoiced(Object baseTotalInvoiced) {
        this.baseTotalInvoiced = baseTotalInvoiced;
    }

    public Object getBaseTotalInvoicedCost() {
        return baseTotalInvoicedCost;
    }

    public void setBaseTotalInvoicedCost(Object baseTotalInvoicedCost) {
        this.baseTotalInvoicedCost = baseTotalInvoicedCost;
    }

    public Object getBaseTotalOfflineRefunded() {
        return baseTotalOfflineRefunded;
    }

    public void setBaseTotalOfflineRefunded(Object baseTotalOfflineRefunded) {
        this.baseTotalOfflineRefunded = baseTotalOfflineRefunded;
    }

    public Object getBaseTotalOnlineRefunded() {
        return baseTotalOnlineRefunded;
    }

    public void setBaseTotalOnlineRefunded(Object baseTotalOnlineRefunded) {
        this.baseTotalOnlineRefunded = baseTotalOnlineRefunded;
    }

    public Object getBaseTotalPaid() {
        return baseTotalPaid;
    }

    public void setBaseTotalPaid(Object baseTotalPaid) {
        this.baseTotalPaid = baseTotalPaid;
    }

    public Object getBaseTotalQtyOrdered() {
        return baseTotalQtyOrdered;
    }

    public void setBaseTotalQtyOrdered(Object baseTotalQtyOrdered) {
        this.baseTotalQtyOrdered = baseTotalQtyOrdered;
    }

    public Object getBaseTotalRefunded() {
        return baseTotalRefunded;
    }

    public void setBaseTotalRefunded(Object baseTotalRefunded) {
        this.baseTotalRefunded = baseTotalRefunded;
    }

    public String getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(String discountAmount) {
        this.discountAmount = discountAmount;
    }

    public Object getDiscountCanceled() {
        return discountCanceled;
    }

    public void setDiscountCanceled(Object discountCanceled) {
        this.discountCanceled = discountCanceled;
    }

    public Object getDiscountInvoiced() {
        return discountInvoiced;
    }

    public void setDiscountInvoiced(Object discountInvoiced) {
        this.discountInvoiced = discountInvoiced;
    }

    public Object getDiscountRefunded() {
        return discountRefunded;
    }

    public void setDiscountRefunded(Object discountRefunded) {
        this.discountRefunded = discountRefunded;
    }

    public String getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String getShippingAmount() {
        return shippingAmount;
    }

    public void setShippingAmount(String shippingAmount) {
        this.shippingAmount = shippingAmount;
    }

    public Object getShippingCanceled() {
        return shippingCanceled;
    }

    public void setShippingCanceled(Object shippingCanceled) {
        this.shippingCanceled = shippingCanceled;
    }

    public Object getShippingInvoiced() {
        return shippingInvoiced;
    }

    public void setShippingInvoiced(Object shippingInvoiced) {
        this.shippingInvoiced = shippingInvoiced;
    }

    public Object getShippingRefunded() {
        return shippingRefunded;
    }

    public void setShippingRefunded(Object shippingRefunded) {
        this.shippingRefunded = shippingRefunded;
    }

    public String getShippingTaxAmount() {
        return shippingTaxAmount;
    }

    public void setShippingTaxAmount(String shippingTaxAmount) {
        this.shippingTaxAmount = shippingTaxAmount;
    }

    public Object getShippingTaxRefunded() {
        return shippingTaxRefunded;
    }

    public void setShippingTaxRefunded(Object shippingTaxRefunded) {
        this.shippingTaxRefunded = shippingTaxRefunded;
    }

    public String getStoreToBaseRate() {
        return storeToBaseRate;
    }

    public void setStoreToBaseRate(String storeToBaseRate) {
        this.storeToBaseRate = storeToBaseRate;
    }

    public String getStoreToOrderRate() {
        return storeToOrderRate;
    }

    public void setStoreToOrderRate(String storeToOrderRate) {
        this.storeToOrderRate = storeToOrderRate;
    }

    public String getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

    public Object getSubtotalCanceled() {
        return subtotalCanceled;
    }

    public void setSubtotalCanceled(Object subtotalCanceled) {
        this.subtotalCanceled = subtotalCanceled;
    }

    public Object getSubtotalInvoiced() {
        return subtotalInvoiced;
    }

    public void setSubtotalInvoiced(Object subtotalInvoiced) {
        this.subtotalInvoiced = subtotalInvoiced;
    }

    public Object getSubtotalRefunded() {
        return subtotalRefunded;
    }

    public void setSubtotalRefunded(Object subtotalRefunded) {
        this.subtotalRefunded = subtotalRefunded;
    }

    public String getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(String taxAmount) {
        this.taxAmount = taxAmount;
    }

    public Object getTaxCanceled() {
        return taxCanceled;
    }

    public void setTaxCanceled(Object taxCanceled) {
        this.taxCanceled = taxCanceled;
    }

    public Object getTaxInvoiced() {
        return taxInvoiced;
    }

    public void setTaxInvoiced(Object taxInvoiced) {
        this.taxInvoiced = taxInvoiced;
    }

    public Object getTaxRefunded() {
        return taxRefunded;
    }

    public void setTaxRefunded(Object taxRefunded) {
        this.taxRefunded = taxRefunded;
    }

    public Object getTotalCanceled() {
        return totalCanceled;
    }

    public void setTotalCanceled(Object totalCanceled) {
        this.totalCanceled = totalCanceled;
    }

    public Object getTotalInvoiced() {
        return totalInvoiced;
    }

    public void setTotalInvoiced(Object totalInvoiced) {
        this.totalInvoiced = totalInvoiced;
    }

    public Object getTotalOfflineRefunded() {
        return totalOfflineRefunded;
    }

    public void setTotalOfflineRefunded(Object totalOfflineRefunded) {
        this.totalOfflineRefunded = totalOfflineRefunded;
    }

    public Object getTotalOnlineRefunded() {
        return totalOnlineRefunded;
    }

    public void setTotalOnlineRefunded(Object totalOnlineRefunded) {
        this.totalOnlineRefunded = totalOnlineRefunded;
    }

    public Object getTotalPaid() {
        return totalPaid;
    }

    public void setTotalPaid(Object totalPaid) {
        this.totalPaid = totalPaid;
    }

    public String getTotalQtyOrdered() {
        return totalQtyOrdered;
    }

    public void setTotalQtyOrdered(String totalQtyOrdered) {
        this.totalQtyOrdered = totalQtyOrdered;
    }

    public Object getTotalRefunded() {
        return totalRefunded;
    }

    public void setTotalRefunded(Object totalRefunded) {
        this.totalRefunded = totalRefunded;
    }

    public Object getCanShipPartially() {
        return canShipPartially;
    }

    public void setCanShipPartially(Object canShipPartially) {
        this.canShipPartially = canShipPartially;
    }

    public Object getCanShipPartiallyItem() {
        return canShipPartiallyItem;
    }

    public void setCanShipPartiallyItem(Object canShipPartiallyItem) {
        this.canShipPartiallyItem = canShipPartiallyItem;
    }

    public String getCustomerIsGuest() {
        return customerIsGuest;
    }

    public void setCustomerIsGuest(String customerIsGuest) {
        this.customerIsGuest = customerIsGuest;
    }

    public String getCustomerNoteNotify() {
        return customerNoteNotify;
    }

    public void setCustomerNoteNotify(String customerNoteNotify) {
        this.customerNoteNotify = customerNoteNotify;
    }

    public String getBillingAddressId() {
        return billingAddressId;
    }

    public void setBillingAddressId(String billingAddressId) {
        this.billingAddressId = billingAddressId;
    }

    public String getCustomerGroupId() {
        return customerGroupId;
    }

    public void setCustomerGroupId(String customerGroupId) {
        this.customerGroupId = customerGroupId;
    }

    public Object getEditIncrement() {
        return editIncrement;
    }

    public void setEditIncrement(Object editIncrement) {
        this.editIncrement = editIncrement;
    }

    public String getEmailSent() {
        return emailSent;
    }

    public void setEmailSent(String emailSent) {
        this.emailSent = emailSent;
    }

    public String getSendEmail() {
        return sendEmail;
    }

    public void setSendEmail(String sendEmail) {
        this.sendEmail = sendEmail;
    }

    public Object getForcedShipmentWithInvoice() {
        return forcedShipmentWithInvoice;
    }

    public void setForcedShipmentWithInvoice(Object forcedShipmentWithInvoice) {
        this.forcedShipmentWithInvoice = forcedShipmentWithInvoice;
    }

    public Object getPaymentAuthExpiration() {
        return paymentAuthExpiration;
    }

    public void setPaymentAuthExpiration(Object paymentAuthExpiration) {
        this.paymentAuthExpiration = paymentAuthExpiration;
    }

    public Object getQuoteAddressId() {
        return quoteAddressId;
    }

    public void setQuoteAddressId(Object quoteAddressId) {
        this.quoteAddressId = quoteAddressId;
    }

    public String getQuoteId() {
        return quoteId;
    }

    public void setQuoteId(String quoteId) {
        this.quoteId = quoteId;
    }

    public String getShippingAddressId() {
        return shippingAddressId;
    }

    public void setShippingAddressId(String shippingAddressId) {
        this.shippingAddressId = shippingAddressId;
    }

    public Object getAdjustmentNegative() {
        return adjustmentNegative;
    }

    public void setAdjustmentNegative(Object adjustmentNegative) {
        this.adjustmentNegative = adjustmentNegative;
    }

    public Object getAdjustmentPositive() {
        return adjustmentPositive;
    }

    public void setAdjustmentPositive(Object adjustmentPositive) {
        this.adjustmentPositive = adjustmentPositive;
    }

    public Object getBaseAdjustmentNegative() {
        return baseAdjustmentNegative;
    }

    public void setBaseAdjustmentNegative(Object baseAdjustmentNegative) {
        this.baseAdjustmentNegative = baseAdjustmentNegative;
    }

    public Object getBaseAdjustmentPositive() {
        return baseAdjustmentPositive;
    }

    public void setBaseAdjustmentPositive(Object baseAdjustmentPositive) {
        this.baseAdjustmentPositive = baseAdjustmentPositive;
    }

    public String getBaseShippingDiscountAmount() {
        return baseShippingDiscountAmount;
    }

    public void setBaseShippingDiscountAmount(String baseShippingDiscountAmount) {
        this.baseShippingDiscountAmount = baseShippingDiscountAmount;
    }

    public Object getBaseSubtotalInclTax() {
        return baseSubtotalInclTax;
    }

    public void setBaseSubtotalInclTax(Object baseSubtotalInclTax) {
        this.baseSubtotalInclTax = baseSubtotalInclTax;
    }

    public String getBaseTotalDue() {
        return baseTotalDue;
    }

    public void setBaseTotalDue(String baseTotalDue) {
        this.baseTotalDue = baseTotalDue;
    }

    public Object getPaymentAuthorizationAmount() {
        return paymentAuthorizationAmount;
    }

    public void setPaymentAuthorizationAmount(Object paymentAuthorizationAmount) {
        this.paymentAuthorizationAmount = paymentAuthorizationAmount;
    }

    public String getShippingDiscountAmount() {
        return shippingDiscountAmount;
    }

    public void setShippingDiscountAmount(String shippingDiscountAmount) {
        this.shippingDiscountAmount = shippingDiscountAmount;
    }

    public String getSubtotalInclTax() {
        return subtotalInclTax;
    }

    public void setSubtotalInclTax(String subtotalInclTax) {
        this.subtotalInclTax = subtotalInclTax;
    }

    public String getTotalDue() {
        return totalDue;
    }

    public void setTotalDue(String totalDue) {
        this.totalDue = totalDue;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public Object getCustomerDob() {
        return customerDob;
    }

    public void setCustomerDob(Object customerDob) {
        this.customerDob = customerDob;
    }

    public String getIncrementId() {
        return incrementId;
    }

    public void setIncrementId(String incrementId) {
        this.incrementId = incrementId;
    }

    public Object getAppliedRuleIds() {
        return appliedRuleIds;
    }

    public void setAppliedRuleIds(Object appliedRuleIds) {
        this.appliedRuleIds = appliedRuleIds;
    }

    public String getBaseCurrencyCode() {
        return baseCurrencyCode;
    }

    public void setBaseCurrencyCode(String baseCurrencyCode) {
        this.baseCurrencyCode = baseCurrencyCode;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getCustomerFirstname() {
        return customerFirstname;
    }

    public void setCustomerFirstname(String customerFirstname) {
        this.customerFirstname = customerFirstname;
    }

    public String getCustomerLastname() {
        return customerLastname;
    }

    public void setCustomerLastname(String customerLastname) {
        this.customerLastname = customerLastname;
    }

    public Object getCustomerMiddlename() {
        return customerMiddlename;
    }

    public void setCustomerMiddlename(Object customerMiddlename) {
        this.customerMiddlename = customerMiddlename;
    }

    public Object getCustomerPrefix() {
        return customerPrefix;
    }

    public void setCustomerPrefix(Object customerPrefix) {
        this.customerPrefix = customerPrefix;
    }

    public Object getCustomerSuffix() {
        return customerSuffix;
    }

    public void setCustomerSuffix(Object customerSuffix) {
        this.customerSuffix = customerSuffix;
    }

    public Object getCustomerTaxvat() {
        return customerTaxvat;
    }

    public void setCustomerTaxvat(Object customerTaxvat) {
        this.customerTaxvat = customerTaxvat;
    }

    public Object getDiscountDescription() {
        return discountDescription;
    }

    public void setDiscountDescription(Object discountDescription) {
        this.discountDescription = discountDescription;
    }

    public Object getExtCustomerId() {
        return extCustomerId;
    }

    public void setExtCustomerId(Object extCustomerId) {
        this.extCustomerId = extCustomerId;
    }

    public Object getExtOrderId() {
        return extOrderId;
    }

    public void setExtOrderId(Object extOrderId) {
        this.extOrderId = extOrderId;
    }

    public String getGlobalCurrencyCode() {
        return globalCurrencyCode;
    }

    public void setGlobalCurrencyCode(String globalCurrencyCode) {
        this.globalCurrencyCode = globalCurrencyCode;
    }

    public Object getHoldBeforeState() {
        return holdBeforeState;
    }

    public void setHoldBeforeState(Object holdBeforeState) {
        this.holdBeforeState = holdBeforeState;
    }

    public Object getHoldBeforeStatus() {
        return holdBeforeStatus;
    }

    public void setHoldBeforeStatus(Object holdBeforeStatus) {
        this.holdBeforeStatus = holdBeforeStatus;
    }

    public String getOrderCurrencyCode() {
        return orderCurrencyCode;
    }

    public void setOrderCurrencyCode(String orderCurrencyCode) {
        this.orderCurrencyCode = orderCurrencyCode;
    }

    public Object getOriginalIncrementId() {
        return originalIncrementId;
    }

    public void setOriginalIncrementId(Object originalIncrementId) {
        this.originalIncrementId = originalIncrementId;
    }

    public Object getRelationChildId() {
        return relationChildId;
    }

    public void setRelationChildId(Object relationChildId) {
        this.relationChildId = relationChildId;
    }

    public Object getRelationChildRealId() {
        return relationChildRealId;
    }

    public void setRelationChildRealId(Object relationChildRealId) {
        this.relationChildRealId = relationChildRealId;
    }

    public Object getRelationParentId() {
        return relationParentId;
    }

    public void setRelationParentId(Object relationParentId) {
        this.relationParentId = relationParentId;
    }

    public Object getRelationParentRealId() {
        return relationParentRealId;
    }

    public void setRelationParentRealId(Object relationParentRealId) {
        this.relationParentRealId = relationParentRealId;
    }

    public String getRemoteIp() {
        return remoteIp;
    }

    public void setRemoteIp(String remoteIp) {
        this.remoteIp = remoteIp;
    }

    public Object getShippingMethod() {
        return shippingMethod;
    }

    public void setShippingMethod(Object shippingMethod) {
        this.shippingMethod = shippingMethod;
    }

    public String getStoreCurrencyCode() {
        return storeCurrencyCode;
    }

    public void setStoreCurrencyCode(String storeCurrencyCode) {
        this.storeCurrencyCode = storeCurrencyCode;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public Object getXForwardedFor() {
        return xForwardedFor;
    }

    public void setXForwardedFor(Object xForwardedFor) {
        this.xForwardedFor = xForwardedFor;
    }

    public Object getCustomerNote() {
        return customerNote;
    }

    public void setCustomerNote(Object customerNote) {
        this.customerNote = customerNote;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getTotalItemCount() {
        return totalItemCount;
    }

    public void setTotalItemCount(String totalItemCount) {
        this.totalItemCount = totalItemCount;
    }

    public Object getCustomerGender() {
        return customerGender;
    }

    public void setCustomerGender(Object customerGender) {
        this.customerGender = customerGender;
    }

    public String getDiscountTaxCompensationAmount() {
        return discountTaxCompensationAmount;
    }

    public void setDiscountTaxCompensationAmount(String discountTaxCompensationAmount) {
        this.discountTaxCompensationAmount = discountTaxCompensationAmount;
    }

    public String getBaseDiscountTaxCompensationAmount() {
        return baseDiscountTaxCompensationAmount;
    }

    public void setBaseDiscountTaxCompensationAmount(String baseDiscountTaxCompensationAmount) {
        this.baseDiscountTaxCompensationAmount = baseDiscountTaxCompensationAmount;
    }

    public String getShippingDiscountTaxCompensationAmount() {
        return shippingDiscountTaxCompensationAmount;
    }

    public void setShippingDiscountTaxCompensationAmount(String shippingDiscountTaxCompensationAmount) {
        this.shippingDiscountTaxCompensationAmount = shippingDiscountTaxCompensationAmount;
    }

    public Object getBaseShippingDiscountTaxCompensationAmnt() {
        return baseShippingDiscountTaxCompensationAmnt;
    }

    public void setBaseShippingDiscountTaxCompensationAmnt(Object baseShippingDiscountTaxCompensationAmnt) {
        this.baseShippingDiscountTaxCompensationAmnt = baseShippingDiscountTaxCompensationAmnt;
    }

    public Object getDiscountTaxCompensationInvoiced() {
        return discountTaxCompensationInvoiced;
    }

    public void setDiscountTaxCompensationInvoiced(Object discountTaxCompensationInvoiced) {
        this.discountTaxCompensationInvoiced = discountTaxCompensationInvoiced;
    }

    public Object getBaseDiscountTaxCompensationInvoiced() {
        return baseDiscountTaxCompensationInvoiced;
    }

    public void setBaseDiscountTaxCompensationInvoiced(Object baseDiscountTaxCompensationInvoiced) {
        this.baseDiscountTaxCompensationInvoiced = baseDiscountTaxCompensationInvoiced;
    }

    public Object getDiscountTaxCompensationRefunded() {
        return discountTaxCompensationRefunded;
    }

    public void setDiscountTaxCompensationRefunded(Object discountTaxCompensationRefunded) {
        this.discountTaxCompensationRefunded = discountTaxCompensationRefunded;
    }

    public Object getBaseDiscountTaxCompensationRefunded() {
        return baseDiscountTaxCompensationRefunded;
    }

    public void setBaseDiscountTaxCompensationRefunded(Object baseDiscountTaxCompensationRefunded) {
        this.baseDiscountTaxCompensationRefunded = baseDiscountTaxCompensationRefunded;
    }

    public String getShippingInclTax() {
        return shippingInclTax;
    }

    public void setShippingInclTax(String shippingInclTax) {
        this.shippingInclTax = shippingInclTax;
    }

    public String getBaseShippingInclTax() {
        return baseShippingInclTax;
    }

    public void setBaseShippingInclTax(String baseShippingInclTax) {
        this.baseShippingInclTax = baseShippingInclTax;
    }

    public Object getCouponRuleName() {
        return couponRuleName;
    }

    public void setCouponRuleName(Object couponRuleName) {
        this.couponRuleName = couponRuleName;
    }

    public String getPaypalIpnCustomerNotified() {
        return paypalIpnCustomerNotified;
    }

    public void setPaypalIpnCustomerNotified(String paypalIpnCustomerNotified) {
        this.paypalIpnCustomerNotified = paypalIpnCustomerNotified;
    }

    public Object getGiftMessageId() {
        return giftMessageId;
    }

    public void setGiftMessageId(Object giftMessageId) {
        this.giftMessageId = giftMessageId;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getDeliveryComment() {
        return deliveryComment;
    }

    public void setDeliveryComment(String deliveryComment) {
        this.deliveryComment = deliveryComment;
    }

    public String getFeeAmount() {
        return feeAmount;
    }

    public void setFeeAmount(String feeAmount) {
        this.feeAmount = feeAmount;
    }

    public String getBaseFeeAmount() {
        return baseFeeAmount;
    }

    public void setBaseFeeAmount(String baseFeeAmount) {
        this.baseFeeAmount = baseFeeAmount;
    }

    public String getFeeAmountRefunded() {
        return feeAmountRefunded;
    }

    public void setFeeAmountRefunded(String feeAmountRefunded) {
        this.feeAmountRefunded = feeAmountRefunded;
    }

    public String getBaseFeeAmountRefunded() {
        return baseFeeAmountRefunded;
    }

    public void setBaseFeeAmountRefunded(String baseFeeAmountRefunded) {
        this.baseFeeAmountRefunded = baseFeeAmountRefunded;
    }

    public String getFeeAmountInvoiced() {
        return feeAmountInvoiced;
    }

    public void setFeeAmountInvoiced(String feeAmountInvoiced) {
        this.feeAmountInvoiced = feeAmountInvoiced;
    }

    public String getBaseFeeAmountInvoiced() {
        return baseFeeAmountInvoiced;
    }

    public void setBaseFeeAmountInvoiced(String baseFeeAmountInvoiced) {
        this.baseFeeAmountInvoiced = baseFeeAmountInvoiced;
    }

    public String getMetroTimeSlot() {
        return metroTimeSlot;
    }

    public void setMetroTimeSlot(String metroTimeSlot) {
        this.metroTimeSlot = metroTimeSlot;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public BAddress getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(BAddress billingAddress) {
        this.billingAddress = billingAddress;
    }

    public SAddress getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(SAddress shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public ArrayList<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(ArrayList<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    public Total getTotal() {
        return total;
    }

    public void setTotal(Total total) {
        this.total = total;
    }

}
