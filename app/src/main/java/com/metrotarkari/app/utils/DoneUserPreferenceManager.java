package com.metrotarkari.app.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by samrakchan on 5/18/17.
 */

public class DoneUserPreferenceManager {
    private static final String SHARED_NAME = "DoneUserPreferenceManager";

    private static final String WALK_THROUGH_PREFERENCE = "DoneWalkThroughPreference";

    private static final String USER_SKIP_LOGIN = "DoneUserSkipLogin";

    private static final String USER_LOGGED_IN = "IsDoneUserLoggedIn";

    private static final String FCM_TOKEN = "fcm_token";

    private static final String USER_NAME = "username";

    private static final String PASSWORD = "password";

    private static final String IS_B2B_USER = "isB2bUser";

    private static final String APP_VERSION_CODE = "AppVersionCode";

    private SharedPreferences mSharedPreferences;


    public DoneUserPreferenceManager(Context context){
        mSharedPreferences = context.getSharedPreferences(SHARED_NAME, Context.MODE_PRIVATE);
    }

    public void setWalkThroughPreference(boolean enabled){
        mSharedPreferences.edit().putBoolean(WALK_THROUGH_PREFERENCE, enabled).apply();
    }

    public boolean getHideWalkThroughPreference(){
        return mSharedPreferences.getBoolean(WALK_THROUGH_PREFERENCE, true);
    }

    public void setUserSkipLogin(boolean skipped){
        mSharedPreferences.edit().putBoolean(USER_SKIP_LOGIN, skipped).apply();
    }

    public void setLoggedInUser(boolean signup){
        mSharedPreferences.edit().putBoolean(USER_LOGGED_IN, signup).apply();
    }

    public void clearAllData(){
        mSharedPreferences.edit().clear().apply();
    }

    public boolean isLoggedInUser(){
        return mSharedPreferences.getBoolean(USER_LOGGED_IN, false);
    }

    public boolean getUserSkipLogin(){
        return mSharedPreferences.getBoolean(USER_SKIP_LOGIN, false);
    }

    public String getFcmToken() {
        return mSharedPreferences.getString(FCM_TOKEN, "");
    }

    public void setFcmToken(String token){
        mSharedPreferences.edit().putString(FCM_TOKEN, token).apply();
    }

    public String getUsername() {
        return mSharedPreferences.getString(USER_NAME, "");
    }

    public void setUsername(String username){
        mSharedPreferences.edit().putString(USER_NAME, username).apply();
    }

    public String getPassword() {
        return mSharedPreferences.getString(PASSWORD, "");
    }

    public void setPassword(String username){
        mSharedPreferences.edit().putString(PASSWORD, username).apply();
    }

    public int getAppVersionCode() {
        return mSharedPreferences.getInt(APP_VERSION_CODE, -1);
    }

    public void setAppVersionCode(int appVersionCode){
        mSharedPreferences.edit().putInt(APP_VERSION_CODE, appVersionCode).apply();
    }

    public boolean isB2bUser() {
        return mSharedPreferences.getBoolean(IS_B2B_USER, true);
    }

    public void setB2bUser(boolean isB2bUser){
        mSharedPreferences.edit().putBoolean(IS_B2B_USER, isB2bUser).apply();
    }
}
