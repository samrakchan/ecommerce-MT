
package com.metrotarkari.app.ui.order.orderdetail.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductOptions {

    @SerializedName("info_buyRequest")
    @Expose
    private InfoBuyRequest infoBuyRequest;

    public InfoBuyRequest getInfoBuyRequest() {
        return infoBuyRequest;
    }

    public void setInfoBuyRequest(InfoBuyRequest infoBuyRequest) {
        this.infoBuyRequest = infoBuyRequest;
    }

}
