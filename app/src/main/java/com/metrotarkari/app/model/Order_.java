package com.metrotarkari.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by samrakchan on 6/12/17.
 */

public class Order_  {

    @SerializedName("invoice_number")
    @Expose
    private String invoiceNumber;

    @SerializedName("payment_method")
    @Expose
    private String paymentMethod;

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

}