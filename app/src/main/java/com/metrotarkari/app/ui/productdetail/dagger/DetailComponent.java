package com.metrotarkari.app.ui.productdetail.dagger;

import com.metrotarkari.app.application.dagger.AppComponent;
import com.metrotarkari.app.ui.productdetail.DetailActivity;

import dagger.Component;

@DetailScope
@Component(modules = { DetailModule.class }, dependencies = AppComponent.class)
public interface DetailComponent {

  void inject(DetailActivity detailActivity);

}
