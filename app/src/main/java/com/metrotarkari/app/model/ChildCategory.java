
package com.metrotarkari.app.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChildCategory implements Parcelable
{

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("nav_type")
    @Expose
    private String navType;

    @SerializedName("additional_image")
    @Expose
    private String image;


    public final static Parcelable.Creator<ChildCategory> CREATOR = new Creator<ChildCategory>() {


        @SuppressWarnings({
                "unchecked"
        })
        public ChildCategory createFromParcel(Parcel in) {
            ChildCategory instance = new ChildCategory();
            instance.name = ((String) in.readValue((String.class.getClassLoader())));
            instance.id = ((String) in.readValue((String.class.getClassLoader())));
            instance.navType = ((String) in.readValue((String.class.getClassLoader())));
            instance.image = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public ChildCategory[] newArray(int size) {
            return (new ChildCategory[size]);
        }

    }
            ;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNavType() {
        return navType;
    }

    public void setNavType(String navType) {
        this.navType = navType;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }


    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(name);
        dest.writeValue(id);
        dest.writeValue(navType);
        dest.writeValue(image);
    }

    public int describeContents() {
        return 0;
    }

}