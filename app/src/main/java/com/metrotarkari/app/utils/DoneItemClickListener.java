package com.metrotarkari.app.utils;

/**
 * Created by samrakchan on 6/9/17.
 */

public interface DoneItemClickListener {
    void onItemClickListener(int position, int viewId, Object object);

}
