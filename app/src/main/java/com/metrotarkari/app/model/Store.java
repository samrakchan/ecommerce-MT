
package com.metrotarkari.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Store {

    @SerializedName("group_id")
    @Expose
    private String groupId;
    @SerializedName("website_id")
    @Expose
    private String websiteId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("root_category_id")
    @Expose
    private String rootCategoryId;
    @SerializedName("default_store_id")
    @Expose
    private String defaultStoreId;
    @SerializedName("storeviews")
    @Expose
    private Storeviews storeviews;

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getWebsiteId() {
        return websiteId;
    }

    public void setWebsiteId(String websiteId) {
        this.websiteId = websiteId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRootCategoryId() {
        return rootCategoryId;
    }

    public void setRootCategoryId(String rootCategoryId) {
        this.rootCategoryId = rootCategoryId;
    }

    public String getDefaultStoreId() {
        return defaultStoreId;
    }

    public void setDefaultStoreId(String defaultStoreId) {
        this.defaultStoreId = defaultStoreId;
    }

    public Storeviews getStoreviews() {
        return storeviews;
    }

    public void setStoreviews(Storeviews storeviews) {
        this.storeviews = storeviews;
    }

}
