package com.metrotarkari.app.ui.forgetpassword.dragger;

import javax.inject.Scope;

@Scope
public @interface ForgetPasswordScope {
}
