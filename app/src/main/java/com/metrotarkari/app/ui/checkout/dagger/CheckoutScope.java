package com.metrotarkari.app.ui.checkout.dagger;

import javax.inject.Scope;

@Scope
public @interface CheckoutScope {
}
