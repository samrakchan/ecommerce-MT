package com.metrotarkari.app.ui.login.dagger;

import javax.inject.Scope;

@Scope
public @interface LoginScope {
}
