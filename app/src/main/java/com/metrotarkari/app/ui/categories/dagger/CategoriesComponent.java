package com.metrotarkari.app.ui.categories.dagger;

import com.metrotarkari.app.application.dagger.AppComponent;
import com.metrotarkari.app.ui.categories.CategoriesFragment;

import dagger.Component;

@CategoriesScope
@Component(modules = { CategoriesModule.class }, dependencies = AppComponent.class)
public interface CategoriesComponent {

  void inject(CategoriesFragment categoriesFragment);

}
