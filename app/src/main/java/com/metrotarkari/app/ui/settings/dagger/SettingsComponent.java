package com.metrotarkari.app.ui.settings.dagger;

import com.metrotarkari.app.application.dagger.AppComponent;
import com.metrotarkari.app.ui.settings.SettingsActivity;

import dagger.Component;

@SettingsScope
@Component(modules = { SettingsModule.class }, dependencies = AppComponent.class)
public interface SettingsComponent {

  void inject(SettingsActivity activity);

}
