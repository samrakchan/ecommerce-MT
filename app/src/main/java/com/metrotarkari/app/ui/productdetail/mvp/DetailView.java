package com.metrotarkari.app.ui.productdetail.mvp;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.ColorUtils;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.google.gson.Gson;
import com.metrotarkari.app.R;
import com.metrotarkari.app.application.DoneApplication;
import com.metrotarkari.app.model.CartQuote;
import com.metrotarkari.app.model.CustomOption;
import com.metrotarkari.app.model.Detail;
import com.metrotarkari.app.model.Product_;
import com.metrotarkari.app.model.Tier;
import com.metrotarkari.app.ui.base.BaseView;
import com.metrotarkari.app.ui.productdetail.DetailActivity;
import com.metrotarkari.app.ui.reviewandratings.reviewlist.ReviewRatingsActivity;
import com.metrotarkari.app.ui.reviewandratings.reviewpost.ReviewPostActivity;
import com.metrotarkari.app.utils.Constant;
import com.metrotarkari.app.utils.DoneItemClickListener;

import java.util.List;

import butterknife.BindView;
import io.reactivex.subjects.BehaviorSubject;

/**
 * Created by samrakchan on 4/27/17.
 */

public class DetailView extends BaseView implements DoneItemClickListener{

    @BindView(R.id.detailRv)
    protected RecyclerView detailRv;

    @BindView(R.id.progressBarRl)
    protected View progressBarRl;

    @BindView(R.id.imageMsgRl)
    protected View noItemView;

    @BindView(R.id.imageMsgNoItemTv)
    protected AppCompatTextView imageMsgNoItemTv;

    private DetailAdapter detailAdapter;
    private Detail detail;
    private DetailActivity activity;

    private final BehaviorSubject<CartQuote> cartQuoteBehaviorSubject = BehaviorSubject.create();

    private Drawable backArrowDrawable;
    private int menuColor = R.color.colorGrey;
    private int currentMenuColor = 0;
    private int menuSize = 0;

    private int mTotalScroll = 0;

    public DetailView(@NonNull DetailActivity activity) {
        super(activity);
        this.activity = activity;
        this.activity.getSupportActionBar().setTitle("");
        detailAdapter = new DetailAdapter(getContext());
        detailAdapter.setListener(this);
        detailRv.setLayoutManager(new LinearLayoutManager(getContext()));
        detailRv.setAdapter(detailAdapter);

        backArrowDrawable = toolbar.getNavigationIcon();

        detailRv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

            }
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                Log.i("DxDy", "DxDy: "+dx+" "+dy);

                mTotalScroll += dy;
                int alfa = 0;
                if(mTotalScroll>255){
                    alfa = 255;
                }else if(mTotalScroll<0){
                    alfa = 0;
                }else{
                    alfa = mTotalScroll;
                }

                int color = ColorUtils.setAlphaComponent(Color.parseColor("#21920f"),alfa);
                //double toolbarAlpha = scale(intColorCode, 0, 255, 0, 1);
                //Log.i("FLOAT VALUE", toolbarAlpha+" sx"+scrollY+" osx"+oldScrollY+" intColorCode "+intColorCode);
                toolbar.setBackgroundColor(color);

                if(mTotalScroll>=100){
                    menuColor = R.color.colorWhite;
                }else{
                    menuColor = R.color.colorGrey;
                }
                changeMenuIconColor(menuColor);
            }
        });
    }

    public void changeMenuIconColor(int color){
        if(activity.getMenu()!=null) {
            menuSize = this.activity.getMenu().size();

            if (currentMenuColor != color) {
                for (int i = 0; i < menuSize; i++) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        activity.getMenu().getItem(i).getIcon().setTint(ContextCompat.getColor(activity, color));//.setColorFilter(ContextCompat.getColor(activity, color), PorterDuff.Mode.SRC_ATOP);
                    }else{
                        boolean isCartIcon = (activity.getMenu().getItem(i).getItemId() == R.id.action_cart);
                        if(isCartIcon) {
                            //activity.getMenu().getItem(i).getIcon().mutate().setColorFilter(ContextCompat.getColor(getContext(), color), PorterDuff.Mode.SRC_IN);
                            Drawable drawable = ContextCompat.getDrawable(getContext(), R.mipmap.ic_toolbar_cart);
                            drawable.setColorFilter(ContextCompat.getColor(activity, color), PorterDuff.Mode.SRC_ATOP);
                            activity.getMenu().getItem(i).setIcon(drawable);

                        }else{
                            activity.getMenu().getItem(i).getIcon().mutate().setColorFilter(ContextCompat.getColor(getContext(), color), PorterDuff.Mode.SRC_IN);
                        }
                    }
                    backArrowDrawable.setColorFilter(ContextCompat.getColor(activity, color), PorterDuff.Mode.SRC_ATOP);
                }
                currentMenuColor = color;
                activity.updateCartInMenu(DoneApplication.getInstance().getCartCount());
            }
        }
    }

    @Override
    public int getContentLayout() {
        return R.layout.activity_item_detail;
    }

    public BehaviorSubject<CartQuote> getCartQuoteBehaviorSubject(){
        return cartQuoteBehaviorSubject;
    }

    public void showProgressBar(boolean showItem){
        if(showItem)
            progressBarRl.setVisibility(View.VISIBLE);
        else
            progressBarRl.setVisibility(View.GONE);
    }

    public void showNoItem(boolean showing,boolean fromNormalOrFromException){
        if(showing) {
            noItemView.setVisibility(View.VISIBLE);
            if (fromNormalOrFromException){
                imageMsgNoItemTv.setText(R.string.sww);
            }
        }
        else {
            noItemView.setVisibility(View.GONE);
        }
    }

    public void setDetail(Detail detail) {
        this.detail = detail;
        Product_ product_ = detail.getProduct();
        if(product_!=null) {
            detailAdapter.addData(product_);
            if (product_.getFormattedTierPrice() != null) {
                for (int i = 0; i < product_.getFormattedTierPrice().size(); i++) {
                    detailAdapter.addData(new Tier(product_.getFormattedTierPrice().get(i)));
                }
            }

            if(product_.getAppOptions() != null && product_.getAppOptions().getCustomOptions() != null) {
                List<CustomOption> list = product_.getAppOptions().getCustomOptions();
                for (CustomOption co : list) {
                    detailAdapter.addData(co);
                }
            }

            detailAdapter.addData("AddToCart");

            detailAdapter.addData(detail.getProduct().getAppReviews());

            if(product_.getProductRelated() != null && product_.getProductRelated().size() > 0) {
                detailAdapter.addData(product_.getProductRelated(), getContext().getString(R.string.related_product));
            }

            if(product_.getProductUpsell() != null && product_.getProductUpsell().size() > 0) {
                detailAdapter.addData(product_.getProductUpsell(), getContext().getString(R.string.you_may_also_like));
            }

            if(product_.getDescription()!=null && !product_.getDescription().isEmpty()) {
                detailAdapter.addData(product_.getDescription());
            }

            detailAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onItemClickListener(int position, int viewId, Object object) {
        if(viewId == R.id.listItemShareFAB){
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_SUBJECT, getContext().getString(R.string.sharing_product));
            intent.putExtra(Intent.EXTRA_TEXT, Constant.DEEP_LINK_PREFIX+detail.getProduct().getEntityId()+Constant.DEEP_LINK_SUFFIX);
            activity.startActivity(Intent.createChooser(intent, getContext().getString(R.string.share_product)));
        }else if(viewId == R.id.itemDetailAddToCartBtn){
            CartQuote cartQuote = new CartQuote();
            cartQuote.setQty(String.valueOf((int)object));
            cartQuote.setProduct(detail.getProduct().getEntityId());
            cartQuoteBehaviorSubject.onNext(cartQuote);
        }else if(viewId == R.id.itemDetailAppReviewCv){
            Gson gson = new Gson();
            String product =  gson.toJson(detail.getProduct(), Product_.class);
            if(detail.getProduct().getAppReviews().getNumber() == 0){
                Intent intentToReviewPost = new Intent(getContext(), ReviewPostActivity.class);
                intentToReviewPost.putExtra(Constant.PRODUCT_DETAIL,product);
                activity.startActivity(intentToReviewPost);
            }else {
                Intent intent = new Intent(getContext(), ReviewRatingsActivity.class);
                intent.putExtra(Constant.PRODUCT_DETAIL, product);
                activity.startActivity(intent);
            }
        }
    }

    public int getMenuColor(){
        if(mTotalScroll>=100){
            currentMenuColor = R.color.colorWhite;
           return menuColor = R.color.colorWhite;
        }else{
            currentMenuColor = R.color.colorGrey;
          return menuColor = R.color.colorGrey;
        }
    }
}
