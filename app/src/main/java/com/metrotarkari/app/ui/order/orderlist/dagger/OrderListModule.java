package com.metrotarkari.app.ui.order.orderlist.dagger;

import com.metrotarkari.app.application.network.DoneNetwork;
import com.metrotarkari.app.ui.order.orderlist.OrderListActivity;
import com.metrotarkari.app.ui.order.orderlist.mvp.OrderListModel;
import com.metrotarkari.app.ui.order.orderlist.mvp.OrderListPresenter;
import com.metrotarkari.app.ui.order.orderlist.mvp.OrderListView;

import dagger.Module;
import dagger.Provides;

@Module
public class OrderListModule {

  private final OrderListActivity activity;

  public OrderListModule(OrderListActivity activity) {
    this.activity = activity;
  }

  @Provides
  @OrderListScope
  public OrderListView view() {
    return new OrderListView(activity);
  }

  @Provides
  @OrderListScope
  public OrderListModel model(DoneNetwork doneNetwork){
    return new OrderListModel(activity, doneNetwork);
  }

  @Provides
  @OrderListScope
  public OrderListPresenter orderListPresenter(OrderListView homeView, OrderListModel model) {
    return new OrderListPresenter(homeView, model);
  }

}
