
package com.metrotarkari.app.ui.checkout.model;


public class PMethod {

    private String method;

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

}
