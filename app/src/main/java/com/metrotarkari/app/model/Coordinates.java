package com.metrotarkari.app.model;

import com.google.auto.value.AutoValue;
import com.google.gson.TypeAdapter;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;
import com.google.gson.Gson;

@Generated("com.robohorse.robopojogenerator")
@AutoValue
public abstract class Coordinates{

	@SerializedName("x")
	public abstract double X();

	@SerializedName("y")
	public abstract double Y();

	public static TypeAdapter<Coordinates> typeAdapter(Gson gson) {
		return new AutoValue_Coordinates.GsonTypeAdapter(gson);
	}
}