package com.metrotarkari.app.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.bignerdranch.expandablerecyclerview.model.Parent;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by samrakchan on 6/6/17.
 */

public class LayerFilter implements Parent<Filter>, Parcelable {
    @SerializedName("attribute")
    @Expose
    private String attribute;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("filter")
    @Expose
    private List<Filter> filter = null;

    private boolean initiallyExpended = false;



    public final static Parcelable.Creator<LayerFilter> CREATOR = new Creator<LayerFilter>() {
        @SuppressWarnings({
                "unchecked"
        })
        public LayerFilter createFromParcel(Parcel in) {
            LayerFilter instance = new LayerFilter();
            instance.attribute = ((String) in.readValue((String.class.getClassLoader())));
            instance.title = ((String) in.readValue((String.class.getClassLoader())));
            in.readList(instance.filter, (Filter.class.getClassLoader()));
            instance.initiallyExpended = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
            return instance;
        }

        public LayerFilter[] newArray(int size) {
            return (new LayerFilter[size]);
        }

    }
            ;

    public String getAttribute() {
        return attribute;
    }

    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setFilter(List<Filter> filter) {
        this.filter = filter;
    }

    public void setInitiallyExpended(boolean initiallyExpended) {
        this.initiallyExpended = initiallyExpended;
    }

    public String getFilterTitle(int position){
        return title;
    }

    @Override
    public List<Filter> getChildList() {
        return filter;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return initiallyExpended;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(attribute);
        dest.writeValue(title);
        dest.writeValue(initiallyExpended);
        dest.writeList(filter);
    }

    public int describeContents() {
        return 0;
    }

}
