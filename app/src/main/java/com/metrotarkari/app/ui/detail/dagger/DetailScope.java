package com.metrotarkari.app.ui.detail.dagger;

import javax.inject.Scope;

@Scope
public @interface DetailScope {
}
