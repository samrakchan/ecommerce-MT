package com.metrotarkari.app.ui.addressbook.addresslist.mvp;

import android.content.Context;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metrotarkari.app.R;
import com.metrotarkari.app.model.BAddress;
import com.metrotarkari.app.utils.DoneItemClickListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Avinash on 6/21/2017.
 */

public class AddressListAdapter extends RecyclerView.Adapter<AddressListAdapter.AddressBookViewHolder>{
    private List<BAddress> mData;
    private Context mContext;
    private DoneItemClickListener listener;


    public AddressListAdapter(Context mContext, DoneItemClickListener listener) {
        mData = new ArrayList<>();
        this.listener = listener;
        this.mContext = mContext;
    }

    public void addData(List<BAddress> datas){
        this.mData.clear();
        this.mData.addAll(datas);
        notifyDataSetChanged();
    }

    @Override
    public AddressListAdapter.AddressBookViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_address_book_child_layout,parent,false);
            return new AddressListAdapter.AddressBookViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AddressBookViewHolder holder, int position) {
        BAddress bAddress = mData.get(position);

        holder.customerAddressBookNameTv.setText(bAddress.getFirstname()+" "+bAddress.getLastname());
        holder.customerAddressBookTv.setText(
                        (TextUtils.isEmpty(bAddress.getEmail())?"":bAddress.getEmail())
                                +"\n"+ (TextUtils.isEmpty(bAddress.getCompany())?"":bAddress.getCompany())
                        +"\n"+ (TextUtils.isEmpty(bAddress.getStreet())?"":bAddress.getStreet())
                        +"\n"+ (TextUtils.isEmpty(bAddress.getCusArea())?"":bAddress.getCusArea())
                        +"\n"+ (TextUtils.isEmpty(bAddress.getCity())?"":bAddress.getCity())
                        +"\n"+ (TextUtils.isEmpty(bAddress.getRegionCode())?"":bAddress.getRegionCode())
                        +"\n"+ (TextUtils.isEmpty(bAddress.getCountryName())?"":bAddress.getCountryName())
                        +"\n"+ mContext.getString(R.string.postal_code)
                        +" "+(TextUtils.isEmpty(bAddress.getPostcode())?"":bAddress.getPostcode())
                        +"\n"+ (TextUtils.isEmpty(bAddress.getTelephone())?"":bAddress.getTelephone()));
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    protected class AddressBookViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private AppCompatTextView customerAddressBookNameTv;
        private AppCompatTextView customerAddressBookTv;
        private AppCompatButton customerAddressBookDeleteBtn;
        private CardView addressBookCardView;
        private AppCompatButton customerAddressBookEditBtn;

        public AddressBookViewHolder(View itemView) {
            super(itemView);
            customerAddressBookNameTv = (AppCompatTextView) itemView.findViewById(R.id.customerAddressBookNameTv);
            customerAddressBookTv = (AppCompatTextView) itemView.findViewById(R.id.customerAddressBookTv);
            customerAddressBookDeleteBtn = (AppCompatButton) itemView.findViewById(R.id.customerAddressBookDeleteBtn);
            addressBookCardView = (CardView) itemView.findViewById(R.id.addressBookCardView);
            customerAddressBookEditBtn = (AppCompatButton) itemView.findViewById(R.id.customerAddressBookEditBtn);

            customerAddressBookDeleteBtn.setOnClickListener(this);
            customerAddressBookEditBtn.setOnClickListener(this);
            addressBookCardView.setOnClickListener(this);
               /* try {
                    Intent intent = new Intent(mContext, DetailActivity.class);
                    Product product = (Product) mData.get(getAdapterPosition());
                    intent.putExtra("PRODUCT_ID", product.getEntityId()+"");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(intent);
                }catch (ClassCastException|NullPointerException e){
                    e.printStackTrace();
                }*/
        }

        @Override
        public void onClick(View v) {
            BAddress address = mData.get(getAdapterPosition());
            if (v.getId() == R.id.customerAddressBookDeleteBtn){
                listener.onItemClickListener(Integer.parseInt(address.getEntityId()),v.getId(),address);
            }else if (v.getId() == R.id.customerAddressBookEditBtn){
                listener.onItemClickListener(getAdapterPosition(),v.getId(),address);
            }else if (v.getId() == R.id.addressBookCardView){
                listener.onItemClickListener(getAdapterPosition(),v.getId(),address);
            }
        }
    }
}
