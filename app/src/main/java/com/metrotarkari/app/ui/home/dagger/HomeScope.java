package com.metrotarkari.app.ui.home.dagger;

import javax.inject.Scope;

@Scope
public @interface HomeScope {
}
