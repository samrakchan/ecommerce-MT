package com.metrotarkari.app.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import com.metrotarkari.app.model.Area;
import com.metrotarkari.app.model.City;
import com.metrotarkari.app.model.Country;
import com.metrotarkari.app.model.Region;
import com.metrotarkari.app.ui.recentsearch.model.RecentSearch;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by samrakchan on 6/13/17.
 */

public class DoneDbAdapter {
    public static final String DB_NAME = "mt_done";
    private static final int DB_VERSION = 7;

    private static final String TABLE_AREA = "AREA";
    private static final String TABLE_CITY = "CITY";
    private static final String TABLE_REGION = "REGION";
    private static final String TABLE_COUNTRY = "COUNTRY";

    private static final String TABLE_STORE_VIEW = "STORE_VIEW";
    private static final String TABLE_CUSTOMER = "CUSTOMER";
    private static final String TABLE_HOME = "HOME";

    private static final String TABLE_RECENT_SEARCH = "SEARCH";

    private static final String COL_ID = "_id";
    private static final String COL_CODE = "CODE";
    private static final String COL_NAME = "NAME";
    private static final String COL_COUNTRY_ID = "COUNTRY_ID";
    private static final String COL_REGION_ID = "REGION_ID";
    private static final String COL_CITY_ID = "CITY_ID";
    private static final String COL_JSON = "COL_JSON";
    private static final String COL_REECENT_SEARCH_NAME = "COL_RECENT_NAME";
    private static final String COL_DATE_TIME_RECENT_SEARCH = "DATE_TIME_RECENT_SEARCH";


    private static final String CREATE_TABLE_COUNTRY = "CREATE TABLE "+TABLE_COUNTRY+" ("
            +COL_ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+COL_CODE +" TEXT, "+COL_NAME
            +" TEXT)";

    private static final String CREATE_TABLE_SEARCH = "CREATE TABLE "+TABLE_RECENT_SEARCH+" ("
            +COL_ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+COL_REECENT_SEARCH_NAME +" TEXT NOT NULL UNIQUE, "+COL_DATE_TIME_RECENT_SEARCH
            +" DATETIME DEFAULT CURRENT_TIMESTAMP)";

   /*  private static final String CREATE_TABLE_SEARCH = "CREATE TABLE "+TABLE_RECENT_SEARCH+" ("
             +COL_ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+COL_REECENT_SEARCH_NAME +" TEXT, "+COL_DATE_TIME_RECENT_SEARCH
             +" DATETIME DEFAULT CURRENT_TIMESTAMP, "+" UNIQUE"+" ("+COL_ID+", "+COL_REECENT_SEARCH_NAME
                +")"+" ON CONFLICT REPLACE)";*/

    private static final String CREATE_TABLE_REGION = "CREATE TABLE "+TABLE_REGION+" ("
            +COL_ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+COL_CODE +" TEXT, "+COL_NAME
            +" TEXT, "+COL_COUNTRY_ID+" TEXT)";

    private static final String CREATE_TABLE_CITY = "CREATE TABLE "+TABLE_CITY+" ("
            +COL_ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+COL_CODE +" TEXT, "+COL_NAME
            +" TEXT, "+COL_REGION_ID+" TEXT)";

    private static final String CREATE_TABLE_AREA = "CREATE TABLE "+TABLE_AREA+" ("
            +COL_ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+COL_CODE +" TEXT, "+COL_NAME
            +" TEXT, "+COL_CITY_ID+" TEXT)";

    private static final String CREATE_TABLE_CONFIG = "CREATE TABLE "+ TABLE_STORE_VIEW +" ("+COL_JSON+" TEXT)";
    private static final String CREATE_TABLE_CUSTOMER = "CREATE TABLE "+TABLE_CUSTOMER+" ("+COL_JSON+" TEXT)";
    private static final String CREATE_TABLE_HOME = "CREATE TABLE "+TABLE_HOME+" ("+COL_JSON+" TEXT)";


    private MetroDbOpenHelper mHelper;
    private SQLiteDatabase db;

    public DoneDbAdapter(Context context){
        mHelper = new MetroDbOpenHelper(context);
    }

    public void openWritableDb(){
        db = mHelper.getWritableDatabase();
    }

    public void openReadableDb(){
        db = mHelper.getReadableDatabase();
    }

    public void closeDb(){
        db.close();
    }

    public long insertIntoCountry(Country country){
        ContentValues cv = new ContentValues();
        cv.put(COL_CODE, country.getCode());
        cv.put(COL_NAME, country.getName());

        return db.insert(TABLE_COUNTRY, COL_NAME, cv);
    }

    public long insertIntoRegion(Region region, String countryCode){
        ContentValues cv = new ContentValues();
        cv.put(COL_CODE, region.getCode());
        cv.put(COL_NAME, region.getName());
        cv.put(COL_COUNTRY_ID, countryCode);

        return db.insert(TABLE_REGION, COL_NAME, cv);
    }

    public long insertIntoCity(City city, String regionCode){
        ContentValues cv = new ContentValues();
        cv.put(COL_CODE, city.getCode());
        cv.put(COL_NAME, city.getName());
        cv.put(COL_REGION_ID, regionCode);

        return db.insert(TABLE_CITY, COL_NAME, cv);
    }


    public long insertIntoArea(Area area, String cityCode){
        ContentValues cv = new ContentValues();
        cv.put(COL_CODE, area.getCode());
        cv.put(COL_NAME, area.getName());
        cv.put(COL_CITY_ID, cityCode);

        return db.insert(TABLE_AREA, COL_NAME, cv);
    }

    public long insetIntoStoreView(String configResponseInJson){
        ContentValues cv = new ContentValues();
        cv.put(COL_JSON, configResponseInJson);

        return db.replace(TABLE_STORE_VIEW, COL_JSON, cv);
    }

    public long insetIntoHome(String homeJsonResponse){
        ContentValues cv = new ContentValues();
        cv.put(COL_JSON, homeJsonResponse);

        return db.replace(TABLE_HOME, COL_JSON, cv);
    }

    public long insertInToCustomer(String customerResponseInJson){
        ContentValues cv = new ContentValues();
        cv.put(COL_JSON, customerResponseInJson);

        return db.replace(TABLE_CUSTOMER, COL_JSON, cv);
    }

    public long insertInToResentSearch(String recentSearchName){
        ContentValues cv = new ContentValues();
        cv.put(COL_REECENT_SEARCH_NAME,recentSearchName);

        return  db.insertWithOnConflict(TABLE_RECENT_SEARCH,null,cv,db.CONFLICT_REPLACE);
    }

    public String getStoreView(){
        try {
            Cursor cu = db.query(TABLE_STORE_VIEW, null, null, null, null, null, null);
            if (cu.moveToFirst()) {
                return cu.getString(cu.getColumnIndex(COL_JSON));
            }
        } catch (SQLiteException e){
            e.printStackTrace();
            return "";
        }
        return "";
    }

    public String getHome(){
        try {
            Cursor cu = db.query(TABLE_HOME, null, null, null, null, null, null);
            if (cu.moveToFirst()) {
                return cu.getString(cu.getColumnIndex(COL_JSON));
            }
        }catch (SQLiteException e){
            e.printStackTrace();
            return "";
        }
        return "";
    }

    public String getCustomer(){
        try {
            Cursor cu = db.query(TABLE_CUSTOMER, null, null, null, null, null, null);
            if (cu.moveToFirst()) {
                return cu.getString(cu.getColumnIndex(COL_JSON));
            }
        }catch (SQLiteException e){
            e.printStackTrace();
            return "";
        }
        return "";
    }

    public List<Country> getCountries(){
        List<Country> countries = new ArrayList<>();
        Cursor cu = db.query(TABLE_COUNTRY, null, null, null, null, null, null);
        if(cu.moveToFirst()){
            do{
                Country country = new Country();
                country.setCode(cu.getString(cu.getColumnIndex(COL_CODE)));
                country.setName(cu.getString(cu.getColumnIndex(COL_NAME)));

                countries.add(country);

            }while (cu.moveToNext());
        }
        return countries;
    }


    public List<RecentSearch> getRecentSearchData(){
        List<RecentSearch> recentSearchNameList = new ArrayList<>();
        Cursor cu = db.query(TABLE_RECENT_SEARCH, null, null, null, null, null, null);
            if (cu.moveToFirst()) {
                do {
                    RecentSearch recentSearch = new RecentSearch();
                    recentSearch.setRecentSearchItemName(cu.getString(cu.getColumnIndex(COL_REECENT_SEARCH_NAME)));
                    recentSearchNameList.add(recentSearch);
                } while (cu.moveToNext());
            }

        return recentSearchNameList;
    }

    public List<Region> getRegion(String countryCode){
        List<Region> regions = new ArrayList<>();
        Cursor cu = db.query(TABLE_REGION, null, COL_COUNTRY_ID+"=?", new String[]{countryCode+""}, null, null, null);
        if(cu.moveToFirst()){
            do{
                Region region = new Region();
                region.setCode(cu.getString(cu.getColumnIndex(COL_CODE)));
                region.setName(cu.getString(cu.getColumnIndex(COL_NAME)));

                regions.add(region);

            }while (cu.moveToNext());
        }
        return regions;
    }

    public List<City> getCity(int regionId){
        List<City> cities = new ArrayList<>();
        Cursor cu = db.query(TABLE_CITY, null, COL_REGION_ID+"=?", new String[]{regionId+""}, null, null, null);
        if(cu.moveToFirst()){
            do{
                City city = new City();
                city.setCode(cu.getString(cu.getColumnIndex(COL_CODE)));
                city.setName(cu.getString(cu.getColumnIndex(COL_NAME)));

                cities.add(city);

            }while (cu.moveToNext());
        }
        return cities;
    }

    public List<Area> getArea(int cityId){
        List<Area> areas = new ArrayList<>();
        Cursor cu = db.query(TABLE_CITY, null, COL_CITY_ID+"=?", new String[]{cityId+""}, null, null, null);
        if(cu.moveToFirst()){
            do{
                Area area = new Area();
                area.setCode(cu.getString(cu.getColumnIndex(COL_CODE)));
                area.setName(cu.getString(cu.getColumnIndex(COL_NAME)));

                areas.add(area);

            }while (cu.moveToNext());
        }
        return areas;
    }

    public void removeAllRowsOfCountry(){
         db.execSQL("DELETE FROM "+ TABLE_COUNTRY);
    }

    public int deleteRecentSearchData(String recentSeachDataName){
        return db.delete(TABLE_RECENT_SEARCH, COL_REECENT_SEARCH_NAME + " = " + "'"+recentSeachDataName+"'", null);
    }

    public int deleteTheExtraData(int value){
        return db.delete(TABLE_RECENT_SEARCH,
                COL_ID+" IN (SELECT "+COL_ID+" FROM "+TABLE_RECENT_SEARCH+" ORDER BY "+COL_ID+" ASC LIMIT "+value+");", null);
    }

    public void removeAllSearchData(){
        db.execSQL("DELETE FROM "+ TABLE_RECENT_SEARCH);
    }

    public void removeAllRowsOfRegion(){
        db.execSQL("DELETE FROM "+ TABLE_REGION);
    }

    public void removeAllRowsOfArea(){
        db.execSQL("DELETE FROM "+ TABLE_AREA);
    }

    public void removeAllRowsOfCity(){
        db.execSQL("DELETE FROM "+ TABLE_CITY);
    }

    private class MetroDbOpenHelper extends SQLiteOpenHelper{

        public MetroDbOpenHelper(Context context) {
            super(context, DB_NAME, null, DB_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase sqLiteDatabase) {
            sqLiteDatabase.execSQL(CREATE_TABLE_COUNTRY);
            sqLiteDatabase.execSQL(CREATE_TABLE_REGION);
            sqLiteDatabase.execSQL(CREATE_TABLE_CITY);
            sqLiteDatabase.execSQL(CREATE_TABLE_AREA);
            sqLiteDatabase.execSQL(CREATE_TABLE_CONFIG);
            sqLiteDatabase.execSQL(CREATE_TABLE_CUSTOMER);
            sqLiteDatabase.execSQL(CREATE_TABLE_SEARCH);
            sqLiteDatabase.execSQL(CREATE_TABLE_HOME);
        }

        @Override
        public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
            if(oldVersion<newVersion){
                sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_COUNTRY);
                sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_REGION);
                sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_CITY);
                sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_AREA);
                sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_STORE_VIEW);
                sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_CUSTOMER);
                sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_RECENT_SEARCH);
                sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_HOME);


                sqLiteDatabase.execSQL(CREATE_TABLE_COUNTRY);
                sqLiteDatabase.execSQL(CREATE_TABLE_REGION);
                sqLiteDatabase.execSQL(CREATE_TABLE_CITY);
                sqLiteDatabase.execSQL(CREATE_TABLE_AREA);
                sqLiteDatabase.execSQL(CREATE_TABLE_CONFIG);
                sqLiteDatabase.execSQL(CREATE_TABLE_CUSTOMER);
                sqLiteDatabase.execSQL(CREATE_TABLE_SEARCH);
                sqLiteDatabase.execSQL(CREATE_TABLE_HOME);
            }

        }
    }
}
