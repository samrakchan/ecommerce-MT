package com.metrotarkari.app.ui.forgetpassword.mvp;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;

import com.jakewharton.rxbinding2.view.RxView;
import com.metrotarkari.app.R;
import com.metrotarkari.app.ui.login.LoginActivity2;
import com.metrotarkari.app.utils.AlertUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import cc.cloudist.acplibrary.ACProgressFlower;
import io.reactivex.Observable;

/**
 * Created by samrakchan on 4/27/17.
 */

public class ForgetPasswordView extends FrameLayout {

    @Nullable
    @BindView(R.id.input_email)
    AppCompatEditText input_email;

    @Nullable
    @BindView(R.id.forgetPasswordBtn)
    AppCompatButton forgetPasswordBtn;

    @BindView(R.id.loginCrossIv)
    AppCompatImageView loginCrossIv;

    @BindView(R.id.forgetPasswordEntryLl)
    View forgetPasswordEntryLl;

    @BindView(R.id.forgotPasswordLl)
    View forgotPasswordLl;

    @BindView(R.id.forgetPasswordEntryPasswordEt)
    AppCompatEditText forgetPasswordEntryPasswordEt;

    @BindView(R.id.forgetPasswordEntryConfirmPasswordEt)
    AppCompatEditText forgetPasswordEntryConfirmPasswordEt;

    @BindView(R.id.forgetPasswordEntryResetPasswordBtn)
    AppCompatButton forgetPasswordEntryResetPasswordBtn;

    private AppCompatActivity activity;

    private final ACProgressFlower progressDialog = new ACProgressFlower.Builder(getContext())
            .themeColor(Color.WHITE)
            .fadeColor(Color.DKGRAY).build();

    public ForgetPasswordView(AppCompatActivity activity){
        super(activity);
        this.activity = activity;

        inflate(activity, R.layout.activity_forget_password, this);

        ButterKnife.bind(this);
    }

    public Observable<Object> observeForgetPasswordButton() {
        return RxView.clicks(forgetPasswordBtn);
    }

    public Observable<Object> observeCrossButton() {
        return RxView.clicks(loginCrossIv);
    }

    public Observable<Object> observeResentPasswordButton() {
        return RxView.clicks(forgetPasswordEntryResetPasswordBtn);
    }

    public String getEmailText() {
        return input_email.getText().toString().trim();
    }

    public EditText getEmailEt(){
        return input_email;
    }

    public String getResetPassword() {
        return forgetPasswordEntryPasswordEt.getText().toString().trim();
    }

    public EditText getResetPasswordEt(){
        return forgetPasswordEntryPasswordEt;
    }

    public String getResetConfirmPassword() {
        return forgetPasswordEntryConfirmPasswordEt.getText().toString().trim();
    }

    public EditText getResetConfirmPasswordEt(){
        return forgetPasswordEntryConfirmPasswordEt;
    }

    public void showProgressDialogue(boolean loading){
        if(loading){
            progressDialog.setCancelable(false);
            progressDialog.show();
        }else{
            progressDialog.dismiss();
        }
    }

    public void onForgetPasswordSuccess(String message){
        Intent intent = new Intent(getContext(), LoginActivity2.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        AlertUtils.createAlert((Activity)getContext(),R.string.ok,R.string.cancel,message,intent).show();
    }

    public void finish(){
        activity.finish();
    }

    public void showPasswordEntryForm(boolean show) {
        forgetPasswordEntryLl.setVisibility(show?View.VISIBLE:View.GONE);
    }

    public void showForgotPasswordForm(boolean show) {
        forgotPasswordLl.setVisibility(show?View.VISIBLE:View.GONE);
    }
}
