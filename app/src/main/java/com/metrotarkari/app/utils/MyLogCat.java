package com.metrotarkari.app.utils;

import android.util.Log;

/**
 * Created by samrakchan on 7/27/17.
 */

public class MyLogCat {

    public static void logCat(String TAG, String logCatText){
        int maxLogSize = 1000;
        for(int i = 0; i <= logCatText.length() / maxLogSize; i++) {
            int start = i * maxLogSize;
            int end = (i+1) * maxLogSize;
            end = end > logCatText.length() ? logCatText.length() : end;
            Log.v(TAG, logCatText.substring(start, end));
        }
    }
}
