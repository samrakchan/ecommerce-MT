package com.metrotarkari.app.ui.accounts.mvp;

import android.support.v4.app.Fragment;

import com.metrotarkari.app.application.network.DoneNetwork;
import com.metrotarkari.app.model.LogoutResponse;

import io.reactivex.Observable;

/**
 * Created by samrakchan on 4/27/17.
 */

public class AccountModel {
    private final Fragment fragment;
    private final DoneNetwork doneNetwork;

    public AccountModel(Fragment fragment, DoneNetwork doneNetwork){
        this.fragment = fragment;
        this.doneNetwork = doneNetwork;
    }
    public Observable<LogoutResponse> getLogout(){
        return doneNetwork.getLogOut();
    }

}
