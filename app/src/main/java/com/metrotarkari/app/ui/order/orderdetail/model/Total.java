
package com.metrotarkari.app.ui.order.orderdetail.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Total {

    @SerializedName("subtotal_excl_tax")
    @Expose
    private String subtotalExclTax;
    @SerializedName("subtotal_incl_tax")
    @Expose
    private String subtotalInclTax;
    @SerializedName("shipping_hand_excl_tax")
    @Expose
    private String shippingHandExclTax;
    @SerializedName("shipping_hand_incl_tax")
    @Expose
    private String shippingHandInclTax;
    @SerializedName("tax")
    @Expose
    private String tax;
    @SerializedName("discount")
    @Expose
    private double discount;
    @SerializedName("grand_total_excl_tax")
    @Expose
    private double grandTotalExclTax;
    @SerializedName("grand_total_incl_tax")
    @Expose
    private String grandTotalInclTax;
    @SerializedName("currency_symbol")
    @Expose
    private String currencySymbol;

    public String getSubtotalExclTax() {
        return subtotalExclTax;
    }

    public void setSubtotalExclTax(String subtotalExclTax) {
        this.subtotalExclTax = subtotalExclTax;
    }

    public String getSubtotalInclTax() {
        return subtotalInclTax;
    }

    public void setSubtotalInclTax(String subtotalInclTax) {
        this.subtotalInclTax = subtotalInclTax;
    }

    public String getShippingHandExclTax() {
        return shippingHandExclTax;
    }

    public void setShippingHandExclTax(String shippingHandExclTax) {
        this.shippingHandExclTax = shippingHandExclTax;
    }

    public String getShippingHandInclTax() {
        return shippingHandInclTax;
    }

    public void setShippingHandInclTax(String shippingHandInclTax) {
        this.shippingHandInclTax = shippingHandInclTax;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public Double getGrandTotalExclTax() {
        return grandTotalExclTax;
    }

    public void setGrandTotalExclTax(Double grandTotalExclTax) {
        this.grandTotalExclTax = grandTotalExclTax;
    }

    public String getGrandTotalInclTax() {
        return grandTotalInclTax;
    }

    public void setGrandTotalInclTax(String grandTotalInclTax) {
        this.grandTotalInclTax = grandTotalInclTax;
    }

    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }

}
