package com.metrotarkari.app.ui.order.orderlist.mvp;

import android.widget.Toast;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.metrotarkari.app.R;
import com.metrotarkari.app.model.Error;
import com.metrotarkari.app.ui.order.orderlist.model.Order;
import com.metrotarkari.app.utils.DoneUserPreferenceManager;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.basgeekball.awesomevalidation.ValidationStyle.UNDERLABEL;

/**
 * Created by samrakchan on 4/30/17.
 */

public class OrderListPresenter {

    private final OrderListView view;
    private final OrderListModel model;


    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    private AwesomeValidation awesomeValidation = new AwesomeValidation(UNDERLABEL);
    private DoneUserPreferenceManager doneUserPreferenceManager;

    public OrderListPresenter(OrderListView view, OrderListModel model){
        this.view = view;
        this.model = model;

        compositeDisposable.add(getOrders());

    }

    private Disposable getOrders(){
        view.showNoItem(false);
        view.showProgressBar(true);
        return model.getOrder().observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(orderResponse -> {

                    List<Error> errorResponse = orderResponse.getErrors();
                    List<Order> orderList = orderResponse.getOrders();

                    if(errorResponse!=null && !errorResponse.isEmpty()){
                        Error error = errorResponse.get(0);
                        Toast.makeText(view.getContext(),error.getMessage()+"",Toast.LENGTH_LONG).show();
                    }else if(orderList!=null) {
                        if (!orderList.isEmpty()) {
                            view.setOrderListData(orderList);
                            view.showProgressBar(false);
                            view.showNoItem(false);
                        }else {
                            view.showNoItem(true);
                        }

                    }else {
                        Toast.makeText(view.getContext(), R.string.sww,Toast.LENGTH_LONG).show();
                    }
                }, throwable -> {
                    throwable.printStackTrace();
                    view.showProgressBar(false);
                    Toast.makeText(view.getContext(),R.string.sww,Toast.LENGTH_LONG).show();
                    view.showNoItem(false);
                });
    }
}
