package com.metrotarkari.app.ui.b2b.view.mvp;

import android.app.Activity;

import com.metrotarkari.app.application.network.DoneNetwork;
import com.metrotarkari.app.model.AddressResponse;
import com.metrotarkari.app.model.CartQuoteContent;
import com.metrotarkari.app.model.ItemParams;
import com.metrotarkari.app.model.WishList;

import java.util.HashMap;

import io.reactivex.Observable;

/**
 * Created by samrakchan on 4/27/17.
 */

public class B2bModel {
    private final Activity activity;
    private final DoneNetwork doneNetwork;

    public B2bModel(Activity activity, DoneNetwork doneNetwork){
        this.activity = activity;
        this.doneNetwork = doneNetwork;
    }

    public void finishActivity(){
        activity.finish();
    }

    public Observable<WishList> getWishList(ItemParams itemParams){
        return doneNetwork.getWishList(itemParams.getPage(), itemParams.getLimit(), itemParams.getOrderBy(), itemParams.getDir());
    }

    public Observable<CartQuoteContent> putCartQuote(HashMap<String, Integer> itemQty){
        return doneNetwork.putQuoteItem(itemQty);
    }

    public Observable<AddressResponse> getAddressBook(){
        return doneNetwork.getAddressBook();
    }
}
