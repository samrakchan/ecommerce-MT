package com.metrotarkari.app.utils;

import android.content.Context;
import android.view.Gravity;
import android.widget.Toast;

/**
 * Created by samrakchan on 7/25/17.
 */

public class MyToast {

    public static void displayToast(Context context, int msg, int duration){
        Toast toast = Toast.makeText(context, msg, duration);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    public static void displayToast(Context context, String msg, int duration){
        Toast toast = Toast.makeText(context, msg, duration);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }
}
