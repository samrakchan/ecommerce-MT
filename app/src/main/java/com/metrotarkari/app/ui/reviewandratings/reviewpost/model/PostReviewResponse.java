package com.metrotarkari.app.ui.reviewandratings.reviewpost.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.metrotarkari.app.model.ErrorResponse;
import com.metrotarkari.app.model.Review;

/**
 * Created by Avinash on 8/10/2017.
 */

public class PostReviewResponse extends ErrorResponse {
    @SerializedName("review")
    @Expose
    private Review review;
    @SerializedName("message")
    @Expose
    private String message;

    public Review getReview() {
        return review;
    }

    public void setReview(Review review) {
        this.review = review;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}

