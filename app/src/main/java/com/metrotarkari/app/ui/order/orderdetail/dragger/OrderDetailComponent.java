package com.metrotarkari.app.ui.order.orderdetail.dragger;



import com.metrotarkari.app.application.dagger.AppComponent;
import com.metrotarkari.app.ui.order.orderdetail.OrderDetailActivity;

import dagger.Component;

@OrderListScope
@Component(modules = { OrderDetailModule.class }, dependencies = AppComponent.class)
public interface OrderDetailComponent {

  void inject(OrderDetailActivity orderDetailActivity);

}
