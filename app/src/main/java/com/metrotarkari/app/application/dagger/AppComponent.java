package com.metrotarkari.app.application.dagger;

import android.content.Context;

import com.metrotarkari.app.application.dagger.module.AppModule;
import com.metrotarkari.app.application.dagger.module.DbModule;
import com.metrotarkari.app.application.dagger.module.GsonModule;
import com.metrotarkari.app.application.dagger.module.NetworkModule;
import com.metrotarkari.app.application.dagger.module.PreferenceModule;
import com.metrotarkari.app.application.network.DoneNetwork;
import com.metrotarkari.app.db.DoneDbAdapter;
import com.metrotarkari.app.utils.DoneUserPreferenceManager;

import dagger.Component;
import okhttp3.OkHttpClient;

@AppScope
@Component(modules = { AppModule.class , NetworkModule.class, GsonModule.class, DbModule.class, PreferenceModule.class})
public interface AppComponent {

    Context context();

    OkHttpClient okhttpClient();

    DoneNetwork doneNetwork();

    DoneDbAdapter dbAdapter();

    DoneUserPreferenceManager doneUserPreferenceManager();


    /*@Named("cache")
    Interceptor getCacheInterceptor();

    @Named("authToken")
    Interceptor getAuthToken();*/
}
