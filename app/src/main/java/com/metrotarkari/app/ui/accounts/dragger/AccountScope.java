package com.metrotarkari.app.ui.accounts.dragger;

import javax.inject.Scope;

@Scope
public @interface AccountScope {
}
