package com.metrotarkari.app.ui.addressbook.addresslist.mvp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.metrotarkari.app.R;
import com.metrotarkari.app.application.DoneApplication;
import com.metrotarkari.app.ui.addressbook.addresslist.dragger.AddressBookModule;
import com.metrotarkari.app.ui.addressbook.addresslist.dragger.DaggerAddressBookComponent;
import com.metrotarkari.app.ui.base.BaseActivity;
import com.metrotarkari.app.ui.addressbook.addressentry.AddressEntryActivity;
import com.metrotarkari.app.utils.Constant;

import javax.inject.Inject;

/**
 * Created by Avinash on 6/21/2017.
 */

public class AddressListActivity extends BaseActivity {

    @Inject
    AddressListView view;

    private Menu menu;

    @Inject
    AddressListPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DaggerAddressBookComponent.builder().appComponent(DoneApplication.get(this).component())
                .addressBookModule(new AddressBookModule(this)).build().inject(this);

        setContentView(view);
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_address_book, menu);
        this.menu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        if(item.getItemId() == R.id.action_add_address){
            Intent intent = new Intent(getApplicationContext(), AddressEntryActivity.class);
            intent.putExtra(Constant.INTENT_FOR,Constant.ADD_ADDRESS);
            startActivity(intent);
            finish();
            return true;
        }else if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return false;
    }

}
