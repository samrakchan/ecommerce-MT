package com.metrotarkari.app.ui.cart.mvp;

import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.jakewharton.rxbinding2.view.RxView;
import com.metrotarkari.app.R;
import com.metrotarkari.app.model.ItemParams;
import com.metrotarkari.app.model.Quoteitem;
import com.metrotarkari.app.model.Total;
import com.metrotarkari.app.ui.adapter.HorizontalItemAdapter;
import com.metrotarkari.app.ui.addressbook.addressentry.AddressEntryActivity;
import com.metrotarkari.app.ui.addressbook.addresslist.mvp.AddressListActivity;
import com.metrotarkari.app.ui.base.BaseView;
import com.metrotarkari.app.ui.cart.CartAdapter;
import com.metrotarkari.app.ui.checkout.CheckoutActivity;
import com.metrotarkari.app.ui.login.LoginActivity2;
import com.metrotarkari.app.utils.AlertUtils;
import com.metrotarkari.app.utils.Constant;
import com.metrotarkari.app.utils.DoneItemClickListener;
import com.metrotarkari.app.utils.Utility;
import com.paginate.Paginate;
import com.paginate.recycler.LoadingListItemCreator;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import cc.cloudist.acplibrary.ACProgressFlower;
import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;

/**
 * Created by samrakchan on 4/27/17.
 */

public class CartView extends BaseView implements Paginate.Callbacks, DoneItemClickListener{

    @Nullable
    @BindView(R.id.cartRv)
    public RecyclerView mCartRv;

    private LinearLayoutManager mLayoutManager;
    private CartAdapter mAdapter;

    @Nullable
    @BindView(R.id.cartCheckOutBtn)
    public AppCompatButton cartCheckOutBtn;

    @BindView(R.id.imageMsgRl)
    public View noItemView;

    @BindView(R.id.progressBarRl)
    public View progressBar;
    
    @BindView(R.id.cartTotalTv)
    public AppCompatTextView cartTotalTv;
    
    @BindView(R.id.cartTaxTv)
    public AppCompatTextView cartTaxTv;

    @BindView(R.id.cartSubTotalTv)
    public AppCompatTextView cartSubTotalTv;

    @BindView(R.id.cartSubTotalLl)
    public LinearLayout cartSubTotalLl;

    @BindView(R.id.cartTaxLl)
    public LinearLayout cartTaxLl;

    @BindView(R.id.cartTotalLl)
    public LinearLayout cartTotalLl;

    @BindView(R.id.cartInfoLl)
    public LinearLayout cartInfoLl;

    @BindView(R.id.cartRecyclerItemRl)
    public RelativeLayout cartRecyclerItemRl;

    @BindView(R.id.cartDiscountLl)
    public LinearLayout cartDiscountLl;

    @BindView(R.id.cartDiscountTv)
    public AppCompatTextView cartDiscountTv;

    private AppCompatActivity activity;

    private int heighOfCartInfoLl;

    private Paginate paginate;

    private final BehaviorSubject<ItemParams> paginationItemParamsBehaviorSubject = BehaviorSubject.create();
    private final BehaviorSubject<HashMap<String, Integer>> changeItemQtyBehavior = BehaviorSubject.create();
    private ItemParams paginationParams = new ItemParams();


    private int page = 0;
    private boolean loading = false;
    protected int threshold = 4;
    protected int totalPages = 1;
    protected int itemsPerPage = 100;
    protected boolean addLoadingRow = true;
    protected boolean customLoadingListItem = false;

    private final ACProgressFlower progressDialog = new ACProgressFlower.Builder(getContext())
            .themeColor(Color.WHITE)
            .fadeColor(Color.DKGRAY).build();

    public CartView(@NonNull AppCompatActivity activity) {
        super(activity);
        this.activity = activity;

        if (paginate != null) {
            paginate.unbind();
        }
        mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        mAdapter = new CartAdapter(getContext());
        mAdapter.addOnItemClickListener(this);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getContext(),DividerItemDecoration.VERTICAL);
        mCartRv.addItemDecoration(dividerItemDecoration);
        mCartRv.setHasFixedSize(true);
        mCartRv.setLayoutManager(mLayoutManager);
        mCartRv.setAdapter(mAdapter);

        loading = false;
        page = 0;

        paginate = Paginate.with(mCartRv, this)
                .setLoadingTriggerThreshold(threshold)
                .addLoadingListItem(addLoadingRow)
                .setLoadingListItemCreator(customLoadingListItem ? new CustomLoadingListItemCreator() : null)
                .build();

        activity.getSupportActionBar().setTitle(R.string.my_cart);
    }

    public void clearItemAndChangePageToZero(){
        mAdapter.clearItem();
        page = 0;
    }


    @Override
    public int getContentLayout() {
        return R.layout.activity_cart;
    }

    public void navigateToLoginActivity(){
        Intent intent = new Intent(getContext(),LoginActivity2.class);
        intent.putExtra(Constant.INTENT_FOR,Constant.CHECKOUT);
        activity.startActivityForResult(intent,Constant.CHECKOUT_ACTIVITY_REQUEST_CODE);
    }


    public AlertDialog showCartAlertDialog(){
        return new AlertDialog.Builder(activity)
                .setMessage(R.string.successfully_added_to_cart)
        .setPositiveButton(R.string.continue_shopping, ((dialogInterface, i) -> {
            dialogInterface.dismiss();
        }))
        .setNegativeButton(R.string.view_cart, (dialogInterface, i) -> {
            dialogInterface.dismiss();
        }).create();
    }

    @Override
    public void onLoadMore() {
        Log.d("Paginate", "onLoadMore");
        loading = true;
        ItemParams ip = new ItemParams();
        ip.setPage(page);
        ip.setLimit(itemsPerPage);
        ip.setDir("");
        ip.setOrderBy("");

        paginationParams = ip;

        paginationItemParamsBehaviorSubject.onNext(paginationParams);
    }

    public Observable<ItemParams> getOnScrollObservable() {
        return paginationItemParamsBehaviorSubject;
    }

    public Observable<Object> proceedToCheckOutClickObservable(){
        return RxView.clicks(cartCheckOutBtn);
    }

    public Observable<HashMap<String, Integer>> onAlertDeleteObservable(){
        return changeItemQtyBehavior;
    }

    public void setTotalPage(int totalPages){
        this.totalPages = totalPages;
    }

    public void setPageIncrement(){
        page = page+1;
    }

    @Override
    public boolean isLoading() {
        return loading;
    }

    @Override
    public boolean hasLoadedAllItems() {
        return page == totalPages;
    }

    public int getCurrentItemSize(){
        return mAdapter.getItemCount();
    }

    public void addItemToProductAdapter(List<Object> products){
        loading = false;
        mAdapter.addData(products);
    }

    public void clearProductAdapterItems(){
        mAdapter.clearItem();
    }

    public void showNoItem(boolean showItem){
        if(showItem)
            noItemView.setVisibility(View.VISIBLE);
        else
            noItemView.setVisibility(View.GONE);
    }

    public void showProgressBar(boolean showing){
        if(showing)
            progressBar.setVisibility(View.VISIBLE);
        else
            progressBar.setVisibility(View.GONE);
    }

    public void showLoading(boolean loading) {
        if (loading) {
            progressDialog.setCancelable(false);
            progressDialog.show();
        } else {
            progressDialog.dismiss();
        }
    }


    public void grandTotalViewHideShow(double total, double tax, double subTotal,double discount){
        if (tax == 0)
            cartTaxLl.setVisibility(View.GONE);
        else
            cartTaxLl.setVisibility(View.VISIBLE);


        if(total == subTotal)
            cartSubTotalLl.setVisibility(View.GONE);
        else
            cartSubTotalLl.setVisibility(View.VISIBLE);


        if (discount==0)
            cartDiscountLl.setVisibility(View.GONE);
        else
            cartDiscountLl.setVisibility(View.VISIBLE);


        ViewTreeObserver vto = cartInfoLl.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(()-> {
                heighOfCartInfoLl = cartInfoLl.getHeight();
        });

        ViewTreeObserver vto1 = cartRecyclerItemRl.getViewTreeObserver();
        vto1.addOnGlobalLayoutListener(() -> {
            RelativeLayout.LayoutParams relativeParams = (RelativeLayout.LayoutParams)cartRecyclerItemRl.getLayoutParams();
            //int marginHeight = Utility.dpToPx((int)getResources().getDimension(R.dimen.content_padding), getContext());
            relativeParams.setMargins(0, 0, 0, heighOfCartInfoLl);  // left, top, right, bottom
            cartRecyclerItemRl.setLayoutParams(relativeParams);
            invalidate();
            requestLayout();
        });
    }


    public void setTotal(String total){
        cartTotalTv.setText(total);
    }

    public void setTax(String tax){
        cartTaxTv.setText(tax);
    }

    public void setSubTotal(String subTotal){
        cartSubTotalTv.setText(subTotal);
    }

    public void setDiscount(String discount){
        cartDiscountTv.setText(discount);
    }

    @Override
    public void onItemClickListener(int position, int viewId, Object object) {
        Quoteitem quoteitem = (Quoteitem) object;
        if(quoteitem!=null){

            if(quoteitem.getItemQtyChangedTo() == 0) {
                //to delete item.
                AlertDialog.Builder builder = new AlertDialog.Builder(activity)
                        .setMessage(R.string.delete_msg)
                        .setCancelable(true)
                        .setPositiveButton(R.string.delete, (dialogInterface, i) -> {
                            String itemId = quoteitem.getItemId();
                            HashMap<String, Integer> map = new HashMap<String, Integer>();
                            map.put(itemId, quoteitem.getItemQtyChangedTo());
                            changeItemQtyBehavior.onNext(map);
                            dialogInterface.dismiss();

                        })
                        .setNegativeButton(R.string.cancel, ((dialogInterface, i) -> {
                            dialogInterface.dismiss();
                        }));
                builder.create();
                builder.show();
            }else {
                //to update Qty.
                AlertDialog.Builder builder = new AlertDialog.Builder(activity)
                        .setMessage(quoteitem.getName()+" "+getContext().getString(R.string.qty_changed_to)+" "+quoteitem.getItemQtyChangedTo())
                        .setCancelable(true)
                        .setPositiveButton(R.string.update, (dialogInterface, i) -> {
                            String itemId = quoteitem.getItemId();
                            HashMap<String, Integer> map = new HashMap<String, Integer>();
                            map.put(itemId, quoteitem.getItemQtyChangedTo());
                            changeItemQtyBehavior.onNext(map);
                            dialogInterface.dismiss();
                        })
                        .setNegativeButton(R.string.cancel, ((dialogInterface, i) -> {
                            dialogInterface.dismiss();
                        }));
                builder.create();
                builder.show();
            }
        }
    }

    private class CustomLoadingListItemCreator implements LoadingListItemCreator {
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View view = inflater.inflate(R.layout.layout_progress, parent, false);
            return new VH(view);
        }
        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

            if (mCartRv.getLayoutManager() instanceof StaggeredGridLayoutManager) {
                StaggeredGridLayoutManager.LayoutParams params = (StaggeredGridLayoutManager.LayoutParams) holder.itemView.getLayoutParams();
                params.setFullSpan(true);
            }
        }
    }


    static class VH extends RecyclerView.ViewHolder {
        public VH(View itemView) {
            super(itemView);
        }
    }

    public void navigateToCheckout(String addressId){
        Intent intent = new Intent(getContext(), CheckoutActivity.class);
        intent.putExtra(Constant.ADDRESS_ID, addressId);
        getContext().startActivity(intent);
    }

    public void navigateToAddressList(){
        Intent intent = new Intent(getContext(), AddressListActivity.class);
        getContext().startActivity(intent);
    }

    public void navigateToAddressEntry(){
        Intent intent = new Intent(getContext(), AddressEntryActivity.class);
        intent.putExtra(Constant.INTENT_FOR, Constant.CHECKOUT);
        activity.startActivity(intent);
    }

}
