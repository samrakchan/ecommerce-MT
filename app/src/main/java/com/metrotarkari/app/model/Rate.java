
package com.metrotarkari.app.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Rate {

    @SerializedName("rate_code")
    @Expose
    private String rateCode;

    @SerializedName("rate_options")
    @Expose
    private List<RateOption> rateOptions = null;

    public String getRateCode() {
        return rateCode;
    }

    public void setRateCode(String rateCode) {
        this.rateCode = rateCode;
    }

    public List<RateOption> getRateOptions() {
        return rateOptions;
    }

    public void setRateOptions(List<RateOption> rateOptions) {
        this.rateOptions = rateOptions;
    }

}
