package com.metrotarkari.app.ui.splash.dagger;

import android.app.Activity;

import com.metrotarkari.app.ui.splash.mvp.SplashModel;
import com.metrotarkari.app.ui.splash.mvp.SplashPresenter;
import com.metrotarkari.app.ui.splash.mvp.SplashView;
import com.metrotarkari.app.utils.DoneUserPreferenceManager;

import dagger.Module;
import dagger.Provides;

@Module
public class SplashModule {

  private final Activity activity;

  public SplashModule(Activity activity) {
    this.activity = activity;
  }

  @Provides
  @SplashScope
  public SplashView view() {
    return new SplashView(activity);
  }

  @Provides
  @SplashScope
  public SplashModel model(){
    return new SplashModel(activity);
  }

  @Provides
  @SplashScope
  public SplashPresenter splashPresenter(SplashView homeView, SplashModel model, DoneUserPreferenceManager preferenceManager) {
    return new SplashPresenter(homeView, model, preferenceManager);
  }

}
