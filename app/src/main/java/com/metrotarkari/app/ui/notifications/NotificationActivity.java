package com.metrotarkari.app.ui.notifications;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.metrotarkari.app.application.DoneApplication;
import com.metrotarkari.app.ui.base.BaseActivity;
import com.metrotarkari.app.ui.notifications.dagger.DaggerNotificationComponent;
import com.metrotarkari.app.ui.notifications.dagger.NotificationModule;
import com.metrotarkari.app.ui.notifications.mvp.NotificationPresenter;
import com.metrotarkari.app.ui.notifications.mvp.NotificationView;

import javax.inject.Inject;

/**
 * Created by Avinash on 7/7/2017.
 */

public class NotificationActivity extends BaseActivity {

    @Inject
    NotificationView view;

    @Inject
    NotificationPresenter presenter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        DaggerNotificationComponent.builder()
                .appComponent(DoneApplication.get(this).component())
                .notificationModule(new NotificationModule(this))
                .build().inject(this);

        setContentView(view);


    }
}
