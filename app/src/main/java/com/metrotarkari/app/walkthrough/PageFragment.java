package com.metrotarkari.app.walkthrough;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.metrotarkari.app.R;

public class PageFragment extends Fragment {

	private static final String ARG_POSITION = "ARG_POSITION";

    private static final int TITLE[] = {R.string.wt_1_title, R.string.wt_2_title, R.string.wt_3_title};
    private static final int DESC[] = {R.string.wt_1_desc, R.string.wt_2_desc, R.string.wt_3_desc};
    private static final int IMG[] = {R.drawable.a_wt_1, R.drawable.a_wt_2, R.drawable.a_wt_3};


	public PageFragment() {
	}

	public static PageFragment newInstance(final int position) {
		final Bundle args = new Bundle();
		args.putInt(ARG_POSITION, position);
		PageFragment f = new PageFragment();
		f.setArguments(args);
		return f;
	}

	@Nullable
	@Override
	public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle
			savedInstanceState) {
		return inflater.inflate(R.layout.fragment_walkthrough_pager, container, false);
	}

	@Override
	public void onViewCreated(final View view, final Bundle savedInstanceState) {
		final int position = getArguments().getInt(ARG_POSITION);

		view.setTag(position);

		final AppCompatImageView walkThroughImgIv = (AppCompatImageView) view.findViewById(R.id.walkThroughImgIv);
		final AppCompatTextView walkThroughTitleTv = (AppCompatTextView) view.findViewById(R.id.walkThroughTitleIv);
		final AppCompatTextView walkThroughDescTv = (AppCompatTextView) view.findViewById(R.id.walkThroughDescIv);

        //walkThroughImgIv.setImageResource(IMG[position]);
        walkThroughTitleTv.setText(TITLE[position]);
        walkThroughDescTv.setText(DESC[position]);

        Glide.with(this)
                .load(IMG[position])
                .crossFade()
                .into(walkThroughImgIv);
	}
}
