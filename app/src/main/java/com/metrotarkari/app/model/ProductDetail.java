package com.metrotarkari.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by samrakchan on 5/29/17.
 */

public class ProductDetail {

    @SerializedName("entity_id")
    @Expose
    private String entityId;
    @SerializedName("attribute_set_id")
    @Expose
    private String attributeSetId;
    @SerializedName("type_id")
    @Expose
    private String typeId;
    @SerializedName("sku")
    @Expose
    private String sku;
    @SerializedName("has_options")
    @Expose
    private String hasOptions;
    @SerializedName("required_options")
    @Expose
    private String requiredOptions;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("meta_title")
    @Expose
    private String metaTitle;
    @SerializedName("meta_description")
    @Expose
    private String metaDescription;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("small_image")
    @Expose
    private String smallImage;
    @SerializedName("thumbnail")
    @Expose
    private String thumbnail;
    @SerializedName("options_container")
    @Expose
    private String optionsContainer;
    @SerializedName("url_key")
    @Expose
    private String urlKey;
    @SerializedName("swatch_image")
    @Expose
    private String swatchImage;
    @SerializedName("gift_message_available")
    @Expose
    private String giftMessageAvailable;
    @SerializedName("meta_keyword")
    @Expose
    private String metaKeyword;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("visibility")
    @Expose
    private String visibility;
    @SerializedName("quantity_and_stock_status")
    @Expose
    private QuantityAndStockStatus quantityAndStockStatus;
    @SerializedName("tax_class_id")
    @Expose
    private String taxClassId;
    @SerializedName("metro_weight_type")
    @Expose
    private String metroWeightType;
    @SerializedName("options")
    @Expose
    private List<Object> options = null;
    @SerializedName("category_ids")
    @Expose
    private List<String> categoryIds = null;

    @SerializedName("is_salable")
    @Expose
    private String isSalable;
    @SerializedName("additional")
    @Expose
    private List<Object> additional = null;
    @SerializedName("images")
    @Expose
    private List<Image> images = null;
    @SerializedName("app_prices")
    @Expose
    private AppPrices appPrices;
    @SerializedName("app_options")
    @Expose
    private AppOptions appOptions;
    @SerializedName("wishlist_item_id")
    @Expose
    private Object wishlistItemId;
    @SerializedName("product_label")
    @Expose
    private Object productLabel;
    @SerializedName("app_reviews")
    @Expose
    private AppReviews appReviews;
    @SerializedName("product_video")
    @Expose
    private Object productVideo;

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public String getAttributeSetId() {
        return attributeSetId;
    }

    public void setAttributeSetId(String attributeSetId) {
        this.attributeSetId = attributeSetId;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getHasOptions() {
        return hasOptions;
    }

    public void setHasOptions(String hasOptions) {
        this.hasOptions = hasOptions;
    }

    public String getRequiredOptions() {
        return requiredOptions;
    }

    public void setRequiredOptions(String requiredOptions) {
        this.requiredOptions = requiredOptions;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMetaTitle() {
        return metaTitle;
    }

    public void setMetaTitle(String metaTitle) {
        this.metaTitle = metaTitle;
    }

    public String getMetaDescription() {
        return metaDescription;
    }

    public void setMetaDescription(String metaDescription) {
        this.metaDescription = metaDescription;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getSmallImage() {
        return smallImage;
    }

    public void setSmallImage(String smallImage) {
        this.smallImage = smallImage;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getOptionsContainer() {
        return optionsContainer;
    }

    public void setOptionsContainer(String optionsContainer) {
        this.optionsContainer = optionsContainer;
    }

    public String getUrlKey() {
        return urlKey;
    }

    public void setUrlKey(String urlKey) {
        this.urlKey = urlKey;
    }

    public String getSwatchImage() {
        return swatchImage;
    }

    public void setSwatchImage(String swatchImage) {
        this.swatchImage = swatchImage;
    }

    public String getGiftMessageAvailable() {
        return giftMessageAvailable;
    }

    public void setGiftMessageAvailable(String giftMessageAvailable) {
        this.giftMessageAvailable = giftMessageAvailable;
    }

    public String getMetaKeyword() {
        return metaKeyword;
    }

    public void setMetaKeyword(String metaKeyword) {
        this.metaKeyword = metaKeyword;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getVisibility() {
        return visibility;
    }

    public void setVisibility(String visibility) {
        this.visibility = visibility;
    }

    public QuantityAndStockStatus getQuantityAndStockStatus() {
        return quantityAndStockStatus;
    }

    public void setQuantityAndStockStatus(QuantityAndStockStatus quantityAndStockStatus) {
        this.quantityAndStockStatus = quantityAndStockStatus;
    }

    public String getTaxClassId() {
        return taxClassId;
    }

    public void setTaxClassId(String taxClassId) {
        this.taxClassId = taxClassId;
    }

    public String getMetroWeightType() {
        return metroWeightType;
    }

    public void setMetroWeightType(String metroWeightType) {
        this.metroWeightType = metroWeightType;
    }

    public List<Object> getOptions() {
        return options;
    }

    public void setOptions(List<Object> options) {
        this.options = options;
    }

    public List<String> getCategoryIds() {
        return categoryIds;
    }

    public void setCategoryIds(List<String> categoryIds) {
        this.categoryIds = categoryIds;
    }

    public String getIsSalable() {
        return isSalable;
    }

    public void setIsSalable(String isSalable) {
        this.isSalable = isSalable;
    }

    public List<Object> getAdditional() {
        return additional;
    }

    public void setAdditional(List<Object> additional) {
        this.additional = additional;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public AppPrices getAppPrices() {
        return appPrices;
    }

    public void setAppPrices(AppPrices appPrices) {
        this.appPrices = appPrices;
    }

    public AppOptions getAppOptions() {
        return appOptions;
    }

    public void setAppOptions(AppOptions appOptions) {
        this.appOptions = appOptions;
    }

    public Object getWishlistItemId() {
        return wishlistItemId;
    }

    public void setWishlistItemId(Object wishlistItemId) {
        this.wishlistItemId = wishlistItemId;
    }

    public Object getProductLabel() {
        return productLabel;
    }

    public void setProductLabel(Object productLabel) {
        this.productLabel = productLabel;
    }

    public AppReviews getAppReviews() {
        return appReviews;
    }

    public void setAppReviews(AppReviews appReviews) {
        this.appReviews = appReviews;
    }

    public Object getProductVideo() {
        return productVideo;
    }

    public void setProductVideo(Object productVideo) {
        this.productVideo = productVideo;
    }
}
