
package com.metrotarkari.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Address {

    @SerializedName("entity_id")
    @Expose
    private String entityId;
    @SerializedName("increment_id")
    @Expose
    private Object incrementId;
    @SerializedName("parent_id")
    @Expose
    private String parentId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("is_active")
    @Expose
    private String isActive;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("company")
    @Expose
    private String company;
    @SerializedName("country_id")
    @Expose
    private String countryId;
    @SerializedName("fax")
    @Expose
    private Object fax;
    @SerializedName("firstname")
    @Expose
    private String firstname;
    @SerializedName("lastname")
    @Expose
    private String lastname;
    @SerializedName("middlename")
    @Expose
    private Object middlename;
    @SerializedName("postcode")
    @Expose
    private String postcode;
    @SerializedName("prefix")
    @Expose
    private Object prefix;
    @SerializedName("region")
    @Expose
    private String region;
    @SerializedName("region_id")
    @Expose
    private Integer regionId;
    @SerializedName("street")
    @Expose
    private String street;
    @SerializedName("suffix")
    @Expose
    private Object suffix;
    @SerializedName("telephone")
    @Expose
    private String telephone;
    @SerializedName("vat_id")
    @Expose
    private Object vatId;
    @SerializedName("vat_is_valid")
    @Expose
    private Object vatIsValid;
    @SerializedName("vat_request_date")
    @Expose
    private Object vatRequestDate;
    @SerializedName("vat_request_id")
    @Expose
    private Object vatRequestId;
    @SerializedName("vat_request_success")
    @Expose
    private Object vatRequestSuccess;
    @SerializedName("cus_area")
    @Expose
    private Object cusArea;
    @SerializedName("landline")
    @Expose
    private Object landline;
    @SerializedName("region_code")
    @Expose
    private String regionCode;
    @SerializedName("country_name")
    @Expose
    private String countryName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("latlng")
    @Expose
    private String latlng;

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public Object getIncrementId() {
        return incrementId;
    }

    public void setIncrementId(Object incrementId) {
        this.incrementId = incrementId;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public Object getFax() {
        return fax;
    }

    public void setFax(Object fax) {
        this.fax = fax;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Object getMiddlename() {
        return middlename;
    }

    public void setMiddlename(Object middlename) {
        this.middlename = middlename;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public Object getPrefix() {
        return prefix;
    }

    public void setPrefix(Object prefix) {
        this.prefix = prefix;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public Integer getRegionId() {
        return regionId;
    }

    public void setRegionId(Integer regionId) {
        this.regionId = regionId;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Object getSuffix() {
        return suffix;
    }

    public void setSuffix(Object suffix) {
        this.suffix = suffix;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public Object getVatId() {
        return vatId;
    }

    public void setVatId(Object vatId) {
        this.vatId = vatId;
    }

    public Object getVatIsValid() {
        return vatIsValid;
    }

    public void setVatIsValid(Object vatIsValid) {
        this.vatIsValid = vatIsValid;
    }

    public Object getVatRequestDate() {
        return vatRequestDate;
    }

    public void setVatRequestDate(Object vatRequestDate) {
        this.vatRequestDate = vatRequestDate;
    }

    public Object getVatRequestId() {
        return vatRequestId;
    }

    public void setVatRequestId(Object vatRequestId) {
        this.vatRequestId = vatRequestId;
    }

    public Object getVatRequestSuccess() {
        return vatRequestSuccess;
    }

    public void setVatRequestSuccess(Object vatRequestSuccess) {
        this.vatRequestSuccess = vatRequestSuccess;
    }

    public Object getCusArea() {
        return cusArea;
    }

    public void setCusArea(Object cusArea) {
        this.cusArea = cusArea;
    }

    public Object getLandline() {
        return landline;
    }

    public void setLandline(Object landline) {
        this.landline = landline;
    }

    public String getRegionCode() {
        return regionCode;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLatlng() {
        return latlng;
    }

    public void setLatlng(String latlng) {
        this.latlng = latlng;
    }

}
