package com.metrotarkari.app.ui.home.mvp;

import com.metrotarkari.app.application.network.DoneNetwork;
import com.metrotarkari.app.model.Home;

import io.reactivex.Observable;

/**
 * Created by samrakchan on 4/27/17.
 */

public class HomeModel {
    private final DoneNetwork doneNetwork;

    public HomeModel(DoneNetwork doneNetwork){
        this.doneNetwork = doneNetwork;
    }

    public Observable<Home> getHome(int limit){
        return doneNetwork.getHomeItems(limit);
    }
}
