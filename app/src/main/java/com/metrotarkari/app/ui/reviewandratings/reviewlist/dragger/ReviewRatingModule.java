package com.metrotarkari.app.ui.reviewandratings.reviewlist.dragger;

import android.support.v7.app.AppCompatActivity;

import com.metrotarkari.app.application.network.DoneNetwork;
import com.metrotarkari.app.ui.reviewandratings.reviewlist.mvp.ReviewRatingModel;
import com.metrotarkari.app.ui.reviewandratings.reviewlist.mvp.ReviewRatingPresenter;
import com.metrotarkari.app.ui.reviewandratings.reviewlist.mvp.ReviewRatingView;

import dagger.Module;
import dagger.Provides;

@Module
public class ReviewRatingModule {

  private final AppCompatActivity activity;

  public ReviewRatingModule(AppCompatActivity activity) {
    this.activity = activity;
  }

  @Provides
  @ReviewRatingScope
  public ReviewRatingView view() {
    return new ReviewRatingView(activity);
  }

  @Provides
  @ReviewRatingScope
  public ReviewRatingModel model(DoneNetwork doneNetwork){
    return new ReviewRatingModel(activity, doneNetwork);
  }

  @Provides
  @ReviewRatingScope
  public ReviewRatingPresenter presenter(ReviewRatingView view, ReviewRatingModel model) {
    return new ReviewRatingPresenter(view, model);
  }

}
