package com.metrotarkari.app.ui.home.dagger;

import com.metrotarkari.app.application.network.DoneNetwork;
import com.metrotarkari.app.db.DoneDbAdapter;
import com.metrotarkari.app.ui.home.HomeFragment;
import com.metrotarkari.app.ui.home.mvp.HomeModel;
import com.metrotarkari.app.ui.home.mvp.HomePresenter;
import com.metrotarkari.app.ui.home.mvp.HomeView;

import dagger.Module;
import dagger.Provides;

@Module
public class HomeModule {

  private final HomeFragment fragment;

  public HomeModule(HomeFragment fragment) {
    this.fragment = fragment;
  }

  @Provides
  @HomeScope
  public HomeView view() {
    return new HomeView(fragment);
  }

  @Provides
  @HomeScope
  public HomeModel model(DoneNetwork doneNetwork){
    return new HomeModel(doneNetwork);
  }

  @Provides
  @HomeScope
  public HomePresenter presenter(HomeView homeView, HomeModel model, DoneDbAdapter doneDbAdapter) {
    return new HomePresenter(homeView, model, doneDbAdapter);
  }

}
