package com.metrotarkari.app.ui.notifications;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metrotarkari.app.R;
import com.metrotarkari.app.model.ChildCategory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Avinash on 7/7/2017.
 */

public class NotificationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Object> mData;
    private Context mContext;

    private static final int VIEW_TYPE_DAILY_BLOCKBUSTER = 1;


    public NotificationAdapter(Context mContext) {
        this.mContext = mContext;
        mData = new ArrayList<>();
    }

    public void addNotificationData(List<Object> objects){
        mData.addAll(objects);
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_DAILY_BLOCKBUSTER){
            View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_notification_first_type,parent,false);
            return new NotificationTypeOneViewHolder(view);
        }else {
            View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_notification_first_type,parent,false);
            return new NotificationTypeOneViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) holder.itemView.getLayoutParams();
        layoutParams.setFullSpan(true);
        Object obj = mData.get(position);

        if (obj instanceof ChildCategory){

            ChildCategory childCategory = (ChildCategory) obj;
            NotificationTypeOneViewHolder notificationTypeOneViewHolder = (NotificationTypeOneViewHolder) holder;
            notificationTypeOneViewHolder.notificationHeadingTv.setText(childCategory.getName());
            notificationTypeOneViewHolder.notificationTypeFirstIv.setImageResource(R.mipmap.ic_launcher);
            notificationTypeOneViewHolder.notificationDescriptionTv.setText(childCategory.getImage());
        }


    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    @Override
    public int getItemViewType(int position) {
        Object object = mData.get(position);
        if (object instanceof ChildCategory) {
            return VIEW_TYPE_DAILY_BLOCKBUSTER;
        }
        return  -1;
    }

    protected class NotificationTypeOneViewHolder extends RecyclerView.ViewHolder{

        AppCompatTextView notificationHeadingTv;
        AppCompatImageView notificationTypeFirstIv;
        AppCompatTextView notificationDescriptionTv;

        public NotificationTypeOneViewHolder(View itemView) {
            super(itemView);

            notificationHeadingTv = (AppCompatTextView) itemView.findViewById(R.id.notificationHeadingTv);
            notificationTypeFirstIv = (AppCompatImageView) itemView.findViewById(R.id.notificationTypeFirstIv);
            notificationDescriptionTv = (AppCompatTextView) itemView.findViewById(R.id.notificationDescriptionTv);


        }
    }
}
