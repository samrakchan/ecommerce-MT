
package com.metrotarkari.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StoreViewResult extends ErrorResponse {

    @SerializedName("storeview")
    @Expose
    private Storeview storeview;

    public Storeview getStoreview() {
        return storeview;
    }

    public void setStoreview(Storeview storeview) {
        this.storeview = storeview;
    }

}
