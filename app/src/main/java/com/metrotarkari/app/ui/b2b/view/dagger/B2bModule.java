package com.metrotarkari.app.ui.b2b.view.dagger;

import com.metrotarkari.app.application.network.DoneNetwork;
import com.metrotarkari.app.db.DoneDbAdapter;
import com.metrotarkari.app.ui.b2b.view.B2bActivity;
import com.metrotarkari.app.ui.b2b.view.mvp.B2bModel;
import com.metrotarkari.app.ui.b2b.view.mvp.B2bPresenter;
import com.metrotarkari.app.ui.b2b.view.mvp.B2bView;
import com.metrotarkari.app.utils.DoneUserPreferenceManager;

import dagger.Module;
import dagger.Provides;

@Module
public class B2bModule {

  private final B2bActivity activity;

  public B2bModule(B2bActivity activity) {
    this.activity = activity;
  }

  @Provides
  @B2bScope
  public B2bView view() {
    return new B2bView(activity);
  }

  @Provides
  @B2bScope
  public B2bModel model(DoneNetwork doneNetwork){
    return new B2bModel(activity, doneNetwork);
  }

  @Provides
  @B2bScope
  public B2bPresenter presenter(B2bView homeView, B2bModel model, DoneUserPreferenceManager preferenceManager, DoneDbAdapter dbAdapterd) {
   /* Intent intent = activity.getIntent();
    boolean intentSource= false;
    if (intent != null){

      intentSource = intent.getBooleanExtra(Constant.CALLED_FROM_CHECKOUT,false);
    }*/
    return new B2bPresenter(homeView, model, preferenceManager,activity,dbAdapterd);
  }

}
