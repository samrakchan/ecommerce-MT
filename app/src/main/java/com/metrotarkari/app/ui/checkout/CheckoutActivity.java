package com.metrotarkari.app.ui.checkout;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.metrotarkari.app.application.DoneApplication;
import com.metrotarkari.app.model.Shipping;
import com.metrotarkari.app.ui.base.BaseActivity;
import com.metrotarkari.app.ui.checkout.dagger.CheckoutModule;
import com.metrotarkari.app.ui.checkout.dagger.DaggerCheckoutComponent;
import com.metrotarkari.app.ui.checkout.mvp.CheckoutPresenter;
import com.metrotarkari.app.ui.checkout.mvp.CheckoutView;
import com.metrotarkari.app.utils.Constant;

import javax.inject.Inject;

/**
 * Created by samrakchan on 3/18/17.
 */

public class CheckoutActivity extends BaseActivity{

    @Inject
    CheckoutView itemListView;

    @Inject
    CheckoutPresenter itemListPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DaggerCheckoutComponent.builder().appComponent(DoneApplication.get(this).component())
                .checkoutModule(new CheckoutModule(this)).build().inject(this);

        setContentView(itemListView);
    }

    @Override
    protected void onDestroy() {
        if(itemListPresenter!=null){
            itemListPresenter.destroy();
        }
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //refreshing the selected address from intent
        if(requestCode == Constant.SELECT_ADDRESS){
            if(resultCode == RESULT_OK){
                itemListPresenter.checkOutInfo(data.getStringExtra(Constant.ID));

            }
            //getting the selected date and time from intent
        }else if (requestCode == Constant.CHANGE_DATE_AND_TIME){
            if (resultCode == RESULT_OK){
                String date = data.getStringExtra(Constant.SELECTED_DATE);
                Shipping time = data.getParcelableExtra(Constant.SELECTED_TIME);
                itemListView.setDateTime(date,time);

            }
            //getting the selected message from intent
        }else if (requestCode == Constant.IS_FOR_SET_MESSAGE){
            if (resultCode == RESULT_OK) {
                String message = data.getStringExtra(Constant.IS_FOR_MESSAGE);
                itemListView.setMessage(message);
            }
        }
    }
}
