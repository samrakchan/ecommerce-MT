package com.metrotarkari.app.ui.list.sortby;

import android.app.Activity;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;

import com.metrotarkari.app.R;
import com.metrotarkari.app.model.Sort;
import com.metrotarkari.app.utils.OrderListener;

import java.util.List;

/**
 * Created by samrakchan on 4/5/17.
 */

public class SortByDialog implements View.OnClickListener{
    private RecyclerView dialogFilterRv;
    private SortByAdapter filterAdapter;
    private AppCompatImageView dialogFilterCloseIv;
    private AlertDialog.Builder builder;
    private AlertDialog alertDialog;


    public SortByDialog(final Activity activity, List<Sort> orders, OrderListener listener){
        builder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_filter, null);
        AppCompatTextView dialogFilterHeaderTv = (AppCompatTextView)view.findViewById(R.id.dialogFilterHeaderTv);
        dialogFilterHeaderTv.setText(R.string.sort);
        dialogFilterHeaderTv.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(activity,R.mipmap.ic_list_sort), null, null, null);

        filterAdapter = new SortByAdapter(activity, orders);

        dialogFilterCloseIv = (AppCompatImageView) view.findViewById(R.id.dialogFilterCloseIv);
        dialogFilterCloseIv.setOnClickListener(this);

        dialogFilterRv = (RecyclerView) view.findViewById(R.id.dialogFilterRv);
        dialogFilterRv.setLayoutManager(new LinearLayoutManager(activity.getApplicationContext()));
        dialogFilterRv.setAdapter(filterAdapter);

        builder.setPositiveButton(R.string.ok, (dialog, which) -> {
            listener.onOrderListener(filterAdapter.getSelectedOrder());
        });
        builder.setView(view);
        builder.create();
        alertDialog = builder.show();

    }

    @Override
    public void onClick(View v) {
        if(alertDialog!=null){
            alertDialog.dismiss();
        }
    }
}
