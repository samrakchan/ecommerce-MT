package com.metrotarkari.app.ui.order.orderlist;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metrotarkari.app.R;
import com.metrotarkari.app.ui.order.orderlist.model.Order;
import com.metrotarkari.app.utils.DoneItemClickListener;
import com.metrotarkari.app.utils.Utility;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Avinash on 7/10/2017.
 */

public class OrderListAdapter extends RecyclerView.Adapter<OrderListAdapter.CategoryItemList>{
    private List<Order> mData;
    private Context mContext;
    private Order orderItem;
    private DoneItemClickListener listener;

    public OrderListAdapter(Context context,DoneItemClickListener listener){
        mData = new ArrayList<>();
        this.mContext = context;
        this.listener = listener;
    }

    public void addData(List<Order> datas){
        this.mData.addAll(datas);
        notifyDataSetChanged();
    }


    @Override
    public CategoryItemList onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_list_item_for_order, parent, false);
        return new CategoryItemList(view);
    }

    @Override
    public void onBindViewHolder(CategoryItemList holder, int position) {
        orderItem = mData.get(position);
       // holder.orderListItemIdTv.setText("#"+orderItem.getIncrementId());
        holder.orderListItemIdTv.setText(((Activity)mContext).getString(R.string.order_list_item_id,orderItem.getIncrementId()));
     //   holder.orderListItemTotalPriceTv.setText("NRs."+Utility.round(Double.parseDouble(orderItem.getBaseGrandTotal()),2)+"");
        holder.orderListItemTotalPriceTv.setText(mContext.getString(R.string.order_list_grand_price,String.valueOf(Utility.round(Double.parseDouble(orderItem.getBaseGrandTotal()),2))));
     //   holder.orderListItemTotalQtyTv.setText("Qty:"+(int)(Double.parseDouble(orderItem.getTotalItemCount())));
        holder.orderListItemTotalQtyTv.setText(mContext.getString(R.string.order_list_quantity,(int)Double.parseDouble(orderItem.getTotalItemCount())));

        if (orderItem.getCustomerFirstname()==null && orderItem.getCustomerLastname()==null){
            holder.orderListItemCustNameTv.setText("");
        }else {
            holder.orderListItemCustNameTv.setText(orderItem.getCustomerFirstname() + " " + orderItem.getCustomerLastname());
        }


        holder.orderListItemDateTimeTv.setText(Utility.convertDateAndTimeForOrderList(orderItem.getCreatedAt()));
       // holder.orderListItemOrderStatusTv.setText("Status : " + orderItem.getStatus());
        holder.orderListItemOrderStatusTv.setText(((Activity)mContext).getString(R.string.order_list_status,orderItem.getStatus()));
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    protected class CategoryItemList extends RecyclerView.ViewHolder  implements View.OnClickListener {

        private AppCompatTextView orderListItemIdTv;
        private AppCompatTextView orderListItemTotalPriceTv;
        private AppCompatTextView orderListItemTotalQtyTv;
        private AppCompatTextView orderListItemCustNameTv;
        private AppCompatTextView orderListItemDateTimeTv;
        private AppCompatTextView orderListItemOrderStatusTv;

        public CategoryItemList(View itemView) {
            super(itemView);

            orderListItemIdTv = (AppCompatTextView) itemView.findViewById(R.id.orderListItemIdTv);
            orderListItemTotalPriceTv = (AppCompatTextView)itemView.findViewById(R.id.orderListItemTotalPriceTv);
            orderListItemTotalQtyTv = (AppCompatTextView) itemView.findViewById(R.id.orderListItemTotalQtyTv);
            orderListItemCustNameTv = (AppCompatTextView)itemView.findViewById(R.id.orderListItemCustNameTv);
            orderListItemDateTimeTv = (AppCompatTextView)itemView.findViewById(R.id.orderListItemDateTimeTv);
            orderListItemOrderStatusTv = (AppCompatTextView) itemView.findViewById(R.id.orderListItemOrderStatusTv);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Order order = mData.get(getAdapterPosition());
            listener.onItemClickListener(getAdapterPosition(),itemView.getId(),order);
        }
    }

}
