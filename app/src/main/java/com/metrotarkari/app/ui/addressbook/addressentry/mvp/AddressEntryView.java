package com.metrotarkari.app.ui.addressbook.addressentry.mvp;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.AppCompatTextView;
import android.util.Patterns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.jakewharton.rxbinding2.view.RxView;
import com.metrotarkari.app.R;
import com.metrotarkari.app.model.Area;
import com.metrotarkari.app.model.BAddress;
import com.metrotarkari.app.model.City;
import com.metrotarkari.app.model.Country;
import com.metrotarkari.app.model.PostOrder;
import com.metrotarkari.app.model.Region;
import com.metrotarkari.app.model.SAddress;
import com.metrotarkari.app.ui.addressbook.addresslist.mvp.AddressListActivity;
import com.metrotarkari.app.ui.addressbook.addresslist.model.PostAddress;
import com.metrotarkari.app.ui.base.BaseViewFragment;
import com.metrotarkari.app.utils.Constant;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import cc.cloudist.acplibrary.ACProgressFlower;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.subjects.BehaviorSubject;

import static com.basgeekball.awesomevalidation.ValidationStyle.UNDERLABEL;

/**
 * Created by samrakchan on 4/27/17.
 */

public class AddressEntryView extends BaseViewFragment implements AdapterView.OnItemSelectedListener{

    private static final String TAG = AddressEntryView.class.getSimpleName();

    @BindView(R.id.addAddressBillingTv)
    public AppCompatTextView addAddressBillingTv;

    @BindView(R.id.billingFirstNameEt)
    public AppCompatEditText billingFirstNameEt;

    @BindView(R.id.billingLastNameEt)
    public AppCompatEditText billingLastNameEt;

    @BindView(R.id.billingEmailAddressEt)
    public AppCompatEditText billingEmailAddressEt;

    @BindView(R.id.billingCountrySpinner)
    public AppCompatSpinner billingCountrySpinner;

    @BindView(R.id.billingRegionACTV)
    public AppCompatAutoCompleteTextView billingRegionACTV;

    @BindView(R.id.billingCityACTV)
    public AppCompatAutoCompleteTextView billingCityACTV;

    @BindView(R.id.billingAreaACTV)
    public AppCompatAutoCompleteTextView billingAreaACTV;

    @BindView(R.id.billingLandlineEt)
    public AppCompatEditText billingLandlineEt;

    @BindView(R.id.billingMobileEt)
    public AppCompatEditText billingMobileEt;

    @BindView(R.id.billingPostCodeEt)
    public AppCompatEditText billingPostCodeEt;

    @BindView(R.id.billingStreetEt)
    public AppCompatEditText billingStreetEt;


    @BindView(R.id.checkoutContinueBtn)
    public AppCompatButton addAddEditBtn;


    @BindView(R.id.billingCompanyEt)
    public AppCompatEditText billingCompanyEt;

    private List<Country> countries;
    private List<String> countriesArray;

    /*

    private List<Region> regionsShipping;
    private ArrayList<String> regionsArrayShipping;

    private List<City> citiesShipping;
    private ArrayList<String> citiesArrayShipping;


    private List<Area> areasShipping;
    private ArrayList<String> areasArrayShipping;
*/

    private List<Region> regionsBilling;
    private ArrayList<String> regionsArrayBilling;

    private List<City> citiesBilling;
    private ArrayList<String> citiesArrayBilling;

    private List<Area> areasBilling;
    private ArrayList<String> areasArrayBilling;

    private final BehaviorSubject<Country> countryCodeBehaviorBilling = BehaviorSubject.create();
    private final BehaviorSubject<Region> regionCodeBehaviorBilling = BehaviorSubject.create();
    private final BehaviorSubject<City> cityCodeBehaviorBilling = BehaviorSubject.create();
/*
    private final BehaviorSubject<Country> countryCodeBehaviorShipping = BehaviorSubject.create();
    private final BehaviorSubject<Region> regionCodeBehaviorShipping = BehaviorSubject.create();
    private final BehaviorSubject<City> cityCodeBehaviorShipping = BehaviorSubject.create();
*/

    private final BehaviorSubject<Integer> addAddressBahviour = BehaviorSubject.create();

    private final ACProgressFlower progressDialog = new ACProgressFlower.Builder(getContext())
            .themeColor(Color.WHITE)
            .fadeColor(Color.DKGRAY).build();

    private AwesomeValidation awesomeValidation = new AwesomeValidation(UNDERLABEL);

    Fragment fragment ;
    private int intentFor;

    private PostOrder postOrder;

    public AddressEntryView(@NonNull Fragment fragment) {
        super(fragment);
        this.fragment = fragment;
        countries = new ArrayList<>();
        countriesArray = new ArrayList<>();
/*
        regionsShipping = new ArrayList<>();
        regionsArrayShipping = new ArrayList<>();

        areasShipping = new ArrayList<>();
        areasArrayShipping = new ArrayList<>();

        citiesShipping = new ArrayList<>();
        citiesArrayShipping = new ArrayList<>();
*/

        regionsBilling = new ArrayList<>();
        regionsArrayBilling = new ArrayList<>();

        areasBilling = new ArrayList<>();
        areasArrayBilling = new ArrayList<>();

        citiesBilling = new ArrayList<>();
        citiesArrayBilling = new ArrayList<>();

        Bundle bundle = fragment.getArguments();
        intentFor = bundle.getInt(Constant.INTENT_FOR);

            //intent for add address or edit address
         if (intentFor == Constant.ADD_ADDRESS || intentFor == Constant.CHECKOUT){
            addAddEditBtn.setText(R.string.add);

         }else if (intentFor == Constant.EDIT_ADDRESS){
             postOrder = bundle.getParcelable(Constant.ADD_ADDRESS_DETAIL);
             if (postOrder!=null){
                 billingEmailAddressEt.setVisibility(View.GONE);
                 addAddEditBtn.setText(R.string.edit);

                 setAddressEditData();

             }
         }


  //      shippingCountrySpinner.setOnItemSelectedListener(this);
        billingCountrySpinner.setOnItemSelectedListener(this);

        /*
        shippingRegionACTV.setListener((adapterView, view, position, l) -> {
            String item = (String)adapterView.getItemAtPosition(position);
            for(int i = 0; i< regionsArrayShipping.size(); i++){

                if(regionsArrayShipping.get(i).equalsIgnoreCase(item)){
                    regionCodeBehaviorShipping.onNext(regionsShipping.get(i));
                }
            }
        });
        shippingCityACTV.setListener((adapterView, view, position, l) -> {

            String item = (String)adapterView.getItemAtPosition(position);
            for(int i = 0; i< citiesArrayShipping.size(); i++){

                if(citiesArrayShipping.get(i).equalsIgnoreCase(item)){
                    cityCodeBehaviorShipping.onNext(citiesShipping.get(i));
                }
            }
        });*/


        billingRegionACTV.setOnItemClickListener((adapterView, view, position, l) -> {
            String item = (String)adapterView.getItemAtPosition(position);
            for(int i = 0; i< regionsArrayBilling.size(); i++){

                if(regionsArrayBilling.get(i).equalsIgnoreCase(item)){
                    regionCodeBehaviorBilling.onNext(regionsBilling.get(i));
                }
            }
        });

        billingCityACTV.setOnItemClickListener((adapterView, view, position, l) -> {

            String item = (String)adapterView.getItemAtPosition(position);
            for(int i=0; i<citiesArrayBilling.size(); i++){

                if(citiesArrayBilling.get(i).equalsIgnoreCase(item)){
                    cityCodeBehaviorBilling.onNext(citiesBilling.get(i));
                }
            }
        });

/*        shippingAddressSameAsBillingSwitch.setOnCheckedChangeListener(
                (compoundButton, b) -> shippingLl.setVisibility(b?View.GONE:View.VISIBLE)
        );*/

        RxView.clicks(addAddEditBtn).subscribeOn(AndroidSchedulers.mainThread()).subscribe(o -> {
            if (awesomeValidation.validate()) {
                addAddressBahviour.onNext(intentFor);
            }
        });

        getValidation();

    }


    @Override
    public int getContentLayout() {
        return R.layout.fragment_add_address;
    }

    public Observable<Country> countrySpinnerBillingObservable(){
        return countryCodeBehaviorBilling;
    }

    public Observable<Region> regionSpinnerBillingObservable(){
        return regionCodeBehaviorBilling;
    }

    public Observable<City> citySpinnerBillingObservable(){
        return cityCodeBehaviorBilling;
    }
/*
    public Observable<Country> countrySpinnerShippingObservable(){
        return countryCodeBehaviorShipping;
    }

    public Observable<Region> regionSpinnerShippingObservable(){
        return regionCodeBehaviorShipping;
    }

    public Observable<City> citySpinnerShippingObservable(){
        return cityCodeBehaviorShipping;
    }
*/
    public Observable<Integer> addEditAddressBtnObservable(){
        return addAddressBahviour;
    }

    public void setCountryList(List<Country> countryList){
        int positionNepalCode = -1;
        this.countries.clear();
        this.countriesArray.clear();

        this.countries.addAll(countryList);

        for(int i=0; i<countries.size(); i++){
            String countryCode = countries.get(i).getCode();
            countriesArray.add(countries.get(i).getName()+"");
            //Log.i("COUNTRY", c.getName()+"");
            if(countryCode.equalsIgnoreCase("NP")){
                positionNepalCode = i;
            }
        }

        ArrayAdapter<String> countryAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, countriesArray);
        countryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //shippingCountrySpinner.setAdapter(countryAdapter);
        billingCountrySpinner.setAdapter(countryAdapter);

        if(positionNepalCode !=-1){
           // shippingCountrySpinner.setSelection(positionNepalCode);
            billingCountrySpinner.setSelection(positionNepalCode);
        }

    }
/*

    public void setRegionListShipping(List<Region> regionList){
        this.regionsShipping.clear();
        this.regionsArrayShipping.clear();

        this.regionsShipping.addAll(regionList);

        for(Region c: regionList){
            regionsArrayShipping.add(c.getName()+"");
            //Log.i("COUNTRY", c.getName()+"");
        }

        AutoCompleteAdapter regionAdaperShipping = new AutoCompleteAdapter(getContext(), android.R.layout.simple_dropdown_item_1line, regionsArrayShipping);
        shippingRegionACTV.setAdapter(regionAdaperShipping);
        shippingRegionACTV.setThreshold(1);

        regionAdaperShipping.notifyDataSetChanged();
    }
*/

    public void setRegionListBilling(List<Region> regionList){
        this.regionsBilling.clear();
        this.regionsArrayBilling.clear();

        this.regionsBilling.addAll(regionList);

        for(Region c: regionList){
            regionsArrayBilling.add(c.getName()+"");
            //Log.i("COUNTRY", c.getName()+"");
        }

        AutoCompleteAdapter regionAdapterBilling = new AutoCompleteAdapter(getContext(), android.R.layout.simple_dropdown_item_1line, regionsArrayBilling);
        billingRegionACTV.setAdapter(regionAdapterBilling);
        billingRegionACTV.setThreshold(1);

        regionAdapterBilling.notifyDataSetChanged();

    }

/*    public void setAreaListShipping(List<Area> areaList){
        this.areasShipping.clear();
        this.areasArrayShipping.clear();

        this.areasShipping.addAll(areaList);

        for(Area a: areaList){
            areasArrayShipping.add(a.getName()+"");
            //Log.i("AREA", a.getName()+"");
        }

        //ArrayAdapter<String> areaAdapterShipping = new ArrayAdapter<String>(getContext(), android.R.layout.simple_dropdown_item_1line, areasArrayShipping);
        AutoCompleteAdapter areaAdapterShipping = new AutoCompleteAdapter(getContext(), android.R.layout.simple_dropdown_item_1line, areasArrayShipping);
        shippingAreaACTV.setAdapter(areaAdapterShipping);
        shippingAreaACTV.setThreshold(1);

        areaAdapterShipping.notifyDataSetChanged();

    }*/

    public void setAreaListBilling(List<Area> areaList){
        this.areasBilling.clear();
        this.areasArrayBilling.clear();

        this.areasBilling.addAll(areaList);

        for(Area a: areaList){
            areasArrayBilling.add(a.getName()+"");
            //Log.i("AREA", a.getName()+"");
        }

        AutoCompleteAdapter areaAdapterBilling = new AutoCompleteAdapter(getContext(), android.R.layout.simple_dropdown_item_1line, areasArrayBilling);
        billingAreaACTV.setAdapter(areaAdapterBilling);
        billingAreaACTV.setThreshold(1);

        areaAdapterBilling.notifyDataSetChanged();
    }

/*
    public void setCityListShipping(List<City> cityList){
        this.citiesShipping.clear();
        this.citiesArrayShipping.clear();


        this.citiesShipping.addAll(cityList);

        for(City c: cityList){
            citiesArrayShipping.add(c.getName()+"");
            //Log.i("CITY", c.getName()+"");
        }

        AutoCompleteAdapter cityAdapterShipping = new AutoCompleteAdapter(getContext(), android.R.layout.simple_dropdown_item_1line, citiesArrayShipping);
        shippingCityACTV.setAdapter(cityAdapterShipping);
        shippingCityACTV.setThreshold(1);

        cityAdapterShipping.notifyDataSetChanged();

    }*/


    public void setCityListBilling(List<City> cityList){
        this.citiesBilling.clear();
        this.citiesArrayBilling.clear();

        this.citiesBilling.addAll(cityList);

        for(City c: cityList){
            citiesArrayBilling.add(c.getName()+"");
        }

        AutoCompleteAdapter cityAdapterBilling = new AutoCompleteAdapter(getContext(), android.R.layout.simple_dropdown_item_1line, citiesArrayBilling);
        billingCityACTV.setAdapter(cityAdapterBilling);
        billingCityACTV.setThreshold(1);

        cityAdapterBilling.notifyDataSetChanged();
    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            Country country = countries.get(position);
            if (country != null) {

          /*      if (shippingCountrySpinner.getId() == adapterView.getId()) {

                    countryCodeBehaviorShipping.onNext(country);

                } else */
          if (billingCountrySpinner.getId() == adapterView.getId()) {

                    countryCodeBehaviorBilling.onNext(country);
                }
            }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }


    public PostOrder getAddressDetail(){

        Country countryB = countries.get(billingCountrySpinner.getSelectedItemPosition());
        //Country countryS = countries.get(shippingCountrySpinner.getSelectedItemPosition());

        BAddress bAddress = new BAddress();
        bAddress.setFirstname(billingFirstNameEt.getText().toString().trim());
        bAddress.setLastname(billingLastNameEt.getText().toString().trim());
        bAddress.setEmail(billingEmailAddressEt.getText().toString().trim());
        bAddress.setCountryName(((String)billingCountrySpinner.getSelectedItem())+"");
        bAddress.setCountryId(countryB.getCode()+"");
        bAddress.setRegionCode(billingRegionACTV.getText().toString().trim());
        bAddress.setCity(billingCityACTV.getText().toString().trim());
        bAddress.setCusArea(billingAreaACTV.getText().toString().trim());
        bAddress.setPostcode(billingPostCodeEt.getText().toString().trim());
        bAddress.setStreet(billingStreetEt.getText().toString().trim());
        bAddress.setLandLine(billingLandlineEt.getText().toString().trim());
        bAddress.setTelephone(billingMobileEt.getText().toString().trim());
        bAddress.setCompany(billingCompanyEt.getText().toString().trim());

        SAddress sAddress = new SAddress();

            sAddress.setFirstname(bAddress.getFirstname());
            sAddress.setLastname(bAddress.getLastname());
            sAddress.setEmail(bAddress.getEmail());
            sAddress.setCountryName(bAddress.getCountryName());
            sAddress.setCountryId(bAddress.getCountryId());
            sAddress.setRegionCode(bAddress.getRegionCode());
            sAddress.setCity(bAddress.getCity());
            sAddress.setCusArea(bAddress.getCusArea());
            sAddress.setPostcode(bAddress.getPostcode());
            sAddress.setStreet(bAddress.getStreet());
            sAddress.setLandLine(bAddress.getLandLine());
            sAddress.setTelephone(bAddress.getTelephone());
            sAddress.setCompany(bAddress.getCompany());

        /*else {

            sAddress.setFirstname(shippingFirstNameEt.getText().toString().trim());
            sAddress.setLastname(shippingLastNameEt.getText().toString().trim());
            sAddress.setEmail(shippingEmailAddressEt.getText().toString().trim());
            sAddress.setCountryName(((String) shippingCountrySpinner.getSelectedItem()) + "");
            sAddress.setCountryId(countryS.getCode() + "");
            sAddress.setRegionCode(shippingRegionACTV.getText().toString().trim());
            sAddress.setCity(shippingCityACTV.getText().toString().trim());
            sAddress.setCusArea(shippingAreaACTV.getText().toString().trim());
            sAddress.setPostcode(shippingPostCodeEt.getText().toString().trim());
            sAddress.setStreet(shippingStreetEt.getText().toString().trim());
            sAddress.setLandLine(shippingLandlineEt.getText().toString().trim());
            sAddress.setTelephone(shippingMobileEt.getText().toString().trim());
            sAddress.setCompany(shippingCompanyEt.getText().toString().trim());
        }*/

        PostOrder postOrder = new PostOrder();
        postOrder.setBAddress(bAddress);
        postOrder.setSAddress(sAddress);


        return postOrder;
    }


    public PostAddress getPostAddressDetail(){
        Country countryB = countries.get(billingCountrySpinner.getSelectedItemPosition());
        PostAddress postAddress = new PostAddress();
        postAddress.setFirstname(billingFirstNameEt.getText().toString().trim());
        postAddress.setLastname(billingLastNameEt.getText().toString().trim());
        postAddress.setCountryName(((String)billingCountrySpinner.getSelectedItem())+"");
        postAddress.setCountryId(countryB.getCode()+"");
        postAddress.setRegionCode(billingRegionACTV.getText().toString().trim());
        postAddress.setCity(billingCityACTV.getText().toString().trim());
        postAddress.setCusArea(billingAreaACTV.getText().toString().trim());
        postAddress.setPostcode(billingPostCodeEt.getText().toString().trim());
        postAddress.setFax("");
        postAddress.setStreet(billingStreetEt.getText().toString().trim());
        postAddress.setLandLine(billingLandlineEt.getText().toString().trim());
        postAddress.setTelephone(billingMobileEt.getText().toString().trim());
        postAddress.setCompany(billingCompanyEt.getText().toString().trim());

        if (intentFor == Constant.EDIT_ADDRESS){
            postAddress.setEntityID(postOrder.getBAddress().getEntityId());
        }


        return postAddress;

    }

    public void showLoading(boolean loading) {
        if (loading) {
            progressDialog.setCancelable(false);
            progressDialog.show();
        } else {
            progressDialog.dismiss();
        }
    }


    public void navigateBackToSource(){
        if (intentFor == Constant.ADD_ADDRESS) {
            Intent intent = new Intent(getContext(), AddressListActivity.class);
            getContext().startActivity(intent);
            fragment.getActivity().finish();
        }else if (intentFor == Constant.EDIT_ADDRESS){
            Intent intent = new Intent(getContext(),AddressListActivity.class);
      //      intent.putExtra("EDITADDRESSRESPONCE",editAddressResponce);
            fragment.startActivity(intent);
            fragment.getActivity().finish();

        }else if(intentFor == Constant.CHECKOUT){
            fragment.getActivity().setResult(Activity.RESULT_OK);
            fragment.getActivity().finish();;
        }
    }

    public void getValidation(){
        this.awesomeValidation.setContext(getContext());
        this.awesomeValidation.addValidation(billingFirstNameEt,Constant.NAME_VALIDATION_REG_EXP,getContext().getString(R.string.error_first_name));
        this.awesomeValidation.addValidation(billingLastNameEt,Constant.NAME_VALIDATION_REG_EXP,getContext().getString(R.string.error_last_name));
        this.awesomeValidation.addValidation(billingRegionACTV,Constant.NAME_VALIDATION_REG_EXP,getContext().getString(R.string.error_region_name));
        this.awesomeValidation.addValidation(billingCityACTV,Constant.NAME_VALIDATION_REG_EXP,getContext().getString(R.string.error_city_name));
        this.awesomeValidation.addValidation(billingAreaACTV,Constant.NAME_VALIDATION_REG_EXP,getContext().getString(R.string.error_street_name));
        this.awesomeValidation.addValidation(billingMobileEt,Constant.PHONE_NUMBER_VALIDATION_REG_EXP,getContext().getString(R.string.error_mobile_number));

        if (intentFor!=Constant.ADD_ADDRESS && intentFor!=Constant.CHECKOUT){
            this.awesomeValidation.addValidation(billingCompanyEt,Constant.NAME_VALIDATION_REG_EXP,getContext().getString(R.string.error_company_name));
        }

        if (intentFor != Constant.ADD_ADDRESS && intentFor != Constant.EDIT_ADDRESS){

            this.awesomeValidation.addValidation(billingEmailAddressEt,Patterns.EMAIL_ADDRESS,getContext().getString(R.string.error_email_address));
        }
    }

    public void setAddressEditData(){
        billingFirstNameEt.setText(postOrder.getBAddress().getFirstname());
        billingLastNameEt.setText(postOrder.getBAddress().getLastname());
        billingCompanyEt.setText(postOrder.getBAddress().getCompany());
        billingRegionACTV.setText(postOrder.getBAddress().getRegionCode());
        billingCityACTV.setText(postOrder.getBAddress().getCity());
        billingAreaACTV.setText(postOrder.getBAddress().getCusArea());
        billingStreetEt.setText(postOrder.getBAddress().getStreet());
        billingPostCodeEt.setText(postOrder.getBAddress().getPostcode());
        billingMobileEt.setText(postOrder.getBAddress().getTelephone());
        billingLandlineEt.setText(postOrder.getBAddress().getLandLine());
    }
}
