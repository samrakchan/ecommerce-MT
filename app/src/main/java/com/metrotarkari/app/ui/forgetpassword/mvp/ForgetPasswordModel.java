package com.metrotarkari.app.ui.forgetpassword.mvp;

import android.support.v7.app.AppCompatActivity;

import com.metrotarkari.app.application.network.DoneNetwork;
import com.metrotarkari.app.model.AuthToken;
import com.metrotarkari.app.model.Customer_;
import com.metrotarkari.app.model.ForgetPasswordResponse;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;

/**
 * Created by samrakchan on 4/30/17.
 */

public class ForgetPasswordModel {
    private final AppCompatActivity activity;
    private DoneNetwork network;

    public ForgetPasswordModel(AppCompatActivity activity, DoneNetwork network){
        this.activity = activity;
        this.network = network;
    }

    public Observable<ForgetPasswordResponse> sendMailForGetPassword(String email){
       HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("email", email);
        return network.sendMailForGetPassword(hashMap);
    }

    public Observable<Customer_> verifyCustomerToken(HashMap<String, String> idToken){
        return network.getCustomerToken(idToken);
    }

    public Observable<Customer_> changePassword(HashMap<String, String> idToken, HashMap<String, String> newPassword){
        return network.changePassword(idToken, newPassword);
    }
}
