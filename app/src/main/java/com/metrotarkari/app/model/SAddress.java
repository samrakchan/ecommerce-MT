
package com.metrotarkari.app.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SAddress implements Parcelable
{

    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("firstname")
    @Expose
    private String firstname;
    @SerializedName("lastname")
    @Expose
    private String lastname;
    @SerializedName("country_id")
    @Expose
    private String countryId;
    @SerializedName("region")
    @Expose
    private String region;
    @SerializedName("region_code")
    @Expose
    private String regionCode;
    @SerializedName("company")
    @Expose
    private String company;
    @SerializedName("postcode")
    @Expose
    private String postcode;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("country_name")
    @Expose
    private String countryName;
    @SerializedName("telephone")
    @Expose
    private String telephone;
    @SerializedName("street")
    @Expose
    private String street;
    @SerializedName("fax")
    @Expose
    private String fax;
    @SerializedName("cus_area")
    @Expose
    private String cusArea;
    @SerializedName("land_line")
    @Expose
    private String landLine;
    public final static Creator<SAddress> CREATOR = new Creator<SAddress>() {


        @SuppressWarnings({
            "unchecked"
        })
        public SAddress createFromParcel(Parcel in) {
            SAddress instance = new SAddress();
            instance.email = ((String) in.readValue((String.class.getClassLoader())));
            instance.firstname = ((String) in.readValue((String.class.getClassLoader())));
            instance.lastname = ((String) in.readValue((String.class.getClassLoader())));
            instance.countryId = ((String) in.readValue((String.class.getClassLoader())));
            instance.region = ((String) in.readValue((String.class.getClassLoader())));
            instance.regionCode = ((String) in.readValue((String.class.getClassLoader())));
            instance.company = ((String) in.readValue((String.class.getClassLoader())));
            instance.postcode = ((String) in.readValue((String.class.getClassLoader())));
            instance.city = ((String) in.readValue((String.class.getClassLoader())));
            instance.countryName = ((String) in.readValue((String.class.getClassLoader())));
            instance.telephone = ((String) in.readValue((String.class.getClassLoader())));
            instance.street = ((String) in.readValue((String.class.getClassLoader())));
            instance.fax = ((String) in.readValue((String.class.getClassLoader())));
            instance.cusArea = ((String) in.readValue((String.class.getClassLoader())));
            instance.landLine = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public SAddress[] newArray(int size) {
            return (new SAddress[size]);
        }

    }
    ;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getRegionCode() {
        return regionCode;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getCusArea() {
        return cusArea;
    }

    public void setCusArea(String cusArea) {
        this.cusArea = cusArea;
    }

    public String getLandLine() {
        return landLine;
    }

    public void setLandLine(String landLine) {
        this.landLine = landLine;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(email);
        dest.writeValue(firstname);
        dest.writeValue(lastname);
        dest.writeValue(countryId);
        dest.writeValue(region);
        dest.writeValue(regionCode);
        dest.writeValue(company);
        dest.writeValue(postcode);
        dest.writeValue(city);
        dest.writeValue(countryName);
        dest.writeValue(telephone);
        dest.writeValue(street);
        dest.writeValue(fax);
        dest.writeValue(cusArea);
        dest.writeValue(landLine);
    }

    public int describeContents() {
        return  0;
    }

}
