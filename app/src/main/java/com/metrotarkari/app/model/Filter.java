package com.metrotarkari.app.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by samrakchan on 6/6/17.
 */

public class Filter implements Parcelable {
    @SerializedName("value")
    @Expose
    private String value;
    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("count")
    @Expose
    private Integer count;

    private boolean selected = false;

    public final static Parcelable.Creator<Filter> CREATOR = new Creator<Filter>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Filter createFromParcel(Parcel in) {
            Filter instance = new Filter();
            instance.value = ((String) in.readValue((String.class.getClassLoader())));
            instance.label = ((String) in.readValue((String.class.getClassLoader())));
            instance.count = ((Integer) in.readValue((Integer.class.getClassLoader())));
            instance.selected = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
            return instance;
        }

        public Filter[] newArray(int size) {
            return (new Filter[size]);
        }

    }
            ;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(value);
        dest.writeValue(label);
        dest.writeValue(count);
        dest.writeValue(selected);
    }

    public int describeContents() {
        return 0;
    }
}
