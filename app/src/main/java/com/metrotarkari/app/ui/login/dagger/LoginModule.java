package com.metrotarkari.app.ui.login.dagger;

import android.support.v7.app.AppCompatActivity;

import com.metrotarkari.app.application.network.DoneNetwork;
import com.metrotarkari.app.db.DoneDbAdapter;
import com.metrotarkari.app.ui.login.LoginActivity2;
import com.metrotarkari.app.ui.login.mvp.LoginModel;
import com.metrotarkari.app.ui.login.mvp.LoginPresenter;
import com.metrotarkari.app.ui.login.mvp.LoginView;
import com.metrotarkari.app.utils.DoneUserPreferenceManager;

import dagger.Module;
import dagger.Provides;

@Module
public class LoginModule {

  private final LoginActivity2 activity;

  public LoginModule(LoginActivity2 activity) {
    this.activity = activity;
  }

  @Provides
  @LoginScope
  public LoginView view() {
    return new LoginView(activity);
  }

  @Provides
  @LoginScope
  public LoginModel model(DoneNetwork doneNetwork){
    return new LoginModel(activity, doneNetwork);
  }

  @Provides
  @LoginScope
  public LoginPresenter loginPresenter(LoginView homeView, LoginModel model , DoneDbAdapter doneDbAdapter, DoneUserPreferenceManager preferenceManager) {
    return new LoginPresenter(homeView, model,doneDbAdapter, preferenceManager);
  }

}
