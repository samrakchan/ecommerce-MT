package com.metrotarkari.app.ui.signup.mvp;

import android.content.Intent;
import android.util.Log;
import android.util.Patterns;
import android.widget.Toast;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.metrotarkari.app.R;
import com.metrotarkari.app.model.Customer;
import com.metrotarkari.app.model.Error;
import com.metrotarkari.app.ui.login.LoginActivity2;
import com.metrotarkari.app.utils.Constant;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.basgeekball.awesomevalidation.ValidationStyle.UNDERLABEL;

/**
 * Created by samrakchan on 4/30/17.
 */

public class SignupPresenter {

    private final SignupView view;
    private final SignupModel model;
    private final boolean isFromCheckOut;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    private AwesomeValidation awesomeValidation = new AwesomeValidation(UNDERLABEL);

    public SignupPresenter(SignupView view, SignupModel model,boolean isFromCheckOut){
        this.view = view;
        this.model = model;
        this.isFromCheckOut = isFromCheckOut;
    }

    public void onCreate() {
        this.awesomeValidation.setContext(view.getContext());
        this.awesomeValidation.addValidation(view.getFirstNameEt(),Constant.NAME_VALIDATION_REG_EXP,view.getContext().getString(R.string.error_first_name));
        this.awesomeValidation.addValidation(view.getLastNameEt(),Constant.NAME_VALIDATION_REG_EXP,view.getContext().getString(R.string.error_last_name));
        this.awesomeValidation.addValidation(view.getEmailEt(), Patterns.EMAIL_ADDRESS, view.getContext().getString(R.string.error_email_address));
        this.awesomeValidation.addValidation(view.getPasswordEt(), Constant.PASSWORD_VALIDATION_REG_EXP, view.getContext().getString(R.string.error_password_length));
        this.awesomeValidation.addValidation(view.getConfirmPasswordEt(),view.getPasswordEt(),view.getContext().getString(R.string.error_password_not_matched));

        //validate phone number.


        view.observeSignupButton().throttleFirst(5, TimeUnit.SECONDS)
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(aVoid -> {
                    if(awesomeValidation.validate()) {
                        Disposable disposable = doSignup(view.getFirstNameText(),view.getLastNameText(),view.getEmailText(), view.getPasswordText(),view.getNewsLetterByToggle()+"");
                        compositeDisposable.add(disposable);
                    }
                });

        view.observeCrossButton().subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(aVoid->{
                    model.finishActivity();
                });
/*
        view.observeSkipTv()
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(aVoid ->{
                    compositeDisposable.add(doAuth());
                });*/
    }

    public void onDestroy(){
        if (compositeDisposable!=null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

  /*  private Disposable doAuth(){
        view.showProgressDialogue(true);
        return model.getAuth(Constant.APP_TOKEN)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(authToken -> {
                    view.showProgressDialogue(false);
                    doneTokenPreferenceManager.setAccessToken(authToken.accessToken());
                    doneTokenPreferenceManager.setRefreshToken(authToken.refreshToken());
                    doneTokenPreferenceManager.setTokenExpireTimestamp(authToken.timestamp());

                    doneUserPreferenceManager.setUserSkipLogin(true);
                    model.navigateToMainActivity();

                }, throwable -> {
                    throwable.printStackTrace();
                    view.showProgressDialogue(false);
                });
    }
*/
    private Disposable doSignup(String firstName,String lastName,String username, String password,String newsLetter){
        view.showLoading(true);
        return model.doSignup(username,firstName,lastName, password,newsLetter)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(customerResponse -> {
                    view.showLoading(false);

                    if(customerResponse!= null){
                        Customer customer  = customerResponse.getCustomer();
                        List<Error> errorResponse = customerResponse.getErrors();

                        Log.d("CUSTOMER",customerResponse.getCustomer()+"");

                        if(errorResponse!=null && !errorResponse.isEmpty()){
                            Error error = errorResponse.get(0);
                            Toast.makeText(view.getContext(),error.getMessage()+"",Toast.LENGTH_LONG).show();
                        }else if (customer!=null){
                            List<String> message = customerResponse.getMessage();
                            String m = message.get(0);
                            if (m != null && !message.isEmpty()){
                                Toast .makeText(view.getContext(),m,Toast.LENGTH_LONG).show();
                                if (isFromCheckOut){
                                    view.navigateBackToLogin();
                                }else {
                                    Intent intent = new Intent(view.getContext(), LoginActivity2.class);
                                    view.getContext().startActivity(intent);
                                    view.finish();
                                }
                            }else {
                                Toast.makeText(view.getContext(),R.string.sww, Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                }, throwable -> {

                    if(throwable.getMessage().contains("End of input at line 1")){
                        //Account created with 200 response.
                        Toast.makeText(view.getContext(), R.string.account_created, Toast.LENGTH_LONG).show();

                        Intent intent = new Intent(view.getContext(), LoginActivity2.class);
                        view.getContext().startActivity(intent);
                        view.finish();
                    }else {
                        view.showLoading(false);
                        Toast.makeText(view.getContext(), throwable.getMessage(), Toast.LENGTH_LONG).show();
                        throwable.printStackTrace();
                    }
                });

    }
}
