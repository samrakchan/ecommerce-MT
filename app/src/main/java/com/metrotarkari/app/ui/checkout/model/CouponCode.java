package com.metrotarkari.app.ui.checkout.model;

/**
 * Created by Avinash on 7/24/2017.
 */

public class CouponCode {

    private String coupenCode;
    private boolean isCoupenApplied;
    private String selectedAddress;

    public String getCoupenCode() {
        return coupenCode;
    }

    public void setCoupenCode(String coupenCode) {
        this.coupenCode = coupenCode;
    }

    public boolean getIsCoupenApplied() {
        return isCoupenApplied;
    }

    public void setIsCoupenApplied(boolean isCoupenApplied) {
        this.isCoupenApplied = isCoupenApplied;
    }

    public String getSelectedAddress() {
        return selectedAddress;
    }

    public void setSelectedAddress(String selectedAddress) {
        this.selectedAddress = selectedAddress;
    }
}
