package com.metrotarkari.app.ui.order.orderlist.dagger;

import javax.inject.Scope;

@Scope
public @interface OrderListScope {
}
