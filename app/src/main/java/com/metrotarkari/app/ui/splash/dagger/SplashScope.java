package com.metrotarkari.app.ui.splash.dagger;

import javax.inject.Scope;

@Scope
public @interface SplashScope {
}
