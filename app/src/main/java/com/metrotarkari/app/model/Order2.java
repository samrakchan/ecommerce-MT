
package com.metrotarkari.app.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Order2 implements Parcelable {

    @SerializedName("billing_address")
    @Expose
    private Address billingAddress;
    @SerializedName("shipping_address")
    @Expose
    private Address shippingAddress;
    @SerializedName("shipping")
    @Expose
    private ArrayList<Shipping> shipping = null;
    @SerializedName("payment")
    @Expose
    private List<Payment> payment = null;
    @SerializedName("total")
    @Expose
    private Total total;
    @SerializedName("quote_item")
    @Expose
    private CartItem quoteItem;


    public final static Creator<Order2> CREATOR = new Creator<Order2>() {

        @SuppressWarnings({
                "unchecked"
        })
        public Order2 createFromParcel(Parcel in) {
            Order2 instance = new Order2();
            instance.billingAddress = ((Address) in.readValue((Address.class.getClassLoader())));
            instance.shippingAddress = ((Address) in.readValue((String.class.getClassLoader())));
            instance.shipping = ((ArrayList<Shipping>) in.readValue((Shipping.class.getClassLoader())));
            instance.payment = (( List<Payment>) in.readValue((Payment.class.getClassLoader())));
            instance.total = ((Total) in.readValue((Total.class.getClassLoader())));
            instance.quoteItem = ((CartItem) in.readValue((CartItem.class.getClassLoader())));
            //instance.bestSell = ((List<Product>) in.readValue((Product.class.getClassLoader())));
            return instance;
        }

        public Order2[] newArray(int size) {
            return (new Order2[size]);
        }

    }
            ;

    public Address getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(Address billingAddress) {
        this.billingAddress = billingAddress;
    }

    public Address getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(Address shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public ArrayList<Shipping> getShipping() {
        return shipping;
    }

    public void setShipping(ArrayList<Shipping> shipping) {
        this.shipping = shipping;
    }

    public List<Payment> getPayment() {
        return payment;
    }

    public void setPayment(List<Payment> payment) {
        this.payment = payment;
    }

    public Total getTotal() {
        return total;
    }

    public void setTotal(Total total) {
        this.total = total;
    }

    public CartItem getQuoteItem() {
        return quoteItem;
    }

    public void setQuoteItem(CartItem quoteItem) {
        this.quoteItem = quoteItem;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(billingAddress);
        dest.writeValue(shippingAddress);
        dest.writeValue(shipping);
        dest.writeValue(payment);
        dest.writeValue(total);
        dest.writeValue(quoteItem);
        //dest.writeValue(bestSell);
    }
}
