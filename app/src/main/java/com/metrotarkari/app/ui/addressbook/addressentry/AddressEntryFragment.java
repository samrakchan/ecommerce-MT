package com.metrotarkari.app.ui.addressbook.addressentry;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.metrotarkari.app.application.DoneApplication;
import com.metrotarkari.app.ui.addressbook.addressentry.dagger.AddAddressModule;
import com.metrotarkari.app.ui.addressbook.addressentry.dagger.DaggerAddAddressComponent;
import com.metrotarkari.app.ui.addressbook.addressentry.mvp.AddressEntryPresenter;
import com.metrotarkari.app.ui.addressbook.addressentry.mvp.AddressEntryView;

import javax.inject.Inject;

/**
 * Created by samrakchan on 6/12/17.
 */

public class AddressEntryFragment extends Fragment {

    @Inject
    AddressEntryView view;

    @Inject
    AddressEntryPresenter presenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        DaggerAddAddressComponent.builder().appComponent(DoneApplication.get(this).component())
                .addAddressModule(new AddAddressModule(this)).build().inject(this);

        return view;
    }
}
