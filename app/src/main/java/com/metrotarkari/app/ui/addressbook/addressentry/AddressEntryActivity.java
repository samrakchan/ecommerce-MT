package com.metrotarkari.app.ui.addressbook.addressentry;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.metrotarkari.app.R;
import com.metrotarkari.app.model.PostOrder;
import com.metrotarkari.app.ui.base.NonDraggerBaseActivity;
import com.metrotarkari.app.utils.Constant;

/**
 * Created by samrakchan on 6/12/17.
 */

public class AddressEntryActivity extends NonDraggerBaseActivity {

    private FragmentManager fragmentManager;
    private AddressEntryFragment addAddressFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //IntentFrom can be from Proceed to Checkout or from address book.
        int intentFor = getIntent().getIntExtra(Constant.INTENT_FOR,-1);


        addAddressFragment = new AddressEntryFragment();

        fragmentManager = getSupportFragmentManager();
        Bundle bundle = new Bundle();
        if (intentFor == 1) {
            getSupportActionBar().setTitle(R.string.add_address);
            bundle.putInt(Constant.INTENT_FOR, intentFor);

        }else if (intentFor == 2){
            getSupportActionBar().setTitle(R.string.edit_Address);
            PostOrder postOrder = getIntent().getParcelableExtra(Constant.ADD_ADDRESS_DETAIL);
            bundle.putInt(Constant.INTENT_FOR,intentFor);
            bundle.putParcelable(Constant.ADD_ADDRESS_DETAIL,postOrder);

        }else if(intentFor == Constant.CHECKOUT){
            getSupportActionBar().setTitle(R.string.add_address);
            bundle.putInt(Constant.INTENT_FOR, Constant.CHECKOUT);
        }

        addAddressFragment.setArguments(bundle);
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.replace(R.id.frameLayout, addAddressFragment);
        ft.commit();

    }

    @Override
    protected int getToolbarTitle() {
        return R.string.add_address;
    }

    @Override
    protected int getContentLayout() {
        return R.layout.activity_address_entry;
    }
}
