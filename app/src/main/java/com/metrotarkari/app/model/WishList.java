package com.metrotarkari.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by samrakchan on 9/18/17.
 */

public class WishList extends ErrorResponse {

    @SerializedName("wishlistitems")
    @Expose
    private List<Product_> wishlistitems;

    @SerializedName("total")
    @Expose
    private Integer total;
    @SerializedName("page_size")
    @Expose
    private Integer pageSize;
    @SerializedName("from")
    @Expose
    private Integer from;
    @SerializedName("total_page")
    @Expose
    private Integer totalPage;
    @SerializedName("current_page")
    @Expose
    private Integer currentPage;
    @SerializedName("message")
    @Expose
    private List<String> message = null;

    @SerializedName("car_total")
    @Expose
    private Integer cartTotal;

    public List<Product_> getWishlistitems() {
        return wishlistitems;
    }

    public void setWishlistitems(List<Product_> wishlistitems) {
        this.wishlistitems = wishlistitems;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getFrom() {
        return from;
    }

    public void setFrom(Integer from) {
        this.from = from;
    }

    public Integer getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(Integer totalPage) {
        this.totalPage = totalPage;
    }

    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

    public List<String> getMessage() {
        return message;
    }

    public void setMessage(List<String> message) {
        this.message = message;
    }

    public Integer getCartTotal() {
        return cartTotal;
    }

    public void setCartTotal(Integer cartTotal) {
        this.cartTotal = cartTotal;
    }
}