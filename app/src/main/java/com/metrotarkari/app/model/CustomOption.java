
package com.metrotarkari.app.model;

import java.util.HashMap;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CustomOption {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("position")
    @Expose
    private String position;
    @SerializedName("isRequired")
    @Expose
    private String isRequired;
    @SerializedName("values")
    @Expose
    private List<HashMap<String, Object>> values = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getIsRequired() {
        return isRequired;
    }

    public void setIsRequired(String isRequired) {
        this.isRequired = isRequired;
    }

    public List<HashMap<String, Object>> getValues() {
        return values;
    }

    public void setValues(List<HashMap<String, Object>> values) {
        this.values = values;
    }

    //    public HashMap<String, Object> getValues() {
//        return values;
//    }
//
//    public void setValues(HashMap<String, Object> values) {
//        this.values = values;
//    }
}
