package com.metrotarkari.app.ui.settings.dagger;

import com.metrotarkari.app.application.network.DoneNetwork;
import com.metrotarkari.app.ui.settings.SettingsActivity;
import com.metrotarkari.app.ui.settings.mvp.SettingsModel;
import com.metrotarkari.app.ui.settings.mvp.SettingsPresenter;
import com.metrotarkari.app.ui.settings.mvp.SettingsView;

import dagger.Module;
import dagger.Provides;

@Module
public class SettingsModule {

  private final SettingsActivity activity;

  public SettingsModule(SettingsActivity activity) {
    this.activity = activity;
  }

  @Provides
  @SettingsScope
  public SettingsView view() {
    return new SettingsView(activity);
  }

  @Provides
  @SettingsScope
  public SettingsModel model(DoneNetwork doneNetwork){
    return new SettingsModel(activity, doneNetwork);
  }

  @Provides
  @SettingsScope
  public SettingsPresenter settingPresenter(SettingsView view, SettingsModel model) {
    return new SettingsPresenter(view, model);
  }

}
