package com.metrotarkari.app.ui.signup.mvp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import com.metrotarkari.app.application.network.DoneNetwork;
import com.metrotarkari.app.model.AuthToken;
import com.metrotarkari.app.model.CustomerResponse;
import com.metrotarkari.app.ui.dashboard.MainActivity;

import java.util.HashMap;

import io.reactivex.Observable;


/**
 * Created by samrakchan on 4/30/17.
 */

public class SignupModel {
    private final AppCompatActivity activity;
    private DoneNetwork network;

    public SignupModel(AppCompatActivity activity, DoneNetwork network){
        this.activity = activity;
        this.network = network;
    }

    public void navigateToMainActivity(){
        Intent intent = new Intent(activity, MainActivity.class);
        activity.startActivity(intent);
        activity.finish();
    }

    public void finishActivity(){
        activity.finish();
    }


    public Observable<CustomerResponse> doSignup(String username, String firstName, String lastName, String password, String newsLetter){
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("email", username);
        hashMap.put("firstname", firstName);
        hashMap.put("lastname",lastName);
        hashMap.put("password",password);
        hashMap.put("news_letter",newsLetter);
        return network.doSignup(hashMap);
    }


    public Observable<AuthToken> getAuth(String token){
        return network.doAuthorization(token);
    }

}
