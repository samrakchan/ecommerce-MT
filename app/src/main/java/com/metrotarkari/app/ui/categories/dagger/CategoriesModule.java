package com.metrotarkari.app.ui.categories.dagger;

import com.metrotarkari.app.application.network.DoneNetwork;
import com.metrotarkari.app.ui.categories.CategoriesFragment;
import com.metrotarkari.app.ui.categories.mvp.CategoriesModel;
import com.metrotarkari.app.ui.categories.mvp.CategoriesPresenter;
import com.metrotarkari.app.ui.categories.mvp.CategoriesView;

import dagger.Module;
import dagger.Provides;

@Module
public class CategoriesModule {

  private final CategoriesFragment fragment;

  public CategoriesModule(CategoriesFragment fragment) {
    this.fragment = fragment;
  }

  @Provides
  @CategoriesScope
  public CategoriesView view() {
    return new CategoriesView(fragment);
  }

  @Provides
  @CategoriesScope
  public CategoriesModel model(DoneNetwork doneNetwork){
    return new CategoriesModel(fragment, doneNetwork);
  }

  @Provides
  @CategoriesScope
  public CategoriesPresenter presenter(CategoriesView homeView, CategoriesModel model) {
    return new CategoriesPresenter(homeView, model);
  }

}
