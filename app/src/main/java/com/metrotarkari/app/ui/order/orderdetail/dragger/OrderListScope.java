package com.metrotarkari.app.ui.order.orderdetail.dragger;

import javax.inject.Scope;

@Scope
public @interface OrderListScope {
}
