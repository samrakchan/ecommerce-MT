package com.metrotarkari.app.ui.home.mvp;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.view.LayoutInflater;
import android.view.View;

import com.bumptech.glide.Glide;
import com.freegeek.android.materialbanner.holder.Holder;
import com.metrotarkari.app.R;
import com.metrotarkari.app.model.Homebanner;

/**
 * Created by samrakchan on 6/7/17.
 */

public class SliderViewHolder implements Holder<Homebanner> {
    private AppCompatImageView sliderIv;

    @Override
    public View createView(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_slider,null);
        sliderIv = (AppCompatImageView) view.findViewById(R.id.sliderIv);
        return view;
    }

    @Override
    public void UpdateUI(Context context, int i, Homebanner homebanner) {
        Glide.with(context)
                .load(homebanner.getBannerName())
                .dontAnimate()
                .fitCenter()
                .into(sliderIv);
    }
}
