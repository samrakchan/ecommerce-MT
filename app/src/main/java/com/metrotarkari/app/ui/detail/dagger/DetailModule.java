package com.metrotarkari.app.ui.detail.dagger;

import com.metrotarkari.app.application.network.DoneNetwork;
import com.metrotarkari.app.ui.detail.DetailActivity;
import com.metrotarkari.app.ui.detail.mvp.DetailModel;
import com.metrotarkari.app.ui.detail.mvp.DetailPresenter;
import com.metrotarkari.app.ui.detail.mvp.DetailView;

import dagger.Module;
import dagger.Provides;

@Module
public class DetailModule {

  private final DetailActivity activity;

  public DetailModule(DetailActivity activity) {
    this.activity = activity;
  }

  @Provides
  @DetailScope
  public DetailView view() {
    return new DetailView(activity);
  }

  @Provides
  @DetailScope
  public DetailModel model(DoneNetwork doneNetwork){
    return new DetailModel(activity, doneNetwork);
  }

  @Provides
  @DetailScope
  public DetailPresenter presenter(DetailView homeView, DetailModel model) {
    return new DetailPresenter(homeView, model, activity);
  }

}
