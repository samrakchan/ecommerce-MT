package com.metrotarkari.app.ui.list.filter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bignerdranch.expandablerecyclerview.ChildViewHolder;
import com.bignerdranch.expandablerecyclerview.ExpandableRecyclerAdapter;
import com.bignerdranch.expandablerecyclerview.ParentViewHolder;
import com.metrotarkari.app.R;
import com.metrotarkari.app.model.Filter;
import com.metrotarkari.app.model.LayerFilter;

import java.util.HashMap;
import java.util.List;

/**
 * Created by samrakchan on 4/4/17.
 */

public class FilterAdapter extends ExpandableRecyclerAdapter<LayerFilter, Filter,FilterAdapter.FilterHeadingViewHolder, FilterAdapter.FilterContentViewHolder> {
    private LayoutInflater mInflater;
    private Context mContext;
    private List<LayerFilter> mData;
    private HashMap<String, String> queryMap;

    /**
     * Primary constructor. Sets up {@link #mParentList} and {@link #mFlatItemList}.
     * <p>
     * Any changes to {@link #mParentList} should be made on the original instance, and notified via
     * {@link #notifyParentInserted(int)}
     * {@link #notifyParentRemoved(int)}
     * {@link #notifyParentChanged(int)}
     * {@link #notifyParentRangeInserted(int, int)}
     * {@link #notifyChildInserted(int, int)}
     * {@link #notifyChildRemoved(int, int)}
     * {@link #notifyChildChanged(int, int)}
     * methods and not the notify methods of RecyclerView.Adapter.
     *
     * @param parentList List of all parents to be displayed in the RecyclerView that this
     *                   adapter is linked to
     */

    public FilterAdapter(Context context, @NonNull List<LayerFilter> parentList) {
        super(parentList);
        mContext = context;
        mData = parentList;
        queryMap = new HashMap<>();
        mInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public FilterHeadingViewHolder onCreateParentViewHolder(@NonNull ViewGroup parentViewGroup, int viewType) {
        View view = mInflater.inflate(R.layout.dialog_adapter_filter_header, parentViewGroup, false);
        return new FilterHeadingViewHolder(view);
    }

    @NonNull
    @Override
    public FilterContentViewHolder onCreateChildViewHolder(@NonNull ViewGroup childViewGroup, int viewType) {
        View view = mInflater.inflate(R.layout.dialog_adapter_filter_content, childViewGroup, false);
        return new FilterContentViewHolder(view);
    }

    @Override
    public void onBindParentViewHolder(@NonNull FilterHeadingViewHolder parentViewHolder, int parentPosition, @NonNull LayerFilter parent) {
        parentViewHolder.bind(parent.getFilterTitle(parentPosition));
    }

    @Override
    public void onBindChildViewHolder(@NonNull FilterContentViewHolder childViewHolder, int parentPosition, int childPosition, @NonNull Filter child) {
        childViewHolder.bind(child);
    }

    protected class FilterHeadingViewHolder extends ParentViewHolder<LayerFilter, Filter>{

        private AppCompatTextView dialogFilterHeaderTitleTv;
        private AppCompatImageView dialogFilterHeaderChevronIv;

        /**
         * Default constructor.
         *
         * @param itemView The {@link View} being hosted in this ViewHolder
         */
        public FilterHeadingViewHolder(@NonNull View itemView) {
            super(itemView);
            dialogFilterHeaderTitleTv = (AppCompatTextView) itemView.findViewById(R.id.dialogFilterContentTitleTv);
            dialogFilterHeaderChevronIv = (AppCompatImageView) itemView.findViewById(R.id.dialogFilterHeaderChevronIv);

        }

        @Override
        public void setExpanded(boolean expanded) {
            super.setExpanded(expanded);
            dialogFilterHeaderChevronIv.setImageResource(expanded==true?R.drawable.ic_minus:R.drawable.ic_plus);
        }

        @Override
        public void onExpansionToggled(boolean expanded) {
            super.onExpansionToggled(expanded);
            dialogFilterHeaderChevronIv.setImageResource(expanded==true?R.drawable.ic_plus:R.drawable.ic_minus);

        }

        public void bind(@NonNull String heading) {
            dialogFilterHeaderTitleTv.setText(heading);
        }
    }


    protected class FilterContentViewHolder extends ChildViewHolder<Filter> implements View.OnClickListener{

        private AppCompatTextView dialogFilterContentTitleTv;
        private AppCompatTextView dialogFilterContentItemCountTv;
        private AppCompatRadioButton dialogFilterContentSelectIndicatorRb;

        /**
         * Default constructor.
         *
         * @param itemView The {@link View} being hosted in this ViewHolder
         */
        public FilterContentViewHolder(@NonNull View itemView) {
            super(itemView);
            dialogFilterContentTitleTv = (AppCompatTextView) itemView.findViewById(R.id.dialogFilterContentTitleTv);
            dialogFilterContentItemCountTv = (AppCompatTextView) itemView.findViewById(R.id.dialogFilterContentItemCountTv);
            dialogFilterContentSelectIndicatorRb = (AppCompatRadioButton) itemView.findViewById(R.id.dialogFilterContentSelectIndicatorRb);

            itemView.setOnClickListener(this);
        }

        public void bind(@NonNull Filter child) {
            dialogFilterContentTitleTv.setText(child.getLabel());
            dialogFilterContentItemCountTv.setText(child.getCount()+" "+ mContext.getString(R.string.items));
            dialogFilterContentSelectIndicatorRb.setChecked(child.isSelected());
        }

        @Override
        public void onClick(View view) {
            if(mData!=null){

                boolean isSelected = false;
                LayerFilter layerFilter = mData.get(getParentAdapterPosition());
                Filter filterData = layerFilter.getChildList().get(getChildAdapterPosition());
                isSelected = filterData.isSelected();


                List<Filter> filters = layerFilter.getChildList();
                for(int i=0; i<filters.size(); i++){
                    Filter filter = filters.get(i);
                    filter.setSelected(false);
                    filters.set(i, filter);
                }
                mData.get(getParentAdapterPosition()).setFilter(filters);

                if(isSelected){
                    filterData.setSelected(false);
                    removeFilterQuery(layerFilter.getAttribute());
                }else {
                    filterData.setSelected(true);
                    addFilterQuery(layerFilter.getAttribute(), filterData.getValue());
                }

                mData.get(getParentAdapterPosition()).getChildList().set(getChildAdapterPosition(), filterData);

                notifyDataSetChanged();
            }

        }
    }

    private void addFilterQuery(String query, String value){
        queryMap.put("filter[layer]["+query+"]", value);
    }

    private void removeFilterQuery(String query){
        queryMap.remove("filter[layer]["+query+"]");
    }

    public HashMap<String, String> getFilterQuery(){
        return queryMap;
    }


}
