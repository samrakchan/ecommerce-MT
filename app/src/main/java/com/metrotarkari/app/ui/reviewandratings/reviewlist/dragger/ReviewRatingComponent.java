package com.metrotarkari.app.ui.reviewandratings.reviewlist.dragger;

import com.metrotarkari.app.application.dagger.AppComponent;
import com.metrotarkari.app.ui.reviewandratings.reviewlist.ReviewRatingsActivity;

import dagger.Component;

@ReviewRatingScope
@Component(modules = { ReviewRatingModule.class }, dependencies = AppComponent.class)
public interface ReviewRatingComponent {

  void inject(ReviewRatingsActivity reviewRatingsActivity);

}
