package com.metrotarkari.app.model;

/**
 * Created by samrakchan on 3/17/17.
 */

public class CategoryItem {
    private String categoryId;
    private String itemName;
    private String itemImage;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemImage() {
        return itemImage;
    }

    public void setItemImage(String itemImage) {
        this.itemImage = itemImage;
    }
}
