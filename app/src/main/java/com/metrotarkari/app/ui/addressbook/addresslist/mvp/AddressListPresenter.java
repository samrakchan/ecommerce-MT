package com.metrotarkari.app.ui.addressbook.addresslist.mvp;

import android.widget.Toast;

import com.metrotarkari.app.R;
import com.metrotarkari.app.model.BAddress;
import com.metrotarkari.app.model.Error;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by samrakchan on 4/27/17.
 */

public class AddressListPresenter {
    private final AddressListView view;
    private final AddressListModel model;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();


    public AddressListPresenter(AddressListView view, AddressListModel model){
        this.view = view;
        this.model = model;
        compositeDisposable.add(getAddressBook());

        compositeDisposable.add(view.deleteAddressBookBehaviour().subscribe(address->{
           compositeDisposable.add(deleteAddress(Integer.parseInt(address.getEntityId())));
        }));
    }

    public void onDestroy(){
        compositeDisposable.dispose();
    }

    private Disposable getAddressBook(){
        view.showProgressBar(true);
        return model.getAddressBook()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(addressResponse -> {
                    List<BAddress> addresses = addressResponse.getAddresses();
                    List<Error> errorResponse = addressResponse.getErrors();

                    if(errorResponse!=null && !errorResponse.isEmpty()){
                        Error error = errorResponse.get(0);
                        Toast.makeText(view.getContext(),error.getMessage()+"",Toast.LENGTH_LONG).show();
                    }else if (addressResponse!=null){
                        if (addresses!=null && !addresses.isEmpty()){
                            view.getAddressBookData(addresses);
                            view.showProgressBar(false);
                            view.showNoItem(false);
                        }else {
                            view.showProgressBar(false);
                            view.showNoItem(true);
                        }
                    }
                }, throwable -> {
                    view.showProgressBar(false);
                    view.showNoItem(false);
                    Toast.makeText(view.getContext(), R.string.sww,Toast.LENGTH_LONG).show();
                    throwable.printStackTrace();
                });
    }

    private Disposable deleteAddress(int addressId){
        view.showLoading(true);
        return model.deleteAddress(addressId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(addressResponse -> {
                    List<BAddress> addresses = addressResponse.getAddresses();
                    List<Error> errorResponse = addressResponse.getErrors();

                    if(errorResponse!=null && !errorResponse.isEmpty()){
                        Error error = errorResponse.get(0);
                        Toast.makeText(view.getContext(),error.getMessage()+"",Toast.LENGTH_LONG).show();
                    }else if (addressResponse!=null){
                        if (addresses!=null && !addresses.isEmpty()){
                            view.getAddressBookData(addresses);
                            view.showLoading(false);
                            view.showNoItem(false);
                        }else {
                            view.showLoading(false);
                            view.showNoItem(true);
                        }
                    }
                }, throwable -> {
                    view.showLoading(false);
                    view.showNoItem(false);
                    Toast.makeText(view.getContext(),R.string.sww,Toast.LENGTH_LONG).show();
                    throwable.printStackTrace();
                });
    }

}
