package com.metrotarkari.app.ui.base.mvp;

import com.metrotarkari.app.application.network.DoneNetwork;

import io.reactivex.Observable;

/**
 * Created by samrakchan on 5/7/17.
 */

public class BaseModel {
    public DoneNetwork doneNetwork;

    public BaseModel(DoneNetwork doneNetwork){
        this.doneNetwork = doneNetwork;
    }

    public Observable<Object> refreshTokeIfExpired(String val1, String val2){
        //return doneNetwork.getRefreshToken(val1, val2);
        return null;
    }
}
