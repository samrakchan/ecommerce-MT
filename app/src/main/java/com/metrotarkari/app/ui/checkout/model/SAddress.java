
package com.metrotarkari.app.ui.checkout.model;


public class SAddress {

    private String entity_id;

    public String getEntityId() {
        return entity_id;
    }

    public void setEntityId(String entityId) {
        this.entity_id = entityId;
    }

}
