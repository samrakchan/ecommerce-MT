package com.metrotarkari.app.ui.categories;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.metrotarkari.app.R;
import com.metrotarkari.app.model.Category;
import com.metrotarkari.app.model.ChildCategory;
import com.metrotarkari.app.ui.list.ItemListActivity;
import com.metrotarkari.app.utils.Constant;
import com.metrotarkari.app.utils.DoneItemClickListener;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by samrakchan on 4/19/17.
 */

public class CategoriesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private Context mContext;
    private List<Object> mData;
    private DoneItemClickListener listener;

    private static final int VIEW_TYPE_CATEGORY_HEADING = 1;
    private static final int VIEW_TYPE_CATEGORY_CHILD = 2;


    public CategoriesAdapter(Context context,DoneItemClickListener listener){
        mContext = context;
        mData = new ArrayList<>();
        this.listener = listener;
    }

    public void addCategoriesItems(List<Object> datas){
        mData.addAll(datas);
        notifyDataSetChanged();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_CATEGORY_HEADING){
            View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_header_item_for_category,parent,false);
            return new CategoriesAdapter.CategoryItemHeaderViewHolder(view);
        }else {
            View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_child_item_vertical_for_category,parent,false);
            return new CategoriesAdapter.CategoryItemChildViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Object object = mData.get(position);

        if (object instanceof Category){
            StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) holder.itemView.getLayoutParams();
            layoutParams.setFullSpan(true);

            Category category = (Category) object;
            CategoryItemHeaderViewHolder categoryItemHeaderViewHolder = (CategoryItemHeaderViewHolder)holder;
            if (category!= null) {
                categoryItemHeaderViewHolder.categoryHeaderTv.setText(category.getName());
            }
        }else if (object instanceof ChildCategory){

            ChildCategory childCategory = (ChildCategory) object;
            CategoryItemChildViewHolder categoryItemChildViewHolder = (CategoryItemChildViewHolder)holder;

            if (childCategory != null) {
                categoryItemChildViewHolder.listItemSubCategoriesNameTv.setText(childCategory.getName());

                if (childCategory.getImage() != null ) {
                        Glide.with(mContext).load(childCategory.getImage()).placeholder(R.mipmap.ic_placeholder_mt).dontAnimate().into(categoryItemChildViewHolder.listItemImageIv);
                }else {
                    Glide.with(mContext).load(R.mipmap.ic_placeholder_mt).dontAnimate().into(categoryItemChildViewHolder.listItemImageIv);
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    @Override
    public int getItemViewType(int position) {
        Object ob = mData.get(position);
        if (ob instanceof Category){
            return VIEW_TYPE_CATEGORY_HEADING;
        }else if (ob instanceof ChildCategory){
            return VIEW_TYPE_CATEGORY_CHILD;
        }
        return -1;
    }

    protected class CategoryItemHeaderViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private AppCompatTextView categoryHeaderTv;


        public CategoryItemHeaderViewHolder(View itemView) {
            super(itemView);
            categoryHeaderTv = (AppCompatTextView) itemView.findViewById(R.id.categoryHeaderTv);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Category category = (Category) mData.get(getAdapterPosition());
            List<ChildCategory> childCategories = category.getChildCategory();
            if (childCategories != null && !childCategories.isEmpty()){

            }else {
                Intent intent = new Intent(mContext, ItemListActivity.class);
                intent.putExtra(Constant.ID, category.getId());
                intent.putExtra(Constant.TITLE, category.getName());
                mContext.startActivity(intent);
            }
        }
    }

    protected class CategoryItemChildViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private CircleImageView listItemImageIv;
        private AppCompatTextView listItemSubCategoriesNameTv;

        public CategoryItemChildViewHolder(View itemView) {
            super(itemView);

            listItemImageIv = (CircleImageView) itemView.findViewById(R.id.listItemImageIv);
            listItemSubCategoriesNameTv = (AppCompatTextView) itemView.findViewById(R.id.listItemSubCategoriesNameTv);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            ChildCategory childCategory = (ChildCategory) mData.get(getAdapterPosition());
            listener.onItemClickListener(getAdapterPosition(),v.getId(),childCategory);

        }
    }
}
