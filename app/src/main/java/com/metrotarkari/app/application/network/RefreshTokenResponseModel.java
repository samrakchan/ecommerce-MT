package com.metrotarkari.app.application.network;

/**
 * Created by samrakchan on 5/7/17.
 */

public class RefreshTokenResponseModel {
    private String response;
    private String respCode;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getRespCode() {
        return respCode;
    }

    public void setRespCode(String respCode) {
        this.respCode = respCode;
    }
}
