
package com.metrotarkari.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Count {

    @SerializedName("1_star")
    @Expose
    private Integer _1Star;
    @SerializedName("2_star")
    @Expose
    private Integer _2Star;
    @SerializedName("3_star")
    @Expose
    private Integer _3Star;
    @SerializedName("4_star")
    @Expose
    private Integer _4Star;
    @SerializedName("5_star")
    @Expose
    private Integer _5Star;

    private String rate;
    private int ratingCount;

    public Integer get1Star() {
        return _1Star;
    }

    public void set1Star(Integer _1Star) {
        this._1Star = _1Star;
    }

    public Integer get2Star() {
        return _2Star;
    }

    public void set2Star(Integer _2Star) {
        this._2Star = _2Star;
    }

    public Integer get3Star() {
        return _3Star;
    }

    public void set3Star(Integer _3Star) {
        this._3Star = _3Star;
    }

    public Integer get4Star() {
        return _4Star;
    }

    public void set4Star(Integer _4Star) {
        this._4Star = _4Star;
    }

    public Integer get5Star() {
        return _5Star;
    }

    public void set5Star(Integer _5Star) {
        this._5Star = _5Star;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public int getRatingCount() {
        return ratingCount;
    }

    public void setRatingCount(int ratingCount) {
        this.ratingCount = ratingCount;
    }
}
