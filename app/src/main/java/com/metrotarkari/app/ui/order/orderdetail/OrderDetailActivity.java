package com.metrotarkari.app.ui.order.orderdetail;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.metrotarkari.app.application.DoneApplication;
import com.metrotarkari.app.ui.base.BaseActivity;
import com.metrotarkari.app.ui.order.orderdetail.dragger.DaggerOrderDetailComponent;
import com.metrotarkari.app.ui.order.orderdetail.dragger.OrderDetailModule;
import com.metrotarkari.app.ui.order.orderdetail.mvp.OrderDetailPresenter;
import com.metrotarkari.app.ui.order.orderdetail.mvp.OrderDetailView;

import javax.inject.Inject;

/**
 * Created by Avinash on 7/10/2017.
 */

public class OrderDetailActivity extends BaseActivity {
    @Inject
    OrderDetailView view;

    @Inject
    OrderDetailPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DaggerOrderDetailComponent.builder().appComponent(DoneApplication.get(this).component())
                .orderDetailModule(new OrderDetailModule(this)).build().inject(this);

        setContentView(view);
    }
}
