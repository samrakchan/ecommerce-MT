package com.metrotarkari.app.ui.addressbook.addressentry.mvp;

import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.metrotarkari.app.R;
import com.metrotarkari.app.db.DoneDbAdapter;
import com.metrotarkari.app.model.BAddress;
import com.metrotarkari.app.model.Error;
import com.metrotarkari.app.model.PostOrder;
import com.metrotarkari.app.model.StoreViewResult;
import com.metrotarkari.app.ui.addressbook.addresslist.model.PostAddress;
import com.metrotarkari.app.ui.checkout.CheckoutActivity;
import com.metrotarkari.app.utils.Constant;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by samrakchan on 4/27/17.
 */

public class AddressEntryPresenter {
    private static final String TAG = AddressEntryPresenter.class.getSimpleName();
    private final AddressEntryView view;
    private final AddressEntryModel model;
    private final DoneDbAdapter doneDbAdapter;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();


    public AddressEntryPresenter(AddressEntryView view, AddressEntryModel model, DoneDbAdapter doneDbAdapter){
        this.view = view;
        this.model = model;
        this.doneDbAdapter = doneDbAdapter;

        getCountry();

        compositeDisposable.add(view.addEditAddressBtnObservable().subscribe(
                o->{

                    if (o == Constant.ADD_ADDRESS || o == Constant.CHECKOUT){
                        PostAddress postAddress = view.getPostAddressDetail();
                        compositeDisposable.add(postAddress(postAddress));
                    }else if (o == Constant.EDIT_ADDRESS){
                        PostAddress postAddress = view.getPostAddressDetail();
                        compositeDisposable.add(postAddressForEdit(postAddress));

                    } else {
                        PostOrder postOrder = view.getAddressDetail();
                        Intent intent = new Intent(view.getContext(), CheckoutActivity.class);
                        intent.putExtra("POST_ORDER", postOrder);
                        intent.putExtra(Constant.IS_FROM_ADDRESS_BOOK_OR_ADDRESS_ENTRY,Constant.FROM_ADDRESS_ENTRY);
                        view.getContext().startActivity(intent);
                    }

                }
        ));

        compositeDisposable.add(view.countrySpinnerBillingObservable().subscribe(
                country -> {
                    if(country != null){
                        view.setRegionListBilling(country.getRegion());
                    }
                }));

        compositeDisposable.add(view.citySpinnerBillingObservable().subscribe(city ->{
            if(city != null){
                view.setAreaListBilling(city.getArea());
            }
        }));

        compositeDisposable.add(view.regionSpinnerBillingObservable().subscribe(region ->{
            if(region != null){
                view.setCityListBilling(region.getCity());
            }
        }));

        //

  /*      compositeDisposable.add(view.countrySpinnerShippingObservable().subscribe(
                country -> {
                    if(country != null){
                        view.setRegionListShipping(country.getRegion());
                    }
                }));

        compositeDisposable.add(view.citySpinnerShippingObservable().subscribe(city ->{
            if(city != null){
                view.setAreaListShipping(city.getArea());
            }
        }));

        compositeDisposable.add(view.regionSpinnerShippingObservable().subscribe(region ->{
            if(region != null){
                view.setCityListShipping(region.getCity());
            }
        }));*/
    }


    public void getCountry(){
        doneDbAdapter.openReadableDb();
        String jsonString = doneDbAdapter.getStoreView();
        Log.i(TAG, "JSON "+jsonString);
        if(jsonString!=null){
            Gson gson = new Gson();
            StoreViewResult configResponse = gson.fromJson(jsonString, StoreViewResult.class);
            List<Error> errorResponse = configResponse.getErrors();

            if(errorResponse!=null && !errorResponse.isEmpty()){
                Error error = errorResponse.get(0);
                Toast.makeText(view.getContext(),error.getMessage()+"",Toast.LENGTH_LONG).show();
            }else if(configResponse!=null && configResponse.getStoreview()!=null) {
                view.setCountryList(configResponse.getStoreview().getCountry());
            }
        }
        doneDbAdapter.closeDb();
    }

    private Disposable postAddress(PostAddress postAddress){
            view.showLoading(true);
            return model.postAddress(postAddress)
                    .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(postAddressResponse -> {
                    view.showLoading(false);
                    BAddress address = postAddressResponse.getbAddress();
                    List<Error> errorResponse = postAddressResponse.getErrors();
                    if(errorResponse!=null && !errorResponse.isEmpty()){
                        Error error = errorResponse.get(0);
                        Toast.makeText(view.getContext(),error.getMessage()+"",Toast.LENGTH_LONG).show();
                    }else if (address!= null){

                        Toast.makeText(view.getContext(), R.string.address_add_success_message,Toast.LENGTH_SHORT).show();
                        view.navigateBackToSource();
                    }else{
                        Toast.makeText(view.getContext(), R.string.sww,Toast.LENGTH_LONG).show();
                    }
                }, throwable -> {
                    view.showLoading(false);
                    Toast.makeText(view.getContext(), R.string.sww,Toast.LENGTH_LONG).show();
                    throwable.printStackTrace();
                });

    }


    private Disposable postAddressForEdit(PostAddress postAddress){
        view.showLoading(true);
        return model.postAddress(postAddress)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(postAddressResponse -> {
                    view.showLoading(false);
                    BAddress editAddressResponce = postAddressResponse.getbAddress();
                    List<Error> errorResponse = postAddressResponse.getErrors();
                    if(errorResponse!=null && !errorResponse.isEmpty()){
                        Error error = errorResponse.get(0);
                        Toast.makeText(view.getContext(),error.getMessage()+"",Toast.LENGTH_LONG).show();
                    }else if (editAddressResponce!= null){
                        Toast.makeText(view.getContext(),"EDITED",Toast.LENGTH_LONG).show();

                        view.navigateBackToSource();
                    }else{
                        Toast.makeText(view.getContext(), R.string.sww,Toast.LENGTH_LONG).show();
                    }
                }, throwable -> {
                    view.showLoading(false);
                    Toast.makeText(view.getContext(), R.string.sww,Toast.LENGTH_LONG).show();
                    throwable.printStackTrace();
                });

    }
}
