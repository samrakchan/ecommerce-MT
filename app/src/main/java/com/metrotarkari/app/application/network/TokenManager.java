package com.metrotarkari.app.application.network;

/**
 * Created by samrakchan on 5/7/17.
 */

public interface TokenManager {
    String getToken();
    boolean hasToken();
    void clearToken();
    String refreshToken();

}
