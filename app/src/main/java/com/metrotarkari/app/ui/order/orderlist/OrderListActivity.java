package com.metrotarkari.app.ui.order.orderlist;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.metrotarkari.app.application.DoneApplication;
import com.metrotarkari.app.ui.base.BaseActivity;
import com.metrotarkari.app.ui.order.orderlist.dagger.DaggerOrderListComponent;
import com.metrotarkari.app.ui.order.orderlist.dagger.OrderListModule;
import com.metrotarkari.app.ui.order.orderlist.mvp.OrderListPresenter;
import com.metrotarkari.app.ui.order.orderlist.mvp.OrderListView;

import javax.inject.Inject;

/**
 * Created by Avinash on 7/10/2017.
 */

public class OrderListActivity extends BaseActivity {

    @Inject
    OrderListView view;

    @Inject
    OrderListPresenter presenter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        DaggerOrderListComponent.builder().appComponent(DoneApplication.get(this).component())
                .orderListModule(new OrderListModule(this)).build().inject(this);

        setContentView(view);


    }
}
