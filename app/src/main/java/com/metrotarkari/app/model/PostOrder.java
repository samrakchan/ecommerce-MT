
package com.metrotarkari.app.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PostOrder implements Parcelable
{

    @SerializedName("b_address")
    @Expose
    private BAddress bAddress;
    @SerializedName("s_address")
    @Expose
    private SAddress sAddress;
    @SerializedName("s_method")
    @Expose
    private SMethod sMethod;
    @SerializedName("p_method")
    @Expose
    private PMethod pMethod;

    public final static Creator<PostOrder> CREATOR = new Creator<PostOrder>() {


        @SuppressWarnings({
            "unchecked"
        })
        public PostOrder createFromParcel(Parcel in) {
            PostOrder instance = new PostOrder();
            instance.bAddress = ((BAddress) in.readValue((BAddress.class.getClassLoader())));
            instance.sAddress = ((SAddress) in.readValue((SAddress.class.getClassLoader())));
            instance.sMethod = ((SMethod) in.readValue((SMethod.class.getClassLoader())));
            instance.pMethod = ((PMethod) in.readValue((PMethod.class.getClassLoader())));
            return instance;
        }

        public PostOrder[] newArray(int size) {
            return (new PostOrder[size]);
        }

    }
    ;

    public BAddress getBAddress() {
        return bAddress;
    }

    public void setBAddress(BAddress bAddress) {
        this.bAddress = bAddress;
    }

    public SAddress getSAddress() {
        return sAddress;
    }

    public void setSAddress(SAddress sAddress) {
        this.sAddress = sAddress;
    }

    public SMethod getSMethod() {
        return sMethod;
    }

    public void setSMethod(SMethod sMethod) {
        this.sMethod = sMethod;
    }

    public PMethod getPMethod() {
        return pMethod;
    }

    public void setPMethod(PMethod pMethod) {
        this.pMethod = pMethod;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(bAddress);
        dest.writeValue(sAddress);
        dest.writeValue(sMethod);
        dest.writeValue(pMethod);
    }

    public int describeContents() {
        return  0;
    }

}
